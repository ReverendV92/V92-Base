
------------------------------------------------------
--	Fistful of Frags (Steam)
--	Bow
------------------------------------------------------

AddCSLuaFile( )

SWEP.PrintName = "Bow" -- (String) Printed name on menu

if not VNTCB then
	Error( "V92 Content Bases not mounted; Removing Weapon: " .. SWEP.PrintName .. "\n" )
	return false
end

SWEP.Base = VNT_BASE_WEAPON -- (String) Weapon base parent this is a child of
SWEP.Spawnable = true -- (Boolean) Can be spawned via the menu
SWEP.AdminOnly = false -- (Boolean) Admin only spawnable

------------------------------------------------------
--	Client Information								--	Info used in the client block of the weapon
------------------------------------------------------

SWEP.WeaponName = "v92_fof_bow" -- (String) Name of the weapon script
SWEP.WeaponEntityName = SWEP.WeaponName .. "_ent" -- (String) Name of the weapon entity in Lua/Entities/Entityname.lua
SWEP.Manufacturer = VNT_WEAPON_MANUFACTURER_VARIOUS -- (String) Gun company that makes this weapon
SWEP.CountryOfOrigin = VNT_WEAPON_COUNTRY_TRIBAL -- (String) Country of origin
SWEP.MagazineName = VNTCB.Magazine.mArrow -- (String) The name of the magazine the weapon uses - used in my Weapon Magazine System
SWEP.Category = VNT_CATEGORY_FISTFULOFFRAGS -- (String) Category
SWEP.Instructions = VNTCB.instructions -- (String) Instruction
SWEP.Author = VNTCB.author -- (String) Author
SWEP.Contact = VNTCB.contact -- (String) Contact
SWEP.Purpose = VNTCB.purpose -- (String) Purpose
SWEP.Slot = VNT_WEAPON_BUCKETPOS_LAUNCHER -- (Integer) Bucket to place weapon in, 1 to 6
SWEP.SlotPos = VNT_WEAPON_SLOTPOS_LAUNCHER -- (Integer) Bucket position
SWEP.ViewModelFOV = 54 -- (Integer) First-person field of view
SWEP.WorkshopID = "" -- (Integer) Workshop ID number of the upload that contains this file.

------------------------------------------------------
--	Model Information								--
------------------------------------------------------

SWEP.ViewModelFlip = false -- (Boolean) Only used for vanilla CS:S models
SWEP.ViewModel = Model( "models/fofpons/bow_v.mdl" ) -- (String) View model - v_*
SWEP.WorldModel = Model( "models/fofpons/bow_w.mdl" ) -- (String) World model - w_*
SWEP.HoldType = "rpg" -- (String) Hold type for our weapon, refer to wiki for animation sets

------------------------------------------------------
--	Gun Types										--	Set the type of weapon - ONLY PICK ONE!
------------------------------------------------------

SWEP.IsGLM = true -- (Boolean) Is this weapon a grenade launcher?

SWEP.GrenadeLauncherEntity = "v92_proj_arrow_fof" -- (String) Name of the grenade launcher shell entity in Lua/Entities/Entityname.lua
SWEP.GrenadeLauncherForce = 2000 -- (Integer) Force of grenade launchers and shell throwers like that.

------------------------------------------------------
--	Primary Fire Settings							--	Settings for the primary fire of the weapon
------------------------------------------------------

SWEP.Primary.ClipSize = 1 -- (Integer) Size of a magazine
SWEP.Primary.DefaultClip = 1 -- (Integer) Default number of ammo you spawn with
SWEP.Primary.Ammo = "arrow" -- (String) Primary ammo used by the weapon, bullets probably
SWEP.Primary.PureDmg = VNTCB.Ammo.a40x46mm[ 1 ] -- (Integer) Base damage, put one number here and the base will do the rest
SWEP.Primary.RPM = 25 -- (Integer) Go to a wikipedia page and look at the RPM of the weapon, then put that here - the base will do the math

------------------------------------------------------
--	Gun Mechanics									--	Various things to tweak the effects and feedback
------------------------------------------------------

--SWEP.IsEntityShooter = true -- (Boolean) Does this weapon shoot entities instead of bullets?
--SWEP.CanReloadWhenNotEmpty = false -- (Boolean) Can we reload when not empty? true=M16, false=M1 Garand
SWEP.CanChamber = false -- (Boolean) Can we load a round into the chamber?
SWEP.FireMode = { false , true , false , false } -- (Table: Boolean, Boolean, Boolean, Boolean ) Enable different fire modes on the weapon; Has modes, Has Single, Has Burst, Has Auto - in that order. You can have more than one, but the first must be true
SWEP.CurrentMode = 1 -- (Integer) Current fire mode of the weapon; used to set the default mode; corresponds to the FireMode table
SWEP.Weight = 1 -- (Integer) The weight in Kilogrammes of our weapon - used in my weapon weight mod!
SWEP.CanFireUnderwater = true -- (Boolean) Can we shoot underwater?
SWEP.StrongPenetration = VNTCB.Ammo.a40x46mm[ 2 ] -- (Integer) Max penetration
SWEP.WeakPenetration = VNTCB.Ammo.a40x46mm[ 3 ] -- (Integer) Max wood penetration
SWEP.EffectiveRange = 250 -- (Integer) Effective range of the weapon in metres.
SWEP.StoppageRate = 0 -- (Integer) Rate of stoppages in the weapon, look up the real world number estimations and just throw that in here.

------------------------------------------------------
--	Special FX										--	Muzzle flashes, shell casings, etc
------------------------------------------------------

SWEP.MuzzleAttach = 1 -- (Integer) The number of the attachment point for muzzle flashes, typically "1"
SWEP.MuzzleFlashType = 1 -- (Integer) The number of the muzzle flash to use; see Lua/Effects/fx_muzzleflash.Lua
SWEP.ShellAttach = 2 -- (String) The name of the attachment point for shell ejections, typically "2" or "eject"
SWEP.ShellType = nil -- (Integer) The shell to use, see Lua/Effects/FX_ShellEject for integers
SWEP.ShellDelay = 0 -- (Float) 	Time between shot firing and shell ejection; useful for bolt-actions and things like that that need a delay

------------------------------------------------------
--	Custom Sounds									--
--		Setup sounds here!							--
------------------------------------------------------

SWEP.UsesSuperSonicAmmo = false -- (Boolean) Is the weapon using supersonic or subsonic ammo?

SWEP.Sounds = {

	["Primary"] = Sound( "FoFpon_Bow.Single" ) , -- (String) Primary shoot sound
	["PrimaryDry"] = Sound( "common/null.wav" ) , -- (String) Primary dry fire sound
	["GrenadeLauncher"] = Sound( "common/null.wav" ) , -- (String) Grenade launcher sound
	["Reload"] = Sound( "FoFpon_Bow.Reload" ) , -- (String) Reload sound

	["Noise_Close"] = Sound( "common/null.wav" ) , --
	["Noise_Distant"] = Sound( "common/null.wav" ) , --
	["Noise_Far"] = Sound( "common/null.wav" ) , --
	["CoreBass_Close"] = Sound( "common/null.wav" ) , --
	["CoreBass_Distant"] = Sound( "common/null.wav" ) , --
	["HiFi"] = Sound( "common/null.wav" ) , --
	["Reflection_Close"] = Sound( "common/null.wav" ) , --
	["Reflection_Far"] = Sound( "common/null.wav" ) , --

}

SWEP.SelectorSwitchSNDType = 1 -- (Integer) 1=US , 2=RU

------------------------------------------------------
--	Ironsight & Run Positions						--	Set our model transforms for running and ironsights
------------------------------------------------------

--SWEP.IronSightsPos = Vector(-2.01, -8.04, -0.48)
--SWEP.IronSightsAng = Vector(1, 0, -7.035)
SWEP.IronSightsPos = Vector(-2, -7.035, -1.491)
SWEP.IronSightsAng = Vector(1, 0, -2.814)
SWEP.RunArmOffset = Vector( 0 , 0 , 0 ) -- (Vector) Sprinting XYZ Transform
SWEP.RunArmAngle = Vector( -20 , 0 , 0 ) -- (Vector) Sprinting XYZ Rotation

------------------------------------------------------
-- Sequence Settings
------------------------------------------------------

SWEP.SeqPrimary = { "fire" }
SWEP.SeqShootLast = { "fire" }
SWEP.SeqIdle = { "idle" }
SWEP.SeqHolster = { "holster" }
SWEP.SeqReload = { "reload" }
SWEP.SeqDraw = { "draw" }

------------------------------------------------------
--	Setup Clientside Info							--	This block must be in every weapon!
------------------------------------------------------

if CLIENT then

	SWEP.WepSelectIcon = surface.GetTextureID( "vgui/hud/" .. SWEP.WeaponName )
	SWEP.RenderGroup = RENDERGROUP_BOTH
	language.Add( SWEP.WeaponName , SWEP.PrintName )
	killicon.Add( SWEP.WeaponName , "vgui/entities/" .. SWEP.WeaponName , Color( 255 , 255 , 255 ) )

elseif SERVER then

	resource.AddWorkshop( SWEP.WorkshopID )

end

------------------------------------------------------
--	SWEP:Initialize() 							--	Called when the weapon is first loaded
------------------------------------------------------

function SWEP:Initialize( )
	self.HoldMeRight = VNTCB.HoldType.Bow -- (String) Hold type table for our weapon, Lua/autorun/sh_v92_base_swep.Lua
	--self:SetHoldType(self.HoldType)
end
