
------------------------------------------------------
--	Fistful of Frags (Steam)
--	Dynamite
------------------------------------------------------

AddCSLuaFile( )

SWEP.PrintName = "Dynamite (Red)" -- (String) Printed name on menu

if not VNTCB then
	Error( "V92 Content Bases not mounted; Removing Weapon: " .. SWEP.PrintName .. "\n" )
	return false
end

SWEP.Base = VNT_BASE_WEAPON_GRENADE -- (String) Weapon base parent this is a child of
SWEP.Spawnable = true -- (Boolean) Can be spawned via the menu
SWEP.AdminOnly = false -- (Boolean) Admin only spawnable

------------------------------------------------------
--	Client Information								--	Info used in the client block of the weapon
------------------------------------------------------

SWEP.WeaponName = "v92_fof_dynamite_red" -- (String) Name of the weapon script
SWEP.WeaponEntityName = SWEP.WeaponName .. "_ent" -- (String) Name of the weapon entity in Lua/Entities/Entityname.lua
SWEP.GrenadeLauncherEntity = SWEP.WeaponName .. "_live" -- (String) Name of the grenade launcher shell entity in Lua/Entities/Entityname.lua
--SWEP.GrenadeLauncherEntity = "v92_grenade_flare" -- (String) Name of the grenade launcher shell entity in Lua/Entities/Entityname.lua
SWEP.GrenadeLauncherForce = 3500 -- (Integer) Force of grenade launchers and shell throwers like that.
SWEP.Manufacturer = VNT_WEAPON_MANUFACTURER_VARIOUS -- (String) Gun company that makes this weapon
SWEP.CountryOfOrigin = VNT_WEAPON_COUNTRY_UNITEDSTATES -- (String) Country of origin
SWEP.Category = VNT_CATEGORY_FISTFULOFFRAGS -- (String) Category
SWEP.Instructions = VNTCB.instructions -- (String) Instruction
SWEP.Author = VNTCB.author -- (String) Author
SWEP.Contact = VNTCB.contact -- (String) Contact
SWEP.Slot = VNT_WEAPON_BUCKETPOS_EXPLOSIVE -- (Integer) Bucket to place weapon in, 1 to 6
SWEP.SlotPos = VNT_WEAPON_SLOTPOS_EXPLOSIVE -- (Integer) Bucket position
SWEP.WorkshopID = "" -- (Integer) Workshop ID number of the upload that contains this file.

------------------------------------------------------
--	Model Information								--
------------------------------------------------------

SWEP.ViewModel = Model( "models/fofpons/dynamite_v.mdl" ) -- (String) View model - v_*
SWEP.WorldModel = Model( "models/fofpons/dynamite_red_w_jb.mdl" ) -- (String) World model - w_*
SWEP.UseHands = false -- (Boolean) Leave at false unless the model uses C_Arms
SWEP.HoldType = "grenade" -- (String) Hold type for our weapon, refer to wiki for animation sets

------------------------------------------------------
--	Gun Types										--	Set the type of weapon - ONLY PICK ONE!
------------------------------------------------------



------------------------------------------------------
--	Primary Fire Settings							--	Settings for the primary fire of the weapon
------------------------------------------------------

SWEP.Primary.ClipSize = -1 -- (Integer) Size of a magazine
SWEP.Primary.DefaultClip = 1 -- (Integer) Default number of ammo you spawn with
SWEP.Primary.Ammo = "dynamite" -- (String) Primary ammo used by the weapon, bullets probably

------------------------------------------------------
--	Gun Mechanics									--	Various things to tweak the effects and feedback
------------------------------------------------------

SWEP.Weight = 1 -- (Integer) The weight in Kilogrammes of our weapon - used in my weapon weight mod!

------------------------------------------------------
--	Grenade Mechanics								--	Grenade Shit
------------------------------------------------------

SWEP.CanFireUnderwater = false -- (Boolean) Can we shoot underwater?
SWEP.FuseTime = 5 -- (Integer) How long the grenade has until it explodes
SWEP.CanCookGrenade = true -- (Boolean) Can we cook the grenade before throwing it?

SWEP.Sounds = {
	["Throw"] = Sound( "WeaponFrag.Throw" ) ,
	["Primed"] = Sound( "Weapon_AR2.Special1" ) ,
	["CookTick"] = Sound( "Weapon_AR2.Empty" ) ,
}

------------------------------------------------------
--	Ironsight & Run Positions						--	Set our model transforms for running and ironsights
------------------------------------------------------

SWEP.IronSightsPos = Vector( 0 , 0 , 0 )
SWEP.IronSightsAng = Vector( 0 , 0 , 0 )
SWEP.RunArmOffset = Vector( 0 , 0 , 0 )
SWEP.RunArmAngle = Vector( -70 , 0 , 0 )

------------------------------------------------------
--	Setup Clientside Info							--	This block must be in every weapon!
------------------------------------------------------

if CLIENT then

	SWEP.WepSelectIcon = surface.GetTextureID( "vgui/hud/" .. SWEP.WeaponName )
	SWEP.RenderGroup = RENDERGROUP_BOTH
	language.Add( SWEP.WeaponName , SWEP.PrintName )
	killicon.Add( SWEP.WeaponName , "vgui/entities/" .. SWEP.WeaponName , Color( 255 , 255 , 255 ) )
	
elseif SERVER then

	resource.AddWorkshop( SWEP.WorkshopID )
	
end

------------------------------------------------------
--	SWEP:Initialize() 							--	Called when the weapon is first loaded
------------------------------------------------------

function SWEP:Initialize( )

	self.HoldMeRight = VNTCB.HoldType.Grenade -- (String) Hold type table for our weapon, Lua/autorun/sh_v92_base_swep.Lua

end

SWEP.SeqIdle = { "idle01" }
SWEP.SeqDraw = { "draw" }
SWEP.SeqPullPin = { "drawbacklow" }
SWEP.SeqThrow = { "throw" }
