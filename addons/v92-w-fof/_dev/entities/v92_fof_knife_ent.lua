
AddCSLuaFile( )

ENT.PrintName = "Knife"

if not VNTCB then
	Error( "V92 Content Bases not mounted; Removing Weapon: " .. ENT.PrintName .. "\n" )
	return false
end

ENT.Base = VNT_BASE_WEAPON_ENTITY
ENT.Type = "anim"

ENT.Author = VNTCB.author
ENT.Category = VNT_CATEGORY_FISTFULOFFRAGS
ENT.Information = "Slice'n'Dice!"

ENT.Spawnable = true
ENT.AdminOnly = false

ENT.SEntModel = Model( "models/fofpons/knife_w.mdl" ) -- (String) Model to use
ENT.SWepName = "v92_fof_knife" -- (String) Name of the weapon entity in Lua/weapons/swep_name.lua
ENT.WeaponName = ENT.SWepName .. "_ent" -- (String) Name of this entity
