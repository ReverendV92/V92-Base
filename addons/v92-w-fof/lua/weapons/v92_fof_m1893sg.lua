
------------------------------------------------------
--	Fistful of Frags (Steam)
--	Winchester Model 1893
--	12-Gauge Shotgun
------------------------------------------------------

AddCSLuaFile( )

SWEP.PrintName = "Model 1893" -- (String) Printed name on menu

if not VNTCB then
	Error( "V92 Content Bases not mounted; Removing Weapon: " .. SWEP.PrintName .. "\n" )
	return false
end

SWEP.Base = VNT_BASE_WEAPON_SHELL -- (String) Weapon base parent this is a child of
SWEP.Spawnable = true -- (Boolean) Can be spawned via the menu
SWEP.AdminOnly = false -- (Boolean) Admin only spawnable

------------------------------------------------------
--	Client Information								--	Info used in the client block of the weapon
------------------------------------------------------

SWEP.WeaponName = "v92_fof_m1893sg" -- (String) Name of the weapon script
SWEP.WeaponEntityName = SWEP.WeaponName .. "_ent" -- (String) Name of the weapon entity in Lua/Entities/Entityname.lua
SWEP.Manufacturer = VNT_WEAPON_MANUFACTURER_WINCHESTER -- (String) Gun company that makes this weapon
SWEP.CountryOfOrigin = VNT_WEAPON_COUNTRY_UNITEDSTATES -- (String) Country of origin
SWEP.MagazineName = VNTCB.Magazine.mShotgun -- (String) The name of the magazine the weapon uses - used in my Weapon Magazine System
SWEP.Category = VNT_CATEGORY_FISTFULOFFRAGS -- (String) Category
SWEP.Instructions = VNTCB.instructions -- (String) Instruction
SWEP.Author = VNTCB.author -- (String) Author
SWEP.Contact = VNTCB.contact -- (String) Contact
SWEP.Slot = VNT_WEAPON_BUCKETPOS_SHOTGUN -- (Integer) Bucket to place weapon in, 1 to 6
SWEP.SlotPos = VNT_WEAPON_SLOTPOS_SHOTGUN -- (Integer) Bucket position
SWEP.ViewModelFOV = 54 -- (Integer) First-person field of view
SWEP.WorkshopID = "" -- (Integer) Workshop ID number of the upload that contains this file.

------------------------------------------------------
--	Model Information								--
------------------------------------------------------

SWEP.ViewModelFlip = false -- (Boolean) Only used for vanilla CS:S models
SWEP.ViewModel = Model( "models/fofpons/m1893sg_v.mdl" ) -- (String) View model - v_*
SWEP.WorldModel = Model( "models/fofpons/m1893sg_w.mdl" ) -- (String) World model - w_*
SWEP.HoldType = "shotgun" -- (String) Hold type for our weapon, refer to wiki for animation sets

------------------------------------------------------
--	Gun Types										--	Set the type of weapon - ONLY PICK ONE!
------------------------------------------------------

------------------------------------------------------
--	Primary Fire Settings							--	Settings for the primary fire of the weapon
------------------------------------------------------

SWEP.Primary.ClipSize = 6 -- (Integer) Size of a magazine
SWEP.Primary.DefaultClip = 36 -- (Integer) Default number of ammo you spawn with
SWEP.Primary.Ammo = "12gauge" -- (String) Primary ammo used by the weapon, bullets probably
SWEP.Primary.PureDmg = VNTCB.Ammo.a12GBuck[ 1 ] -- (Integer) Base damage, put one number here and the base will do the rest
SWEP.Primary.RPM = 60 -- (Integer) Go to a wikipedia page and look at the RPM of the weapon, then put that here - the base will do the math
SWEP.Primary.NumShots = 8 -- (Integer) How many bullets per shot; think of this as a shotgun shell, higher integer, more pellets
SWEP.Primary.Force = 32 -- (Integer) Amount of force for the bullet to emit on impact
SWEP.CanChamber = true -- (Boolean) Can we load a round into the chamber?

------------------------------------------------------
--	Gun Mechanics									--	Various things to tweak the effects and feedback
------------------------------------------------------

SWEP.FireMode = { false , true , false , false } -- (Table: Boolean, Boolean, Boolean, Boolean ) Enable different fire modes on the weapon; Has modes, Has Single, Has Burst, Has Auto - in that order. You can have more than one, but the first must be true
SWEP.CurrentMode = 1 -- (Integer) Current fire mode of the weapon; used to set the default mode; corresponds to the FireMode table
SWEP.Weight = 4 -- (Integer) The weight in Kilogrammes of our weapon - used in my weapon weight mod!
SWEP.CanFireUnderwater = false -- (Boolean) Can we shoot underwater?
SWEP.StrongPenetration = VNTCB.Ammo.a12GBuck[ 2 ] -- (Integer) Max penetration
SWEP.WeakPenetration = VNTCB.Ammo.a12GBuck[ 3 ] -- (Integer) Max wood penetration
SWEP.EffectiveRange = 50 -- (Integer) Effective range of the weapon in metres.
SWEP.RevolverAction = 0 -- (Integer) Revolver action style, 0=disregard, 1=Single-Action, 2=Double-Action
SWEP.BlackFriday = true -- (Boolean) Black Friday Doorbuster Sales
SWEP.StoppageRate = 0 -- (Integer) Rate of stoppages in the weapon, look up the real world number estimations and just throw that in here.

------------------------------------------------------
--	Special FX										--	Muzzle flashes, shell casings, etc
------------------------------------------------------

SWEP.MuzzleAttach = 1 -- (Integer) The number of the attachment point for muzzle flashes, typically "1"
SWEP.MuzzleFlashType = 4 -- (Integer) The number of the muzzle flash to use; see Lua/Effects/fx_muzzleflash.Lua
SWEP.ShellAttach = 2 -- (Integer) The number of the attachment point for shell ejections, typically "2"
SWEP.ShellType = 2 -- (Integer) The shell to use, see Lua/Effects/FX_ShellEject for integers
SWEP.ShellDelay = 0.5 -- (Float) Time between shot firing and shell ejection; useful for bolt-actions and things like that that need a delay
SWEP.RevolverShells = nil -- (Table: Integer) Set to any integer to enable 'revolver' style shell ejections; I.E. when you reload it dumps all of them at once. Integer should be shell count.

------------------------------------------------------
--	Shotgun Settings
--	Settings for shotguns
------------------------------------------------------

SWEP.PumpAction = true -- (Boolean) Is the shotgun pump action?

------------------------------------------------------
--	Sniper Settings
--	Settings for sniper rifles
------------------------------------------------------

SWEP.ManualCocking = true -- (Boolean) Does the weapon have the bolt pull assigned to ACT_VM_PULLPIN

------------------------------------------------------
--	Custom Sounds									--	Setup sounds here!
------------------------------------------------------

SWEP.Sounds = {

	["Primary"] = Sound( "FoFpon_M1893SG.Single" ) ,
	["Primary_Dry"] = Sound("FoFpon_Pistol.Empty") ,
	["Draw"] = Sound( "FoFpon_M1893SG.Draw" ) ,
	["Reload"] = Sound( "FoFpon_M1893SG.Draw" ) ,

	["Noise_Close"] = Sound( "BF3.BulletCraft.Noise.Forest.Close" ) ,
	["Noise_Distant"] = Sound( "BF3.BulletCraft.Noise.Forest.Distant" ) ,
	["Noise_Far"] = Sound( "BF3.BulletCraft.Noise.Forest.Far" ) ,
	["CoreBass_Close"] = Sound( "BF3.BulletCraft.CoreBass.Close.OneShot_3" ) ,
	["CoreBass_Distant"] = Sound( "BF3.BulletCraft.CoreBass.Distant.OneShot_1" ) ,
	["HiFi"] = Sound( "BF3.BulletCraft.HiFi.Shotgun" ) ,
	["Reflection_Close"] = Sound( "BF3.BulletCraft.Reflection.Forest.Close" ) ,
	["Reflection_Far"] = Sound( "BF3.BulletCraft.Reflection.Forest.Far" ) ,
}

SWEP.SelectorSwitchSNDType = 1 -- (Integer) 1=US , 2=RU
SWEP.UsesSuperSonicAmmo = true -- (Boolean) Is the weapon using supersonic or subsonic ammo?

------------------------------------------------------
--	Ironsight & Run Positions						--	Set our model transforms for running and ironsights
------------------------------------------------------

SWEP.IronSightsPos = Vector(-4.281, -7.035, 3.47)
SWEP.IronSightsAng = Vector(1, 0, 0)
SWEP.RunArmOffset = Vector(0, -4.281, 0)
SWEP.RunArmAngle = Vector(-8, 22.7, -22.201)

------------------------------------------------------
--	Setup Clientside Info							--	This block must be in every weapon!
------------------------------------------------------

if CLIENT then

	SWEP.WepSelectIcon = surface.GetTextureID( "vgui/hud/" .. SWEP.WeaponName )
	SWEP.RenderGroup = RENDERGROUP_BOTH
	language.Add( SWEP.WeaponName , SWEP.PrintName )
	killicon.Add( SWEP.WeaponName , "vgui/entities/" .. SWEP.WeaponName , Color( 255 , 255 , 255 ) )

elseif SERVER then

	resource.AddWorkshop( SWEP.WorkshopID )

end

------------------------------------------------------
--	SWEP:Initialize() 							--	Called when the weapon is first loaded
------------------------------------------------------

function SWEP:Initialize( )

	self.HoldMeRight = VNTCB.HoldType.Shotgun -- (String) Hold type table for our weapon, Lua/autorun/sh_v92_base_swep.Lua

end

SWEP.Sequences.Draw = { "draw" }
SWEP.Sequences.Holster = { "holster" }
SWEP.Sequences.Idle = { "idle01" }
SWEP.Sequences.ReloadStart = { "reload1" }
SWEP.Sequences.ReloadLoop = { "reload2" }
SWEP.Sequences.ReloadEnd = { "reload3" }
SWEP.Sequences.Primary = { "fire01" , "fire02" }
SWEP.Sequences.Primary_Dry = { "dryfire" }
SWEP.Sequences.PullPin = { "pump" , "pump2" , "pump3" }

