
------------------------------------------------------
--	Fistful of Frags (Steam)
--	Live Dynamite
------------------------------------------------------

ENT.PrintName = "Live Dynamite (Yellow)"

if not VNTCB then

	Error( "V92 Content Bases not mounted; Removing Weapon: " .. SWEP.PrintName .. "\n" )
	return false

end

AddCSLuaFile()

ENT.Type = "anim"
ENT.Base = "v92_grenade_frag"

ENT.Type = "anim" -- Type of entity
ENT.Author = VNTCB.Info.author -- Author name
ENT.Contact = VNTCB.Info.contact -- Contact
ENT.Purpose = VNTCB.Info.purpose -- Purpose
ENT.Instructions = VNTCB.Info.instructions -- Instructions
ENT.Category = VNT_CATEGORY_FISTFULOFFRAGS

ENT.Model = Model( "models/fofpons/dynamite_w_jb.mdl" ) -- Model to use
ENT.Skin = 1
ENT.Mass = 5 -- Mass in KG
ENT.EmitsCookOffLight = true -- Emits a flicker when cooking off
ENT.CookOffLightColour = Color( 255 , 255 , 0 ) -- Color of the flicker light during cook off
ENT.CookOffBrightness = 1 -- Cook off light brightness
ENT.CookOffSize = { 256 , 512 } -- Min , Max Randomized values for the size of the glow

-- Sound table
ENT.Sounds = {

	["BounceConcrete"] = Sound( "BF1942.Grenade.Collide_Concrete" ) ,
	["BounceMetal"] = Sound( "BF1942.Grenade.Collide_Metal" ) ,
	["BounceSand"] = Sound( "BF1942.Grenade.Collide_Sand" ) ,
	["BounceWood"] = Sound( "BF1942.Grenade.Collide_Wood" ) ,
	["Debris"] = Sound( "FoFpon_Dynamite.Debris" ) ,
	["Explosion"] = Sound( "FoFpon_Dynamite.Explode" ) ,
	["WaterExplosion"] = Sound( "WaterExplosionEffect.Sound" ) ,
	["CookSound"] = Sound( "FoFpon_Dynamite.Single" ) ,

}
