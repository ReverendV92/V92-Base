
 --  Emplacements Base , V92 Version
 --  Enhanced/Modified version of the 'Emplacement' add - on: https://steamcommunity.com/sharedfiles/filedetails/?id = 213515639
 --  Original add - on by Wolly , BOT_09 , Poke
 --  BOT_09: https://steamcommunity.com/id/ylsid
 --  Wolly: https://steamcommunity.com/id/Wolly
 --  Poke: https://steamcommunity.com/id/1234567865456

AddCSLuaFile( )

ENT.Type = "anim"
ENT.Base = VNT_BASE_EMPLACEMENT
ENT.Category = VNT_CATEGORY_FISTFULOFFRAGS
ENT.PrintName = "Gatling Gun"
ENT.Instructions = VNTCB.instructions --  ( String ) Instruction
ENT.Author = VNTCB.author --  ( String ) Author
ENT.Contact = VNTCB.contact --  ( String ) Contact
ENT.Purpose = VNTCB.purpose --  ( String ) Purpose

ENT.Spawnable = true
ENT.AdminOnly = true

ENT.AutomaticFrameAdvance = true

ENT.Settings = { 
	["Base"] = {
		["Model"] = Model( "models/fofpons/gatling/base.mdl" ) , --  Tripod/Stand model
		["Pos"] = Vector( -10 , 0 , 0 ) , --  Spawn Position
		["Ang"] = Angle( 0 , 0 , 0 ) , --  Spawn Angle
	} , 
	["Turret"] = {
		["Model"] = Model( "models/fofpons/gatling/top.mdl" ) , --  Actual gun model
		["Pos"] = Vector( 0 , 0 , 40 ) , --  Pos offset
		["Ang"] = Angle( 0 , 0 , 0 ) , --  Angle offset
		["Float"] = 3 , -- Max turn of the turret
		["ForwardAxis"] = "Y" , -- Forward Axis, either X or Y or Z; Will default to X if you don't give a valid argument
		["Muzzle"] = "muzzle" , --  Muzzle
		["Pivot"] = "aimrotation" , --  Pivot point
	} , 
	["BulletOrigin"] = Vector( 0 , 0 , 0 ) , --  Origin of the shots relative to the muzzle attachment
	["ControlRange"] = 72 , -- Max range to control the turret
	["Deadzone"] = 0.7 , -- Max amount of difference between player view point and the turret angle before it moves, 'dead zone'
	["KickBack"] = Vector( -64 , 0 , 0 ) , -- Knockback on the gun
	["Projectile"] = nil , --  Name of one of my projectile shell , preferably
	["IsAuto"] = true , --  Is automatic?
	["RPM"] = 250 , --  Rounds per minute
	["NumberOfShots"] = 1 , --  Number of shots ton fire per fire
	["Force"] = 5 , --  Amount of push - back per shot on the target it hits
	["Damage"] = VNTCB.Ammo.a4570Gov[1] , --  Damage done per bullet , works best if you use my ammo tables
	["Penetrate"] = {
		["Enabled"] = true , --  Can penetrate? Works best on hitscan mode
		["Strong"] = VNTCB.Ammo.a4570Gov[2] , --  Strong penetration level , same as before - use ammo tables
		["Weak"] = VNTCB.Ammo.a4570Gov[3] , --  Weak penetration level , same as before - use ammo tables
	} ,
	["Richochet"] = {
		["Enabled"] = true , -- Can richochet
		["Max"] = { 0 , 3 } , -- Min/Max counts
		["Sound"] = Sound( "FX_RicochetSound.Ricochet" ) ,
	},
	["MuzzleFlash"] = {
		["Enabled"] = true , -- Muzzle flash toggle
		["Name"] = "fx_muzzleflash" , --  Muzzle Flash FX file; typically will be fx_muzzleflash or MuzzleFlash
		["Type"] = 4 , --  If using fx_muzzleflash , this determines the type of effect; else it's just the scale
	} , 
	["ShellEject"] = {
		["Enabled"] = true , -- Shell eject toggle
		["Name"] = "fx_shelleject" , --  Shell eject FX file; typically will be fx_shelleject or shell eject
		["Type"] = 22 , --  If using fx_shelleject , this determines the type of effect; else it's just the scale
		["Attach"] = "eject" , -- Eject attachment point
	} , 
	["Tracer"] = { 
		["Enabled"] = true , -- HasTracer
		["Name"] = nil , -- Custom Tracer name
		["RNG"] = { 0 , 1 } , -- Math RNG
	} , 
	["EffectiveRange"] = 500 , --  Effective range in metres of the turret
	["Distance"] = 56756 , --  Maximum distance to fire; only relevant to hitscan types
}

ENT.Sounds = {
	
	["Mount"] = Sound( "FoFpon_Gatling.Mount" ) , --  Mount sound
	["Dismount"] = Sound( "FoFpon_Gatling.Mount" ) , --  Dismount sound
	["Fire"] = Sound( "FoFpon_Gatling.Single" ) , 
	["EnableBulletCraft"] = true , --  Toggle BulletCraft sound effects
	["Noise_Close"] = Sound( "BF3.BulletCraft.Noise.Forest.Close" ) , 
	["Noise_Distant"] = Sound( "BF3.BulletCraft.Noise.Forest.Distant" ) , 
	["Noise_Far"] = Sound( "BF3.BulletCraft.Noise.Forest.Far" ) , 
	["CoreBass_Close"] = Sound( "BF3.BulletCraft.CoreBass.Close.LMG_1" ) , 
	["CoreBass_Distant"] = Sound( "BF3.BulletCraft.CoreBass.Distant.LMG_1" ) , 
	["HiFi"] = Sound( "BF3.BulletCraft.HiFi.M2HB" ) , 
	["Reflection_Close"] = Sound( "BF3.BulletCraft.Reflection.Forest.Close" ) , 
	["Reflection_Far"] = Sound( "BF3.BulletCraft.Reflection.Forest.Far" ) , 

}

ENT.Sequences.uences = {
	["Fire"] = { "fire" } , --  Fire sequence table
	["Mount"] = { "idle" } , --  Activate sequences
	["Dismount"] = { "idle" } , --  Dismount sequences
	["Idle"] = { "idle" } , --  Idle while mounted sequences
}
