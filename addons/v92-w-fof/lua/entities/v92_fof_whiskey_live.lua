
------------------------------------------------------
--	Fistful of Frags (Steam)
--	Whiskey
------------------------------------------------------

AddCSLuaFile()

ENT.Type = "anim"
ENT.Base = VNT_BASE_DEPLOYABLE_MEDICAL

ENT.PrintName = "Whiskey"
ENT.Category = VNT_CATEGORY_FISTFULOFFRAGS
ENT.Author = VNTCB.Info.author
ENT.Purpose = VNTCB.Info.purpose
ENT.Instructions = VNTCB.Info.instructions
ENT.Contact = VNTCB.Info.contact
ENT.Spawnable = false
ENT.AdminOnly = true

ENT.RemainingCharge = GetConVarNumber("VNT_SWep_Deployable_Charge")
ENT.ActionDelay = CurTime()
ENT.PickupSound = Sound( "FoFpon_Whiskey.Detonate" )
ENT.PickupModel = Model( "models/fofpons/whiskey_w.mdl" )
ENT.RemoveTime = 300
