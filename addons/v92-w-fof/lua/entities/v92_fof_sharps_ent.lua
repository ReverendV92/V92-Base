
------------------------------------------------------
--	Fistful of Frags (Steam)
--	Sharps Rifle 1874 
--	.45-70 Gov. Sharpshooter Rifle
------------------------------------------------------

AddCSLuaFile( )

ENT.PrintName = "Sharps 1874"

if not VNTCB then
	Error( "V92 Content Bases not mounted; Removing Weapon: " .. ENT.PrintName .. "\n" )
	return false
end

ENT.Base = VNT_BASE_WEAPON_ENTITY
ENT.Type = "anim"

ENT.Author = VNTCB.author
ENT.Category = VNT_CATEGORY_FISTFULOFFRAGS
ENT.Information = "Uses .45-70 Gov. Ammo"

ENT.Spawnable = true
ENT.AdminOnly = false

ENT.SEntModel = Model( "models/fofpons/sharps_w.mdl" ) -- (String) Model to use
ENT.SWepName = "v92_fof_sharps" -- (String) Name of the weapon entity in Lua/weapons/swep_name.lua
ENT.WeaponName = ENT.SWepName .. "_ent" -- (String) Name of this entity
