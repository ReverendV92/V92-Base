
AddCSLuaFile()

ENT.Base = "base_anim"
ENT.Type = "anim"

ENT.PrintName		= "Sakarias Repair Station Tool"
ENT.Author			= "Sakarias88"
ENT.Category 		= "Sakarias88"
ENT.Contact    		= ""
ENT.Purpose 		= ""
ENT.Instructions 	= "" 

ENT.Spawnable		= false
ENT.AdminOnly		= false

if CLIENT then

	function ENT:Initialize() end
	function ENT:Think() end
	function ENT:OnRestore() end

end

if SERVER then

	ENT.RepairDelay = CurTime() + 2

	function ENT:Initialize()
		
		self:SetModel( "models/jessev92/bf2/weapons/wrench_w.mdl" )
		self:SetOwner(self.Owner)
		self:PhysicsInit(SOLID_VPHYSICS)
		self:SetMoveType(MOVETYPE_VPHYSICS)
		self:SetSolid(SOLID_VPHYSICS)

		local phys = self:GetPhysicsObject()
		if(phys:IsValid()) then phys:Wake() end
		phys:SetMass( 5 )
		
	end

	function ENT:PhysicsCollide( data, phys ) 
		ent = data.HitEntity

		local dont = true	
		if not(string.find( ent:GetClass( ), "sent_sakarias_car" )) or string.find( ent:GetClass( ), "sent_sakarias_carwheel" ) or string.find( ent:GetClass( ), "sent_sakarias_carwheel_punked" ) then
			dont = false
		end
		
		if dont and self.RepairDelay < CurTime() then
			self.RepairDelay = CurTime() + 2
			ent.DoRepair = true
			self:EmitSound("BF2.Common.Repair.Loop")
			self:Remove()
		end

	end

	function ENT:Think()
		if SERVER then
			for _, _V in pairs(ents.FindInSphere(self:GetPos(), 64)) do
				if _V:GetClass() == "ent_m15mine_bf2" or _V:GetClass() == "ent_m18mine_bf2" or _V:GetClass() == "mine" then
					_V:Remove()
					self:EmitSound("BF2.Common.Repair.Loop")
				end
			end
		end
	end

end