
AddCSLuaFile()

ENT.PrintName = "M15 Anti-Vehicle Mine"

if not VNTCB then
	Error( "V92 Content Bases not mounted; Removing Weapon: " .. ENT.PrintName .. "\n" )
	return false
end

ENT.Base = VNT_BASE_MINE_VEHICLE
ENT.Type = "anim"
ENT.Author = "V92"
ENT.Spawnable = true
ENT.AdminOnly = true

ENT.MineModel = Model( "models/jessev92/bf2/weapons/m15mine_w.mdl" )
ENT.TimeToArm = 3
ENT.DeployedSound = Sound( "BF2.M15Mine.Draw" )
ENT.ArmedSound = Sound( "BF2.M15Mine.Armed" )
ENT.ExplosionSound = Sound( "HL2.Generic.Explosion" )
