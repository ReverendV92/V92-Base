
AddCSLuaFile( )

ENT.PrintName = "Medical Kit"

if not VNTCB then
	Error( "V92 Content Bases not mounted; Removing Weapon: " .. ENT.PrintName .. "\n" )
	return false
end

ENT.Base = VNT_BASE_WEAPON_ENTITY
ENT.Type = "anim"
ENT.Author = VNTCB.author
ENT.Information = "Uses Medical Bag Ammo"
ENT.Category = VNT_CATEGORY_BATTLEFIELD2
ENT.Spawnable = true
ENT.AdminOnly = false
ENT.SWepName = "v92_bf2_medicbag" -- (String) Name of the weapon entity in Lua/weapons/swep_name.lua
ENT.WeaponName = ENT.SWepName .. "_ent"	-- (String) Name of this entity
ENT.SEntModel = Model( "models/JesseV92/bf2/weapons/bag_medic_w.mdl" ) -- (String) Model to use