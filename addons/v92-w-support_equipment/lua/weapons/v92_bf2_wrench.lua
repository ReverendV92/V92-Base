
AddCSLuaFile()

SWEP.Spawnable      	= true
SWEP.AdminOnly			= false

SWEP.ViewModel			= Model( "models/weapons/c_grenade.mdl" )
SWEP.WorldModel			= Model( "models/jessev92/weapons/bf2_wrench_w.mdl" )

if (CLIENT) then

	local	_SELFENTNAME	= "eq_wrench_bf2"

	SWEP.PrintName			= "Wrench"
	SWEP.Category			= VNT_CATEGORY_BATTLEFIELD2
	SWEP.Author   			= VNTCB.author
	SWEP.Contact        	= VNTCB.contact
	SWEP.Instructions 		= VNTCB.instructions
	SWEP.Slot 				= VNT_WEAPON_BUCKETPOS_SUPPORT
	SWEP.SlotPos 			= VNT_WEAPON_SLOTPOS_SUPPORT
	SWEP.WepSelectIcon		= surface.GetTextureID("vgui/hud/" .. _SELFENTNAME )
	SWEP.ViewModelFOV		= 66.5
	SWEP.BounceWeaponIcon	= false
	SWEP.DrawCrosshair		= true

	language.Add( _SELFENTNAME, _INFONAME ) 
	killicon.Add( _SELFENTNAME, "vgui/entities/".. _SELFENTNAME , Color( 255, 255, 255 ) )
	language.Add("ent_wrench_bf2", _INFONAME )

end

SWEP.UseDel = CurTime()

function SWEP:Initialize()
	self:SetHoldType( "knife" ) --melee
end


function SWEP:PrimaryAttack()
	if not IsFirstTimePredicted() then return end
	if self.UseDel < CurTime() then
		self.UseDel = CurTime() + 2
		
		
		self:SendWeaponAnim( ACT_VM_SECONDARYATTACK )
		self.Owner:DoAttackEvent( ) 
		if (SERVER) then
			local tool = ents.Create( "ent_wrench_bf2" ) 
			tool:SetPos(self.Owner:GetShootPos() + self.Owner:GetAimVector() * 5)
			tool:SetAngles(self.Owner:EyeAngles())
			tool:Spawn()
			tool:SetOwner( self.Owner )
			tool:Fire("kill", "", 5)
			tool:GetPhysicsObject():ApplyForceCenter( self.Owner:GetVelocity() + self.Owner:GetAimVector() * 3000)
			tool:GetPhysicsObject():AddAngleVelocity(Vector(math.random(-500,500),math.random(-500,500),math.random(-500,500))) 	
			tool:GetPhysicsObject():SetMass(1)

			self.Weapon:EmitSound("weapons/slam/throw.wav", 100, math.random(90,110) )
		end
	end
end

function SWEP:SecondaryAttack()
	if not IsFirstTimePredicted() then return end
end

function SWEP:Holster()
	return true
end

function SWEP:Deploy()
	self.Weapon:SendWeaponAnim( ACT_VM_PULLBACK_LOW ) 
	return true
end

SWEP.Primary.Delay				= 0
SWEP.Primary.Recoil				= 0
SWEP.Primary.Damage				= 0
SWEP.Primary.NumShots			= 0
SWEP.Primary.Cone				= 0	
SWEP.Primary.ClipSize			= -1
SWEP.Primary.DefaultClip		= -1
SWEP.Primary.Automatic   		= true
SWEP.Primary.Ammo         		= "none"
SWEP.Secondary.Delay			= 0
SWEP.Secondary.Recoil			= 0
SWEP.Secondary.Damage			= 0
SWEP.Secondary.NumShots			= 0
SWEP.Secondary.Cone		  		= 0
SWEP.Secondary.ClipSize			= -1
SWEP.Secondary.DefaultClip		= -1
SWEP.Secondary.Automatic   		= true
SWEP.Secondary.Ammo         	= "none"