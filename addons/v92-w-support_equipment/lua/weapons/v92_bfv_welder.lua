
AddCSLuaFile()

if (SERVER) then
	SWEP.Weight				= 3
end

if (CLIENT) then

	local	_SELFENTNAME	= "eq_welder_bfv"

	SWEP.PrintName			= "Blowtorch"
	SWEP.Category			= VNT_CATEGORY_BATTLEFIELDVIETNAM
	SWEP.Author   			= VNTCB.author
	SWEP.Contact        	= VNTCB.contact
	SWEP.Instructions 		= VNTCB.instructions
	SWEP.Slot 				= VNT_WEAPON_BUCKETPOS_SUPPORT
	SWEP.SlotPos 			= VNT_WEAPON_SLOTPOS_SUPPORT
	SWEP.WepSelectIcon		= surface.GetTextureID("vgui/hud/" .. _SELFENTNAME )
	SWEP.ViewModelFOV		= 66.5
	SWEP.BounceWeaponIcon	= false

	language.Add( _SELFENTNAME, _INFONAME ) 
	killicon.Add( _SELFENTNAME, "vgui/entities/".. _SELFENTNAME , Color( 255, 255, 255 ) )

end

SWEP.Spawnable 				= true
SWEP.AdminOnly				= false

SWEP.ViewModel 				= Model("models/jessev92/bfv/weapons/blowtorch_c.mdl")
SWEP.WorldModel 			= Model("models/jessev92/bfv/weapons/blowtorch_w.mdl")
SWEP.UseHands				= true
SWEP.HoldType 				= "knife" 

SWEP.Primary.ClipSize 		= -1 
SWEP.Primary.DefaultClip 	= -1 
SWEP.Primary.Automatic 		= false 
SWEP.Primary.Ammo 			= "none" 

SWEP.Secondary.ClipSize 	= -1 
SWEP.Secondary.DefaultClip 	= -1 
SWEP.Secondary.Automatic 	= false 
SWEP.Secondary.Ammo 		= "none" 

function SWEP:Initialize() self:SetHoldType(self.HoldType) end

SWEP.LightSound = Sound( "BFV.BlowTorch.Light" )
function SWEP:Deploy()

	self:EmitSound("Mag_Gen.Draw")
		
	local vm = self.Owner:GetViewModel()
	vm:ResetSequence( vm:LookupSequence( "Draw" ) )

	self:SetNextPrimaryFire(CurTime() + self.Owner:GetViewModel():SequenceDuration())
	self:SetNextSecondaryFire(CurTime() + self.Owner:GetViewModel():SequenceDuration())
	--self:Idle()
	return true

end

function SWEP:Idle() self.Weapon:SendWeaponAnim( ACT_VM_IDLE ) end

function SWEP:Holster( wep )
	local vm = self.Owner:GetViewModel()
	return true
end

function SWEP:PrimaryAttack()
	if not IsFirstTimePredicted() then return end
	if (SERVER) then
		self:TorchDoorShut()
		self.Owner:SetAnimation( PLAYER_ATTACK1 )
		self:SetNextPrimaryFire(CurTime() + self.Owner:GetViewModel():SequenceDuration())
		self:SetNextSecondaryFire(CurTime() + self.Owner:GetViewModel():SequenceDuration())
    end
end

function SWEP:SecondaryAttack()
	if not IsFirstTimePredicted() then return end
	
	if (SERVER) then
		self:TorchDoorOpen()
		self.Owner:SetAnimation( PLAYER_ATTACK1 )
		self:SetNextPrimaryFire(CurTime() + self.Owner:GetViewModel():SequenceDuration())
		self:SetNextSecondaryFire(CurTime() + self.Owner:GetViewModel():SequenceDuration())
    end
end

local torchableThings = {
	"func_door",
	"func_door_rotating",
	"prop_door_rotating"
}

SWEP.HitDistance = 48
SWEP.TorchingSound = Sound("BFV.BlowTorch.Torching")

local ducking = false

function SWEP:TorchDoorOpen()
	local tr = util.TraceLine( {
		start = self.Owner:GetShootPos(),
		endpos = self.Owner:GetShootPos() + self.Owner:GetAimVector() * self.HitDistance,
		filter = self.Owner
	} )

	if ( !IsValid( tr.Entity ) ) then 
		tr = util.TraceHull( {
			start = self.Owner:GetShootPos(),
			endpos = self.Owner:GetShootPos() + self.Owner:GetAimVector() * self.HitDistance,
			filter = self.Owner,
			mins = Vector( -10, -10, -8 ),
			maxs = Vector( 10, 10, 8 )
		} )
	end

	if ( IsValid( tr.Entity ) ) then
		for k,v in pairs(ents.GetAll()) do
			if ( tr.Hit ) && (tr.Entity:GetClass(table.HasValue(torchableThings) ) ) then

				self.Owner:Freeze(true)
				self.Owner:Crouching()
				
				local vm = self.Owner:GetViewModel()
				vm:ResetSequence( vm:LookupSequence( "WeldIn" ) )
				self.Owner:EmitSound(self.TorchingSound) 		
				vm:ResetSequence( vm:LookupSequence( "WeldLoop" ) ) 	
				
				local effectPos = Vector( self.Owner:GetEyeTrace() )
				--local effectPos = Vector( tr.Entity:GetPos() )
				local sparks = EffectData()
					sparks:SetAttachment( self.Weapon:LookupAttachment( "TorchFlame" ) )
					sparks:SetAngles( tr.Entity:GetAngles() )
					sparks:SetScale( 1 )
				util.Effect( "ManhackSparks", sparks )
				
				
				self:SetNextPrimaryFire(CurTime() + self.Owner:GetViewModel():SequenceDuration())
				self:SetNextSecondaryFire(CurTime() + self.Owner:GetViewModel():SequenceDuration())
		
				timer.Simple( 3, function() 
					vm:ResetSequence( vm:LookupSequence( "WeldOut" ) ) 
					self.Owner:StopSound(self.TorchingSound) 
					tr.Entity:Fire("Unlock","",0)
					tr.Entity:Fire("Toggle","",0.5)
					self.Owner:Freeze(false)
				end)
			else
				return false
			end
		end
	end
end
	
function SWEP:TorchDoorShut()
	local tr = util.TraceLine( {
		start = self.Owner:GetShootPos(),
		endpos = self.Owner:GetShootPos() + self.Owner:GetAimVector() * self.HitDistance,
		filter = self.Owner
	} )

	if ( !IsValid( tr.Entity ) ) then 
		tr = util.TraceHull( {
			start = self.Owner:GetShootPos(),
			endpos = self.Owner:GetShootPos() + self.Owner:GetAimVector() * self.HitDistance,
			filter = self.Owner,
			mins = Vector( -10, -10, -8 ),
			maxs = Vector( 10, 10, 8 )
		} )
	end
	
	if ( IsValid( tr.Entity ) ) then
	for k,v in pairs(ents.GetAll()) do
		if ( tr.Hit ) && (tr.Entity:GetClass(table.HasValue(torchableThings) ) ) then

			self.Owner:Freeze(true)
			
			local vm = self.Owner:GetViewModel()
			vm:ResetSequence( vm:LookupSequence( "WeldIn" ) ) 
			self.Owner:EmitSound(self.TorchingSound) 			
			vm:ResetSequence( vm:LookupSequence( "WeldLoop" ) ) 
			
			self:SetNextPrimaryFire(CurTime() + self.Owner:GetViewModel():SequenceDuration())
			self:SetNextSecondaryFire(CurTime() + self.Owner:GetViewModel():SequenceDuration())
		
			timer.Simple( 3, function() 
				vm:ResetSequence( vm:LookupSequence( "WeldOut" ) ) 	
				self.Owner:StopSound(self.TorchingSound) 
				tr.Entity:Fire("Lock","",0)
				tr.Entity:Fire("Toggle","",0)
				self.Owner:Freeze(false)
			end)
		else
			return false
		end
	end
	end
end
