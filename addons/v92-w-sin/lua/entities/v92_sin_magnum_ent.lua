
AddCSLuaFile( )

if not VNTCB then
	Error( "V92 Content Bases not mounted; Removing Weapon:\n" )
	return false
end

ENT.Base = VNT_BASE_WEAPON_ENTITY
ENT.Type = "anim"
ENT.PrintName = "Auto Magnum"
ENT.Author = VNTCB.author
ENT.Information = "Uses .44 Magnum Ammo"
ENT.Category = VNT_CATEGORY_SIN
ENT.Spawnable = true
ENT.AdminOnly = false
ENT.SWepName = "v92_sin_magnum" -- (String) Name of the weapon entity in Lua/weapons/swep_name.lua
ENT.WeaponName = ENT.SWepName .. "_ent"	-- (String) Name of this entity
ENT.SEntModel = Model( "models/jessev92/sin/weapons/magnum_w.mdl" ) -- (String) Model to use