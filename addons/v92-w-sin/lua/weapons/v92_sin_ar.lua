
------------------------------------------------------
--	SiN Episodes: Emergence
--	SinTEK Industries Assault Rifle
--	5.56x45mm Assault Rifle
------------------------------------------------------

AddCSLuaFile( )

SWEP.PrintName = "Assault Rifle" -- (String) Printed name on menu

if not VNTCB then
	Error( "V92 Content Bases not mounted; Removing Weapon: " .. SWEP.PrintName .. "\n" )
	return false
end

SWEP.Base = VNT_BASE_WEAPON -- (String) Weapon base parent this is a child of
SWEP.Spawnable = true -- (Boolean) Can be spawned via the menu
SWEP.AdminOnly = false -- (Boolean) Admin only spawnable
------------------------------------------------------
--	Client Information								--	Info used in the client block of the weapon
------------------------------------------------------
SWEP.WeaponName = "v92_sin_ar" -- (String) Name of the weapon script
SWEP.WeaponEntityName = SWEP.WeaponName .. "_ent" -- (String) Name of the weapon entity in Lua/Entities/Entityname.lua
SWEP.Manufacturer = VNT_WEAPON_MANUFACTURER_SINTEK -- (String) Gun company that makes this weapon
SWEP.CountryOfOrigin = VNT_WEAPON_COUNTRY_UNITEDSTATES -- (String) Country of origin
SWEP.MagazineName = VNTCB.Magazine.mSinTEKAR -- (String) The name of the magazine the weapon uses - used in my Weapon Magazine System
SWEP.Category = VNT_CATEGORY_SIN -- (String) Category
SWEP.Instructions = VNTCB.instructions -- (String) Instruction
SWEP.Author = VNTCB.author -- (String) Author
SWEP.Contact = VNTCB.contact -- (String) Contact
SWEP.Slot = VNT_WEAPON_BUCKETPOS_RIFLE -- (Integer) Bucket to place weapon in, 1 to 6
SWEP.SlotPos = VNT_WEAPON_SLOTPOS_RIFLE -- (Integer) Bucket position
SWEP.ViewModelFOV = 64 -- (Integer) First-person field of view
SWEP.WorkshopID = "" -- (Integer) Workshop ID number of the upload that contains this file.

------------------------------------------------------
--	Model Information								--
------------------------------------------------------

SWEP.ViewModelFlip = false -- (Boolean) Only used for vanilla CS:S models
SWEP.ViewModel = Model( "models/jessev92/sin/weapons/ar_v.mdl" ) -- (String) View model - v_*
SWEP.WorldModel = Model( "models/jessev92/sin/weapons/ar_w.mdl" ) -- (String) World model - w_*
SWEP.UseHands = false -- (Boolean) Leave at false unless the model uses C_Arms
SWEP.HoldType = "ar2" -- (String) Hold type for our weapon, refer to wiki for animation sets

------------------------------------------------------
--	Gun Types										--	Set the type of weapon - ONLY PICK ONE!
------------------------------------------------------

SWEP.DeployableType = 1 -- (Integer) 0=None, 1=Grenade Launcher, 2=Bipod, 3=Model Swap Bipod, 4=Model Swap Stock Deploy, 5=Deployable

------------------------------------------------------
--	Sniper Settings									--	Settings for sniper rifles
------------------------------------------------------

SWEP.ScopeType = 1 -- (Integer) Type of scope, 0=none, 1=overlay, 2=Render Target
SWEP.ScopeMaterial = 2 -- (Integer) Type of overlay, 0=Red Dot, 1=EOTech, 2=ACOG, 3=SVD, 4=M14, 5=L42A1
SWEP.ScopeMultipliers = { 1 , 0.6 , 0.6 } -- (Table: Float, Float, Float) Zoom Muliplier, Default Zoom x, Close Zoom x
SWEP.BoltAction = false -- (Boolean) Is this bolt action? Removes the player from the scope after firing
SWEP.ReturnToScope = false -- (Boolean) Return to scope after cycling the bolt?
SWEP.HasVariableZoom = false -- (Boolean) Does the weapon have variable zoom?

------------------------------------------------------
--	Primary Fire Settings							--	Settings for the primary fire of the weapon
------------------------------------------------------

SWEP.Primary.ClipSize = VNTCB.Magazine.mSinTEKAR[3] -- (Integer) Size of a magazine
SWEP.Primary.DefaultClip = VNTCB.Magazine.mSinTEKAR[3] -- (Integer) Default number of ammo you spawn with
SWEP.Primary.Ammo = "556x45mmnato" -- (String) Primary ammo used by the weapon, bullets probably
SWEP.Primary.PureDmg = VNTCB.Ammo.a556NATO[ 1 ] -- (Integer) Base damage, put one number here and the base will do the rest
SWEP.Primary.RPM = 700 -- (Integer) Go to a wikipedia page and look at the RPM of the weapon, then put that here - the base will do the math

------------------------------------------------------
--	Secondary Fire Settings							--	Settings for the alt-fire; if it has none, leave this be
------------------------------------------------------

SWEP.Secondary.ClipSize = -1 -- (Integer) Size of a secondary magazine; if no alt-fire, set to -1
SWEP.Secondary.DefaultClip = 3 -- (Integer) Default number of projectiles in the alt fire mag; if none, set to -1
SWEP.Secondary.Ammo = "40x46mmgrenade" -- (String) Ammo used by the grenade launcher, if it doesn't have one, leave at "none"

------------------------------------------------------
--	Gun Mechanics									--	Various things to tweak the effects and feedback
------------------------------------------------------

SWEP.CanReloadWhenNotEmpty = true -- (Boolean) Can we reload when not empty? true=M16, false=M1 Garand
SWEP.FireMode = { true , true , true , true } -- (Table: Boolean, Boolean, Boolean, Boolean ) Enable different fire modes on the weapon; Has modes, Has Single, Has Burst, Has Auto - in that order. You can have more than one, but the first must be true
SWEP.CurrentMode = 3 -- (Integer) Current fire mode of the weapon; used to set the default mode; corresponds to the FireMode table
SWEP.Weight = 7 -- (Integer) The weight in Kilogrammes of our weapon - used in my weapon weight mod!
SWEP.StrongPenetration = VNTCB.Ammo.a556NATO[ 2 ] -- (Integer) Max penetration
SWEP.WeakPenetration = VNTCB.Ammo.a556NATO[ 3 ] -- (Integer) Max wood penetration
SWEP.EffectiveRange = 150 -- (Integer) Effective range of the weapon in metres.
SWEP.StoppageRate = 2000 -- (Integer) Rate of stoppages in the weapon, look up the real world number estimations and just throw that in here.
SWEP.BurstCount = 3 -- (Integer) Amounts of shots to be fired by burst
SWEP.BurstDelay = 0.05 -- (Float) Time between bolt cycles in the burst

------------------------------------------------------
--	Special FX										--	Muzzle flashes, shell casings, etc
------------------------------------------------------

SWEP.MuzzleAttach = 1 -- (Integer) The number of the attachment point for muzzle flashes, typically "1"
SWEP.MuzzleFlashType = 4 -- (Integer) The number of the muzzle flash to use; see Lua/Effects/fx_muzzleflash.Lua
SWEP.ShellAttach = 2 -- (Integer) The number of the attachment point for shell ejections, typically "2"
SWEP.ShellType = 15 -- (Integer) The shell to use, see Lua/Effects/FX_ShellEject for integers

------------------------------------------------------
--	Grenades
--	These'll come in handy
------------------------------------------------------

-- SWEP.GrenadeLauncherEntity = VNT_PROJECTILE_GRENADE_NATO -- (String) Name of the grenade launcher shell entity in Lua/Entities/Entityname.lua
SWEP.GrenadeLauncherEntity = "v92_sin_ar_grenade_live" -- (String) Name of the grenade launcher shell entity in Lua/Entities/Entityname.lua
SWEP.GrenadeLauncherForce = 1750 -- (Integer) Force of grenade launchers and shell throwers like that.

------------------------------------------------------
--	Melee Settings									--
------------------------------------------------------

SWEP.MeleeAnimType = 1 -- (Integer) Melee type; 0=holdtype animation, 1=pistol whip, 2=rifle butt, 3=random between them
SWEP.MeleeRange = 70 -- (Integer) Range of melee weapon swings
SWEP.HasMeleeAttack = true -- (Boolean) Does this weapon have a pistol whip or rifle butt animation?

------------------------------------------------------
--	Custom Sounds									--	Setup sounds here!
------------------------------------------------------

SWEP.Sounds = {
	["Primary"] = Sound( "SiN.AssaultRifle.Single" ), -- (String) Primary shoot sound
	["PrimaryDry"] = Sound( "SiN.AssaultRifle.Empty" ), -- (String) Primary dry fire sound
	["GrenadeLauncher"] = Sound( "SiN.AssaultRifle.Double" ) , -- (String) Grenade launcher sound
	["Reload"] = Sound( "SiN.AssaultRifle.Reload" ), -- (String) Reload sound
	["WhipHitCharacter"] = Sound( "SiN.AssaultRifle.Melee_Hit" ), -- (String) Sound for pistol whip hits
	["WhipHitWall"] = Sound( "SiN.AssaultRifle.Melee_Hit_World" ), -- (String) Sound for pistol whip hits
	["WhipMiss"] = Sound( "SiN.AssaultRifle.Melee_Miss" ), -- (String) Sound for pistol whip misses

	["Noise_Close"] = Sound( "BF3.BulletCraft.Noise.Rifle.Close" ),
	["Noise_Distant"] = Sound( "BF3.BulletCraft.Noise.Forest.Distant" ),
	["Noise_Far"] = Sound( "BF3.BulletCraft.Noise.Forest.Far" ),
	["CoreBass_Close"] = Sound( "BF3.BulletCraft.CoreBass.Close.LMG_12" ),
	["CoreBass_Distant"] = Sound( "BF3.BulletCraft.CoreBass.Distant.LMG_11" ),
	["HiFi"] = Sound( "BF3.BulletCraft.HiFi.UMP" ),
	["Reflection_Close"] = Sound( "BF3.BulletCraft.Reflection.Forest.Close" ),
	["Reflection_Far"] = Sound( "BF3.BulletCraft.Reflection.Forest.Far" ),
}

SWEP.ReloadSNDDelay = 0 -- (Float) Reload sound delay
SWEP.SelectorSwitchSNDType = 1 -- (Integer) 1=US , 2=RU
SWEP.UsesSuperSonicAmmo = true -- (Boolean) Is the weapon using supersonic or subsonic ammo?
------------------------------------------------------
--	Ironsight & Run Positions						--	Set our model transforms for running and ironsights
------------------------------------------------------
SWEP.IronSightsPos = Vector(-4.531, -5, 2.039)
SWEP.IronSightsAng = Vector(0, 0, 0)

SWEP.RunArmOffset = Vector(5.427, -20, 0.201)
SWEP.RunArmAngle = Vector(0, 70, 0)

------------------------------------------------------
--	Setup Clientside Info							--	This block must be in every weapon!
------------------------------------------------------
if CLIENT then

	SWEP.WepSelectIcon = surface.GetTextureID( "vgui/hud/" .. SWEP.WeaponName )
	SWEP.RenderGroup = RENDERGROUP_BOTH
	language.Add( SWEP.WeaponName , SWEP.PrintName )
	killicon.Add( SWEP.WeaponName , "vgui/entities/" .. SWEP.WeaponName , Color( 255 , 255 , 255 ) )

elseif SERVER then

	resource.AddWorkshop( SWEP.WorkshopID )

end

------------------------------------------------------
--	SWEP:Initialize() 							--	Called when the weapon is first loaded
------------------------------------------------------
function SWEP:Initialize( )

	self.HoldMeRight = VNTCB.HoldType.Rifle -- (String) Hold type table for our weapon, Lua/autorun/sh_v92_base_swep.Lua

end

SWEP.SeqPrimary = { "fire01" , "fire02" , "fire03" , "fire04" }
SWEP.SeqPrimaryEmpty = { "dryfire" }
SWEP.SeqSecondary = { "altfire" }
SWEP.SeqIdle = { "idle01" }
SWEP.SeqReload = { "reload" }
SWEP.SeqDraw = { "draw" , "draw2" }
SWEP.SeqHolster = { "holster" }
SWEP.SeqHitCenter = { "swing_hit" , "swing_hit2" }
SWEP.SeqMissCenter = { "swing_miss" , "swing_miss2" }

SWEP.SeqIdleToLow = { "idletolow" }
SWEP.SeqLowIdle = { "lowidle" }
SWEP.SeqLowToIdle = { "lowtoidle" }
SWEP.SeqReloadM203 = { "g_launcher_load" }
