
AddCSLuaFile( )

ENT.PrintName = "Back: Backpack"

if not VNTCB then

	Error( "V92 Content Bases not mounted; Removing Weapon: " .. self.PrintName .. "\n" )

	return false

end

ENT.Base = VNT_BASE_WEAPON_ENTITY
ENT.Type = "anim"
ENT.Author = VNTCB.author
ENT.Information = "SHIFT+E to use"
ENT.Category = VNT_CATEGORY_STALKER
ENT.Spawnable = true
ENT.AdminOnly = false
ENT.SWepName = "v92_stk_sunrise_backpack" -- (String) Name of the weapon entity in Lua/weapons/swep_name.lua
ENT.WeaponName = ENT.SWepName .. "_ent"	-- (String) Name of this entity
ENT.SEntModel = Model( "models/jessev92/stalker/items/clothing/sunrise_backpack1.mdl" ) -- (String) Model to use