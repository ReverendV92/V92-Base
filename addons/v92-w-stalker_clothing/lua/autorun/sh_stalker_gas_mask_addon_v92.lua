
local function GasMaskDeathCleanup()
	for _,_P in pairs( player.GetAll() ) do
		if ((!_P:Alive()) or (!IsValid(_P))) and _P:GetNWBool( "STALKER_PlyGasItemOn" ) != false then
			if CLIENT then
				RunConsoleCommand( "pp_mat_overlay", "" )
				_P.ItemMDL = _P.ItemMDL or ClientsideModel( "models/jessev92/stalker/weapons/gas_mask_sunrise_w.mdl", RENDERGROUP_BOTH)
				local ItemMDL = _P.ItemMDL	
				ItemMDL:Remove()
			end
			if SERVER then
				_P:SetNWBool( "STALKER_PlyGasItemOn", false )
			end
		end
	end
end

timer.Create( "STALKERGasMaskDeathRemovalTimer", 2, 0, GasMaskDeathCleanup )
