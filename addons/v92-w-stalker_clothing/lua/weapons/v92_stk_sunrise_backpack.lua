

AddCSLuaFile( )

------------------------------------------------------
--	Prevent this file from loading if for some odd reason the base Lua isn't loaded
------------------------------------------------------

if not VNTCB then return false end 

------------------------------------------------------
--	Spawn settings									
--	Can we spawn this?
------------------------------------------------------

SWEP.Spawnable = true -- (Boolean) Can be spawned via the menu
SWEP.AdminOnly = false -- (Boolean) Admin only spawnable
SWEP.Base = VNT_BASE_WEAPON_CLOTHING -- (Weapon) Base to derive from

------------------------------------------------------
--	Client Information								
--	Info used in the client block of the weapon
------------------------------------------------------

SWEP.Settings.Weapon = "v92_stk_sunrise_backpack" -- (String) Name of the weapon script
SWEP.WeaponEntityName = SWEP.Settings.Weapon .. "_ent" -- (String) Name of the weapon entity in Lua/Entities/Entityname.lua
SWEP.PrintName = "Backpack" -- (String) Printed name on menu
SWEP.Category = VNT_CATEGORY_STALKER -- (String) Category
SWEP.Instructions = "RELOAD: Item On/Off"	
SWEP.Author = VNTCB.author -- (String) Author
SWEP.Contact = VNTCB.contact -- (String) Contact
SWEP.Purpose = VNTCB.purpose -- (String) Purpose
SWEP.Slot = VNT_WEAPON_BUCKETPOS_CLOTHING -- (Integer) Bucket to place weapon in, 1 to 6
SWEP.SlotPos = VNT_WEAPON_SLOTPOS_CLOTHING -- (Integer) Bucket position
SWEP.Settings.WorkshopID = "631372037" -- (Integer) Workshop ID number of the upload that contains this file.

------------------------------------------------------
--	Model Information								
--	Model settings and infomation
------------------------------------------------------

SWEP.ViewModel = Model("models/jessev92/weapons/mask_c.mdl") -- (String) View model - v_*
SWEP.WorldModel = "" -- (String) World model - w_*
SWEP.UseHands = true -- (Boolean) Leave at false unless the model uses C_Arms

------------------------------------------------------
--	Primary Fire Settings							--	Settings for the primary fire of the weapon
------------------------------------------------------

-- SWEP.Primary.ClipSize = -1 -- (Integer) Size of a magazine
-- SWEP.Primary.DefaultClip = 600 -- (Integer) Default number of ammo you spawn with
-- SWEP.Primary.Ammo = "gasmaskfilters" -- (String) Primary ammo used by the weapon, bullets probably

SWEP.Primary.ClipSize = -1 -- (Integer) Size of a magazine
SWEP.Primary.DefaultClip = -1 -- (Integer) Default number of ammo you spawn with
SWEP.Primary.Ammo = "none" -- (String) Primary ammo used by the weapon, bullets probably

------------------------------------------------------
--	Gun Mechanics									--	Various things to tweak the effects and feedback
------------------------------------------------------

SWEP.Weight = 1 -- (Integer) The weight in Kilogrammes of our weapon - used in my weapon weight mod!

------------------------------------------------------
--	Mask up when you're ready to start the job!
------------------------------------------------------

SWEP.ItemGearSlot = VNT_GEAR_SLOT_BACK -- (String: Body Part Slot) 
SWEP.ItemModel = Model( "models/jessev92/stalker/items/clothing/sunrise_backpack1.mdl" )
SWEP.ItemSkin = 0 -- (Integer) skin number
SWEP.ItemColor = Color( 255 , 255 , 255 , 255 ) -- (Vector:Color) Color of the entity
SWEP.ItemBGZero = 0 -- (Integer) Bodygroup Zero Subgroup Setting
SWEP.ItemBGOne = 0 -- (Integer) Bodygroup One Subgroup Setting
SWEP.ItemBGTwo = 0 -- (Integer) Bodygroup Two Subgroup Setting
SWEP.ItemBGThree = 0 -- (Integer) Bodygroup Three Subgroup Setting
SWEP.ItemBGFour = 0 -- (Integer) Bodygroup Four Subgroup Setting
SWEP.ItemBGFive = 0 -- (Integer) Bodygroup Five Subgroup Setting

SWEP.Sounds = {
	["ItemOn"] = Sound( "VNTCB.Items.Zipper" ) ,
	["ItemOff"] = Sound( "VNTCB.Items.Zipper" ) ,
	["ItemFoleyOn"] = Sound( "VNTCB.Items.GasMask.FoleyOn" ) ,
	["ItemFoleyOff"] = Sound( "VNTCB.Items.GasMask.FoleyOff" ) ,
}

------------------------------------------------------
--	Setup Clientside Info							
--	This block must be in every weapon!
------------------------------------------------------

if CLIENT then

	SWEP.WepSelectIcon = surface.GetTextureID( "vgui/hud/" .. SWEP.Settings.Weapon )
	SWEP.RenderGroup = RENDERGROUP_BOTH
	language.Add( SWEP.Settings.Weapon , SWEP.PrintName )
	killicon.Add( SWEP.Settings.Weapon , "vgui/entities/" .. SWEP.Settings.Weapon , Color( 255 , 255 , 255 ) )
	
elseif SERVER then

	resource.AddWorkshop( SWEP.Settings.WorkshopID )
	
end

------------------------------------------------------
--	SWEP:Initialize() 							
--	Called when the weapon is first loaded
------------------------------------------------------

function SWEP:Initialize( )

	self.HoldMeRight = VNTCB.HoldType.Normal -- (String) Hold type table for our weapon, Lua/autorun/sh_v92_base_swep.Lua

end
