
---------------------------------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------------------------
--	Half-Life 2: Expanded - Sub-Machine Guns
---------------------------------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------------------------

--	Catch-22 Diabolis Ex Machina Prevention

if SERVER then
	resource.AddWorkshop("505106454") 	--	V92 Code Bases
end

-- SOUND GUIDE
----------
-- Channel = Typically CHAN_WEAPON or CHAN_ITEM for gunshots and gun parts respectively; CHAN_BODY for foley
-- Volume = Leave at 1.0
-- Pitch = Typically between 95 and 105 for gunshots
-- Level = For gunshots, follow this estimation table as a LOOSE guideline:
----------
--	.22LR = 40dB
--	9x19mm = 60dB
--	.38 Special = 70dB
--	5.56x45mm = 70dB
--	7.62x51mm = 90dB
--	.45 ACP = 90dB
--	.50 AE = 95dB
--	.50 BMG = 100dB
--	12-Gauge Buckshot = 120dB
--	Explosion = 200dB
--	Suppressed = 2/3 of normal
--	Empty = 60dB 
--	Weapon sounds = ~50dB (use your head)
--	Foley = ~30dB (use your head)
----------
----------

if not VNTCB then
	Error( "V92 Content Bases not mounted :: Removing Autorun File :: HL2X SMGs\n" )

	return false
end

if VNTCB.Plugins then
	table.insert( VNTCB.Plugins, 0, "VNT :: HL2X SMG Pack" )
end

------------------------------------------------------------------------------------------
--	HL2X :: Sub-Machine Guns
------------------------------------------------------------------------------------------

---------------------------------------------
--	H&K MP5 Navy
---------------------------------------------

sound.Add({	name	= "HL2_OldSch_MP5Navy.Single",
	channel			= CHAN_WEAPON,
	volume			= 1.0,
	level			= 65,
	pitch			= { 95, 105 },
	sound			= "^)jessev92/hl2/weapons/MP5Navy/single.wav",
})
util.PrecacheSound("jessev92/hl2/weapons/MP5Navy/single.wav")

sound.Add({	name	= "HL2_OldSch_MP5Navy.GL",
	channel			= CHAN_WEAPON,
	volume			= 1.0,
	level			= 65,
	pitch			= { 95, 105 },
	sound			= "^)jessev92/ins1/weapons/m16a4/m203_fire.wav",
})
util.PrecacheSound("jessev92/ins1/weapons/m16a4/m203_fire.wav")

sound.Add({	name	= "HL2_OldSch_MP5Navy.Reload",
	channel			= CHAN_ITEM,
	volume			= 1.0,
	level			= 45,
	sound			= "jessev92/hl2/weapons/MP5Navy/reload.wav",
})
util.PrecacheSound("jessev92/hl2/weapons/MP5Navy/reload.wav")

sound.Add( { name 	= "HL2_OldSch_MP5Navy.Draw",
	channel 		= CHAN_BODY,
	volume			= 1.0,
	level			= 45,
	pitch			= { 95, 105 },
	sound				= "jessev92/weapons/univ/draw1.wav",
} )
util.PrecacheSound("jessev92/weapons/univ/draw1.wav")

sound.Add( { name 	= "HL2_OldSch_MP5Navy.Holster",
	channel 		= CHAN_BODY,
	volume			= 1.0,
	level			= 45,
	pitch			= { 95, 105 },
	sound			= "jessev92/weapons/univ/holster1.wav",
} )
util.PrecacheSound("jessev92/weapons/univ/holster1.wav")

---------------------------------------------
--	H&K MP5K
---------------------------------------------

sound.Add({	name	= "HL2_MP5K.Single",
	channel			= CHAN_WEAPON,
	volume			= 1.0,
	level			= 65,
	pitch			= { 95, 105 },
	sound			= "jessev92/hl2/weapons/mp5k/single.wav",
})
util.PrecacheSound("jessev92/hl2/weapons/mp5k/single.wav")

sound.Add({	name	= "HL2_MP5K.SingleSup",
	channel			= CHAN_WEAPON,
	volume			= 1.0,
	level			= 45,
	pitch			= { 95, 105 },
	sound			= "jessev92/hl2/weapons/mp5k/single_sil.wav",
})
util.PrecacheSound("jessev92/hl2/weapons/mp5k/single_sil.wav")

sound.Add({ name	= "HL2_MP5K.GL",
	channel			= CHAN_WEAPON,
	volume			= 1.0,
	level			= 65,
	pitch			= { 95, 105 },
	sound			= "jessev92/weapons/univ/throw_gren.wav",
})
util.PrecacheSound("jessev92/weapons/univ/throw_gren.wav")

sound.Add({	name	= "HL2_MP5K.BoltPull",
	channel			= CHAN_ITEM,
	volume			= 1.0,
	level			= 50,
	sound			= "jessev92/hl2/weapons/mp5k/boltpull.wav",
})
util.PrecacheSound("jessev92/hl2/weapons/mp5k/boltpull.wav")

sound.Add({	name	= "HL2_MP5K.Reload",
	channel			= CHAN_ITEM,
	volume			= 1.0,
	level			= 50,
	sound			= "jessev92/hl2/weapons/mp5k/reload.wav",
})
util.PrecacheSound("jessev92/hl2/weapons/mp5k/reload.wav")

sound.Add({	name 	= "HL2_MP5K.Draw",
	channel 		= CHAN_BODY,
	volume			= 1.0,
	level			= 45,
	pitch			= { 95, 105 },
	sound			= "jessev92/weapons/univ/draw1.wav",
} )
util.PrecacheSound("jessev92/weapons/univ/draw1.wav")

sound.Add({	name 	= "HL2_MP5K.Holster",
	channel 		= CHAN_BODY,
	volume			= 1.0,
	level			= 45,
	pitch			= { 95, 105 },
	sound			= "jessev92/weapons/univ/holster1.wav",
} )
util.PrecacheSound("jessev92/weapons/univ/holster1.wav")


---------------------------------------------
--	PP-19 Bizon
---------------------------------------------

sound.Add({	name	= "HL2_PP19.Single",
	channel			= CHAN_WEAPON,
	volume			= 1.0,
	level			= 65,
	pitch			= { 95, 105 },
	sound			= "jessev92/hl2/weapons/pp19/single.wav",
})
util.PrecacheSound("jessev92/hl2/weapons/pp19/single.wav")

sound.Add({	name	= "HL2_PP19.GL",
	channel			= CHAN_WEAPON,
	volume			= 1.0,
	level			= 65,
	pitch			= { 95, 105 },
	sound			= "jessev92/weapons/univ/gl1.wav",
})
util.PrecacheSound("jessev92/weapons/univ/gl1.wav")

sound.Add({	name	= "HL2_PP19.Reload",
	channel			= CHAN_ITEM,
	volume			= 1.0,
	level			= 50,
	sound			= "jessev92/hl2/weapons/pp19/reload.wav",
})
util.PrecacheSound("jessev92/hl2/weapons/pp19/reload.wav")

sound.Add({	name	= "HL2_PP19.Draw",
	channel			= CHAN_BODY,
	volume			= 1.0,
	level			= 45,
	pitch			= { 95, 105 },
	sound			= "jessev92/weapons/univ/draw1.wav",
})
util.PrecacheSound("jessev92/weapons/univ/draw1.wav")

---------------------------------------------
--	Brugger & Thomet MP9
---------------------------------------------

sound.Add( {
	name = "HL2_MP9.Single" ,
	channel = CHAN_WEAPON ,
	volume = 1.0 ,
	level = 60 ,
	pitch = { 95 , 105 } ,
	sound = "jessev92/hl2/weapons/mp9/single.wav"
} )
util.PrecacheSound( "jessev92/hl2/weapons/mp9/single.wav" )

sound.Add( {
	name = "HL2_MP9.GL" ,
	channel = CHAN_WEAPON ,
	volume = 1.0 ,
	level = 60 ,
	pitch = { 95 , 105 } ,
	sound = "jessev92/weapons/univ/throw2.wav"
} )
util.PrecacheSound( "jessev92/weapons/univ/throw2.wav" )

sound.Add( {
	name = "HL2_MP9.Reload" ,
	channel = CHAN_ITEM ,
	volume = 1.0 ,
	level = 50 ,
	pitchstart = 95 ,
	pitchend = 96 ,
	sound = "jessev92/hl2/weapons/mp9/reload.wav"
} )
util.PrecacheSound( "jessev92/hl2/weapons/mp9/reload.wav" )

sound.Add( {
	name = "HL2_MP9.Draw" ,
	channel = CHAN_ITEM ,
	volume = 1.0 ,
	level = 45 ,
	pitchstart = 95 ,
	pitchend = 96 ,
	sound = "jessev92/weapons/univ/draw1.wav"
} )
util.PrecacheSound( "jessev92/weapons/univ/draw1.wav" )
