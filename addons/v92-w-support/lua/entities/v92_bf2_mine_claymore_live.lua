
AddCSLuaFile()

ENT.PrintName = "Active M18 Mine"

if not VNTCB then
	Error( "V92 Content Bases not mounted; Removing Weapon: " .. ENT.PrintName .. "\n" )
	return false
end

ENT.Base = VNT_BASE_MINE_PERSONNEL
ENT.Type = "anim"
ENT.Author = "V92"
ENT.Spawnable = true
ENT.AdminOnly = true

ENT.MineModel = Model( "models/jessev92/bf2/weapons/m18mine_prop.mdl" )
ENT.TimeToArm = 3
ENT.DeployedSound = Sound( "BF2.M18Mine.Draw" )
ENT.ArmedSound = Sound( "BF2.M18Mine.Armed" )
ENT.ExplosionSound = Sound( "HL2.Generic.Explosion" )
