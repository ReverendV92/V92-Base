
AddCSLuaFile()

local ActionDelay		= CurTime()
local TorchOn			= false

if (SERVER) then
	SWEP.Weight 			= 5 
	SWEP.AutoSwitchTo 		= false 
	SWEP.AutoSwitchFrom 	= false 
end

if (CLIENT) then

	local	_SELFENTNAME	= "v92_bms_torch"

	SWEP.PrintName			= "Torch"
	SWEP.Category			= VNT_CATEGORY_BLACKMESA
	SWEP.Author   			= VNTCB.author
	SWEP.Contact        	= VNTCB.contact
	SWEP.Instructions 		= VNTCB.instructions
	SWEP.Slot 				= VNT_WEAPON_BUCKETPOS_SUPPORT
	SWEP.SlotPos 			= VNT_WEAPON_SLOTPOS_SUPPORT
	SWEP.WepSelectIcon		= surface.GetTextureID("vgui/hud/" .. _SELFENTNAME )
	SWEP.ViewModelFOV		= 66.5
	SWEP.BounceWeaponIcon	= false

	language.Add( _SELFENTNAME, _INFONAME ) 
	killicon.Add( _SELFENTNAME, "vgui/entities/".. _SELFENTNAME , Color( 255, 255, 255 ) )

	if GetConVarNumber( "VNT_SWep_TorchBMS_Toggle" ) != 0 then
		function FlashlightBind( ply, bind, pressed )
			if not pressed then return false end
			if (bind == "impulse 100") then return true end
		end
		hook.Add( "PlayerBindPress", "BMSTorchSWepFlashlightBind", FlashlightBind )
		hook.Add( "EntityEmitSound", "BMSTorchDefSndDisable", function(data) if data.SoundName == "items/flashlight1.wav" then return false end end)
	end
	
end

SWEP.Spawnable 					= true 
SWEP.AdminOnly					= false
SWEP.ViewModelFlip 				= false
SWEP.ViewModel 					= Model("models/jessev92/bms/weapons/torch_c.mdl")
SWEP.WorldModel 				= Model("models/jessev92/bms/weapons/torch_w.mdl")
SWEP.UseHands					= true
SWEP.Primary.ClipSize 			= -1 
SWEP.Primary.DefaultClip 		= -1 
SWEP.Primary.Automatic 			= false 
SWEP.Primary.Ammo 				= "none" 
SWEP.Secondary.ClipSize			= -1 
SWEP.Secondary.DefaultClip 		= -1 
SWEP.Secondary.Automatic 		= false 
SWEP.Secondary.Ammo 			= "none" 

function SWEP:Initialize() 
	self:SetHoldType("pistol")
end
function SWEP:Reload() end
function SWEP:Think()
	if IsValid(self.Owner) then
		-- oops
		--[[
		if SERVER then
			if self.Owner:KeyDown(IN_SPEED) and self.Owner:GetVelocity():Length() > self.Owner:GetWalkSpeed() and self.Owner:GetMoveType() == 2 then
				self.Weapon:SendWeaponAnim( ACT_VM_PULLBACK )
			else
				self:Idle()
			end
		end
		if CLIENT then
			if self.Owner:GetNWInt( "Battery" ) == 0 then
				self.Weapon:SendWeaponAnim( ACT_VM_FIDGET )
			end
		end
		--]]
	end
end

function SWEP:Idle() self.Weapon:SendWeaponAnim( ACT_VM_IDLE ) end

function SWEP:NoBattery() self.Weapon:SendWeaponAnim( ACT_VM_FIDGET ) end

function SWEP:Deploy()
	self.Weapon:SendWeaponAnim( ACT_VM_DRAW ) 
	if(SERVER) then
		timer.Simple( 0.15, function() 
			self:Idle() 
			self.Owner:FlashlightIsOn(true)
			TorchOn = true
		end)
	end
end

function SWEP:Holster( wep )
	if(SERVER) then
		self.Weapon:SendWeaponAnim( ACT_VM_HOLSTER )
		self.Owner:FlashlightIsOn(false)
		self.Owner:Flashlight(false)
		TorchOn = false
	end
	return true
end

function SWEP:PrimaryAttack()
	if not IsFirstTimePredicted() then return end
	if SERVER then
		if TorchOn == false then
			self.Owner:FlashlightIsOn(true)
			self.Owner:Flashlight(true)
			--self:Idle()
			TorchOn = true
		end
    end
end

function SWEP:SecondaryAttack()
	if not IsFirstTimePredicted() then return end
	if SERVER then
		if TorchOn == true then
			self.Owner:FlashlightIsOn(false)
			self.Owner:Flashlight(false)
			--self:Idle()
			TorchOn = false
		end
    end
end

