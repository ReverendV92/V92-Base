
------------------------------------------------------
--	Counter-Strike: Source							--
--	Flashbang Grenade 								--
------------------------------------------------------

ENT.PrintName = "Live M18 Smoke Grenade"

if not VNTCB then

	Error( "V92 Content Bases not mounted; Removing Weapon: " .. SWEP.PrintName .. "\n" )
	return false
	
elseif IsMounted( "cstrike" ) == false then

	Error( "Counter-Strike: Source not mounted; Removing Weapon: " .. SWEP.PrintName .. "\n" )
	return false
	
end

AddCSLuaFile()

ENT.Type = "anim"
ENT.Base = "v92_grenade_flashbang"

ENT.Type = "anim" -- Type of entity
ENT.Author = VNTCB.Info.author -- Author name
ENT.Contact = VNTCB.Info.contact -- Contact
ENT.Purpose = VNTCB.Info.purpose -- Purpose
ENT.Instructions = VNTCB.Info.instructions -- Instructions

-- Model to use
ENT.Model = Model( "models/weapons/w_eq_flashbang.mdl" )

-- Mass in KG
ENT.Mass = 5

-- Sound table
ENT.Sounds = {

	["BounceConcrete"] = Sound( "Flashbang.Bounce" ) ,
	["BounceMetal"] = Sound( "Flashbang.Bounce" ) ,
	["BounceSand"] = Sound( "Flashbang.Bounce" ) ,
	["BounceWood"] = Sound( "Flashbang.Bounce" ) ,
	["Debris"] = Sound( "common/null.wav" ) ,
	["Explosion"] = Sound( "Flashbang.Explode" ) ,
	["WaterExplosion"] = Sound( "Flashbang.Explode" ) ,

}
