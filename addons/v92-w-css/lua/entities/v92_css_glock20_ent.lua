
AddCSLuaFile( )

if not VNTCB then
	Error( "V92 Content Bases not mounted; Removing Weapon:\n" )
	return false
elseif IsMounted( "cstrike" ) == false then
	Error( "Counter-Strike: Source not mounted: Removing Entity\n" )
	return false
end

ENT.Base = VNT_BASE_WEAPON_ENTITY
ENT.Type = "anim"
ENT.PrintName = "Glock 20"
ENT.Author = VNTCB.author
ENT.Information = "Uses 10mm Auto Ammo"
ENT.Category = VNT_CATEGORY_CSTRIKESOURCE
ENT.Spawnable = true
ENT.AdminOnly = false
ENT.SWepName = "v92_css_glock20" -- (String) Name of the weapon entity in Lua/weapons/swep_name.lua
ENT.WeaponName = ENT.SWepName .. "_ent"	-- (String) Name of this entity
ENT.SEntModel = Model( "models/weapons/w_pist_glock18.mdl" ) -- (String) Model to use