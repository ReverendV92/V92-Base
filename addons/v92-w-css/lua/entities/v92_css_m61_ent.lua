
------------------------------------------------------
--	Counter-Strike: Source							--
--	M61 Fragmentation Grenade 						--
------------------------------------------------------

ENT.PrintName = "M61 Frag Grenade"

if not VNTCB then
	Error( "V92 Content Bases not mounted; Removing Weapon: " .. SWEP.PrintName .. "\n" )
	return false
elseif IsMounted( "cstrike" ) == false then
	Error( "Counter-Strike: Source not mounted; Removing Weapon: " .. SWEP.PrintName .. "\n" )
	return false
end

AddCSLuaFile( )

ENT.Base = VNT_BASE_WEAPON_ENTITY
ENT.Type = "anim"
ENT.Author = VNTCB.author
ENT.Information = "Uses M61 Frag Ammo"
ENT.Category = VNT_CATEGORY_CSTRIKESOURCE
ENT.Spawnable = true
ENT.AdminOnly = false
ENT.SWepName = "v92_css_m61" -- (String) Name of the weapon entity in Lua/weapons/swep_name.lua
ENT.WeaponName = ENT.SWepName .. "_ent"	-- (String) Name of this entity
ENT.SEntModel = Model( "models/weapons/w_eq_fraggrenade.mdl" ) -- (String) Model to use
