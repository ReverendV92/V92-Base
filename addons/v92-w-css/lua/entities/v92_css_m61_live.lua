
------------------------------------------------------
--	Counter-Strike: Source							--
--	M61 Fragmentation Grenade 						--
------------------------------------------------------

ENT.PrintName = "Live M61 Smoke Grenade"

if not VNTCB then

	Error( "V92 Content Bases not mounted; Removing Weapon: " .. SWEP.PrintName .. "\n" )
	return false
	
elseif IsMounted( "cstrike" ) == false then

	Error( "Counter-Strike: Source not mounted; Removing Weapon: " .. SWEP.PrintName .. "\n" )
	return false
	
end

AddCSLuaFile()

ENT.Type = "anim"
ENT.Base = "v92_grenade_frag"

ENT.Type = "anim" -- Type of entity
ENT.Author = VNTCB.Info.author -- Author name
ENT.Contact = VNTCB.Info.contact -- Contact
ENT.Purpose = VNTCB.Info.purpose -- Purpose
ENT.Instructions = VNTCB.Info.instructions -- Instructions

-- Model to use
ENT.Model = Model( "models/weapons/w_eq_fraggrenade.mdl" )

-- Mass in KG
ENT.Mass = 5

-- Sound table
ENT.Sounds = {

	["BounceConcrete"] = Sound( "HEGrenade.Bounce" ) ,
	["BounceMetal"] = Sound( "HEGrenade.Bounce" ) ,
	["BounceSand"] = Sound( "HEGrenade.Bounce" ) ,
	["BounceWood"] = Sound( "HEGrenade.Bounce" ) ,
	["Debris"] = Sound( "BaseGrenade.Explode" ) ,
	["Explosion"] = Sound( "BaseExplosionEffect.Sound" ) ,
	["WaterExplosion"] = Sound( "WaterExplosionEffect.Sound" ) ,

}
