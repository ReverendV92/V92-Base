
------------------------------------------------------
--	Counter-Strike: Source							--
--	M18 Smoke Grenade 								--
--	Colour: Grey									--
------------------------------------------------------

AddCSLuaFile( )

ENT.PrintName = "Live M18 Smoke Grenade"

if not VNTCB then
	Error( "V92 Content Bases not mounted; Removing Weapon: " .. SWEP.PrintName .. "\n" )
	return false
elseif IsMounted( "cstrike" ) == false then
	Error( "Counter-Strike: Source not mounted; Removing Weapon: " .. SWEP.PrintName .. "\n" )
	return false
end

AddCSLuaFile()

ENT.Type = "anim"
ENT.Base = "v92_grenade_smoke"

ENT.Type = "anim" -- Type of entity
ENT.Author = VNTCB.Info.author -- Author name
ENT.Contact = VNTCB.Info.contact -- Contact
ENT.Purpose = VNTCB.Info.purpose -- Purpose
ENT.Instructions = VNTCB.Info.instructions -- Instructions

-- Model to use
ENT.Model = Model( "models/weapons/w_eq_smokegrenade.mdl" )

-- Mass in KG
ENT.Mass = 5

-- Sound table
ENT.Sounds = {

	["BounceConcrete"] = Sound( "SmokeGrenade.Bounce" ) ,
	["BounceMetal"] = Sound( "SmokeGrenade.Bounce" ) ,
	["BounceSand"] = Sound( "SmokeGrenade.Bounce" ) ,
	["BounceWood"] = Sound( "SmokeGrenade.Bounce" ) ,
	["Debris"] = Sound( "common/null.wav" ) ,
	["Explosion"] = Sound( "BaseSmokeEffect.Sound" ) ,
	["WaterExplosion"] = Sound( "BaseSmokeEffect.Sound" ) ,

}

ENT.SmokeColour = Color( 150 , 150 , 150 )
