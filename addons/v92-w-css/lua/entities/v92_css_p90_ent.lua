
AddCSLuaFile( )

if not VNTCB then
	Error( "V92 Content Bases not mounted; Removing Weapon:\n" )
	return false
elseif IsMounted( "cstrike" ) == false then
	Error( "Counter-Strike: Source not mounted: Removing Entity\n" )
	return false
end

ENT.Base = VNT_BASE_WEAPON_ENTITY
ENT.Type = "anim"
ENT.PrintName = "P90"
ENT.Author = VNTCB.author
ENT.Information = "Uses 5.7x28mm NATO Ammo"
ENT.Category = VNT_CATEGORY_CSTRIKESOURCE
ENT.Spawnable = true
ENT.AdminOnly = false
ENT.SWepName = "v92_css_p90" -- (String) Name of the weapon entity in Lua/weapons/swep_name.lua
ENT.WeaponName = ENT.SWepName .. "_ent"	-- (String) Name of this entity
ENT.SEntModel = Model( "models/weapons/w_smg_p90.mdl" ) -- (String) Model to use