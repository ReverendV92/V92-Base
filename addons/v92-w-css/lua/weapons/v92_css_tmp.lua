
------------------------------------------------------
--	Counter-Strike: Source							--
--	Br�gger & Thomet MP9							--
--	9x19mm NATO Semi/Auto Machine Pistol			--
------------------------------------------------------

AddCSLuaFile( )

SWEP.PrintName = "MP9" -- (String) Printed name on menu

if not VNTCB then
	Error( "V92 Content Bases not mounted; Removing Weapon: " .. SWEP.PrintName .. "\n" )
	return false
elseif IsMounted( "cstrike" ) == false then
	Error( "Counter-Strike: Source not mounted; Removing Weapon: " .. SWEP.PrintName .. "\n" )
	return false
end

SWEP.Base = VNT_BASE_WEAPON -- (String) Weapon base parent this is a child of
SWEP.Spawnable = true -- (Boolean) Can be spawned via the menu
SWEP.AdminOnly = false -- (Boolean) Admin only spawnable
------------------------------------------------------
--	Client Information								--	Info used in the client block of the weapon
------------------------------------------------------
SWEP.WeaponName = "v92_css_tmp" -- (String) Name of the weapon script
SWEP.WeaponEntityName = SWEP.WeaponName .. "_ent" -- (String) Name of the weapon entity in Lua/Entities/Entityname.lua
SWEP.Manufacturer = VNT_WEAPON_MANUFACTURER_BRUGGERTHOMET -- (String) Gun company that makes this weapon
SWEP.CountryOfOrigin = VNT_WEAPON_COUNTRY_AUSTRIA -- (String) Country of origin
SWEP.MagazineName = VNTCB.Magazine.mTMP -- (String) The name of the magazine the weapon uses - used in my Weapon Magazine System
SWEP.Category = VNT_CATEGORY_CSTRIKESOURCE -- (String) Category
SWEP.Instructions = VNTCB.instructions -- (String) Instruction
SWEP.Author = VNTCB.author -- (String) Author
SWEP.Contact = VNTCB.contact -- (String) Contact
SWEP.Slot = VNT_WEAPON_BUCKETPOS_SMG -- (Integer) Bucket to place weapon in, 1 to 6
SWEP.SlotPos = VNT_WEAPON_SLOTPOS_SMG -- (Integer) Bucket position
SWEP.ViewModelFOV = 65 -- (Integer) First-person field of view
SWEP.WorkshopID = "788152557" -- (Integer) Workshop ID number of the upload that contains this file.

------------------------------------------------------
--	Model Information								--
------------------------------------------------------
SWEP.ViewModelFlip = false -- (Boolean) Only used for vanilla CS:S models
SWEP.ViewModel = Model( "models/weapons/cstrike/c_smg_tmp.mdl" ) -- (String) View model - v_*
SWEP.WorldModel = Model( "models/weapons/w_smg_tmp.mdl" ) -- (String) World model - w_*
SWEP.UseHands = true -- (Boolean) Leave at false unless the model uses C_Arms
SWEP.HoldType = "smg" -- (String) Hold type for our weapon, refer to wiki for animation sets
------------------------------------------------------
--	Gun Types										--	Set the type of weapon - ONLY PICK ONE!
------------------------------------------------------
------------------------------------------------------
--	Primary Fire Settings							--	Settings for the primary fire of the weapon
------------------------------------------------------
SWEP.Primary.ClipSize = 30 -- (Integer) Size of a magazine
SWEP.Primary.DefaultClip = 30 -- (Integer) Default number of ammo you spawn with
SWEP.Primary.Ammo = "9x19mmnato" -- (String) Primary ammo used by the weapon, bullets probably
SWEP.Primary.PureDmg = VNTCB.Ammo.a9x19mmNATO[ 1 ] -- (Integer) Base damage, put one number here and the base will do the rest
SWEP.Primary.RPM = 900 -- (Integer) Go to a wikipedia page and look at the RPM of the weapon, then put that here - the base will do the math
------------------------------------------------------
--	Gun Mechanics									--	Various things to tweak the effects and feedback
------------------------------------------------------
SWEP.FireMode = { true , true , false , true } -- (Table: Boolean, Boolean, Boolean, Boolean ) Enable different fire modes on the weapon; Has modes, Has Single, Has Burst, Has Auto - in that order. You can have more than one, but the first must be true
SWEP.CurrentMode = 1 -- (Integer) Current fire mode of the weapon; used to set the default mode; corresponds to the FireMode table
SWEP.Weight = 1 -- (Integer) The weight in Kilogrammes of our weapon - used in my weapon weight mod!
SWEP.StrongPenetration = VNTCB.Ammo.a9x19mmNATO[ 2 ] -- (Integer) Max penetration
SWEP.WeakPenetration = VNTCB.Ammo.a9x19mmNATO[ 3 ] -- (Integer) Max wood penetration
SWEP.EffectiveRange = 100 -- (Integer) Effective range of the weapon in metres.
SWEP.StoppageRate = 1500 -- (Integer) Rate of stoppages in the weapon, look up the real world number estimations and just throw that in here.
------------------------------------------------------
--	Special FX										--	Muzzle flashes, shell casings, etc
------------------------------------------------------
SWEP.MuzzleAttach = 1 -- (Integer) The number of the attachment point for muzzle flashes, typically "1"
SWEP.MuzzleFlashType = 2 -- (Integer) The number of the muzzle flash to use; see Lua/Effects/fx_muzzleflash.Lua
SWEP.ShellAttach = 2 -- (Integer) The number of the attachment point for shell ejections, typically "2"
SWEP.ShellType = nil -- (Integer) The shell to use, see Lua/Effects/FX_ShellEject for integers
------------------------------------------------------
--	Custom Sounds									--	Setup sounds here!
------------------------------------------------------

SWEP.Sounds = {

	["PrimarySup"] = Sound( "Weapon_TMP.Single" ) , -- (String) Primary suppressed shoot sound
	["PrimaryDry"] = Sound("VNTCB.SWep.Empty5") , -- (String) Primary dry fire sound
	["Reload"] = Sound( "VNTCB.SWep.Draw1" ) , -- (String) Reload sound

	["SonicBoom_Sup"] = Sound( "common/null.wav" ) ,
	["Noise_Close"] = Sound( "BF3.BulletCraft.Noise.Suppressed.Distant" ) , --
	["Noise_Distant"] = Sound( "BF3.BulletCraft.Noise.Suppressed.Distant" ) , --
	["Noise_Far"] = Sound( "BF3.BulletCraft.Noise.Suppressed.Far" ) , --
	["CoreBass_Close"] = Sound( "BF3.BulletCraft.CoreBass.Close.OneShot_1" ) , --
	["CoreBass_Distant"] = Sound( "common/null.wav" ) , --
	["HiFi"] = Sound( "BF3.BulletCraft.HiFi.Glock" ) , --
	["Reflection_Close"] = Sound( "common/null.wav" ) , --
	["Reflection_Far"] = Sound( "common/null.wav" ) , --

}

SWEP.SelectorSwitchSNDType = 2 -- (Integer) 1=US , 2=RU
SWEP.UsesSuperSonicAmmo = false -- (Boolean) Is the weapon using supersonic or subsonic ammo?
------------------------------------------------------
--	Ironsight & Run Positions						--	Set our model transforms for running and ironsights
------------------------------------------------------
SWEP.IronSightsPos = Vector(-7.035, -10, 3.015)
SWEP.IronSightsAng = Vector(0, 0, 0)
SWEP.RunArmOffset = Vector(2, -15, -2)
SWEP.RunArmAngle = Vector(11, 70, -10)
------------------------------------------------------
--	Setup Clientside Info							--	This block must be in every weapon!
------------------------------------------------------
if CLIENT then

	SWEP.WepSelectIcon = surface.GetTextureID( "vgui/hud/" .. SWEP.WeaponName )
	SWEP.RenderGroup = RENDERGROUP_BOTH
	language.Add( SWEP.WeaponName , SWEP.PrintName )
	killicon.Add( SWEP.WeaponName , "vgui/entities/" .. SWEP.WeaponName , Color( 255 , 255 , 255 ) )

elseif SERVER then

	resource.AddWorkshop( SWEP.WorkshopID )

end

------------------------------------------------------
--	SWEP:Initialize() 							--	Called when the weapon is first loaded
------------------------------------------------------
function SWEP:Initialize( )
	self.HoldMeRight = VNTCB.HoldType.ForeGrip -- (String) Hold type table for our weapon, Lua/autorun/sh_v92_base_swep.Lua
	self:Set_IsSuppressorAttached( true )
end

SWEP.SeqPrimary = { "fire1" , "fire2" }
SWEP.SeqIdle = { "idle1" }
SWEP.SeqReload = { "reload" }
SWEP.SeqDraw = { "draw" }
