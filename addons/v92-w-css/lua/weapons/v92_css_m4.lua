
------------------------------------------------------
--	Counter-Strike: Source							--
--	Colt M4 Carbine									--
--	5.56x45mm Burst/Semi-Auto rifle					--
------------------------------------------------------

AddCSLuaFile( )

SWEP.PrintName = "M4" -- (String) Printed name on menu

if not VNTCB then
	Error( "V92 Content Bases not mounted; Removing Weapon: " .. SWEP.PrintName .. "\n" )
	return false
elseif IsMounted( "cstrike" ) == false then
	Error( "Counter-Strike: Source not mounted; Removing Weapon: " .. SWEP.PrintName .. "\n" )
	return false
end

SWEP.Base = VNT_BASE_WEAPON -- (String) Weapon base parent this is a child of
SWEP.Spawnable = true -- (Boolean) Can be spawned via the menu
SWEP.AdminOnly = false -- (Boolean) Admin only spawnable
------------------------------------------------------
--	Client Information								--	Info used in the client block of the weapon
------------------------------------------------------
SWEP.WeaponName = "v92_css_m4" -- (String) Name of the weapon script
SWEP.WeaponEntityName = SWEP.WeaponName .. "_ent" -- (String) Name of the weapon entity in Lua/Entities/Entityname.lua
SWEP.Manufacturer = VNT_WEAPON_MANUFACTURER_COLT -- (String) Gun company that makes this weapon
SWEP.CountryOfOrigin = VNT_WEAPON_COUNTRY_UNITEDSTATES -- (String) Country of origin
SWEP.MagazineName = VNTCB.Magazine.mNATO556 -- (String) The name of the magazine the weapon uses - used in my Weapon Magazine System
SWEP.Category = VNT_CATEGORY_CSTRIKESOURCE -- (String) Category
SWEP.Instructions = VNTCB.instructions -- (String) Instruction
SWEP.Author = VNTCB.author -- (String) Author
SWEP.Contact = VNTCB.contact -- (String) Contact
SWEP.Slot = VNT_WEAPON_BUCKETPOS_CARBINE -- (Integer) Bucket to place weapon in, 1 to 6
SWEP.SlotPos = VNT_WEAPON_SLOTPOS_CARBINE -- (Integer) Bucket position
SWEP.ViewModelFOV = 65 -- (Integer) First-person field of view
SWEP.WorkshopID = "788152557" -- (Integer) Workshop ID number of the upload that contains this file.

------------------------------------------------------
--	Model Information								--
------------------------------------------------------
SWEP.ViewModelFlip = false -- (Boolean) Only used for vanilla CS:S models
SWEP.ViewModel = Model( "models/weapons/cstrike/c_rif_m4a1.mdl" ) -- (String) View model - v_*
SWEP.WorldModel = Model( "models/weapons/w_rif_m4a1.mdl" ) -- (String) World model - w_*
SWEP.WorldModelDefault = Model( "models/weapons/w_rif_m4a1.mdl" ) -- (String) World model - w_*
SWEP.UseHands = true -- (Boolean) Leave at false unless the model uses C_Arms
SWEP.HoldType = "ar2" -- (String) Hold type for our weapon, refer to wiki for animation sets
------------------------------------------------------
--	Custom Suppressor Settings						--	This is where we define our suppressor settings
------------------------------------------------------
SWEP.SuppressorType = 2 -- (Integer) 0=No Suppressor, 1=Model Swap, 2=Animation
SWEP.WorldModelSuppressed = Model( "models/weapons/w_rif_m4a1_silencer.mdl" ) -- (String) The Suppressed world model
------------------------------------------------------
--	Gun Types										--	Set the type of weapon - ONLY PICK ONE!
------------------------------------------------------
SWEP.WeaponType = VNTCB.WeaponType.Carbine -- (Integer) 1=Melee, 2=Pistol, 3=Rifle, 4=Shotgun, 5=Sniper, 6=Grenade Launcher, 7=Rocket Launcher
------------------------------------------------------
--	Primary Fire Settings							--	Settings for the primary fire of the weapon
------------------------------------------------------
SWEP.Primary.ClipSize = 30 -- (Integer) Size of a magazine
SWEP.Primary.DefaultClip = 30 -- (Integer) Default number of ammo you spawn with
SWEP.Primary.Ammo = "556x45mmnato" -- (String) Primary ammo used by the weapon, bullets probably
SWEP.Primary.PureDmg = VNTCB.Ammo.a556NATO[ 1 ] -- (Integer) Base damage, put one number here and the base will do the rest
SWEP.Primary.RPM = 900 -- (Integer) Go to a wikipedia page and look at the RPM of the weapon, then put that here - the base will do the math
------------------------------------------------------
--	Gun Mechanics									--	Various things to tweak the effects and feedback
------------------------------------------------------
SWEP.FireMode = { true , true , true , false } -- (Table: Boolean, Boolean, Boolean, Boolean ) Enable different fire modes on the weapon; Has modes, Has Single, Has Burst, Has Auto - in that order. You can have more than one, but the first must be true
SWEP.CurrentMode = 1 -- (Integer) Current fire mode of the weapon; used to set the default mode; corresponds to the FireMode table
SWEP.Weight = 4 -- (Integer) The weight in Kilogrammes of our weapon - used in my weapon weight mod!
SWEP.StrongPenetration = VNTCB.Ammo.a556NATO[ 2 ] -- (Integer) Max penetration
SWEP.WeakPenetration = VNTCB.Ammo.a556NATO[ 3 ] -- (Integer) Max wood penetration
SWEP.EffectiveRange = 500 -- (Integer) Effective range of the weapon in metres.
SWEP.StoppageRate = 1000 -- (Integer) Rate of stoppages in the weapon, look up the real world number estimations and just throw that in here.
SWEP.BurstCount = 3 -- (Integer) Amounts of shots to be fired by burst
SWEP.BurstDelay = 0.05 -- (Float) Time between bolt cycles in the burst
------------------------------------------------------
--	Special FX										--	Muzzle flashes, shell casings, etc
------------------------------------------------------
SWEP.MuzzleAttach = 1 -- (Integer) The number of the attachment point for muzzle flashes, typically "1"
SWEP.MuzzleFlashType = 2 -- (Integer) The number of the muzzle flash to use; see Lua/Effects/fx_muzzleflash.Lua
SWEP.ShellAttach = 2 -- (Integer) The number of the attachment point for shell ejections, typically "2"
SWEP.ShellType = nil -- (Integer) The shell to use, see Lua/Effects/FX_ShellEject for integers
------------------------------------------------------
--	Custom Sounds									--	Setup sounds here!
------------------------------------------------------

SWEP.Sounds = {

	["Primary"] = Sound( "Weapon_M4A1.Single" ) , -- (String) Primary shoot sound
	["PrimarySup"] = Sound( "Weapon_M4A1.Silenced" ) , -- (String) Primary suppressed shoot sound
	["PrimaryDry"] = Sound("VNTCB.SWep.Empty5") , -- (String) Primary dry fire sound
	["Reload"] = Sound( "Weapon_M4A1.Deploy" ) , -- (String) Reload sound

	["Noise_Close"] = Sound( "BF3.BulletCraft.Noise.Rifle.Close" ) , --
	["Noise_Distant"] = Sound( "BF3.BulletCraft.Noise.Forest.Distant" ) , --
	["Noise_Far"] = Sound( "BF3.BulletCraft.Noise.Forest.Far" ) , --
	["CoreBass_Close"] = Sound( "BF3.BulletCraft.CoreBass.Close.LMG_4" ) , --
	["CoreBass_Distant"] = Sound( "BF3.BulletCraft.CoreBass.Distant.LMG_6" ) , --
	["HiFi"] = Sound( "BF3.BulletCraft.HiFi.M4" ) , --
	["Reflection_Close"] = Sound( "BF3.BulletCraft.Reflection.Forest.Close" ) , --
	["Reflection_Far"] = Sound( "BF3.BulletCraft.Reflection.Forest.Far" ) , --

}

SWEP.SelectorSwitchSNDType = 1 -- (Integer) 1=US , 2=RU
SWEP.UsesSuperSonicAmmo = true -- (Boolean) Is the weapon using supersonic or subsonic ammo?
------------------------------------------------------
--	Ironsight & Run Positions						--	Set our model transforms for running and ironsights
------------------------------------------------------
SWEP.IronSightsPos = Vector(-7.75, -6.835, 0.5) -- (Vector) Ironsight XYZ Transform
SWEP.IronSightsAng = Vector(3, -1.4, -3) -- (Vector) Ironsight XYZ Rotation
SWEP.RunArmOffset = Vector(3, -10, -0)
SWEP.RunArmAngle = Vector(0, 70, 0)
------------------------------------------------------
--	Setup Clientside Info							--	This block must be in every weapon!
------------------------------------------------------
if CLIENT then
	SWEP.WepSelectIcon = surface.GetTextureID( "vgui/hud/" .. SWEP.WeaponName )
	SWEP.RenderGroup = RENDERGROUP_BOTH
	language.Add( SWEP.WeaponName , SWEP.PrintName )
	killicon.Add( SWEP.WeaponName , "vgui/entities/" .. SWEP.WeaponName , Color( 255 , 255 , 255 ) )
elseif SERVER then
	resource.AddWorkshop( SWEP.WorkshopID )
end
------------------------------------------------------
--	SWEP:Initialize() 							--	Called when the weapon is first loaded
------------------------------------------------------
function SWEP:Initialize( )
	self.HoldMeRight = VNTCB.HoldType.Rifle -- (String) Hold type table for our weapon, Lua/autorun/sh_v92_base_swep.Lua
end

SWEP.SeqPrimary = { "shoot1_unsil" , "shoot2_unsil" , "shoot3_unsil" }
SWEP.SeqPrimarySupp = { "shoot1" , "shoot2" , "shoot3" }

SWEP.SeqIdle = { "idle_unsil" }
SWEP.SeqIdleSupp = { "idle" }

SWEP.SeqShootLast = { "shootlast_unsil" }
SWEP.SeqShootLastSupp = { "shootlast" }

SWEP.SeqReload = { "reload_unsil" }
SWEP.SeqReloadSupp = { "reload" }

SWEP.SeqDraw = { "draw_unsil" }
SWEP.SeqDrawSupp = { "draw" }

SWEP.SeqSuppOn = { "add_silencer" }
SWEP.SeqSuppOff = { "detach_silencer" }
