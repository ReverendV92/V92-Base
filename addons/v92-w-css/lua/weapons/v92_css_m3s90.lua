
------------------------------------------------------
--	Counter-Strike: Source							--
--	Benelli M3 Super 90								--
--	12-Gauge Pump-Action Shotgun					--
------------------------------------------------------

AddCSLuaFile( )

SWEP.PrintName = "M3 Super 90" -- (String) Printed name on menu

if not VNTCB then
	Error( "V92 Content Bases not mounted; Removing Weapon: " .. SWEP.PrintName .. "\n" )
	return false
elseif IsMounted( "cstrike" ) == false then
	Error( "Counter-Strike: Source not mounted; Removing Weapon: " .. SWEP.PrintName .. "\n" )
	return false
end

SWEP.Base = VNT_BASE_WEAPON_SHELL -- (String) Weapon base parent this is a child of
SWEP.Spawnable = true -- (Boolean) Can be spawned via the menu
SWEP.AdminOnly = false -- (Boolean) Admin only spawnable
------------------------------------------------------
--	Client Information								--	Info used in the client block of the weapon
------------------------------------------------------
SWEP.WeaponName = "v92_css_m3s90" -- (String) Name of the weapon script
SWEP.WeaponEntityName = SWEP.WeaponName .. "_ent" -- (String) Name of the weapon entity in Lua/Entities/Entityname.lua
SWEP.Manufacturer = VNT_WEAPON_MANUFACTURER_BENELLI -- (String) Gun company that makes this weapon
SWEP.CountryOfOrigin = VNT_WEAPON_COUNTRY_ITALY -- (String) Country of origin
SWEP.MagazineName = VNTCB.Magazine.mShotgun -- (String) The name of the magazine the weapon uses - used in my Weapon Magazine System
SWEP.Category = VNT_CATEGORY_CSTRIKESOURCE -- (String) Category
SWEP.Instructions = VNTCB.instructions -- (String) Instruction
SWEP.Author = VNTCB.author -- (String) Author
SWEP.Contact = VNTCB.contact -- (String) Contact
SWEP.Slot = VNT_WEAPON_BUCKETPOS_SHOTGUN -- (Integer) Bucket to place weapon in, 1 to 6
SWEP.SlotPos = VNT_WEAPON_SLOTPOS_SHOTGUN -- (Integer) Bucket position
SWEP.ViewModelFOV = 70 -- (Integer) First-person field of view
SWEP.WorkshopID = "788152557" -- (Integer) Workshop ID number of the upload that contains this file.

------------------------------------------------------
--	Model Information								--
------------------------------------------------------
SWEP.ViewModelFlip = false -- (Boolean) Only used for vanilla CS:S models
SWEP.ViewModel = Model( "models/weapons/cstrike/c_shot_m3super90.mdl" ) -- (String) View model - v_*
SWEP.WorldModel = Model( "models/weapons/w_shot_m3super90.mdl" ) -- (String) World model - w_*
SWEP.HoldType = "shotgun" -- (String) Hold type for our weapon, refer to wiki for animation sets
SWEP.UseHands = true -- (Boolean) Leave at false unless the model uses C_Arms
SWEP.HoldType = "shotgun" -- (String) Hold type for our weapon, refer to wiki for animation sets

------------------------------------------------------
--	Gun Types										--	Set the type of weapon - ONLY PICK ONE!
------------------------------------------------------

SWEP.IsShotgun = true

------------------------------------------------------
--	Shotgun Settings								--	Settings for shotguns
------------------------------------------------------

SWEP.PumpAction = false -- (Boolean) Is the shotgun pump action?

------------------------------------------------------
--	Primary Fire Settings							--	Settings for the primary fire of the weapon
------------------------------------------------------

SWEP.Primary.ClipSize = 8 -- (Integer) Size of a magazine
SWEP.Primary.DefaultClip = 8 -- (Integer) Default number of ammo you spawn with
SWEP.Primary.Ammo = "12gauge" -- (String) Primary ammo used by the weapon, bullets probably
SWEP.Primary.PureDmg = VNTCB.Ammo.a12GBuck[ 1 ] -- (Integer) Base damage, put one number here and the base will do the rest
SWEP.Primary.RPM = 68 -- (Integer) Go to a wikipedia page and look at the RPM of the weapon, then put that here - the base will do the math
SWEP.Primary.Cone = 0.085 -- (Float) 	Accuracy of the weapon; 0.004 is the average for rifles, 0.006 for short-barrel weapons, 0.008 for pistols

------------------------------------------------------
--	Gun Mechanics									--	Various things to tweak the effects and feedback
------------------------------------------------------

SWEP.FireMode = { false , true , false , false } -- (Table: Boolean, Boolean, Boolean, Boolean ) Enable different fire modes on the weapon; Has modes, Has Single, Has Burst, Has Auto - in that order. You can have more than one, but the first must be true
SWEP.CurrentMode = 1 -- (Integer) Current fire mode of the weapon; used to set the default mode; corresponds to the FireMode table
SWEP.Primary.NumShots = 8 -- (Integer) How many bullets per shot; think of this as a shotgun shell, higher integer, more pellets
SWEP.Weight = 5 -- (Integer) The weight in Kilogrammes of our weapon - used in my weapon weight mod!
SWEP.StrongPenetration = VNTCB.Ammo.a12GBuck[ 2 ] -- (Integer) Max penetration
SWEP.WeakPenetration = VNTCB.Ammo.a12GBuck[ 3 ] -- (Integer) Max wood penetration
SWEP.EffectiveRange = 50 -- (Integer) Effective range of the weapon in metres.
SWEP.BlackFriday = true -- (Boolean) Black Friday Doorbuster Sales
SWEP.StoppageRate = 0 -- (Integer) Rate of stoppages in the weapon, look up the real world number estimations and just throw that in here.

------------------------------------------------------
--	Special FX										--	Muzzle flashes, shell casings, etc
------------------------------------------------------

SWEP.MuzzleAttach = 1 -- (Integer) The number of the attachment point for muzzle flashes, typically "1"
SWEP.MuzzleFlashType = 5 -- (Integer) The number of the muzzle flash to use; see Lua/Effects/fx_muzzleflash.Lua
SWEP.ShellAttach = 2 -- (Integer) The number of the attachment point for shell ejections, typically "2"
SWEP.ShellType = nil -- (Integer) The shell to use, see Lua/Effects/FX_ShellEject for integers

------------------------------------------------------
--	Custom Sounds									--	Setup sounds here!
------------------------------------------------------

SWEP.Sounds = {

	["Primary"] = Sound( "Weapon_M3.Single" ) , -- (String) Primary shoot sound
	["PrimaryDry"] = Sound("VNTCB.SWep.Empty6") , -- (String) Primary dry fire sound
	["Reload"] = Sound( "VNTCB.SWep.Draw1" ) , -- (String) Reload sound

	["Noise_Close"] = Sound( "BF3.BulletCraft.Noise.Shotgun.Close" ) , --
	["Noise_Distant"] = Sound( "BF3.BulletCraft.Noise.Forest.Distant" ) , --
	["Noise_Far"] = Sound( "BF3.BulletCraft.Noise.Forest.Far" ) , --
	["CoreBass_Close"] = Sound( "BF3.BulletCraft.CoreBass.Close.OneShot_2" ) , --
	["CoreBass_Distant"] = Sound( "BF3.BulletCraft.CoreBass.Distant.OneShot_1" ) , --
	["HiFi"] = Sound( "BF3.BulletCraft.HiFi.Shotgun" ) , --
	["Reflection_Close"] = Sound( "BF3.BulletCraft.Reflection.Forest.Close" ) , --
	["Reflection_Far"] = Sound( "BF3.BulletCraft.Reflection.Forest.Far" ) , --

}

SWEP.SelectorSwitchSNDType = 1 -- (Integer) 1=US , 2=RU
SWEP.UsesSuperSonicAmmo = true -- (Boolean) Is the weapon using supersonic or subsonic ammo?
------------------------------------------------------
--	Ironsight & Run Positions						--	Set our model transforms for running and ironsights
------------------------------------------------------
SWEP.IronSightsPos = Vector(-7.66, -10, 3.52)
SWEP.IronSightsAng = Vector(0, 0, 0)
SWEP.RunArmOffset = Vector(2, -16, -2)
SWEP.RunArmAngle = Vector(9, 70, 0)
------------------------------------------------------
--	Setup Clientside Info							--	This block must be in every weapon!
------------------------------------------------------
if CLIENT then
	SWEP.CSMuzzleFlashes = true
	SWEP.WepSelectIcon = surface.GetTextureID( "vgui/hud/" .. SWEP.WeaponName )
	SWEP.RenderGroup = RENDERGROUP_BOTH
	language.Add( SWEP.WeaponName , SWEP.PrintName )
	killicon.Add( SWEP.WeaponName , "vgui/entities/" .. SWEP.WeaponName , Color( 255 , 255 , 255 ) )
elseif SERVER then
	resource.AddWorkshop( SWEP.WorkshopID )
end
------------------------------------------------------
--	SWEP:Initialize() 							--	Called when the weapon is first loaded
------------------------------------------------------
function SWEP:Initialize( )
	self.HoldMeRight = VNTCB.HoldType.Shotgun -- (String) Hold type table for our weapon, Lua/autorun/sh_v92_base_swep.Lua
end

SWEP.SeqPrimary = { "shoot1" , "shoot2" }
SWEP.SeqReloadStart = { "start_reload" }
SWEP.SeqReloadLoop = { "insert" }
SWEP.SeqReloadEnd = { "after_reload" }
SWEP.SeqDraw = { "draw" }
SWEP.SeqIdle = { "idle" }
