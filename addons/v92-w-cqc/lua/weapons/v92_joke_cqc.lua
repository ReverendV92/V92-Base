
------------------------------------------------------
--	V92 Joke Weapons								--
--	EgoRaptor CQC! CQC! Weapon						--
------------------------------------------------------

AddCSLuaFile( )

SWEP.PrintName = "CQC!" -- (String) Printed name on menu

if not VNTCB then
	Error( "V92 Content Bases not mounted; Removing Weapon: " .. self.PrintName .. "\n" )
	return false
end

SWEP.Base = VNT_BASE_WEAPON_MELEE -- (String) Weapon base parent this is a child of
SWEP.Spawnable = true -- (Boolean) Can be spawned via the menu
SWEP.AdminOnly = false -- (Boolean) Admin only spawnable

------------------------------------------------------
--	Client Information								
--	Info used in the client block of the weapon
------------------------------------------------------

SWEP.WeaponName = "v92_joke_cqc" -- (String) Name of the weapon script
SWEP.WeaponEntityName = SWEP.WeaponName .. "_ent" -- (String) Name of the weapon entity in Lua/Entities/Entityname.lua
SWEP.Manufacturer = "The Boss" -- (String) Gun company that makes this weapon
SWEP.CountryOfOrigin = VNT_WEAPON_COUNTRY_VARIOUS -- (String) Country of origin
SWEP.Category = VNT_CATEGORY_VNT -- (String) Category
SWEP.Instructions = VNTCB.instructions -- (String) Instruction
SWEP.Author = VNTCB.author -- (String) Author
SWEP.Contact = VNTCB.contact -- (String) Contact
SWEP.Purpose = VNTCB.purpose -- (String) Purpose
SWEP.Slot = VNT_WEAPON_BUCKETPOS_MELEE -- (Integer) Bucket to place weapon in, 1 to 6
SWEP.SlotPos = VNT_WEAPON_SLOTPOS_MELEE -- (Integer) Bucket position
SWEP.WorkshopID = "818750632" -- (Integer) Workshop ID number of the upload that contains this file.

------------------------------------------------------
--	Model Information								
--
------------------------------------------------------

SWEP.ViewModelFlip = false -- (Boolean) Only used for vanilla CS:S models
SWEP.ViewModel = Model( "models/jessev92/weapons/fists_c.mdl" ) -- (String) View model - v_*
SWEP.WorldModel = Model( "" ) -- (String) World model - w_*
SWEP.HoldType = "knife" -- (String) Hold type for our weapon, refer to wiki for animation sets
SWEP.UseHands = true -- (Boolean) Leave at false unless the model uses C_Arms

------------------------------------------------------
--	Gun Types										
--	Set the type of weapon
------------------------------------------------------

------------------------------------------------------
--	Primary Fire Settings							
--	Settings for the primary fire of the weapon
------------------------------------------------------

SWEP.Primary.ClipSize = -1 -- (Integer) Size of a magazine
SWEP.Primary.DefaultClip = 0 -- (Integer) Default number of ammo you spawn with
SWEP.Primary.Ammo = "none" -- (String) Primary ammo used by the weapon, bullets probably
SWEP.Primary.RPM = 500 -- (Integer) Go to a wikipedia page and look at the RPM of the weapon, then put that here - the base will do the math
SWEP.Primary.PureDmg = 25 -- (Integer) Base damage, put one number here and the base will do the rest
SWEP.Primary.Force = 2 -- (Integer) Amount of force for the bullet to emit on impact

------------------------------------------------------
--	Secondary Fire Settings							
--	Settings for the alt-fire; if it has none, leave this be
------------------------------------------------------

SWEP.Secondary.ClipSize = -1 -- (Integer) Size of a secondary magazine; if no alt-fire, set to -1
SWEP.Secondary.DefaultClip = 0 -- (Integer) Default number of projectiles in the alt fire mag; if none, set to -1
SWEP.Secondary.Ammo = "none" -- (String) Primary ammo used by the weapon, bullets probably
SWEP.Secondary.RPM = 45 -- (Integer) Go to a wikipxedia page and look at the RPM of the weapon, then put that here - the base will do the math
SWEP.Secondary.PureDmg = 50 -- (Integer) Base damage, put one number here and the base will do the rest

------------------------------------------------------
--	Gun Mechanics									
--	Various things to tweak the effects and feedback
------------------------------------------------------

SWEP.Weight = 1 -- (Integer) The weight in Kilogrammes of our weapon - used in my weapon weight mod!

------------------------------------------------------
--	Melee Settings									
--
------------------------------------------------------

SWEP.MeleeAnimType = 3 -- (Integer) Melee type; 0=holdtype animation, 1=pistol whip, 2=rifle butt
SWEP.MeleeRange = 45 -- (Integer) Range of melee weapon swings
SWEP.HasMeleeAttack = false -- (Boolean) Does this weapon have a pistol whip or rifle butt animation?
SWEP.AltFireMelee = true -- (Boolean) Is the alt fire a melee attack?
SWEP.IsBladedMelee = false -- (Boolean) Is the melee a blade?

------------------------------------------------------
--	Custom Sounds									
--	Setup sounds here!
------------------------------------------------------

SWEP.Sounds = {

	["Primary"] = Sound( "VNTCB.CQC.VO" ) , -- (String) Primary shoot sound
	["PistolWhipHit"] = Sound( "VNTCB.CQC.VO" ) , -- (String) Sound for pistol whip hits
	["PistolWhipMiss"] = Sound( "VNTCB.CQC.VO" ) , -- (String) Sound for pistol whip misses
	["MeleeHitWall"] = Sound( "VNTCB.CQC.VO" ) , -- (String) Sound for melee world hits
	["MeleeHitCharacter"] = Sound( "VNTCB.CQC.VO" ) , -- (String) Sound for melee flesh hits
	["MeleeMiss"] = Sound( "VNTCB.CQC.VO" ) , -- (String) Sound for melee misses

}

------------------------------------------------------
--	Ironsight & Run Positions						
--	Set our model transforms for running and ironsights
------------------------------------------------------

SWEP.IronSightsPos = Vector( 0 , 0 , 0 ) -- (Vector) Ironsight XYZ Transform
SWEP.IronSightsAng = Vector( 0 , 0 , 0 ) -- (Vector) Ironsight XYZ Rotation
SWEP.RunArmOffset = Vector( -10 , -14 , -1.5 ) -- (Vector) Sprinting XYZ Transform
SWEP.RunArmAngle = Vector( -25 , 0 , -50 ) -- (Vector) Sprinting XYZ Rotation

------------------------------------------------------
--	Setup Clientside Info							
--	This block must be in every weapon!
------------------------------------------------------

if CLIENT then
	SWEP.WepSelectIcon = surface.GetTextureID( "vgui/hud/" .. SWEP.WeaponName )
	SWEP.RenderGroup = RENDERGROUP_BOTH
	language.Add( SWEP.WeaponName , SWEP.PrintName )
	killicon.Add( SWEP.WeaponName , "vgui/entities/" .. SWEP.WeaponName , Color( 255 , 255 , 255 ) )
elseif SERVER then
	resource.AddWorkshop( SWEP.WorkshopID )
end --	Setup Clientside Info - This block must be in every weapon!

------------------------------------------------------
--	SWEP:Initialize() 							
--	Called when the weapon is first loaded
------------------------------------------------------

function SWEP:Initialize( )
	self.HoldMeRight = VNTCB.HoldType.Knife -- (String) Hold type table for our weapon, Lua/autorun/sh_v92_base_swep.Lua
end
