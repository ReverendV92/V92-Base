
---------------------------------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------------------------
--	CQC! CQC! CQC! CQC!
---------------------------------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------------------------

--	Catch-22 Diabolis Ex Machina Prevention

if SERVER then
	resource.AddWorkshop("505106454") 	--	V92 Code Bases
end

-- SOUND GUIDE
----------
-- Channel = Typically CHAN_WEAPON or CHAN_ITEM for gunshots and gun parts respectively; CHAN_BODY for foley
-- Volume = Leave at 1.0
-- Pitch = Typically between 95 and 105 for gunshots
-- Level = For gunshots, follow this estimation table as a LOOSE guideline:
----------
--	.22LR = 40dB
--	9x19mm = 60dB
--	.38 Special = 70dB
--	5.56x45mm = 70dB
--	7.62x51mm = 90dB
--	.45 ACP = 90dB
--	.50 AE = 95dB
--	.50 BMG = 100dB
--	12-Gauge Buckshot = 120dB
--	Explosion = 200dB
--	Suppressed = 2/3 of normal
--	Empty = 60dB 
--	Weapon sounds = ~50dB (use your head)
--	Foley = ~30dB (use your head)
----------
----------

if not VNTCB then
	Error( "V92 Content Bases not mounted :: Removing Autorun File :: CQC\n" )

	return false
end

if VNTCB.Plugins then
	table.insert( VNTCB.Plugins, 0, "VNT :: CQC" )
end

------------------------------------------------------------------------------------------
--	CQC! CQC! CQC! CQC!
------------------------------------------------------------------------------------------

---------------------------------------------
--	CQC
---------------------------------------------
sound.Add( {
	name = "VNTCB.CQC.VO" ,
	channel = CHAN_VOICE ,
	volume = 1.0 ,
	level = 60 ,
	pitch = { 90 , 110 } ,
	sound = { "jessev92/vo/cqc/cqc1.wav","jessev92/vo/cqc/cqc2.wav","jessev92/vo/cqc/cqc3.wav","jessev92/vo/cqc/cqc4.wav" }
} )
util.PrecacheSound( "jessev92/vo/cqc/cqc1.wav" )
util.PrecacheSound( "jessev92/vo/cqc/cqc2.wav" )
util.PrecacheSound( "jessev92/vo/cqc/cqc3.wav" )
util.PrecacheSound( "jessev92/vo/cqc/cqc4.wav" )
