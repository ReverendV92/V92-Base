
AddCSLuaFile()

function EFFECT:Init( data )
	self.Fancy = !tobool(LocalPlayer():GetInfo("vnt_cl_vehicle_esmokeeffect"))
	if self.Fancy then return end
	local Ent = data:GetEntity()
	local Pos = data:GetOrigin()
	local Plane = Ent.IsHVAPPlane
	if !Ent:IsValid() then return end	
	local Col = data:GetColor()
	local Scale = data:GetScale()
	local Vel = Ent:GetVelocity()	
	local Vell = Vel:Length()/512
	local rpm = data:GetRadius()
	
	local emitter = ParticleEmitter( Pos )
	emitter:SetNearClip( 8, 320 )

	local particle = emitter:Add( "particle/smokesprites_000"..math.random(1,9), Pos+VectorRand()*math.random(-8,8))

	if (particle) then
		particle:SetVelocity(Vel/2-(Ent:GetForward()*rpm*5+Ent:GetForward()*0.12)*(Vell+160))
		particle:SetLifeTime(0) 
		particle:SetDieTime(2.56+Scale) 
		particle:SetStartAlpha( math.Clamp(70*Scale+(Plane and 128 or 32), 0,255) )
		particle:SetEndAlpha(0)
		particle:SetStartSize(32*Scale+(Plane and 70 or 50))
		particle:SetEndSize(42*-Scale+(Plane and 400 or 220))
		particle:SetAngles( Angle(0,0,0) )
		particle:SetAngleVelocity( Angle(0,math.random(-2,2),0) ) 
		particle:SetRoll(math.Rand( 0, 360 ))
		particle:SetColor(Col, Col, Col, 255)
		particle:SetAirResistance(16)
		particle:SetCollide(true)
		particle:SetBounce(0.256)
	end
	
	emitter:Finish()
	
end

function EFFECT:Think()		
	return false
end

function EFFECT:Render()

end
