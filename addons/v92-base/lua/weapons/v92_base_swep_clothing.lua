
AddCSLuaFile( )

------------------------------------------------------
--	Prevent this file from loading if for some odd reason the base Lua isn't loaded
if not VNTCB then return false end 

------------------------------------------------------
--	Spawn settings									
--	Can we spawn this?
------------------------------------------------------

SWEP.Spawnable = false -- (Boolean) Can be spawned via the menu
SWEP.AdminOnly = false -- (Boolean) Admin only spawnable
SWEP.Base = VNT_BASE_WEAPON -- (Weapon) Base to derive from

------------------------------------------------------
--	Client Information								
--	Info used in the client block of the weapon
------------------------------------------------------

SWEP.WeaponName = "v92_base_swep_clothing" -- (String) Name of the weapon script
SWEP.WeaponEntityName = SWEP.WeaponName .. "_ent" -- (String) Name of the weapon entity in Lua/Entities/Entityname.lua
SWEP.PrintName = "V92 Clothing Base" -- (String) Printed name on menu
SWEP.Category = VNT_CATEGORY_VNT -- (String) Category
SWEP.Instructions = "RELOAD: Item On/Off"	
SWEP.Author = VNTCB.author -- (String) Author
SWEP.Contact = VNTCB.contact -- (String) Contact
SWEP.Purpose = VNTCB.purpose -- (String) Purpose
SWEP.Slot = VNT_WEAPON_BUCKETPOS_CLOTHING -- (Integer) Bucket to place weapon in, 1 to 6
SWEP.SlotPos = VNT_WEAPON_SLOTPOS_CLOTHING -- (Integer) Bucket position
SWEP.WorkshopID = "505106454" -- (Integer) Workshop ID number of the upload that contains this file.

------------------------------------------------------
--	Model Information								
--	Model settings and infomation
------------------------------------------------------

SWEP.ViewModel = Model("models/jessev92/weapons/mask_c.mdl") -- (String) View model - v_*
SWEP.WorldModel = "" -- (String) World model - w_*
SWEP.UseHands = true -- (Boolean) Leave at false unless the model uses C_Arms

------------------------------------------------------
--	Primary Fire Settings							--	Settings for the primary fire of the weapon
------------------------------------------------------

SWEP.Primary.ClipSize = -1 -- (Integer) Size of a magazine
SWEP.Primary.DefaultClip = -1 -- (Integer) Default number of ammo you spawn with
SWEP.Primary.Ammo = "none" -- (String) Primary ammo used by the weapon, bullets probably

------------------------------------------------------
--	Gun Mechanics									--	Various things to tweak the effects and feedback
------------------------------------------------------

SWEP.Weight = 1 -- (Integer) The weight in Kilogrammes of our weapon - used in my weapon weight mod!

------------------------------------------------------
--	Mask up when you're ready to start the job!
------------------------------------------------------

-- SWEP.Protections = { DMG_PARALYZE , DMG_NERVEGAS , DMG_POISON } -- (Table: Damage Types) Damage types to protect from; See: http://wiki.garrysmod.com/page/Enums/DMG

SWEP.ItemGearSlot = GEAR_SLOT_OTHER -- (String: Body Part Slot) 
SWEP.ItemModel = Model( "models/nova/w_headgear.mdl" )
SWEP.ItemSkin = 0 -- (Integer) skin number
SWEP.ItemMaterial = nil -- (Material) Use this to override the material on the item
SWEP.ItemColor = Color( 255 , 255 , 255 , 255 ) -- (Vector:Color) Color of the entity
SWEP.ItemBGZero = 0 -- (Integer) Bodygroup Zero Subgroup Setting
SWEP.ItemBGOne = 0 -- (Integer) Bodygroup One Subgroup Setting
SWEP.ItemBGTwo = 0 -- (Integer) Bodygroup Two Subgroup Setting
SWEP.ItemBGThree = 0 -- (Integer) Bodygroup Three Subgroup Setting
SWEP.ItemBGFour = 0 -- (Integer) Bodygroup Four Subgroup Setting
SWEP.ItemBGFive = 0 -- (Integer) Bodygroup Five Subgroup Setting

-- Fallback settings are in case this is a whole-body replacement like a STALKER body suit, and if you remove it you want the player to still have a body
-- Use this to reset the player to another model if you don't want floating heads

SWEP.UseFallback = false -- (Boolean) If this is something like a whole-body suit, make this true
SWEP.FallbackModel = ""
SWEP.FallbackSkin = 0
SWEP.FallbackMaterial = nil -- (Material) Use this to override the material on the item
SWEP.FallbackColor = Color( 255 , 255 , 255 , 255 )
SWEP.FallbackBGZero = 0
SWEP.FallbackBGOne = 0
SWEP.FallbackBGTwo = 0
SWEP.FallbackBGThree = 0
SWEP.FallbackBGFour = 0
SWEP.FallbackBGFive = 0

SWEP.Sounds = {
	["ItemOn"] = Sound( "common/null.wav" ) ,
	["ItemOff"] = Sound( "common/null.wav" ) ,
	["ItemFoleyOn"] = Sound( "common/null.wav" ) ,
	["ItemFoleyOff"] = Sound( "common/null.wav" ) ,
}

------------------------------------------------------
--	Setup Clientside Info							
--	This block must be in every weapon!
------------------------------------------------------

if CLIENT then

	SWEP.WepSelectIcon = surface.GetTextureID( "vgui/hud/" .. SWEP.WeaponName )
	SWEP.RenderGroup = RENDERGROUP_BOTH
	language.Add( SWEP.WeaponName , SWEP.PrintName )
	killicon.Add( SWEP.WeaponName , "vgui/entities/" .. SWEP.WeaponName , Color( 255 , 255 , 255 ) )
	
elseif SERVER then

	resource.AddWorkshop( SWEP.WorkshopID )
	
end

------------------------------------------------------
--	SWEP:Initialize() 							
--	Called when the weapon is first loaded
------------------------------------------------------

function SWEP:Initialize( )

	self.HoldMeRight = VNTCB.HoldType.Normal -- (String) Hold type table for our weapon, Lua/autorun/sh_v92_base_ENT.Lua

end

local ActionDelay = CurTime()
local AnimDelay = CurTime()
local ItemGearSlotTaken = {}
local ItemMDL = nil
local FallbackMDL = nil

function SWEP:GrabEarAnim()

	if IsValid( self ) and IsValid( self.Owner ) then

		local vm = self.Owner:GetViewModel()

		AnimDelay = CurTime() + 1.5
		self.Owner.ChatGestureWeight = self.Owner.ChatGestureWeight or 0

		if ( AnimDelay > CurTime() ) then
			self.Owner.ChatGestureWeight = math.Approach( self.Owner.ChatGestureWeight, 1, 1 )
		else
			self.Owner.ChatGestureWeight = math.Approach( self.Owner.ChatGestureWeight, 0, 1 )
		end

		if ( self.Owner.ChatGestureWeight > 0 ) then
		
			self.Owner:AnimRestartGesture( GESTURE_SLOT_VCD, ACT_GMOD_IN_CHAT, true )
			self.Owner:AnimSetGestureWeight( GESTURE_SLOT_VCD, self.Owner.ChatGestureWeight )
		
		end

	end

end

function SWEP:PutItemOn( )

	if IsValid( self ) and IsValid( self.Owner ) then

		if ItemGearSlotTaken[1] == true and ItemGearSlotTaken[2] == self.ItemGearSlot then

			if SERVER then

				self:EmitSound( "vo/npc/male01/ohno.wav" )

				self:TakeItemOff( "swap" )

			end

			return false

		else

			if SERVER then

				ItemMDL = ents.Create( "prop_physics" )
				ItemMDL:SetModel( self.ItemModel or Model( "models/nova/w_headgear.mdl" ) )
				ItemMDL:SetSkin( self.ItemSkin or 0 )

				if self.ItemMaterial != nil then

					ItemMDL:SetMaterial( self.ItemMaterial )

				end

				ItemMDL:SetColor( self.ItemColor or Color( 255 , 255 , 255 , 255 ) )
				ItemMDL:SetBodygroup( 0 , self.ItemBGZero or 0 )
				ItemMDL:SetBodygroup( 1 , self.ItemBGOne or 0 )
				ItemMDL:SetBodygroup( 2 , self.ItemBGTwo or 0 )
				ItemMDL:SetBodygroup( 3 , self.ItemBGThree or 0 )
				ItemMDL:SetBodygroup( 4 , self.ItemBGFour or 0 )
				ItemMDL:SetBodygroup( 5 , self.ItemBGFive or 0 )
				ItemMDL:AddEffects( EF_BONEMERGE )
				ItemMDL:SetParent( self.Owner )

				self.Owner:EmitSound( self.Sounds.ItemOn )
				self.Owner:EmitSound( self.Sounds.ItemFoleyOn )
				self.Owner:SetNWBool( self.ItemGearSlot , true )

			end

			if CLIENT then

				local vm = self.Owner:GetViewModel()
				vm:SendViewModelMatchingSequence( vm:LookupSequence( "on" ) )

				self.Owner:ViewPunch( Angle( 1 , 0 , 0 ) )

			end

			ItemGearSlotTaken = { true , self.ItemGearSlot }
			self:GrabEarAnim( )

		end

	end

end

function SWEP:TakeItemOff( newItem )

	if IsValid( self ) and IsValid( self.Owner ) then

		if ItemGearSlotTaken[1] == false then

			if SERVER then

				self:EmitSound( "vo/npc/male01/ohno.wav" )

			end

			return false

		else

			if SERVER then

				SafeRemoveEntity( ItemMDL )

				self.Owner:EmitSound( self.Sounds.ItemOff )
				self.Owner:EmitSound( self.Sounds.ItemFoleyOff )
				self.Owner:SetNWBool( self.ItemGearSlot , false )

				if self.UseFallback then

					FallbackMDL = ents.Create( "prop_physics" )
					FallbackMDL:SetModel( self.FallbackModel or Model( "models/player/group01/male_01.mdl" ) )
					FallbackMDL:SetSkin( self.FallbackSkin or 0 ) 

					if self.FallbackMaterial != nil then

						FallbackMDL:SetMaterial( self.FallbackMaterial )

					end

					FallbackMDL:SetColor( self.FallbackColor or Color( 255 , 255 , 255 , 255 ) )
					FallbackMDL:SetBodygroup( 0 , self.FallbackBGZero or 0 )
					FallbackMDL:SetBodygroup( 1 , self.FallbackBGOne or 0 )
					FallbackMDL:SetBodygroup( 2 , self.FallbackBGTwo or 0 )
					FallbackMDL:SetBodygroup( 3 , self.FallbackBGThree or 0 )
					FallbackMDL:SetBodygroup( 4 , self.FallbackBGFour or 0 )
					FallbackMDL:SetBodygroup( 5 , self.FallbackBGFive or 0 )
					FallbackMDL:AddEffects( EF_BONEMERGE )
					FallbackMDL:SetParent( self.Owner )

				end

			end

			if CLIENT then

				local vm = self.Owner:GetViewModel()
				vm:SendViewModelMatchingSequence( vm:LookupSequence( "off" ) )

				self.Owner:ViewPunch( Angle( 1 , 0 , 0 ) )

			end

			ItemGearSlotTaken = { false , self.ItemGearSlot }
			self:GrabEarAnim( )
			
			local CycleItem = newItem
			
			if CycleItem == "swap" then

				self:PutItemOn( )
				
				print( "cycling..." )

			end

		end

	end

end

function SWEP:Reload( )

	if IsValid( self ) and IsValid( self.Owner ) then

		if ActionDelay <= CurTime( ) then

			if self.Owner:GetNWBool( self.ItemGearSlot ) == false then

				self:PutItemOn( )

			elseif self.Owner:GetNWBool( self.ItemGearSlot ) == true then

				self:TakeItemOff( )

			end

			ActionDelay = CurTime() + 3

		end
		
	end

end

function SWEP:PrimaryAttack( )

	return false

end

function SWEP:SecondaryAttack( )

	if IsValid( self ) and IsValid( self.Owner ) then
	
		if self.Owner:KeyDown( IN_USE ) then

			if IsValid( ItemMDL ) then

				ItemMDL:Remove( )

			end

			if IsValid( FallbackMDL ) then

				FallbackMDL:Remove( )

			end

		end
	
	end

	return false

end

function SWEP:OnDrop( )

	if IsValid( ItemMDL ) then

		ItemMDL:Remove( )

	end

	if IsValid( FallbackMDL ) then

		FallbackMDL:Remove( )

	end

end

function SWEP:CustomThink( )

	if not IsValid( ItemMDL ) then

		SafeRemoveEntity( ItemMDL )

	end

	if not IsValid( FallbackMDL ) then

		SafeRemoveEntity( FallbackMDL )

	end

	return false

end