
AddCSLuaFile()

-------------------------------------------------------
--	V92 Base Weapon Audio
--	Encoded by V92
--	Profile Link:	http://steamcommunity.com/id/JesseVanover/
--	Workshop Link:	http://steamcommunity.com/sharedfiles/filedetails/?id=
-------------------------------------------------------

---------------------------------------------------------------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------------------------------------------------------
--	V92 BASE
---------------------------------------------------------------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------------------------------------------------------

--------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------
--	ITEMS
--------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------

sound.Add( {
	["name"] = "VNTCB.Items.Zipper" ,
	["channel"] = CHAN_ITEM ,
	["level"] = 60 ,
	["volume"] = 1.0 ,
	["pitch"] = 85 ,
	["sound"] = { 
		"^)jessev92/items/zipper1.wav"
	}
} )
util.PrecacheSound( "jessev92/items/zipper1.wav" )

---------------------------------------------
---------------------------------------------
--	GAS MASK
---------------------------------------------
---------------------------------------------

sound.Add( { ["name"] = "VNTCB.Items.GasMask.On" ,
	["channel"] = CHAN_ITEM ,
	["level"] = 75 ,
	["volume"] = 1.0 ,
	["pitch"] = { 95 , 105 } ,
	["sound"] = { 
		"jessev92/items/gas_mask_on.wav" ,
	} ,
} )

sound.Add( { ["name"] = "VNTCB.Items.GasMask.Off" ,
	["channel"] = CHAN_ITEM ,
	["level"] = 75 ,
	["volume"] = 1.0 ,
	["pitch"] = { 95 , 105 } ,
	["sound"] = { 
		"jessev92/items/gas_mask_off.wav" ,
	} ,
} )

sound.Add( { ["name"] = "VNTCB.Items.GasMask.FoleyOn" ,
	["channel"] = CHAN_BODY ,
	["level"] = 75 ,
	["volume"] = 1.0 ,
	["pitch"] = { 95 , 105 } ,
	["sound"] = { 
		"jessev92/weapons/univ/iron_in3.wav" ,
	} ,
} )

sound.Add( { ["name"] = "VNTCB.Items.GasMask.FoleyOff" ,
	["channel"] = CHAN_BODY ,
	["level"] = 75 ,
	["volume"] = 1.0 ,
	["pitch"] = { 95 , 105 } ,
	["sound"] = { 
		"jessev92/weapons/univ/iron_out3.wav" ,
	} ,
} )

--------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------
--	WEAPONS
--------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------

-------------------------------------------------------
-------------------------------------------------------
--	UNIVERSAL
-------------------------------------------------------
-------------------------------------------------------

---------------------------------------------
---------------------------------------------
--	FIRE MODES
---------------------------------------------
---------------------------------------------

sound.Add( {
	name = "VNTCB.SWep.Semi" ,
	channel = CHAN_ITEM ,
	level = 60 ,
	volume = 1.0 ,
	pitch = 85 ,
	sound = { 
		"^)jessev92/weapons/univ/auto_semiauto_switch.wav"
	}
} )
util.PrecacheSound( "jessev92/weapons/univ/auto_semiauto_switch.wav" )

sound.Add( {
	name = "VNTCB.SWep.Semi" ,
	channel = CHAN_ITEM ,
	level = 60 ,
	volume = 1.0 ,
	pitch = 85 ,
	sound = { "^)jessev92/weapons/univ/auto_semiauto_switch.wav" }
} )
util.PrecacheSound( "jessev92/weapons/univ/auto_semiauto_switch.wav" )

sound.Add( {
	name = "VNTCB.SWep.Burst" ,
	channel = CHAN_ITEM ,
	level = 60 ,
	volume = 1.0 ,
	pitch = 100 ,
	sound = { "^)jessev92/weapons/univ/auto_semiauto_switch.wav" }
} )
util.PrecacheSound( "jessev92/weapons/univ/auto_semiauto_switch.wav" )

sound.Add( {
	name = "VNTCB.SWep.Auto" ,
	channel = CHAN_ITEM ,
	level = 60 ,
	volume = 1.0 ,
	pitch = 115 ,
	sound = { "^)jessev92/weapons/univ/auto_semiauto_switch.wav" }
} )
util.PrecacheSound( "jessev92/weapons/univ/auto_semiauto_switch.wav" )

sound.Add( {
	name = "VNTCB.SWep.Safety" ,
	channel = CHAN_ITEM ,
	level = 60 ,
	volume = 1.0 ,
	pitch = 100 ,
	sound = { "^)jessev92/weapons/univ/auto_semiauto_switch.wav" }
} )
util.PrecacheSound( "jessev92/weapons/univ/auto_semiauto_switch.wav" )

---------------------------------------------
---------------------------------------------
--	EMPTY
---------------------------------------------
---------------------------------------------

sound.Add( {
	name = "VNTCB.SWep.Empty1" ,
	channel = CHAN_ITEM ,
	level = 75 ,
	volume = 1.0 ,
	pitch = { 90 , 110 } ,
	sound = { "^)jessev92/weapons/univ/empty1.wav" }
} )
util.PrecacheSound( "jessev92/weapons/univ/empty1.wav" )

sound.Add( {
	name = "VNTCB.SWep.Empty2" ,
	channel = CHAN_ITEM ,
	level = 75 ,
	volume = 1.0 ,
	pitch = { 90 , 110 } ,
	sound = { "^)jessev92/weapons/univ/empty2.wav" }
} )
util.PrecacheSound( "jessev92/weapons/univ/empty2.wav" )

sound.Add( {
	name = "VNTCB.SWep.Empty3" ,
	channel = CHAN_ITEM ,
	level = 75 ,
	volume = 1.0 ,
	pitch = { 90 , 110 } ,
	sound = { "^)jessev92/weapons/univ/Empty3.wav" }
} )
util.PrecacheSound( "jessev92/weapons/univ/empty3.wav" )

sound.Add( {
	name = "VNTCB.SWep.Empty4" ,
	channel = CHAN_ITEM ,
	level = 75 ,
	volume = 1.0 ,
	pitch = { 90 , 110 } ,
	sound = { "^)jessev92/weapons/univ/Empty4.wav" }
} )
util.PrecacheSound( "jessev92/weapons/univ/empty4.wav" )

sound.Add( {
	name = "VNTCB.SWep.Empty5" ,
	channel = CHAN_ITEM ,
	level = 75 ,
	volume = 1.0 ,
	pitch = { 90 , 110 } ,
	sound = { "^)jessev92/weapons/univ/Empty5.wav" }
} )
util.PrecacheSound( "jessev92/weapons/univ/empty5.wav" )

sound.Add( {
	name = "VNTCB.SWep.Empty6" ,
	channel = CHAN_ITEM ,
	level = 75 ,
	volume = 1.0 ,
	pitch = { 90 , 110 } ,
	sound = { "^)jessev92/weapons/univ/Empty6.wav" }
} )
util.PrecacheSound( "jessev92/weapons/univ/empty6.wav" )

---------------------------------------------
---------------------------------------------
--	FOLEY
---------------------------------------------
---------------------------------------------

sound.Add( {
	name = "VNTCB.SWep.Grab1" ,
	channel = CHAN_BODY ,
	level = 75 ,
	volume = 1.0 ,
	pitch = { 90 , 110 } ,
	sound = { "^)jessev92/weapons/univ/grab1.wav" }
} )
util.PrecacheSound( "jessev92/weapons/univ/grab1.wav" )

sound.Add( {
	name = "VNTCB.SWep.Throw1" ,
	channel = CHAN_BODY ,
	level = 75 ,
	volume = 1.0 ,
	pitch = { 90 , 110 } ,
	sound = { "^)jessev92/weapons/univ/throw1.wav" }
} )
util.PrecacheSound( "jessev92/weapons/univ/throw1.wav" )

---------------------------------------------
---------------------------------------------
--	STEEL SIGHTS
---------------------------------------------
---------------------------------------------

sound.Add( {
	name = "VNTCB.SWep.IronIn1" ,
	channel = CHAN_BODY ,
	level = 75 ,
	volume = 1.0 ,
	pitch = { 95 , 105 } ,
	sound = "jessev92/weapons/univ/iron_in1.wav"
} )
util.PrecacheSound( "jessev92/weapons/univ/iron_in1.wav" )

sound.Add( {
	name = "VNTCB.SWep.IronOut1" ,
	channel = CHAN_BODY ,
	level = 75 ,
	volume = 1.0 ,
	pitch = { 95 , 105 } ,
	sound = "jessev92/weapons/univ/iron_out1.wav"
} )
util.PrecacheSound( "jessev92/weapons/univ/iron_out1.wav" )

sound.Add( {
	name = "VNTCB.SWep.IronIn2" ,
	channel = CHAN_BODY ,
	level = 75 ,
	volume = 1.0 ,
	pitch = { 95 , 105 } ,
	sound = "jessev92/weapons/univ/iron_in2.wav"
} )
util.PrecacheSound( "jessev92/weapons/univ/iron_in2.wav" )

sound.Add( {
	name = "VNTCB.SWep.IronOut2" ,
	channel = CHAN_BODY ,
	level = 75 ,
	volume = 1.0 ,
	pitch = { 95 , 105 } ,
	sound = "jessev92/weapons/univ/iron_out2.wav"
} )
util.PrecacheSound( "jessev92/weapons/univ/iron_out2.wav" )

sound.Add( {
	name = "VNTCB.SWep.IronIn3" ,
	channel = CHAN_BODY ,
	level = 75 ,
	volume = 1.0 ,
	pitch = { 95 , 105 } ,
	sound = "jessev92/weapons/univ/iron_in3.wav"
} )
util.PrecacheSound( "jessev92/weapons/univ/iron_in3.wav" )

sound.Add( {
	name = "VNTCB.SWep.IronOut3" ,
	channel = CHAN_BODY ,
	level = 75 ,
	volume = 1.0 ,
	pitch = { 95 , 105 } ,
	sound = "jessev92/weapons/univ/iron_out3.wav"
} )
util.PrecacheSound( "jessev92/weapons/univ/iron_out3.wav" )

sound.Add( {
	name = "VNTCB.SWep.ZoomIn" ,
	channel = CHAN_ITEM ,
	level = 75 ,
	volume = 1.0 ,
	pitch = { 90 , 110 } ,
	sound = { "jessev92/weapons/univ/iron_in2.wav" }
} )
util.PrecacheSound( "jessev92/weapons/univ/iron_in2.wav" )

sound.Add( {
	name = "VNTCB.SWep.ZoomOut" ,
	channel = CHAN_ITEM ,
	level = 75 ,
	volume = 1.0 ,
	pitch = { 90 , 110 } ,
	sound = { "jessev92/weapons/univ/iron_out2.wav" }
} )
util.PrecacheSound( "jessev92/weapons/univ/iron_out2.wav" )

sound.Add( {
	name = "VNTCB.SWep.ZoomCycleIn" ,
	channel = CHAN_ITEM ,
	level = 75 ,
	volume = 1.0 ,
	pitch = { 90 } ,
	sound = { "^)jessev92/weapons/univ/sniper_zoomchange.wav" }
} )
util.PrecacheSound( "jessev92/weapons/univ/sniper_zoomchange.wav" )

sound.Add( {
	name = "VNTCB.SWep.ZoomCycleOut" ,
	channel = CHAN_ITEM ,
	level = 75 ,
	volume = 1.0 ,
	pitch = { 110 } ,
	sound = { "^)jessev92/weapons/univ/sniper_zoomchange.wav" }
} )
util.PrecacheSound( "jessev92/weapons/univ/sniper_zoomchange.wav" )

---------------------------------------------
---------------------------------------------
--	DRAW / HOLSTER
---------------------------------------------
---------------------------------------------

sound.Add( {
	name = "VNTCB.SWep.Draw1" ,
	channel = CHAN_BODY ,
	level = 75 ,
	volume = 1.0 ,
	pitch = { 95, 105 } ,
	sound = "jessev92/weapons/univ/draw1.wav"
} )
util.PrecacheSound( "jessev92/weapons/univ/draw1.wav" )

sound.Add( {
	name = "VNTCB.SWep.Draw2" ,
	channel = CHAN_BODY ,
	level = 75 ,
	volume = 1.0 ,
	pitch = { 95, 105 } ,
	sound = "jessev92/weapons/univ/draw2.wav"
} )
util.PrecacheSound( "jessev92/weapons/univ/draw2.wav" )

sound.Add( {
	name = "VNTCB.SWep.Draw3" ,
	channel = CHAN_BODY ,
	level = 75 ,
	volume = 1.0 ,
	pitch = { 95, 105 } ,
	sound = "jessev92/weapons/univ/draw3.wav"
} )
util.PrecacheSound( "jessev92/weapons/univ/draw3.wav" )

sound.Add( {
	name = "VNTCB.SWep.Holster1" ,
	channel = CHAN_BODY ,
	level = 75 ,
	volume = 1.0 ,
	pitch = { 95, 105 } ,
	sound = "jessev92/weapons/univ/holster1.wav"
} )
util.PrecacheSound( "jessev92/weapons/univ/holster1.wav" )

---------------------------------------------
---------------------------------------------
--	JAMMING
---------------------------------------------
---------------------------------------------

sound.Add( {
	name = "Jam.Short.Rifle" ,
	channel = CHAN_ITEM ,
	level = 75 ,
	volume = 0.25 ,
	--pitch		= { 95, 105 },
	sound = "^)jessev92/weapons/univ/jam1.wav"
} )
util.PrecacheSound( "jessev92/weapons/univ/jam1.wav" )

sound.Add( {
	name = "Jam.Long.Rifle" ,
	channel = CHAN_ITEM ,
	level = 75 ,
	volume = 0.25 ,
	--pitch		= { 95, 105 },
	sound = "^)jessev92/weapons/univ/jam2.wav"
} )
util.PrecacheSound( "jessev92/weapons/univ/jam2.wav" )

sound.Add( {
	name = "Jam.Short.Pistol" ,
	channel = CHAN_ITEM ,
	level = 75 ,
	volume = 0.25 ,
	--pitch		= { 95, 105 },
	sound = "^)jessev92/weapons/univ/jam3.wav"
} )
util.PrecacheSound( "jessev92/weapons/univ/jam3.wav" )

sound.Add( {
	name = "Jam.Long.Pistol" ,
	channel = CHAN_ITEM ,
	level = 75 ,
	volume = 0.25 ,
	--pitch		= { 95, 105 },
	sound = "^)jessev92/weapons/univ/jam4.wav"
} )
util.PrecacheSound( "jessev92/weapons/univ/jam4.wav" )
