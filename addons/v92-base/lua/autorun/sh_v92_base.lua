AddCSLuaFile( )

local version = "Version: 2017/07/09 @ 2300\n"
------------------------------------------------------
--	Milestones										--
------------------------------------------------------
--	Add Penetration - Done 2016/09/28
--	Redux shotgun reloads - Done 2016/09/27
--	Contact & instructions - Done 2016/09/29
--	Purpose - Done 2016/10/02
--	Dynamic hold type animations - Done 2016/10/14
--	Single muzzle flash effect system - Done 2016/10/14
--	Single shell ejection effect system - Done 2016/10/14
--	Updated options menu - Done 2016/10/23
--	Added holster animations to the animation system - Done 2016/10/23
--	Added reset HUD button to menu - Done 2016/10/23
--	Made new RoF icons - Done 2016/10/23
--	Added revolver-style shell ejections - Done 2016/11/08
--	Fixed default positions for HUD - Done 2016/11/08
--	Dynamic sound ranges - Done 2016/12/09
--	Accuracy: Float --> Effective Range - Done 2016/12/10
--	Added door busting ability to weapons - Done 2016/12/27
--	Added stance icons to HUD - Done 2017/01/02
--	Added Jamming/Weapon Reliability - Done 2017/01/09
--	Added toggle for iron sight behavior - Done 2017/01/19
--	Added dynamic lights to muzzle flashes - Done 2017/01/22
--	Added BulletCraft sound effects to the weapons - Done 2017/02/10
--	Added support for 'special' fire types - Done 2017/03/01
--	Added support for DoD:S Weapon Pack - Done 2017/03/04
--	Added support for rifle-butt melees - Done 2017/03/05
--	Added support for up to five kitbag gibs - Done 2017/03/05
--	Added Shell-Loading Weapon Base: weapons/v92_base_swep_shell.lua - Done 2017/03/26
--	Added Melee Weapon Base: weapons/v92_base_swep_melee.lua - Done 2017/03/26
--	Re-coded shell reloads from sratch to work how I initially intended - Done 2017/03/26
--	Made zoom cycling fade in and out instead of being instant - Done 2017/03/26
--	Added grenade weapon base: weapons/v92_base_swep_grenade.lua - Done 2017/03/29
--	Added Frag grenade entity base - Done 2017/03/29
--	Added Smoke grenade entity base - Done 2017/03/29
--	Added Flashbang grenade entity base - Done 2017/03/29
--	Added Flare grenade entity base - Done 2017/03/29
--	Added Akimbo Base - Done 2017/03/30
--	Released first version of the V92 Vehicle Base - Done 2017/03/30
--	Released the Damage Inc. vehicle pack - Done 2107/03/30
--	Released the Parachute Weapon Base - Done 2017/04/01
--	Released the SiN weapon pack - Done 2017/05/31
------------------------------------------------------
------------------------------------------------------
------------------------------------------------------
--	Changes this Patch
------------------------------------------------------
------------------------------------------------------
------------------------------------------------------
--	Changelog:
------------------------------------------------------
------------------------------------------------------
--	Affected Add-Ons
------------------------------------------------------

------------------------------------------------------
------------------------------------------------------
------------------------------------------------------
--	Stretch Goals
------------------------------------------------------
------------------------------------------------------
------------------------------------------------------
--	Finish the clothing base
--	All-weapon rifle butting/pistol whipping
--	Fixed RT scopes being fully zoomed by default
--	Added zoom cycling to RT scopes
--	Improve akimbo functionality
--	Add DOOM mode - Get your E1M1 on
--	Attachments?
--	My version of bullet penetration because the Zoey code needs to go
--	My own iron sight code - MadCow/Siminov works but it's hacky and messy
--	Modernize the options menu - make my own spawn tab and move all my shit there like SimfPhys?
--	Better support for Weapon Weight System
--	Better support for Support SWeps
--	Better support for RT scopes
--	Toggle Zoom for RT Scopes
--	Add AutoWatch
--	Add Richochets
--	Add Weapon Magazine System
--	Add Verbose Information Screen
--	Add Bodily Harm System
------------------------------------------------------
--	Child Packs
------------------------------------------------------
--	[V] Damage Inc. ( 894893543 )
--
--	[W] Half-Life 2: X Melee ( 709715382 )
--	[W] Half-Life 2: X Pistol ( 709716426 )
--	[W] Half-Life 2: X Rifle ( 709718975 )
--	[W] Half-Life 2: X SMG ( 709717775 )
--	[W] Half-Life 2: X Shotgun ( 709723868 )
--	[W] Half-Life 2: X Sniper ( 709719836 )
--	[W] Action Half-Life 2 ( 914590631 )
--	[W] CQC! ( 818750632 )
--	[W] BF3 M320 ( 855378553 )
--	[W] HEAT ( 763862893 )
--	[W] Halo: Reach ( 503422951 )
--	[W] Hitman Blood Money Safari Rifle ( 762491157 )
--	[W] Counter-Strike: Source( 788152557 )
--	[W] Day of Defeat: Source ( 891239198 )
--	[W] Resistance & Liberation ( ERROR )
--	
--	[W] BF2 Parachute ( 895157475 )
--	[W] BF3 Parachute ( ERROR )
--	[W] BF2142 Parachute ( 895159273 )
--	[W] Just Cause Parachute ( 895912718 )
--	[W] Frontlines: FoW Parachute ( 895160212 )
--	[W] GTA IV Parachute ( 895161301 )
--	[W] Resistance & Liberation Parachute ( 895158385 )
------------------------------------------------------
-- CVars
------------------------------------------------------

if not ConVarExists( "VNT_Suicide_Disabled" ) then
	CreateConVar( "VNT_Suicide_Disabled" , 0 , {  FCVAR_REPLICATED , FCVAR_ARCHIVE  } , "( Boolean ) Toggle V92 Base Suicide disabler; 1 to block suicides, 0 to allow suicides" )
end

if not ConVarExists( "vnt_proj_dmg" ) then
	CreateConVar( "vnt_proj_dmg" , 3 , {  FCVAR_ARCHIVE , FCVAR_REPLICATED  } , "( Float ) Damage multiplier of the V92 projectile shell entities" )
end

if not ConVarExists( "vnt_proj_rad" ) then
	CreateConVar( "vnt_proj_rad" , 2 , {  FCVAR_ARCHIVE , FCVAR_REPLICATED  } , "( Float ) Radius multiplier of the V92 projectile shell entities" )
end

if CLIENT then
	
	if not ConVarExists( "VNT_Debug" ) then		
		CreateClientConVar( "VNT_Debug" , 0 , true , false , "( Boolean ) Toggle debug mode for V92 Base" )	
	end
	
	if not ConVarExists( "VNT_Base_ViewBob_Toggle" ) then		
		CreateClientConVar( "VNT_Base_ViewBob_Toggle" , 0 , true , false , "( Boolean ) The view bob toggle for the V92 Base" )	
	end
	
	if not ConVarExists( "VNT_ViewBob_Mul" ) then		
		CreateClientConVar( "VNT_ViewBob_Mul" , 0.5 , true , false , "( Float ) The view bob multiplier for the V92 Base" )	
	end

	if not ConVarExists( "VNT_Base_SWep_HUD_StanceX" ) then
		CreateClientConVar( "VNT_Base_SWep_HUD_StanceX" , ( ScrW( ) * 0.47 ) , true , false , "( Float ) The X-position on the screen of the Stance icon in the V92 SWep Base" )
	end

	if not ConVarExists( "VNT_Base_SWep_HUD_StanceY" ) then
		CreateClientConVar( "VNT_Base_SWep_HUD_StanceY" , ( ScrH( ) - 175 ) , true , false , "( Float ) The Y-position on the screen of the Stance icon in the V92 SWep Base" )
	end

	if not ConVarExists( "VNT_Base_SWep_HUD_SuppX" ) then
		CreateClientConVar( "VNT_Base_SWep_HUD_SuppX" , ( ScrW( ) * 0.55 ) , true , false , "( Float ) The X-position on the screen of the Suppressor icon in the V92 SWep Base" )
	end

	if not ConVarExists( "VNT_Base_SWep_HUD_SuppY" ) then
		CreateClientConVar( "VNT_Base_SWep_HUD_SuppY" , ( ScrH( ) - 200 ) , true , false , "( Float ) The Y-position on the screen of the Suppressor icon in the V92 SWep Base" )
	end

	if not ConVarExists( "VNT_Base_SWep_HUD_StockX" ) then
		CreateClientConVar( "VNT_Base_SWep_HUD_StockX" , ( ScrW( ) * 0.45 ) , true , false , "( Float ) The X-position on the screen of the stock icon in the V92 SWep Base" )
	end

	if not ConVarExists( "VNT_Base_SWep_HUD_StockY" ) then
		CreateClientConVar( "VNT_Base_SWep_HUD_StockY" , ( ScrH( ) - 200 ) , true , false , "( Float ) The Y-position on the screen of the stock icon in the V92 SWep Base" )
	end

	if not ConVarExists( "VNT_Base_SWep_HUD_RoFX" ) then
		CreateClientConVar( "VNT_Base_SWep_HUD_RoFX" , ( ScrW( ) * 0.5 ) , true , false , "( Float ) The X-position on the screen of the Rate of Fire icon in the V92 SWep Base" )
	end

	if not ConVarExists( "VNT_Base_SWep_HUD_RoFY" ) then
		CreateClientConVar( "VNT_Base_SWep_HUD_RoFY" , ( ScrH( ) - 25 ) , true , false , "( Float ) The Y-position on the screen of the Rate of Fire icon in the V92 SWep Base" )
	end

	if not ConVarExists( "VNT_Base_SWep_HUD_BipodX" ) then
		CreateClientConVar( "VNT_Base_SWep_HUD_BipodX" , ( ScrW( ) * 0.6 ) , true , false , "( Float ) The X-position on the screen of the Bipod icon in the V92 SWep Base" )
	end

	if not ConVarExists( "VNT_Base_SWep_HUD_BipodY" ) then
		CreateClientConVar( "VNT_Base_SWep_HUD_BipodY" , ( ScrH( ) - 75 ) , true , false , "( Float ) The Y-position on the screen of the Bipod icon in the V92 SWep Base" )
	end

	if not ConVarExists( "VNT_Base_SWep_HUD_GLMX" ) then
		CreateClientConVar( "VNT_Base_SWep_HUD_GLMX" , ( ScrW( ) * 0.4 ) , true , false , "( Float ) The X-position on the screen of the Grenade Launcher icon in the V92 SWep Base" )
	end

	if not ConVarExists( "VNT_Base_SWep_HUD_GLMY" ) then
		CreateClientConVar( "VNT_Base_SWep_HUD_GLMY" , ( ScrH( ) - 75 ) , true , false , "( Float ) The Y-position on the screen of the Grenade Launcher icon in the V92 SWep Base" )
	end

	if not ConVarExists( "VNT_Base_SWep_TracerToggle" ) then
		CreateClientConVar( "VNT_Base_SWep_TracerToggle" , 1 , true , false , "( Boolean ) Toggle the tracers in the V92 SWep Base" )
	end

	if not ConVarExists( "VNT_Base_SWep_ShellToggle" ) then
		CreateClientConVar( "VNT_Base_SWep_ShellToggle" , 1 , true , false , "( Boolean ) Toggle the shell ejections in the V92 SWep Base" )
	end

	if not ConVarExists( "VNT_Base_SWep_ShellTime" ) then
		CreateClientConVar( "VNT_Base_SWep_ShellTime" , 30 , true , false , "( Integer ) Lifetime for shell ejections in the V92 SWep Base" )
	end

	if not ConVarExists( "VNT_Base_SWep_FlashToggle" ) then
		CreateClientConVar( "VNT_Base_SWep_FlashToggle" , 1 , true , false , "( Boolean ) Toggle the muzzle flashes in the V92 SWep Base" )
	end
	
	if not ConVarExists( "VNT_Base_SWep_IronsightToggle" ) then
		CreateClientConVar( "VNT_Base_SWep_IronsightToggle" , 0 , true , false , "( Boolean ) True to Toggle Ironsight , False to Hold It for the V92 SWep Base" )
	end
	
	if not ConVarExists( "VNT_Base_SWep_IronSight_Sensitivity" ) then
		CreateClientConVar( "VNT_Base_SWep_IronSight_Sensitivity" , 0.2 , true , false , "( Float ) Sensitivity of mouse movement while iron sighted in the V92 SWep Base" )
	end
	
	if not ConVarExists( "VNT_Base_SWep_BulletCraft_Toggle" ) then
		CreateClientConVar( "VNT_Base_SWep_BulletCraft_Toggle" , 1 , true , false , "( Boolean ) CLIENT Toggle the BulletCraft sound effects in the V92 SWep Base" )
	end
	
end

if not ConVarExists("VNT_STALKER_GasMask_Filter_Ammo") then 
	
	CreateConVar( "VNT_STALKER_GasMask_Filter_Ammo" , 0 ,  { FCVAR_REPLICATED, FCVAR_ARCHIVE } )
	
end

if not ConVarExists( "VNT_Base_SWep_ClampToggle" ) then
	CreateConVar( "VNT_Base_SWep_ClampToggle" , 1 , {  FCVAR_REPLICATED , FCVAR_ARCHIVE  } , "( Boolean ) Toggle CVar Clamping in the V92 SWep Base" )
end

if not ConVarExists( "VNT_Base_SWep_JamToggle" ) then
	CreateConVar( "VNT_Base_SWep_JamToggle" , 0 , {  FCVAR_REPLICATED , FCVAR_ARCHIVE  } , "( Boolean ) Toggle jamming in the V92 SWep Base" )
end

if not ConVarExists( "VNT_Base_SWep_SpeedWeight" ) then
	CreateConVar( "VNT_Base_SWep_SpeedWeight" , 1 , {  FCVAR_REPLICATED , FCVAR_ARCHIVE  } , "( Boolean ) Toggle the weight entitySpeed modifier for V92 SWep Base weapons" )
end

if not ConVarExists( "VNT_Base_SWep_SpeedToggle" ) then
	CreateConVar( "VNT_Base_SWep_SpeedToggle" , 1 , {  FCVAR_REPLICATED , FCVAR_ARCHIVE  } , "( Boolean ) Toggle the entitySpeed modifiers for V92 SWep Base weapons" )
end

if not ConVarExists( "VNT_Base_SWep_Speed_Run" ) then
	CreateConVar( "VNT_Base_SWep_Speed_Run" , 300 , {  FCVAR_REPLICATED , FCVAR_ARCHIVE  } , "( Integer ) Set the max run entitySpeed for V92 SWep base weapons" )
end

if not ConVarExists( "VNT_Base_SWep_Speed_Walk" ) then
	CreateConVar( "VNT_Base_SWep_Speed_Walk" , 150 , {  FCVAR_REPLICATED , FCVAR_ARCHIVE  } , "( Integer ) Set the max walk entitySpeed for V92 SWep base weapons" )
end

if not ConVarExists( "VNT_Base_SWep_BlackFriday_Toggle" ) then
	CreateConVar( "VNT_Base_SWep_BlackFriday_Toggle" , 1 , {  FCVAR_REPLICATED , FCVAR_ARCHIVE  } , "( Integer ) Set the door buster master enable/disable toggle for V92 SWep base weapons" )
end

if not ConVarExists( "VNT_Base_SWep_BlackFriday_ForceMul" ) then
	CreateConVar( "VNT_Base_SWep_BlackFriday_ForceMul" , 1 , {  FCVAR_REPLICATED , FCVAR_ARCHIVE  } , "( Integer ) Set the door buster multiplier for V92 SWep base weapons" )
end

if not ConVarExists( "VNT_Base_SWep_BlackFriday_Range" ) then
	CreateConVar( "VNT_Base_SWep_BlackFriday_Range" , 150 , {  FCVAR_REPLICATED , FCVAR_ARCHIVE  } , "( Integer ) Set the door buster range for V92 SWep base weapons" )
end

if not ConVarExists( "VNT_Base_SWep_BlackFriday_ResetTimer" ) then
	CreateConVar( "VNT_Base_SWep_BlackFriday_ResetTimer" , 300 , {  FCVAR_REPLICATED , FCVAR_ARCHIVE  } , "( Integer ) Set the door buster reset time for V92 SWep base weapons" )
end

if not ConVarExists( "VNT_Base_SWep_BlackFriday_ResetToggle" ) then
	CreateConVar( "VNT_Base_SWep_BlackFriday_ResetToggle" , 1 , {  FCVAR_REPLICATED , FCVAR_ARCHIVE  } , "( Boolean ) Do doors blown off by V92 SWep base weapons reset?" )
end

if not ConVarExists( "VNT_Base_SWep_SprintNShoot" ) then
	CreateConVar( "VNT_Base_SWep_SprintNShoot" , 0 , {  FCVAR_REPLICATED , FCVAR_ARCHIVE  } , "( Boolean ) Toggle the ability to sprint and shoot" )
end

if not ConVarExists( "VNT_Base_SWep_RecoilMul" ) then
	CreateConVar( "VNT_Base_SWep_RecoilMul" , 1 , {  FCVAR_REPLICATED , FCVAR_ARCHIVE  } , "( Float ) Set the clamp for the max recoil for V92 SWep base weapons" )
end

if not ConVarExists( "VNT_Base_SWep_AccuracyMul" ) then
	CreateConVar( "VNT_Base_SWep_AccuracyMul" , 1 , {  FCVAR_REPLICATED , FCVAR_ARCHIVE  } , "( Float ) Set the clamp for the max accuracy bloom for V92 SWep base weapons" )
end

if not ConVarExists( "VNT_Base_SWep_DamageMul" ) then
	CreateConVar( "VNT_Base_SWep_DamageMul" , 1 , {  FCVAR_REPLICATED , FCVAR_ARCHIVE  } , "( Integer ) Set the damage multiplier for V92 SWep base weapons" )
end

if not ConVarExists( "VNT_Base_SWep_RichochetLimit" ) then
	CreateConVar( "VNT_Base_SWep_RichochetLimit" , 5 , {  FCVAR_REPLICATED , FCVAR_ARCHIVE  } , "( Integer ) Set the clamp for the max richochets for V92 SWep base weapons" )
end

if not ConVarExists( "VNT_Base_SWep_SpawnUnloaded" ) then
	CreateConVar( "VNT_Base_SWep_SpawnUnloaded" , 1 , {  FCVAR_REPLICATED , FCVAR_ARCHIVE  } , "( Boolean ) Should the weapon spawn unloaded?" )
end

if not ConVarExists( "VNT_Base_SWep_SpawnAmmo" ) then
	CreateConVar( "VNT_Base_SWep_SpawnAmmo" , 0 , {  FCVAR_REPLICATED , FCVAR_ARCHIVE  } , "( Boolean ) Should the weapon spawn with ammo?" )
end

if not ConVarExists( "VNT_Base_SWep_PrintLocation" ) then
	CreateClientConVar( "VNT_Base_SWep_PrintLocation" , 0 , {  FCVAR_REPLICATED , FCVAR_ARCHIVE  } , true , false , "( Integer ) Print location of the messages; 0 = Centre of screen , 1 = Chat & Console , 2 = Console Only" )
end

if not ConVarExists( "VNT_Base_SWep_Doom_Toggle" ) then
	CreateClientConVar( "VNT_Base_SWep_Doom_Toggle" , 0 , {  FCVAR_REPLICATED , FCVAR_ARCHIVE  } , true , false , "( Boolean ) YOU ARE HUGE THAT MEANS YOU HAVE HUGE GUTSnot  RIP AND TEAR!" )
end

if not ConVarExists( "VNT_Base_SWep_DepthModToggle" ) then
	CreateConVar( "VNT_Base_SWep_DepthModToggle" , 1 , {  FCVAR_REPLICATED , FCVAR_ARCHIVE  } , "( Boolean ) If you're too close to a wall , should we lower the weapon? Works for V92 SWep Base." )
end

if not ConVarExists("VNT_SWep_TorchBMS_Toggle") then CreateConVar("VNT_SWep_TorchBMS_Toggle" , 1 ,  { FCVAR_REPLICATED , FCVAR_ARCHIVE } ) end

if not ConVarExists("VNT_SWep_Deployable_PlaceDelay") then CreateConVar("VNT_SWep_Deployable_PlaceDelay" , 5 , { FCVAR_REPLICATED , FCVAR_ARCHIVE } ) end
if not ConVarExists("VNT_SWep_Deployable_FindRadius") then CreateConVar("VNT_SWep_Deployable_FindRadius" , 96 , { FCVAR_REPLICATED , FCVAR_ARCHIVE } ) end

if not ConVarExists("VNT_SWep_Deployable_Medical_GiveDelay") then CreateConVar("VNT_SWep_Deployable_Medical_GiveDelay" , 2 , { FCVAR_REPLICATED , FCVAR_ARCHIVE } ) end
if not ConVarExists("VNT_SWep_Deployable_Medical_Charge") then CreateConVar("VNT_SWep_Deployable_Medical_Charge" , 300 , { FCVAR_REPLICATED , FCVAR_ARCHIVE } ) end
if not ConVarExists("VNT_SWep_Deployable_Medical_Apply") then CreateConVar("VNT_SWep_Deployable_Medical_Apply" , 5 , { FCVAR_REPLICATED , FCVAR_ARCHIVE } ) end

if not ConVarExists("VNT_SWep_Deployable_Ammo_GiveDelay") then CreateConVar("VNT_SWep_Deployable_Ammo_GiveDelay" , 2 , { FCVAR_REPLICATED , FCVAR_ARCHIVE } ) end
if not ConVarExists("VNT_SWep_Deployable_Ammo_Charge") then CreateConVar("VNT_SWep_Deployable_Ammo_Charge" , 300 , { FCVAR_REPLICATED , FCVAR_ARCHIVE } ) end
if not ConVarExists("VNT_SWep_Deployable_Ammo_Apply") then CreateConVar("VNT_SWep_Deployable_Ammo_Apply" , 5 , { FCVAR_REPLICATED , FCVAR_ARCHIVE } ) end

if not ConVarExists("VNT_SWep_Mines_PlaceDelay") then CreateConVar("VNT_SWep_Mines_PlaceDelay" , 5 ,  { FCVAR_REPLICATED, FCVAR_ARCHIVE } ) end
if not ConVarExists("VNT_SWep_Mines_FindRadius") then CreateConVar("VNT_SWep_Mines_FindRadius" , 128 ,  { FCVAR_REPLICATED, FCVAR_ARCHIVE } ) end

------------------------------------------------------
------------------------------------------------------
--	Global Tables
--	Used to provide info globally and to prevent errors
------------------------------------------------------
------------------------------------------------------
VNTCB = VNTCB or {  } 

------------------------------------------------------
--	Generic Information
--	Author and contact information
------------------------------------------------------
VNTCB.Info = VNTCB.Info or { 
	["author"] = "Reverend Jesse V92" , 
	["contact"] = "Steam Group: The Cult of V92" , 
	["instructions"] = "Read the Description!" , 
	["purpose"] = "\"Hic , nos tueatur\"" , 
	
	["name"] = "V92 Add-On Base\n" , 
	["version"] = version , 
	["link"] = "Workshop: http://steamcommunity.com/sharedfiles/filedetails/?id=505106454" , 
	["group"] = "Group: http://steamcommunity.com/groups/CultOfV92" , 
	["vntid"] = "Profile: http://steamcommunity.com/id/JesseVanover" , 
	["smartass"] = "V92: Fixing Your Shit Since 2008!" , 
 } 

concommand.Add( "VNT_About" , function( ply , cmd , args )
	print( "----------\n" .. 
	VNTCB.Info.name .. "\n"  .. 
	VNTCB.Info.link .. "\n" .. 
	VNTCB.Info.version .. "\n" .. 
	"Author: " .. VNTCB.Info.author .. "\n"  .. 
	VNTCB.Info.vntid .. "\n" ..
	VNTCB.Info.group .. "\n\n" .. 
	VNTCB.Info.smartass .. "\n----------" )
end )

------------------------------------------------------
--	Categories
--	Common categories used in my shite content
--	Don't take these as a sign of confirmed future content. I'm just adding a tonne to make it easier if I ever need them.
--	Feel free to use them.
------------------------------------------------------

VNT_CATEGORY_VNT = "V92: General"
VNT_CATEGORY_AH64 = "V92: Apache Air Assault"
VNT_CATEGORY_ARMYMEN = "V92: Army Men"
VNT_CATEGORY_BATTLEFIELD = "V92: Battlefield"
VNT_CATEGORY_BATTLEFIELDUSA = "V92: Battlefield US"
VNT_CATEGORY_BATTLEFIELDRUSSIA = "V92: Battlefield RU"
VNT_CATEGORY_BATTLEFIELDCHINA = "V92: Battlefield CH"
VNT_CATEGORY_BATTLEFIELDMEC = "V92: Battlefield MEC"
VNT_CATEGORY_BATTLEFIELDIDF = "V92: Battlefield IDF"
VNT_CATEGORY_BATTLEFIELD1942 = "V92: Battlefield 1942"
VNT_CATEGORY_BATTLEFIELDVIETNAM = "V92: Battlefield Vietnam"
VNT_CATEGORY_BATTLEFIELD2142 = "V92: Battlefield 2142"
VNT_CATEGORY_BATTLEFIELD2142NS = "V92: Battlefield 2142 Northern Strike"
VNT_CATEGORY_BATTLEFIELD2 = "V92: Battlefield 2"
VNT_CATEGORY_BATTLEFIELD2USMC = "V92: Battlefield 2 USMC"
VNT_CATEGORY_BATTLEFIELD2MEC = "V92: Battlefield 2 MEC"
VNT_CATEGORY_BATTLEFIELD2PLA = "V92: Battlefield 2 PLA"
VNT_CATEGORY_BATTLEFIELD2SF = "V92: Battlefield 2 Special Forces"
VNT_CATEGORY_BATTLEFIELS2AF = "V92: Battlefield 2 Armoured Fury"
VNT_CATEGORY_BATTLEFIELD2EF = "V92: Battlefield 2 Euro Forces"
VNT_CATEGORY_BATTLEFIELD2EOD = "V92: Battlefield 2 Eve of Destruction"
VNT_CATEGORY_BATTLEFIELD2AI = "V92: Battlefield 2 Allied Intent"
VNT_CATEGORY_BATTLEFIELD2ALPHA = "V92: Battlefield 2 Alpha Project"
VNT_CATEGORY_BATTLEFIELD2PR = "V92: Battlefield 2 Project Reality"
VNT_CATEGORY_BATTLEFIELD1943 = "V92: Battlefield 1943"
VNT_CATEGORY_BATTLEFIELD3 = "V92: Battlefield 3"
VNT_CATEGORY_BATTLEFIELD4 = "V92: Battlefield 4"
VNT_CATEGORY_BATTLEFIELDHARDLINE = "V92: Battlefield Hardline"
VNT_CATEGORY_BATTLEFIELD1 = "V92: Battlefield 1"
VNT_CATEGORY_CALLOFDUTY = "V92: Call of Duty"
VNT_CATEGORY_CRYSIS1 = "V92: Crysis"
VNT_CATEGORY_CRYSIS2 = "V92: Crysis 2"
VNT_CATEGORY_CRYSIS3 = "V92: Crysis 3"
VNT_CATEGORY_CSTRIKE = "V92: Counter-Strike"
VNT_CATEGORY_CSTRIKESOURCE = "V92: Counter-Strike: Source"
VNT_CATEGORY_CSTRIKEGO = "V92: Counter-Strike: Globally Offensive"
VNT_CATEGORY_CSTRIKECZ = "V92: Counter-Strike: Condition Zero"
VNT_CATEGORY_FALLOUT = "V92: FallOut"
VNT_CATEGORY_FALLOUT3 = "V92: FallOut 3"
VNT_CATEGORY_FALLOUT4 = "V92: FallOut 4"
VNT_CATEGORY_FALLOUTNEWVEGAS = "V92: FallOut New Vegas"
VNT_CATEGORY_FISTFULOFFRAGS = "V92: Fistful of Frags"
VNT_CATEGORY_LEADANDGOLD = "V92: Lead and Gold"
VNT_CATEGORY_HALO = "V92: Halo"
VNT_CATEGORY_HALO1 = "V92: Halo Combat Evolved"
VNT_CATEGORY_HALO2 = "V92: Halo 2"
VNT_CATEGORY_HALO3 = "V92: Halo 3"
VNT_CATEGORY_HALOODST = "V92: Halo 3 ODST"
VNT_CATEGORY_HALOREACH = "V92: Halo Reach"
VNT_CATEGORY_HALO4 = "V92: Halo 4"
VNT_CATEGORY_HALO5 = "V92: Halo 5"
VNT_CATEGORY_HEAT = "V92: HEAT"
VNT_CATEGORY_RAMBO = "V92: Rambo"
VNT_CATEGORY_PAYDAY = "V92: PAYDAY"
VNT_CATEGORY_HL1 = "V92: Half-Life"
VNT_CATEGORY_HL2 = "V92: Half-Life 2"
VNT_CATEGORY_HL2EXPANDED = "V92: Half-Life 2: Expanded"
VNT_CATEGORY_AHL2 = "V92: Action Half-Life 2"
VNT_CATEGORY_INSURGENCY = "V92: Insurgency"
VNT_CATEGORY_INSURGENCY2 = "V92: Insurgency 2"
VNT_CATEGORY_BLACKMESA = "V92: Black Mesa"
VNT_CATEGORY_JUSTCAUSE = "V92: Just Cause"
VNT_CATEGORY_FRONTLINES = "V92: Frontlines FoW"
VNT_CATEGORY_MERCENARIES2 = "V92: Mercenaries 2"
VNT_CATEGORY_RAVAGED = "V92: Ravaged"
VNT_CATEGORY_DNF = "V92: Duke Nukem Forever"
VNT_CATEGORY_L4D = "V92: L4D"
VNT_CATEGORY_L4D2 = "V92: L4D2"
VNT_CATEGORY_MGS = "V92: Metal Gear Solid"
VNT_CATEGORY_MGS2 = "V92: Metal Gear Solid 2"
VNT_CATEGORY_MGS3 = "V92: Metal Gear Solid 3"
VNT_CATEGORY_MGS4 = "V92: Metal Gear Solid 4"
VNT_CATEGORY_MGSV = "V92: Metal Gear Solid V"
VNT_CATEGORY_SHADOWCOMPLEX = "V92: Shadow Complex"
VNT_CATEGORY_STARTREK = "V92: Star Trek"
VNT_CATEGORY_STARWARS = "V92: Star Wars"
VNT_CATEGORY_STARGATE = "V92: Stargate"
VNT_CATEGORY_STALKER = "V92: S.T.A.L.K.E.R."
VNT_CATEGORY_SIN = "V92: SiN Episodes"
VNT_CATEGORY_WW1 = "V92: World War I"
VNT_CATEGORY_WW2 = "V92: World War II"
VNT_CATEGORY_DAMAGEINC = "V92: Damage Inc."
VNT_CATEGORY_DODS = "V92: Day of Defeat: Source"
VNT_CATEGORY_RNL = "V92: Resistance & Liberation"
VNT_CATEGORY_REDORC = "V92: Red Orchestra"
VNT_CATEGORY_DOI = "V92: Day of Infamy"
VNT_CATEGORY_COH = "V92: Company of Heroes"
VNT_CATEGORY_WORLDOFTANKS = "V92: World of Tanks"
VNT_CATEGORY_WW2ALLIES = "V92: WW2 Allies"
VNT_CATEGORY_WW2AXIS = "V92: WW2 Axis"
VNT_CATEGORY_WW2USA = "V92: WW2 United States"
VNT_CATEGORY_WW2UK = "V92: WW2 United Kingdom"
VNT_CATEGORY_WW2GERMANY = "V92: WW2 Nazi Germany"
VNT_CATEGORY_WW2ITALY = "V92: WW2 Royal Italian Army"
VNT_CATEGORY_WW2USSR = "V92: WW2 U.S.S.R."
VNT_CATEGORY_WW2JAPAN = "V92: WW2 Empire of Japan"
VNT_CATEGORY_WARHAMMER = "V92: Warhammer"
VNT_CATEGORY_WARHAMMER40K = "V92: Warhammer 40K"
VNT_CATEGORY_GTAIV = "V92: GTA IV"
VNT_CATEGORY_GTAV = "V92: GTA V"
VNT_CATEGORY_GS = "GMOD Soldiers"

------------------------------------------------------
--	Content Bases
--	Shortcuts to all my bases
------------------------------------------------------

VNT_BASE_WEAPON = "v92_base_swep"
-- VNT_BASE_WEAPON_NPC = "v92_base_swep_npc"
VNT_BASE_WEAPON_SHELL = "v92_base_swep_shell"
VNT_BASE_WEAPON_AKIMBO = "v92_base_swep_akimbo"
VNT_BASE_WEAPON_MELEE = "v92_base_swep_melee"
VNT_BASE_WEAPON_GRENADE = "v92_base_swep_grenade"
VNT_BASE_WEAPON_DEPLOYABLE = "v92_base_swep_deployable"
VNT_BASE_WEAPON_BIPOD = "v92_base_swep_bipod"
VNT_BASE_WEAPON_PARACHUTE = "v92_base_swep_parachute"
VNT_BASE_WEAPON_CLOTHING = "v92_base_swep_clothing"
VNT_BASE_WEAPON_ATTACHMENT = "v92_base_attachment"
VNT_BASE_WEAPON_MAGAZINE = "v92_base_magazine"
VNT_BASE_WEAPON_ENTITY = "v92_base_wepent"
VNT_BASE_AMMO_CRATE = "v92_base_ammocrate"
VNT_BASE_AMMO_BOX = "v92_base_ammobox"
VNT_BASE_PROJECTILE = "v92_base_projectile"
VNT_BASE_GRENADE = "v92_base_grenade"
VNT_BASE_KITBAG = "v92_base_kitbag"
VNT_BASE_MINE_VEHICLE = "v92_base_mine_vehicle"
VNT_BASE_MINE_PERSONNEL = "v92_base_mine_personnel"
VNT_BASE_DEPLOYABLE_MEDICAL = "v92_deployable_medic"
VNT_BASE_DEPLOYABLE_AMMO = "v92_deployable_ammo"

-- VNT_BASE_VEHICLE = "v92_base_veh"
VNT_BASE_VEHICLE_HELICOPTER = "v92_base_veh_heli"
VNT_BASE_VEHICLE_TWIN = "v92_base_veh_heli_twin"
VNT_BASE_VEHICLE_PLANE = "v92_base_veh_plane"
-- VNT_BASE_VEHICLE_LAND = "v92_base_veh_land"
-- VNT_BASE_VEHICLE_LANDTREADS = "v92_base_veh_land_treaded"
-- VNT_BASE_VEHICLE_LANDWHEELS = "v92_base_veh_land_wheeled"
-- VNT_BASE_VEHICLE_LANDHALFTRACK = "v92_base_veh_land_halftrack"
-- VNT_BASE_VEHICLE_LANDCYCLE = "v92_base_veh_land_cycle"
-- VNT_BASE_VEHICLE_SEA = "v92_base_veh_naval"
-- VNT_BASE_VEHICLE_SEASAIL = "v92_base_veh_naval_sail"
VNT_BASE_VEHICLE_ROTOR = "v92_veh_rotor"
VNT_BASE_VEHICLE_ROTOR_TAIL = "v92_veh_rotor_tail"
VNT_BASE_VEHICLE_ENGINE = "v92_veh_engine"
VNT_BASE_VEHICLE_EXTRA = "v92_veh_extra"
VNT_BASE_VEHICLE_SEAT = "v92_veh_seat"
VNT_BASE_VEHICLE_HITDETECTOR = "v92_veh_hitdetector"
VNT_BASE_VEHICLE_TURRET = "v92_veh_turret"
VNT_BASE_VEHICLE_STATION = "v92_base_veh_station"
VNT_BASE_VEHICLE_POD = "v92_pod_base"
VNT_BASE_VEHICLE_POD_GUN = "v92_pod_gun"
VNT_BASE_VEHICLE_POD_AIMED = "v92_pod_aimedgun"
VNT_BASE_VEHICLE_POD_AIMED2 = "v92_pod_aimedgun2"
-- VNT_BASE_VEHICLE_POD_CANNON = "v92_veh_pod_cannon"
VNT_BASE_VEHICLE_POD_GATLING = "v92_pod_gatling"
VNT_BASE_VEHICLE_POD_BOMB = "v92_pod_bomb"
VNT_BASE_VEHICLE_POD_MISSILE = "v92_pod_missile"
VNT_BASE_VEHICLE_POD_ROCKET = "v92_pod_rocket"
VNT_BASE_VEHICLE_CM_FLARES_HELI = "v92_veh_cm_flares_heli"
VNT_BASE_VEHICLE_CM_FLARES_PLANE = "v92_veh_cm_flares_plane"
VNT_BASE_VEHICLE_CM_SMOKE = "v92_veh_cm_smoke"
-- VNT_BASE_NPC = "v92_base_npc"

------------------------------------------------------
--	Plugins table
--	Unused - for now...
------------------------------------------------------

VNTCB.Plugins = VNTCB.Plugins or {  RootBase  } 

---------------------------------------------
---------------------------------------------
--	V92 SWep Bases Lua Autorun Shit
--	This is stuff for:
--	Lua/weapons/v92_base_swep.lua
---------------------------------------------
---------------------------------------------

------------------------------------------------------
-- AUTORUN CODE
------------------------------------------------------

------------------------------------------------------
--	Automatic Code Download
--	It Downloads Code Automatically
------------------------------------------------------

if SERVER then

	resource.AddWorkshop( "505106454" ) -- V92 Base
	resource.AddWorkshop( "941433215" ) -- Unarmed SWep

	-- Shared
	for _ , file in pairs( file.Find( "autorun/sh_v92_*.lua" , "LUA" ) ) do
		AddCSLuaFile( "autorun/"..file )
	end
	
	-- Client
	for _ , file in pairs( file.Find( "autorun/client/cl_v92_*.lua" , "LUA" ) ) do
		AddCSLuaFile( "autorun/client/"..file )
	end
	
	-- Server
	for _ , file in pairs( file.Find( "autorun/server/sv_v92_*.lua" , "LUA" ) ) do
		include( "autorun/server/"..file )
	end

	-- Weapons
	for _ , file in pairs( file.Find( "weapons/v92_*.lua" , "LUA" ) ) do
		AddCSLuaFile( "weapons/"..file )
	end
	
	-- Entities
	for _ , file in pairs( file.Find( "entities/v92_*.lua" , "LUA" ) ) do
		AddCSLuaFile( "entities/"..file )
	end
	
	-- Special
	for _ , file in pairs( file.Find( "VNT/*.lua" , "LUA" ) ) do
		AddCSLuaFile( "VNT/"..file )
	end
	
end

---------------------------------------------
---------------------------------------------
-- This is our Reset button. 
-- It will essentially reset the base in case you've fucked up all the options
---------------------------------------------
---------------------------------------------
concommand.Add( "VNT_Reset" , function( ply , cmd , args , argStr )

	RunConsoleCommand( "VNT_Base_SWep_SpeedToggle" , 1 )
	RunConsoleCommand( "VNT_Base_SWep_Speed_Run" , 300 )
	RunConsoleCommand( "VNT_Base_SWep_Speed_Walk" , 150 )
	RunConsoleCommand( "VNT_Base_SWep_RecoilMul" , 1 )
	RunConsoleCommand( "VNT_Base_SWep_AccuracyMul" , 1 )
	RunConsoleCommand( "VNT_Base_SWep_DamageMul" , 5 )
	RunConsoleCommand( "VNT_Base_SWep_BlackFriday_Toggle" , 1 )
	RunConsoleCommand( "VNT_Base_SWep_BlackFriday_ForceMul" , 1 )
	RunConsoleCommand( "VNT_Base_SWep_BlackFriday_Range" , 150 )
	RunConsoleCommand( "VNT_Base_SWep_BlackFriday_ResetToggle" , 0 )
	RunConsoleCommand( "VNT_Base_SWep_BlackFriday_ResetTimer" , 300 )
	RunConsoleCommand( "VNT_Base_SWep_RichochetLimit" , 5 )
	RunConsoleCommand( "VNT_Base_SWep_SpawnUnloaded" , 1 )
	RunConsoleCommand( "VNT_Base_SWep_SpawnAmmo" , 1 )
	RunConsoleCommand( "VNT_Base_SWep_DepthModToggle" , 1 )
	RunConsoleCommand( "VNT_Base_SWep_SprintNShoot" , 0 )
	RunConsoleCommand( "VNT_Base_SWep_IronSight_Sensitivity" , 0.2 )
	RunConsoleCommand( "VNT_Base_SWep_ClampToggle" , 1 )
	RunConsoleCommand( "VNT_Base_SWep_JamToggle" , 1 )
	--RunConsoleCommand( "VNT_Base_SWep_JamChance" , 1000 )
	--RunConsoleCommand( "VNT_Base_SWep_JamChance_Override" , 0 )

	RunConsoleCommand( "vnt_proj_dmg" , 3 )
	RunConsoleCommand( "vnt_proj_rad" , 2 )
	RunConsoleCommand( "VNT_Debug" , 0 )
	RunConsoleCommand( "VNT_Base_ViewBob_Toggle" , 0 )
	RunConsoleCommand( "VNT_ViewBob_Mul" , 0.5 )
	RunConsoleCommand( "VNT_Base_SWep_IronsightToggle" , 0 )
	RunConsoleCommand( "VNT_Base_SWep_PrintLocation" , 0 )

	if ply then

		RunConsoleCommand( "VNT_Base_SWep_HUD_StanceX" , ( ScrW( ) * 0.47 ) )
		RunConsoleCommand( "VNT_Base_SWep_HUD_StanceY" , ( ScrH( ) - 180 ) )
		RunConsoleCommand( "VNT_Base_SWep_HUD_RoFX" , ( ScrW( ) * 0.5 ) )
		RunConsoleCommand( "VNT_Base_SWep_HUD_RoFY" , ( ScrH( ) - 25 ) )
		RunConsoleCommand( "VNT_Base_SWep_HUD_SuppX" , ( ScrW( ) * 0.55 ) )
		RunConsoleCommand( "VNT_Base_SWep_HUD_SuppY" , ( ScrH( ) - 200 ) )
		RunConsoleCommand( "VNT_Base_SWep_HUD_StockX" , ( ScrW( ) * 0.45 ) )
		RunConsoleCommand( "VNT_Base_SWep_HUD_StockY" , ( ScrH( ) - 200 ) )
		RunConsoleCommand( "VNT_Base_SWep_HUD_BipodX" , ( ScrW( ) * 0.6 ) )
		RunConsoleCommand( "VNT_Base_SWep_HUD_BipodY" , ( ScrH( ) - 75 ) )
		RunConsoleCommand( "VNT_Base_SWep_HUD_GLMX" , ( ScrW( ) * 0.4 ) )
		RunConsoleCommand( "VNT_Base_SWep_HUD_GLMY" , ( ScrH( ) - 75 ) )
		RunConsoleCommand( "VNT_Base_SWep_FlashToggle" , 1 )
		RunConsoleCommand( "VNT_Base_SWep_TracerToggle" , 1 )
		RunConsoleCommand( "VNT_Base_SWep_ShellToggle" , 1 )
		RunConsoleCommand( "VNT_Base_SWep_ShellTime" , 30 )
		RunConsoleCommand( "VNT_Base_SWep_BulletCraft_Toggle" , 1 )

		ply:ChatPrint( "[V92B] Base Console Commands & Settings Reset!" )

	end
	
end , "(Command) A command that when run will reset all client settings for the V92 base." , { FCVAR_CLIENTCMD_CAN_EXECUTE , FCVAR_UNLOGGED }  )

if CLIENT then

	---------------------------------------------
	---------------------------------------------
	--	Control Panel
	--	ToDo: Re-write to GM13 style
	---------------------------------------------
	---------------------------------------------
	local function vntCodeBasesOptions( Panel )
		Panel:ClearControls( )

		Panel:AddControl( "Header" , { 
			Text = "V92 Weapon Base Control Panel" , 
			Description = [[V92 Weapon Bases Control Panel
			This panel contains all the CVars and options in my weapons.
			If I could find a way to work it in , there's an option for it.
			
			If you have suggestions , bug reports , etc , leave them in the comments or preferably in my Steam Group:
			http://steamcommunity.com/groups/CultOfV92
			
			Bases by Reverend Jesse V92:
			http://steamcommunity.com/id/JesseVanover
			]]
		 }  )

		Panel:AddControl( "Button" , { 
			Label = "Print 'About' to Console" , 
			Command = "VNT_About"
		 }  )
		
		Panel:AddControl( "Button" , { 
			Label = "Reset V92 Base" , 
			Command = "VNT_Reset"
		 }  )
				
		Panel:AddControl( "Checkbox" , { 
			Label = "Toggle View Bob" , 
			Command = "VNT_Base_ViewBob_Toggle"
		 }  )
		
		Panel:AddControl( "Slider" , { 
			Label = "Stance Icon X Pos" , 
			Type = "Integer" , 
			Min = ( 0 ) , 
			Max = ( ScrW( ) ) , 
			Command = "VNT_Base_SWep_HUD_StanceX"
		 }  )

		Panel:AddControl( "Slider" , { 
			Label = "Stance Icon Y Pos" , 
			Type = "Integer" , 
			Min = ( 0 ) , 
			Max = ( ScrH( ) ) , 
			Command = "VNT_Base_SWep_HUD_StanceY"
		 }  )

		Panel:AddControl( "Slider" , { 
			Label = "Bipod Icon X Pos" , 
			Type = "Integer" , 
			Min = ( 0 ) , 
			Max = ( ScrW( ) ) , 
			Command = "VNT_Base_SWep_HUD_BipodX"
		 }  )

		Panel:AddControl( "Slider" , { 
			Label = "Bipod Icon Y Pos" , 
			Type = "Integer" , 
			Min = ( 0 ) , 
			Max = ( ScrH( ) ) , 
			Command = "VNT_Base_SWep_HUD_BipodY"
		 }  )

		Panel:AddControl( "Slider" , { 
			Label = "Stock Icon X Pos" , 
			Type = "Integer" , 
			Min = ( 0 ) , 
			Max = ( ScrW( ) ) , 
			Command = "VNT_Base_SWep_HUD_StockX"
		 }  )

		Panel:AddControl( "Slider" , { 
			Label = "Stock Icon Y Pos" , 
			Type = "Integer" , 
			Min = ( 0 ) , 
			Max = ( ScrH( ) ) , 
			Command = "VNT_Base_SWep_HUD_StockY"
		 }  )

		Panel:AddControl( "Slider" , { 
			Label = "GLM Icon X Pos" , 
			Type = "Integer" , 
			Min = ( 0 ) , 
			Max = ( ScrW( ) ) , 
			Command = "VNT_Base_SWep_HUD_GLMX"
		 }  )

		Panel:AddControl( "Slider" , { 
			Label = "GLM Icon Y Pos" , 
			Type = "Integer" , 
			Min = ( 0 ) , 
			Max = ( ScrH( ) ) , 
			Command = "VNT_Base_SWep_HUD_GLMX"
		 }  )

		Panel:AddControl( "Slider" , { 
			Label = "RoF Icon X Pos" , 
			Type = "Integer" , 
			Min = ( 0 ) , 
			Max = ( ScrW( ) ) , 
			Command = "VNT_Base_SWep_HUD_RoFX"
		 }  )

		Panel:AddControl( "Slider" , { 
			Label = "RoF Icon Y Pos" , 
			Type = "Integer" , 
			Min = ( 0 ) , 
			Max = ( ScrH( ) ) , 
			Command = "VNT_Base_SWep_HUD_RoFY"
		 }  )

		Panel:AddControl( "Slider" , { 
			Label = "Suppressor Icon X Pos" , 
			Type = "Integer" , 
			Min = ( 0 ) , 
			Max = ( ScrW( ) ) , 
			Command = "VNT_Base_SWep_HUD_SuppX"
		 }  )

		Panel:AddControl( "Slider" , { 
			Label = "Suppressor Icon Y Pos" , 
			Type = "Integer" , 
			Min = ( 0 ) , 
			Max = ( ScrH( ) ) , 
			Command = "VNT_Base_SWep_HUD_SuppY"
		 }  )

		Panel:AddControl( 	"Slider" , 		 { 	
			Label = "Message Location" , 	
			Type = "Integer" , 
			Min = 0 , 	
			Max = 2 , 	
			Command = "VNT_Base_SWep_PrintLocation"	 } 
		 )

		Panel:AddControl( "Checkbox" , { 
			Label = "Ironsight Hold or Toggle" , 
			Command = "VNT_Base_SWep_IronsightToggle"
		 }  )

		Panel:AddControl( "Slider" , { 
			Label = "Iron Sight Sensitivity" , 
			Command = "VNT_Base_SWep_IronSight_Sensitivity" , 
			Type = "float" , 
			Min = "0.1" , 
			Max = "1"
		 }  )

		Panel:AddControl( "Checkbox" , { 
			Label = "Jamming Toggle" , 
			Command = "VNT_Base_SWep_JamToggle"
		 }  )

		--	Removed - Per-Weapon Basis because it's better
		--[[
		Panel:AddControl( 	"Slider" , 		 { 	
			Label = "Jam Chance" , 	
			Type = "Integer" , 	
			Min = 10 , 	
			Max = 5000 , 	
			Command = "VNT_Base_SWep_JamChance"	 } 
		 )
		--]]

		Panel:AddControl( "Checkbox" , { 
			Label = "Shell Ejection Toggle" , 
			Command = "VNT_Base_SWep_ShellToggle"
		 }  )

		Panel:AddControl( 	"Slider" , 		 { 	
			Label = "Shell Lifetime" , 	
			Type = "Integer" , 
			Min = 5 , 	
			Max = 60 , 	
			Command = "VNT_Base_SWep_ShellTime"	 } 
		 )

		Panel:AddControl( "Checkbox" , { 
			Label = "Muzzle Flash Toggle" , 
			Command = "VNT_Base_SWep_FlashToggle"
		 }  )

		Panel:AddControl( "Checkbox" , { 
			Label = "Tracer Toggle" , 
			Command = "VNT_Base_SWep_TracerToggle"
		 }  )

		Panel:AddControl( "Checkbox" , { 
			Label = "Depth Mod Toggle" , 
			Command = "VNT_Base_SWep_DepthModToggle"
		 }  )

		Panel:AddControl( "Slider" , { 
			Label = "Recoil Multiplier" , 
			Command = "VNT_Base_SWep_RecoilMul" , 
			Type = "integer" , 
			Min = "1" , 
			Max = "5"
		 }  )

		 --[[
		Panel:AddControl( "Slider" , { 
			Label = "Accuracy Multiplier" , 
			Command = "VNT_Base_SWep_AccuracyMul" , 
			Type = "integer" , 
			Min = "1" , 
			Max = "3"
		 }  )
		 --]]

		Panel:AddControl( "Checkbox" , { 
			Label = "BulletCraft Toggle" , 
			Command = "VNT_Base_SWep_BulletCraft_Toggle"
		 }  )

		Panel:AddControl( "Checkbox" , { 
			Label = "Black Friday Toggle" , 
			Command = "VNT_Base_SWep_BlackFriday_Toggle"
		 }  )

		Panel:AddControl( "Slider" , { 
			Label = "Black Friday Multiplier" , 
			Command = "VNT_Base_SWep_BlackFriday_ForceMul" , 
			Type = "integer" , 
			Min = "1" , 
			Max = "5"
		 }  )

		Panel:AddControl( "Slider" , { 
			Label = "Black Friday Range" , 
			Command = "VNT_Base_SWep_BlackFriday_Range" , 
			Type = "integer" , 
			Min = "64" , 
			Max = "256"
		 }  )

		Panel:AddControl( "Checkbox" , { 
			Label = "Black Friday Reset Toggle" , 
			Command = "VNT_Base_SWep_BlackFriday_ResetToggle"
		 }  )

		Panel:AddControl( "Slider" , { 
			Label = "Black Friday Reset Timer" , 
			Command = "VNT_Base_SWep_BlackFriday_ResetTimer" , 
			Type = "integer" , 
			Min = "16" , 
			Max = "3600"
		 }  )

		 --[[
		Panel:AddControl( "Slider" , { 
			Label = "Damage Multiplier" , 
			Command = "VNT_Base_SWep_DamageMul" , 
			Type = "integer" , 
			Min = "1" , 
			Max = "10"
		 }  )
		 --]]

	end

	---------------------------------------------
	---------------------------------------------
	--	Add the tool menu
	---------------------------------------------
	---------------------------------------------
	local function vntWepBaseToolMenu( )
		spawnmenu.AddToolMenuOption( "Options" , "V92" , "VNTWepBaseControls" , "Weapon Base" , "" , "" , vntCodeBasesOptions )
	end
	hook.Add( "PopulateToolMenu" , "vntWepBaseToolMenu" , vntWepBaseToolMenu )
	
	---------------------------------------------
	---------------------------------------------
	--	Add the tool menu
	---------------------------------------------
	---------------------------------------------
	local function vntOptions( Panel )
		Panel:ClearControls( )

		Panel:AddControl( "Header" , { 
			Text = "V92VBConPan" , 
			Description = [[V92 Vehicle Bases Control Panel
			This panel contains all the CVars and options in my vehicles.
			If I could find a way to work it in , there's an option for it.
			
			If you have suggestions , bug reports , etc , leave them in the comments or preferably in my Steam Group:
			http://steamcommunity.com/groups/CultOfV92
			
			Bases by Reverend Jesse V92:
			http://steamcommunity.com/id/JesseVanover
			]]
		 }  )

		Panel:AddControl( "Button" , { 
			Label = "Print 'About' to Console" , 
			Command = "VNT_About"
		 }  )
		
		Panel:AddControl( "Button" , { 
			Label = "Reset V92 Base" , 
			Command = "VNT_Reset"
		 }  )

		Panel:AddControl( "Slider" , { 
			Label = "Shell Damage Multiplier" , 
			Command = "vnt_proj_dmg" , 
			Type = "float" , 
			Min = "0.1" , 
			Max = "10"
		 }  )

		Panel:AddControl( "Slider" , { 
			Label = "Shell Splash Radius Multiplier" , 
			Command = "vnt_proj_rad" , 
			Type = "float" , 
			Min = "0.1" , 
			Max = "10"
		 }  )
	end

	---------------------------------------------
	---------------------------------------------
	--	Add the Vehicle tool menu
	---------------------------------------------
	---------------------------------------------
	local function vntVehBaseToolMenu( )
		spawnmenu.AddToolMenuOption( "Options" , "V92" , "VNTVehBaseControls" , "Vehicle Base" , "" , "" , vntOptions )
	end
	hook.Add( "PopulateToolMenu" , "vntVehBaseToolMenu" , vntVehBaseToolMenu )
	
	---------------------------------------------
	---------------------------------------------
	--	CVar Clamps
	--	Prevent obnoxious values
	---------------------------------------------
	---------------------------------------------
	if GetConVarNumber( "VNT_Base_SWep_ClampToggle" ) != 1 then
	
		return
		
	else
	
		local CVarIconSize = GetConVarNumber( "VNT_Base_SWep_IconSize" )
		if CVarIconSize  <  0.1 then CVarIconSize = 0.1 elseif CVarIconSize  >  2 then CVarIconSize = 2 end
		
		local CVarShellRadMul = GetConVarNumber( "vnt_proj_rad" )
		local CVarShellDmgMul = GetConVarNumber( "vnt_proj_dmg" )
		if CVarShellRadMul  <  0.1 then CVarShellRadMul = 0.1 elseif CVarShellRadMul  >  10 then CVarShellRadMul = 10 end
		if CVarShellDmgMul  <  0.1 then CVarShellDmgMul = 0.1 elseif CVarShellDmgMul  >  10 then CVarShellDmgMul = 10 end
		local CVarPrintLoc = GetConVarNumber( "VNT_Base_SWep_PrintLocation" )
		if CVarPrintLoc  <  0 then CVarPrintLoc = 0 elseif CVarPrintLoc  >  2 then CVarPrintLoc = 2 end

		local CVarFlashTogg = GetConVarNumber( "VNT_Base_SWep_FlashToggle" )
		if CVarFlashTogg  <  0 then CVarFlashTogg = 0 elseif CVarFlashTogg  >  1 then CVarFlashTogg = 1 end

		local CVarTracerTogg = GetConVarNumber( "VNT_Base_SWep_TracerToggle" )
		if CVarTracerTogg  <  0 then CVarTracerTogg = 0 elseif CVarTracerTogg  >  1 then CVarTracerTogg = 1 end

		local CVarShellTogg = GetConVarNumber( "VNT_Base_SWep_ShellToggle" )
		if CVarShellTogg  <  0 then CVarShellTogg = 0 elseif CVarShellTogg  >  1 then CVarShellTogg = 1 end

		local CVarShellTime = GetConVarNumber( "VNT_Base_SWep_ShellTime" )
		if CVarShellTime  <  0 then CVarShellTime = 0 elseif CVarShellTime  >  60 then CVarShellTime = 60 end

		local CVarRunSpeed = GetConVarNumber( "VNT_Base_SWep_Speed_Run" )
		if CVarRunSpeed  <  25 then CVarRunSpeed = 25 elseif CVarRunSpeed  >  500 then CVarRunSpeed = 500 end

		local CVarWalkSpeed = GetConVarNumber( "VNT_Base_SWep_Speed_Walk" )
		if CVarWalkSpeed  <  25 then CVarWalkSpeed = 25 elseif CVarWalkSpeed  >  250 then CVarWalkSpeed = 250 end

		local CVarRecoilMul = GetConVarNumber( "VNT_Base_SWep_RecoilMul" )
		if CVarRecoilMul  <  1 then CVarRecoilMul = 1 elseif CVarRecoilMul  >  5 then CVarRecoilMul = 5 end

		local CVarAccMul = GetConVarNumber( "VNT_Base_SWep_AccuracyMul" )
		if CVarAccMul  <  1 then CVarAccMul = 1 elseif CVarAccMul  >  5 then CVarAccMul = 5 end

		local CVarDmgMul = GetConVarNumber( "VNT_Base_SWep_DamageMul" )
		if CVarDmgMul  <  1 then CVarDmgMul = 1 elseif CVarDmgMul  >  15 then CVarDmgMul = 15 end

		local CVarBlackFridayTimeClamp = GetConVarNumber( "VNT_Base_SWep_BlackFriday_ResetTimer" )
		if CVarBlackFridayTimeClamp  <  10 then CVarBlackFridayTimeClamp = 10 end

		local CVarHUDDeployX = GetConVarNumber( "VNT_Base_SWep_HUD_DeployX" )
		if CVarHUDDeployX  <  1 then CVarHUDDeployX = 1 elseif CVarHUDDeployX  >  ScrW( ) then CVarHUDDeployX = ScrW( ) end

		local CVarHUDDeployY = GetConVarNumber( "VNT_Base_SWep_HUD_DeployY" )
		if CVarHUDDeployY  <  1 then CVarHUDDeployY = 1 elseif CVarHUDDeployY  >  ScrH( ) then CVarHUDDeployY = ScrH( ) end

		local CVarHUDRoFX = GetConVarNumber( "VNT_Base_SWep_HUD_RoFX" )
		if CVarHUDRoFX  <  1 then CVarHUDRoFX = 1 elseif CVarHUDRoFX  >  ScrW( ) then CVarHUDRoFX = ScrW( ) end

		local CVarHUDRoFY = GetConVarNumber( "VNT_Base_SWep_HUD_RoFY" )
		if CVarHUDRoFY  <  1 then CVarHUDRoFY = 1 elseif CVarHUDRoFY  >  ScrH( ) then CVarHUDRoFY = ScrH( ) end

		local CVarHUDSupX = GetConVarNumber( "VNT_Base_SWep_HUD_SuppX" )
		if CVarHUDSupX  <  1 then CVarHUDSupX = 1 elseif CVarHUDSupX  >  ScrW( ) then CVarHUDSupX = ScrW( ) end

		local CVarHUDSupY = GetConVarNumber( "VNT_Base_SWep_HUD_SuppY" )
		if CVarHUDSupY  <  1 then CVarHUDSupY = 1 elseif CVarHUDSupY  >  ScrH( ) then CVarHUDSupY = ScrH( ) end

		local CVarRicLimit = GetConVarNumber( "VNT_Base_SWep_RichochetLimit" )
		if CVarRicLimit  <  1 then CVarRicLimit = 1 elseif CVarRicLimit  >  10 then CVarRicLimit = 10 end

		local CVarSpawnUnloaded = GetConVarNumber( "VNT_Base_SWep_SpawnUnloaded" )
		if CVarSpawnUnloaded  <  0 then CVarSpawnUnloaded = 0 elseif CVarSpawnUnloaded  >  1 then CVarSpawnUnloaded = 1 end

		local CVarSpawnAmmo = GetConVarNumber( "VNT_Base_SWep_SpawnAmmo" )
		if CVarSpawnAmmo  <  0 then CVarSpawnAmmo = 0 elseif CVarSpawnAmmo  >  1 then CVarSpawnAmmo = 1 end

		local CVarDepth = GetConVarNumber( "VNT_Base_SWep_DepthModToggle" )
		if CVarDepth  <  0 then CVarDepth = 0 elseif CVarDepth  >  1 then CVarDepth = 1 end
	end
	
	---------------------------------------------
	---------------------------------------------
	--	Headshot Effect
	--	Play an effect when the player is killed via 
	--	headshot like in CS:S if it's mounted
	--	Fun fact: this was the first part of the base
	---------------------------------------------
	---------------------------------------------
	if IsMounted( "cstrike" ) then
		local function VNTHeadShotNotifyScript( _P )
			if _P:LastHitGroup( ) ==  1 then
				_P:EmitSound( "CSS.Player.Headshot" )
			end
		end
		hook.Add( "DoPlayerDeath" , "VNTHeadShotNotifyScript" , VNTHeadShotNotifyScript )
	end

	local function GetMaterialType( )
	
		local TraceResult = util.TraceLine( { 
			start 	 = LocalPlayer( ) , 
			endpos 	 = LocalPlayer( ):GetAimVector( ) , 
		 }  )

		print( "Material type is: " .. TraceResult.MatType )
		
	end
	concommand.Add( "getmattype" , GetMaterialType )

elseif SERVER then

	---------------------------------------------
	---------------------------------------------
	--	If by some diabolis ex machina reason the addon isn't fully downloaded , download my base this way just in case...
	---------------------------------------------
	---------------------------------------------
	resource.AddWorkshop( "505106454" ) -- V92 Content Bases

	---------------------------------------------
	---------------------------------------------
	--	Use Animations
	--	When you interact with an object , play a world anim
	---------------------------------------------
	---------------------------------------------
	local function UseAnimations( ply )
		ply:SetAnimation( ACT_GMOD_GESTURE_ITEM_GIVE )
	end
	hook.Add( "PlayerUse" , "UseAnimations" , UseAnimations )

	---------------------------------------------
	---------------------------------------------
	--	Drop Weapon script , for non-V92 base weapons
	---------------------------------------------
	---------------------------------------------
	local function Drop( ply )
		ply:DoAnimationEvent( ACT_GMOD_GESTURE_ITEM_DROP )
		ply:DropWeapon( ply:GetActiveWeapon( ) )
	end
	concommand.Add( "Drop" , Drop )

	---------------------------------------------
	---------------------------------------------
	--	Disable suicide script, for preventing the use of 'kill' or my 'ninjadeath' command
	---------------------------------------------
	---------------------------------------------
	local function VNTSuicideToggle( ply )

		if GetConVarNumber( "VNT_Suicide_Disabled" ) == 1 then

			if ply:IsPlayer( ) then

				ply:ChatPrint( "Sorry, suiciding is currently disabled!" )

				return false

			end

		end

	end
	hook.Add("CanPlayerSuicide", "VNTSuicideToggle", VNTSuicideToggle)

	---------------------------------------------
	---------------------------------------------
	--	Blank SWep Giving Hook
	--	Gives a blank , unusable , hidden SWep that allows the model swap system to work correctly
	---------------------------------------------
	---------------------------------------------

	local function VNTSBGiveBlank( ply )
		ply:Give( "v92_int_blank" )
	end
	hook.Add( "PlayerSpawn" , "VNTSBGiveBlank" , VNTSBGiveBlank )
	
	---------------------------------------------
	---------------------------------------------
	--	Ignore Table
	--	Copied from Zoey who copied from Kilburn
	---------------------------------------------
	---------------------------------------------
	
	local meta = FindMetaTable( "Entity" )
	if ( not meta ) then return end

	local TriggerEntities = { 
		trigger_autosave = true , 
		trigger_changelevel = true , 
		trigger_finale = true , 
		trigger_gravity = true , 
		trigger_hurt = true , 
		trigger_impact = true , 
		trigger_look = true , 
		trigger_multiple = true , 
		trigger_once = true , 
		trigger_physics_trap = true , 
		trigger_playermovement = true , 
		trigger_proximity = true , 
		trigger_push = true , 
		trigger_remove = true , 
		trigger_rpgfire = true , 
		trigger_soundscape = true , 
		trigger_serverragdoll = true , 
		trigger_soundscape = true , 
		trigger_teleport = true , 
		trigger_transition = true , 
		trigger_vphysics_motion = true , 
		trigger_waterydeath = true , 
		trigger_weapon_dissolve = true , 
		trigger_weapon_strip = true , 
		trigger_wind = true , 
		func_occluder = true , 
		func_precipitation = true , 
		func_smokevolume = true , 
		func_vehicleclip = true , 
		func_areaportal = true , 
		func_areaportalwindow = true , 
		func_dustcloud = true , 
		point_hurt = true , 
		ambient_generic = true , 
		env_steam = true , 
		func_button = true , 
		npc_r = true , 
		npc_template_r = true , 
		env_smokestack = true , 
		item_battery = true , 
		item_healthvial = true , 
		item_healthkit = true , 
		weapon_pistol = true , 
		weapon_357 = true , 
		weapon_ar2 = true , 
		weapon_crossbow = true , 
		weapon_smg1 = true , 
		weapon_frag = true , 
		weapon_stunstick = true , 
		weapon_crowbar = true , 
		weapon_rpg = true , 
		weapon_slam = true , 
		weapon_shotgun = true , 
		func_door_rotating = true , 
		spotlight_end = true , 
		func_door = true , 
		assault_assaultpoint = true
	 } 

	function meta:IsTrigger( )
		if TriggerEntities[ self:GetClass( ) ] then return true end

		return false
	end

	---------------------------------------------
	---------------------------------------------
	--	RT Scope Skybox Fix
	--	If a map doesn't have a 3D skybox , the scope won't work , so create a sky_camera
	---------------------------------------------
	---------------------------------------------

	-- hook.Add( "InitPostEntity" , "RTScopeFix" , function( ) --	InitPostEntity hook , add "RTScopeFix" , Function:

		-- if #ents.FindByClass( "sky_camera" ) <= 0 then --	If the number of "sky_camera" entities <= 0 then

			-- local skycam = ents.Create( "sky_camera" ) --	Create one
			-- skycam:Spawn( )
			-- skycam:SetPos( skycam:GetPos( ) + Vector( 0 , 0 , 500 ) )
			-- skycam:Fire( "3D Skybox Scale" , 0 )
			
		-- end
		
	-- end )
	

end

---------------------------------------------
---------------------------------------------
--	WEAPON BASE AUTORUN CODE
---------------------------------------------
---------------------------------------------

properties.Add( "VNTSB_DisableBlackFriday", 
{
	MenuLabel	=	"Disable Black Friday on this Door",
	Order		=	1985,
	MenuIcon	=	"icon16/door.png",
	
	Filter		=	function( self, ent, ply ) 
						if ( !IsValid( ent ) or !gamemode.Call( "CanProperty", ply, name, ent ) ) then
						
							return false 
							
						else
						
							if ( ent:GetClass() == "prop_door_rotating" ) or ( ent:GetClass() == "func_door_rotating" ) then 
							
								if ent:GetNWBool("VNTSB_DisableBlackFriday") == false then
								
									return true
									
								else
									
									return false
									
								end
								
							end
							
						end
						
					end,
					
	Action		=	function( self, ent )
	
						self:MsgStart()
							net.WriteEntity( ent )
						self:MsgEnd()
						
					end,
					
	Receive		=	function( self, length, player )
					
						local ent = net.ReadEntity()
						if ( !self:Filter( ent, player ) ) then return end
	
						ent:SetNWBool("VNTSB_DisableBlackFriday",true)
					end	

})


properties.Add( "VNTSB_EnableBlackFriday", 
{
	MenuLabel	=	"Enable Black Friday on this Door",
	Order		=	1985,
	MenuIcon	=	"icon16/door_open.png",
	
	Filter		=	function( self, ent, ply ) 
						if ( !IsValid( ent ) or !gamemode.Call( "CanProperty", ply, name, ent ) ) then
							return false 
							
						else
						
							if ( ent:GetClass() == "prop_door_rotating" ) or ( ent:GetClass() == "func_door_rotating" ) then 
							
								if ent:GetNWBool("VNTSB_DisableBlackFriday") == true then
								
									return true
									
								else
									
									return false
									
								end
								
							end
							
						end
						
					end,
					
	Action		=	function( self, ent )
	
						self:MsgStart()
							net.WriteEntity( ent )
						self:MsgEnd()
						
					end,
					
	Receive		=	function( self, length, player )
					
						local ent = net.ReadEntity()
						if ( !self:Filter( ent, player ) ) then return end
	
						ent:SetNWBool("VNTSB_DisableBlackFriday",false)

					end	

})

if CLIENT then

	local function inOutQuart(t, b, c, d)

		if GetConVarNumber( "VNT_Base_ViewBob_Toggle" ) == 1 and LocalPlayer():GetActiveWeapon().Base == VNT_BASE_WEAPON then

			t = t / d * 2
			if t < 1 then
				return c / 2 * math.pow(t, 4) + b
			else
				t = t - 2
				return -c / 2 * (math.pow(t, 4) - 2) + b
			end
			
		end
		
	end

	local function TrnView( ply, origin, angles, fov )
		
		if GetConVarNumber( "VNT_Base_ViewBob_Toggle" ) == 1 and LocalPlayer():GetActiveWeapon().Base == VNT_BASE_WEAPON then

			if ply:GetMoveType() == 8 then
				return
			else
		
			local view = {}
		
			local ang = ply:EyeAngles()
			local vel = ply:GetVelocity()
			local velr = Vector(ang:Forward():DotProduct(vel),ang:Right():DotProduct(vel),ang:Up():DotProduct(vel))
		
			view.origin = origin
			view.ply = ply
			view.angles = angles
		
			local viewbob = math.atan(math.sin(math.sin(CurTime()*20)))*velr:Length()*0.01
			local cos1 = math.cos(CurTime() * 7.5)
			local cos2 = math.cos(CurTime() * 5)
		
			view.origin.z = origin.z + viewbob
			view.angles.p = angles.p + cos1 * 0.10 *velr:Length()*0.01
			view.angles.y = angles.y + cos2 * 0.10 *velr:Length()*0.01
		
			--return view
		
			end
			
		end
		
	end

	local function TrnWepView(wep, vm, oldPos, oldAng, pos, ang)
		
		if GetConVarNumber( "VNT_Base_ViewBob_Toggle" ) == 1 and LocalPlayer():GetActiveWeapon().Base == VNT_BASE_WEAPON then

			if LocalPlayer():GetMoveType() == 8 then
				return
			else
			local view = {}
		
			local ang = LocalPlayer():EyeAngles()
			local vel = LocalPlayer():GetVelocity()
			local velr = Vector(ang:Forward():DotProduct(vel),ang:Right():DotProduct(vel),ang:Up():DotProduct(vel))
		

			view.oldPos = oldPos
			view.oldAng = oldAng
			view.pos = pos
			view.ang = ang
		
			local viewbob = math.atan(math.sin(math.sin(CurTime()*20)))*velr:Length()*0.0095
		
			view.pos.z = pos.z + viewbob
			
			--if(velr.x>=150) then
				--if(curtimecheck == 0) then
					--curtimestore = CurTime()
					--curtimecheck = 1
				--end
			--print(CurTime()-curtimestore)
			
			--view.pos.y = pos.y + inOutQuart(math.Clamp(CurTime()-curtimestore,0,1),0,2 ,1)
			--else
				--curtimecheck = 0
			--end
		
			--return view
			end
			
		end
		
	end
	
	hook.Add("CalcView", "TrnViewHook", TrnView)
	hook.Add("CalcViewModelView", "TrnWepViewHook", TrnWepView)

end

if SERVER then

	-- local function DepleteFilters( ply, maskStatus )

		-- if IsValid( ply ) then

			-- print( "filter deplete: ply valid" )

			-- maskStatus = ply:GetNWBool( "PlayerItemSlot_" .. ItemGearSlot .. "_On" )

			-- if maskStatus then
				
				-- print( "filter deplete: mask on" )

				-- local TriggerEnt = "trigger_hurt"
				-- local HurtEnt = "POINT_HURT"
				
				-- if owner:TakeDamage( dmgInfo )then

					-- print( "filter deplete: took damage" )

					-- if dmgInfo:IsDamageType( table.HasValue( self.Protections ) ) then

						-- print( "filter deplete: triggered" )

						-- TriggerEnt:SetDamage( 0 )
						-- HurtEnt:SetDamage( 0 )

					-- end

				-- end		

				-- timer.Simple( 1 , function( )

					-- if IsValid( ply ) then

						-- FilterCallBack( ply , maskStatus )

					-- end

				-- end )

			-- end

		-- end
		
		-- return false

	-- end
	
	-- local function FilterCallBack( ply , maskStatus )

		-- if IsValid( ply ) and maskStatus then

			-- ply:RemoveAmmo( 1 , "gasmaskfilters" )

		-- end

		-- return false

	-- end

	local function VNT_Parachute_Move( ply , movedata )

		local MaxFallSpeed = 200 -- Maximum speed to drop at. Set to 500 or less if you don't want fall damage
		local SlowDownSpeed = 50 -- Speed to slow descent at
		local velocity = movedata:GetVelocity( )

		if not ply:IsOnGround( ) then

			if ply:GetNWInt( "V92ParachutingPhase" ) == 1 then

				if ( ( velocity.z * -1 ) > MaxFallSpeed ) then

					movedata:SetVelocity( velocity + Vector( 0 , 0 , SlowDownSpeed ) )

				end
		
			end

		end

	end
	hook.Add( "SetupMove" , "VNT_Parachute_Move" , VNT_Parachute_Move )

end

---------------------------------------------
---------------------------------------------
--	WEAPON SLOTS
---------------------------------------------
---------------------------------------------

VNT_WEAPON_SLOT_OTHER = 0
VNT_WEAPON_SLOT_PRIMARY = 1
VNT_WEAPON_SLOT_SIDEARM = 2
VNT_WEAPON_SLOT_MELEE = 3
VNT_WEAPON_SLOT_GRENADE1 = 4
VNT_WEAPON_SLOT_GRENADE2 = 5
VNT_WEAPON_SLOT_UTILITY = 6
VNT_WEAPON_SLOT_SPECIALTY = 7
VNT_WEAPON_SLOT_CLOTHING = 8
VNT_WEAPON_SLOT_NONE = 92

---------------------------------------------
---------------------------------------------
--	WEAPON ATTACHMENT SLOTS
---------------------------------------------
---------------------------------------------

VNT_WEAPON_ATTACHMENT_OTHER = 0
VNT_WEAPON_ATTACHMENT_UNIQUE = 1
VNT_WEAPON_ATTACHMENT_SUPPRESSOR = 2
VNT_WEAPON_ATTACHMENT_UNDERBARREL = 3
VNT_WEAPON_ATTACHMENT_LASER = 4
VNT_WEAPON_ATTACHMENT_RAILLEFT = 5
VNT_WEAPON_ATTACHMENT_RAILRIGHT = 6
VNT_WEAPON_ATTACHMENT_BARREL = 7
VNT_WEAPON_ATTACHMENT_MAGAZINE = 8
VNT_WEAPON_ATTACHMENT_OPTIC = 9
VNT_WEAPON_ATTACHMENT_STOCK = 10
VNT_WEAPON_ATTACHMENT_GRIP = 11
VNT_WEAPON_ATTACHMENT_FOREGRIP = 12

---------------------------------------------
---------------------------------------------
--	CLOTHING/GEAR SLOTS
---------------------------------------------
---------------------------------------------

VNT_GEAR_SLOT_OTHER = 0
VNT_GEAR_SLOT_NONE = 1

VNT_GEAR_SLOT_MASK = 11
VNT_GEAR_SLOT_HAT = 12
VNT_GEAR_SLOT_NECK = 13
VNT_GEAR_SLOT_EYES = 14
VNT_GEAR_SLOT_MOUTH = 15

VNT_GEAR_SLOT_BODY = 21
VNT_GEAR_SLOT_BACK = 22
VNT_GEAR_SLOT_TORSO = 23
VNT_GEAR_SLOT_BELT = 24
VNT_GEAR_SLOT_LEGS = 25
VNT_GEAR_SLOT_CHEST = 26

VNT_GEAR_SLOT_LEFT_BICEP = 31
VNT_GEAR_SLOT_LEFT_FOREARM = 32
VNT_GEAR_SLOT_LEFT_HAND = 33
VNT_GEAR_SLOT_LEFT_FINGERS = 34
VNT_GEAR_SLOT_LEFT_GLOVE = 35

VNT_GEAR_SLOT_RIGHT_BICEP = 41
VNT_GEAR_SLOT_RIGHT_FOREARM = 42
VNT_GEAR_SLOT_RIGHT_HAND = 43
VNT_GEAR_SLOT_RIGHT_FINGERS = 44
VNT_GEAR_SLOT_RIGHT_GLOVE = 45

VNT_GEAR_SLOT_LEFT_THIGH = 51
VNT_GEAR_SLOT_LEFT_KNEE = 52
VNT_GEAR_SLOT_LEFT_CALF = 53
VNT_GEAR_SLOT_LEFT_FOOT = 54
VNT_GEAR_SLOT_LEFT_TOE = 55

VNT_GEAR_SLOT_RIGHT_THIGH = 61
VNT_GEAR_SLOT_RIGHT_KNEE = 62
VNT_GEAR_SLOT_RIGHT_CALF = 63
VNT_GEAR_SLOT_RIGHT_FOOT = 64
VNT_GEAR_SLOT_RIGHT_TOE = 65

-- VNT_GEAR_SLOT_ = 

---------------------------------------------
---------------------------------------------
--	PROJECTILE ENTITIES
---------------------------------------------
---------------------------------------------

VNT_PROJECTILE_GRENADE_NATO = "v92_proj_40x46mm"
VNT_PROJECTILE_GRENADE_WARSAW = "v92_proj_vog25"
VNT_PROJECTILE_ROCKET_BAZOOKA_ARMYMEN = "v92_proj_am_bazrkt"
VNT_PROJECTILE_ROCKET_BAZOOKA_DODS = "v92_proj_m6a1_heat"
VNT_PROJECTILE_ROCKET_M79_HALO = "v92_proj_m79rocket"
VNT_PROJECTILE_ROCKET_STINGER = "v92_proj_stinger"
VNT_PROJECTILE_ROCKET_TOW = "v92_proj_tow"
VNT_PROJECTILE_ROCKET_ZUNI = "v92_proj_zuni"
VNT_PROJECTILE_MISSILE = "v92_proj_missile"
VNT_PROJECTILE_SHELL_20MM = "v92_proj_20mm"
VNT_PROJECTILE_SHELL_25MM = "v92_proj_25mm"
VNT_PROJECTILE_SHELL_37MM = "v92_proj_37mm"
VNT_PROJECTILE_SHELL_40MM = "v92_proj_40mm"
VNT_PROJECTILE_SHELL_50MM = "v92_proj_50mm"
VNT_PROJECTILE_SHELL_76MM = "v92_proj_76mm"
VNT_PROJECTILE_SHELL_88MM = "v92_proj_88mm"
VNT_PROJECTILE_SHELL_90MM = "v92_proj_90mm"
VNT_PROJECTILE_SHELL_100MM = "v92_proj_100mm"
VNT_PROJECTILE_SHELL_105MM = "v92_proj_105mm"
VNT_PROJECTILE_SHELL_120MM = "v92_proj_120mm"
VNT_PROJECTILE_SHELL_125MM = "v92_proj_125mm"
VNT_PROJECTILE_SHELL_128MM = "v92_proj_128mm"
VNT_PROJECTILE_SHELL_150MM = "v92_proj_150mm"
VNT_PROJECTILE_SHELL_155MM = "v92_proj_155mm"
VNT_PROJECTILE_SHELL_M68_HALO = "v92_proj_m68shell"

---------------------------------------------
---------------------------------------------
--	Weapon Types
---------------------------------------------
---------------------------------------------

VNTCB.WeaponType = VNTCB.WeaponType or {
	["Melee"] = 1 ,
	["Pistol"] = 2 ,
	["Revolver"] = 2 ,
	["Dual"] = 2 ,
	["SMG"] = 3 ,
	["Carbine"] = 3 ,
	["Rifle"] = 3 ,
	["LMG"] = 3 ,
	["Shotgun"] = 4 ,
	["Sniper"] = 5 ,
	["GLauncher"] = 6 ,
	["RLauncher"] = 7 ,
	["Support"] = 8 ,
	["Thrown"] = 9 ,
}

---------------------------------------------
---------------------------------------------
--	Weapon Hold Types
---------------------------------------------
---------------------------------------------

VNTCB.HoldType = VNTCB.HoldType or {
	["Normal"] = { -- 0 = Name
		"normal" , -- 1 = Standard
		"normal" , -- 2 = Ironsighted
		"normal" , -- 3 = Crouched
		"normal" , -- 4 = Crouched Ironsighted
		"normal" , -- 5 = Reloading
		"normal" , -- 6 = Crouched Reloading
		"normal" , -- 7 = Prone
		"normal" , -- 8 = Holstered
		"normal" , -- 9 = Holstered Crouched
	} ,
	["Fists"] = { -- 0 = Name
		"fists" , -- 1 = Standard
		"fists" , -- 2 = Ironsighted
		"fists" , -- 3 = Crouched
		"fists" , -- 4 = Crouched Ironsighted
		"fists" , -- 5 = Reloading
		"fists" , -- 6 = Crouched Reloading
		"fists" , -- 7 = Prone
		"normal" , -- 8 = Holstered
		"normal" , -- 9 = Holstered Crouched
	} ,
	["Melee"] = { -- 0 = Name
		"melee" , -- 1 = Standard
		"melee2" , -- 2 = Ironsighted
		"melee" , -- 3 = Crouched
		"melee2" , -- 4 = Crouched Ironsighted
		"melee2" , -- 5 = Reloading
		"melee2" , -- 6 = Crouched Reloading	
		"melee" , -- 7 = Prone
		"normal" , -- 8 = Holstered
		"normal" , -- 9 = Holstered Crouched
	} ,
	["Sword"] = { -- 0 = Name
		"melee2" , -- 1 = Standard
		"melee2" , -- 2 = Ironsighted
		"melee2" , -- 3 = Crouched
		"melee2" , -- 4 = Crouched Ironsighted
		"melee2" , -- 5 = Reloading
		"melee2" , -- 6 = Crouched Reloading	
		"melee" , -- 7 = Prone
		"normal" , -- 8 = Holstered
		"normal"  , -- 9 = Holstered Crouched
	} ,
	["Knife"] = { -- 0 = Name
		"knife" , -- 1 = Standard
		"knife" , -- 2 = Ironsighted
		"knife" , -- 3 = Crouched
		"knife" , -- 4 = Crouched Ironsighted
		"knife" , -- 5 = Reloading
		"knife" , -- 6 = Crouched Reloading
		"knife" , -- 7 = Prone
		"normal" , -- 8 = Holstered
		"normal"  , -- 9 = Holstered Crouched
	} ,
	["Pistol"] = { -- 0 = Name
		"pistol" , -- 1 = Standard
		"revolver" , -- 2 = Ironsighted
		"revolver" , -- 3 = Crouched
		"pistol" , -- 4 = Crouched Ironsighted
		"pistol" , -- 5 = Reloading
		"pistol" , -- 6 = Crouched Reloading
		"pistol" , -- 7 = Prone
		"normal" , -- 8 = Holstered
		"normal"  , -- 9 = Holstered Crouched
	} ,
	["Revolver"] = { -- 0 = Name
		"pistol" , -- 1 = Standard
		"revolver" , -- 2 = Ironsighted
		"revolver" , -- 3 = Crouched
		"pistol" , -- 4 = Crouched Ironsighted
		"revolver" , -- 5 = Reloading
		"revolver" , -- 6 = Crouched Reloading
		"pistol" , -- 7 = Prone
		"normal" , -- 8 = Holstered
		"normal" , -- 9 = Holstered Crouched
	} ,
	["C96"] = { -- 0 = Name
		"pistol" , -- 1 = Standard
		"rpg" , -- 2 = Ironsighted
		"rpg" , -- 3 = Crouched
		"pistol" , -- 4 = Crouched Ironsighted
		"pistol" , -- 5 = Reloading
		"pistol" , -- 6 = Crouched Reloading
		"pistol" , -- 7 = Prone
		"normal" , -- 8 = Holstered
		"normal" , -- 9 = Holstered Crouched
	} ,
	["Dual"] = { -- 0 = Name
		"duel" , -- 1 = Standard
		"duel" , -- 2 = Ironsighted
		"duel" , -- 3 = Crouched
		"duel" , -- 4 = Crouched Ironsighted
		"duel" , -- 5 = Reloading
		"duel" , -- 6 = Crouched Reloading
		"duel" , -- 7 = Prone
		"normal" , -- 8 = Holstered
		"normal" , -- 9 = Holstered Crouched
	} ,
	["ForeGrip"] = { -- 0 = Name
		"smg" , -- 1 = Standard
		"smg" , -- 2 = Ironsighted
		"smg" , -- 3 = Crouched
		"smg" , -- 4 = Crouched Ironsighted
		"smg" , -- 5 = Reloading
		"smg" , -- 6 = Crouched Reloading
		"smg" , -- 7 = Prone
		"passive" , -- 8 = Holstered
		"normal" , -- 9 = Holstered Crouched
	} , 
	["HL2SMG"] = { -- 0 = Name
		"smg" , -- 1 = Standard
		"smg" , -- 2 = Ironsighted
		"smg" , -- 3 = Crouched
		"smg" , -- 4 = Crouched Ironsighted
		"pistol" , -- 5 = Reloading
		"pistol" , -- 6 = Crouched Reloading
		"smg" , -- 7 = Prone
		"passive" , -- 8 = Holstered
		"normal" , -- 9 = Holstered Crouched
	} , 
	["SMG"] = { -- 0 = Name
		"smg" , -- 1 = Standard
		"ar2" , -- 2 = Ironsighted
		"smg" , -- 3 = Crouched
		"ar2" , -- 4 = Crouched Ironsighted
		"smg" , -- 5 = Reloading
		"ar2" , -- 6 = Crouched Reloading
		"ar2" , -- 7 = Prone
		"passive" , -- 8 = Holstered
		"normal" , -- 9 = Holstered Crouched
	} , 
	["Carbine"] = { -- 0 = Name
		"ar2" , -- 1 = Standard
		"ar2" , -- 2 = Ironsighted
		"ar2" , -- 3 = Crouched
		"ar2" , -- 4 = Crouched Ironsighted
		"ar2" , -- 5 = Reloading
		"ar2" , -- 6 = Crouched Reloading
		"ar2" , -- 7 = Prone
		"passive" , -- 8 = Holstered
		"normal" , -- 9 = Holstered Crouched
	} , 
	["Rifle"] = { -- 0 = Name
		"ar2" , -- 1 = Standard
		"rpg" , -- 2 = Ironsighted
		"shotgun" , -- 3 = Crouched
		"ar2" , -- 4 = Crouched Ironsighted
		"ar2" , -- 5 = Reloading
		"ar2" , -- 6 = Crouched Reloading
		"ar2" , -- 7 = Prone
		"passive" , -- 8 = Holstered
		"normal" , -- 9 = Holstered Crouched
	} , 
	["LMG"] = { -- 0 = Name
		"ar2" , -- 1 = Standard
		"rpg" , -- 2 = Ironsighted
		"ar2" , -- 3 = Crouched
		"rpg" , -- 4 = Crouched Ironsighted
		"ar2" , -- 5 = Reloading
		"ar2" , -- 6 = Crouched Reloading
		"ar2" , -- 7 = Prone
		"passive" , -- 8 = Holstered
		"normal" , -- 9 = Holstered Crouched
	} , 	
	["M1919"] = { -- 0 = Name
		"shotgun" , -- 1 = Standard
		"ar2" , -- 2 = Ironsighted
		"shotgun" , -- 3 = Crouched
		"ar2" , -- 4 = Crouched Ironsighted
		"smg" , -- 5 = Reloading
		"smg" , -- 6 = Crouched Reloading
		"pistol" , -- 7 = Prone
		"passive" , -- 8 = Holstered
		"shotgun" , -- 9 = Holstered Crouched
	} , 
	["Shotgun"] = { -- 0 = Name
		"shotgun" , -- 1 = Standard
		"ar2" , -- 2 = Ironsighted
		"shotgun" , -- 3 = Crouched
		"ar2" , -- 4 = Crouched Ironsighted
		"shotgun" , -- 5 = Reloading
		"shotgun" , -- 6 = Crouched Reloading
		"shotgun" , -- 7 = Prone
		"passive" , -- 8 = Holstered
		"normal" , -- 9 = Holstered Crouched
	} , 
	["Sniper"] = { -- 0 = Name
		"ar2" , -- 1 = Standard
		"rpg" , -- 2 = Ironsighted
		"ar2" , -- 3 = Crouched
		"ar2" , -- 4 = Crouched Ironsighted
		"ar2" , -- 5 = Reloading
		"ar2" , -- 6 = Crouched Reloading
		"ar2" , -- 7 = Prone
		"passive" , -- 8 = Holstered
		"normal" , -- 9 = Holstered Crouched
	} , 
	["GLauncher"] = { -- 0 = Name
		"ar2" , -- 1 = Standard
		"rpg" , -- 2 = Ironsighted
		"ar2" , -- 3 = Crouched
		"rpg" , -- 4 = Crouched Ironsighted
		"shotgun" , -- 5 = Reloading
		"shotgun" , -- 6 = Crouched Reloading
		"ar2" , -- 7 = Prone
		"passive" , -- 8 = Holstered
		"normal" , -- 9 = Holstered Crouched
	} , 
	["GLauncherForeGrip"] = { -- 0 = Name
		"smg" , -- 1 = Standard
		"rpg" , -- 2 = Ironsighted
		"smg" , -- 3 = Crouched
		"rpg" , -- 4 = Crouched Ironsighted
		"shotgun" , -- 5 = Reloading
		"shotgun" , -- 6 = Crouched Reloading
		"smg" , -- 7 = Prone
		"passive" , -- 8 = Holstered
		"normal" , -- 9 = Holstered Crouched
	} , 
	["RLauncher"] = { -- 0 = Name
		"rpg" , -- 1 = Standard
		"rpg" , -- 2 = Ironsighted
		"rpg" , -- 3 = Crouched
		"rpg" , -- 4 = Crouched Ironsighted
		"rpg" , -- 5 = Reloading
		"rpg" , -- 6 = Crouched Reloading
		"rpg" , -- 7 = Prone
		"passive" , -- 8 = Holstered
		"normal" , -- 9 = Holstered Crouched
	} , 
	["Support"] = { -- 0 = Name
		"slam" , -- 1 = Standard
		"slam" , -- 2 = Ironsighted
		"slam" , -- 3 = Crouched
		"slam" , -- 4 = Crouched Ironsighted
		"fists" , -- 5 = Reloading
		"fists" , -- 6 = Crouched Reloading
		"slam" , -- 7 = Prone
		"normal" , -- 8 = Holstered
		"normal" , -- 9 = Holstered Crouched
	} , 
	["Grenade"] = { -- 0 = Name
		"grenade" , -- 1 = Standard
		"grenade" , -- 2 = Ironsighted
		"grenade" , -- 3 = Crouched
		"grenade" , -- 4 = Crouched Ironsighted
		"grenade" , -- 5 = Reloading
		"grenade" , -- 6 = Crouched Reloading
		"grenade" , -- 7 = Prone
		"normal" , -- 8 = Holstered
		"normal" , -- 9 = Holstered Crouched
	} ,
}

---------------------------------------------
---------------------------------------------
--	BUCKET POSITIONS
---------------------------------------------
---------------------------------------------

VNT_WEAPON_BUCKETPOS_MELEE = 0
VNT_WEAPON_BUCKETPOS_TOOL = 0
VNT_WEAPON_BUCKETPOS_UTILITY = 0
VNT_WEAPON_BUCKETPOS_PISTOL = 1
VNT_WEAPON_BUCKETPOS_REVOLVER = 1
VNT_WEAPON_BUCKETPOS_DUAL = 1
VNT_WEAPON_BUCKETPOS_SMG = 2
VNT_WEAPON_BUCKETPOS_CARBINE = 2
VNT_WEAPON_BUCKETPOS_RIFLE = 2
VNT_WEAPON_BUCKETPOS_LMG = 2
VNT_WEAPON_BUCKETPOS_DMR = 2
VNT_WEAPON_BUCKETPOS_SNIPER = 2
VNT_WEAPON_BUCKETPOS_SHOTGUN = 2
VNT_WEAPON_BUCKETPOS_GRENADE = 3
VNT_WEAPON_BUCKETPOS_MINE = 3
VNT_WEAPON_BUCKETPOS_EXPLOSIVE = 3
VNT_WEAPON_BUCKETPOS_SUPPORT = 3
VNT_WEAPON_BUCKETPOS_LAUNCHER = 4
VNT_WEAPON_BUCKETPOS_UNIQUE = 4
VNT_WEAPON_BUCKETPOS_OTHER = 5
VNT_WEAPON_BUCKETPOS_CLOTHING = 5
VNT_WEAPON_BUCKETPOS_NONE = 92

---------------------------------------------
---------------------------------------------
--	SLOT POSITIONS
---------------------------------------------
---------------------------------------------

VNT_WEAPON_SLOTPOS_MELEE = 16
VNT_WEAPON_SLOTPOS_TOOL = 24
VNT_WEAPON_SLOTPOS_UTILITY = 64
VNT_WEAPON_SLOTPOS_PISTOL = 16
VNT_WEAPON_SLOTPOS_REVOLVER = 32
VNT_WEAPON_SLOTPOS_DUAL = 48
VNT_WEAPON_SLOTPOS_SMG = 16
VNT_WEAPON_SLOTPOS_CARBINE = 24
VNT_WEAPON_SLOTPOS_RIFLE = 32
VNT_WEAPON_SLOTPOS_LMG = 48
VNT_WEAPON_SLOTPOS_DMR = 56
VNT_WEAPON_SLOTPOS_SNIPER = 64
VNT_WEAPON_SLOTPOS_SHOTGUN = 72
VNT_WEAPON_SLOTPOS_GRENADE = 16
VNT_WEAPON_SLOTPOS_MINE = 24
VNT_WEAPON_SLOTPOS_EXPLOSIVE = 32
VNT_WEAPON_SLOTPOS_SUPPORT = 48
VNT_WEAPON_SLOTPOS_LAUNCHER = 16
VNT_WEAPON_SLOTPOS_UNIQUE = 16
VNT_WEAPON_SLOTPOS_OTHER = 32
VNT_WEAPON_SLOTPOS_CLOTHING = 48
VNT_WEAPON_SLOTPOS_NONE = 92

---------------------------------------------
---------------------------------------------
--	WEAPON MANUFACTURERS
---------------------------------------------
---------------------------------------------
VNT_WEAPON_MANUFACTURER_VNT = "Vanover Design Bureau"
VNT_WEAPON_MANUFACTURER_VARIOUS = "Various"
VNT_WEAPON_MANUFACTURER_UNKNOWN = "Unknown"
VNT_WEAPON_MANUFACTURER_COLT = "Colt's Manufacturing Company"
VNT_WEAPON_MANUFACTURER_BROWNING = "Browning Arms Company"
VNT_WEAPON_MANUFACTURER_SPRINGFIELD = "Springfield Armoury, Inc."
VNT_WEAPON_MANUFACTURER_SMITHWESSON = "Smith & Wesson"
VNT_WEAPON_MANUFACTURER_SHARPS = "Sharps Rifle Manufacturing Company"
VNT_WEAPON_MANUFACTURER_WINCHESTER = "Winchester Repeating Arms Company"
VNT_WEAPON_MANUFACTURER_REMINGTON = "Remington Arms Company"
VNT_WEAPON_MANUFACTURER_ZM = "Z-M Weapons"
VNT_WEAPON_MANUFACTURER_KIMBER = "Kimber Manufacturing"
VNT_WEAPON_MANUFACTURER_BARRET = "Barret Firearms Manufacturing"
VNT_WEAPON_MANUFACTURER_SOG = "SOG Specialty Knives"
VNT_WEAPON_MANUFACTURER_CHINALAKE = "China Lake Naval Weapons Center"
VNT_WEAPON_MANUFACTURER_MAC = "Military Armament Corporation"
VNT_WEAPON_MANUFACTURER_CRYE = "Crye Precision L.L.C."
VNT_WEAPON_MANUFACTURER_AUTOORDNANCE = "Auto Ordnance Corp."
VNT_WEAPON_MANUFACTURER_GMINLAND = "General Motors' Inland Division"
VNT_WEAPON_MANUFACTURER_MOSSBERG = "O.F. Mossberg & Sons"
VNT_WEAPON_MANUFACTURER_SINTEK = "SinTEK Industries"
VNT_WEAPON_MANUFACTURER_ACCURACYINTL = "Accuracy International"
VNT_WEAPON_MANUFACTURER_HK = "Heckler & Koch G.m.b.H."
VNT_WEAPON_MANUFACTURER_BRUGGERTHOMET = "Brugger & Thomet"
VNT_WEAPON_MANUFACTURER_GLOCK = "Glock Ges.m.b.H."
VNT_WEAPON_MANUFACTURER_CZ = "Czech Arms Factory"
VNT_WEAPON_MANUFACTURER_STEYR = "Steyr Mannlicher"
VNT_WEAPON_MANUFACTURER_BENELLI = "Benelli Arms S.p.A."
VNT_WEAPON_MANUFACTURER_FRANCHI = "Franchi S.p.A."
VNT_WEAPON_MANUFACTURER_BERETTA = "Beretta Arms Factory"
VNT_WEAPON_MANUFACTURER_SIG = "SIG Sauer"
VNT_WEAPON_MANUFACTURER_FN = "FN Herstal"
VNT_WEAPON_MANUFACTURER_STAR = "Star Bonifacio Echeverria, S.A."
VNT_WEAPON_MANUFACTURER_NEXTER = "Nexter Systems"
VNT_WEAPON_MANUFACTURER_MAUSER = "Mauser"
VNT_WEAPON_MANUFACTURER_WALTHER = "Walther Arms"
VNT_WEAPON_MANUFACTURER_IMI = "Israel Military Industries"
VNT_WEAPON_MANUFACTURER_NORINCO = "China North Industries Corporation"
VNT_WEAPON_MANUFACTURER_TULA = "Tula Arms Plant"
VNT_WEAPON_MANUFACTURER_IZHEVSK = "Izhevsk Mechanical Plant"
VNT_WEAPON_MANUFACTURER_LAMBDA = "Lambda Resistance"
VNT_WEAPON_MANUFACTURER_COMBINE = "Universal Union"
VNT_WEAPON_MANUFACTURER_MISRIAH = "Misriah Armoury"
VNT_WEAPON_MANUFACTURER_ARMALITEUNSC = "Armalite MG"
VNT_WEAPON_MANUFACTURER_WEAPONSYSTECH = "Weapon System Technologies"
VNT_WEAPON_MANUFACTURER_PLASTIC = "Plastic Nations"

---------------------------------------------
---------------------------------------------
--	COUNTRIES
---------------------------------------------
---------------------------------------------

VNT_WEAPON_COUNTRY_UNITEDSTATES = "United States"
VNT_WEAPON_COUNTRY_UNITEDKINGDOM = "United Kingdom"
VNT_WEAPON_COUNTRY_SOVIETUNION = "Soviet Union"
VNT_WEAPON_COUNTRY_FRANCE = "France"
VNT_WEAPON_COUNTRY_ITALY = "Italy"
VNT_WEAPON_COUNTRY_SPAIN = "Spain"
VNT_WEAPON_COUNTRY_JAPANEMPIRE = "Empire of Japan"
VNT_WEAPON_COUNTRY_JAPAN = "Japan"
VNT_WEAPON_COUNTRY_GERMANEMPIRE = "German Empire"
VNT_WEAPON_COUNTRY_GERMANY = "Germany"
VNT_WEAPON_COUNTRY_NAZIGERMANY = "Nazi Germany"
VNT_WEAPON_COUNTRY_WESTGERMANY = "West Germany"
VNT_WEAPON_COUNTRY_EASTGERMANY = "East Germany"
VNT_WEAPON_COUNTRY_CHINA = "China"
VNT_WEAPON_COUNTRY_BELGIUM = "Belgium"
VNT_WEAPON_COUNTRY_AUSTRIA = "Austria"
VNT_WEAPON_COUNTRY_ISRAEL = "Israel"
VNT_WEAPON_COUNTRY_CZECH = "Czechoslovakia"
VNT_WEAPON_COUNTRY_CZECHREPUBLIC = "Czech Republic"
VNT_WEAPON_COUNTRY_VARIOUS = "Various"
VNT_WEAPON_COUNTRY_EARTH = "Earth"
VNT_WEAPON_COUNTRY_MARS = "Mars"
VNT_WEAPON_COUNTRY_COMBINE = "Universal Union"
VNT_WEAPON_COUNTRY_PLASTIC = "Plastic Nation"

---------------------------------------------
---------------------------------------------
--	Weapon Magazine Types
--	Used in my Weapon Magazine Plugin
--	Mag/clip name, number of mags/clips owned, number of rounds in mag, mag "self-destructs" on use
--	All can be overridden in SWeps
---------------------------------------------
---------------------------------------------

VNTCB.Magazine = VNTCB.Magazine or {
	["mMP7"] = { "HK 4.6x30mm MP7 Magazine" , 0 , 20 , false } ,
	["m357C"] = { ".357 Magnum Moon Clip" , 0 , 6 , true } ,
	["m38SC"] = { ".38 Special Moon Clip" , 0 , 6 , true } ,
	["mMAC1045"] = { "MAC-10 .45 ACP Magazine" , 0 , 30 , false } ,
	["mAWM"] = { "Arctic Warfare Magnum Magazine" , 0 , 10 , false } ,
	["mScout"] = { "Steyr Scout Magazine" , 0 , 10 , false } ,
	["mDEagle"] = { "Desert Eagle Magazine" , 0 , 7 , false } ,
	["mP2209x19"] = { "P228 9x19mm Magazine" , 0 , 8 , false } ,
	["mP22045"] = { "P220 .45 ACP Magazine" , 0 , 8 , false } ,
	["m57"] = { "Five-seveN Magazine" , 0 , 20 , false } ,
	["mMRC"] = { "MRC Magazine" , 0 , 50 , false } ,
	["mP90"] = { "P90 Magazine" , 0 , 50 , false } ,
	["mUMP45"] = { "UMP-45 .45 ACP Magazine" , 0 , 12 , false } ,
	["mMStar45"] = { "Star Megastar .45 ACP Magazine" , 0 , 12 , false } ,
	["mTMP"] = { "TMP/MP9 Magazine" , 0 , 30 , false } ,
	["mIMI556"] = { "IMI 5.56x45mm Magazine" , 0 , 50 , false } ,
	["mHK556"] = { "HK 5.56x45mm Magazine" , 0 , 25 , false } ,
	["mHK762"] = { "HK 7.62x51mm Magazine" , 0 , 20 , false } ,
	["mUSP9"] = { "HK 9x19mm USP Magazine" , 0 , 15 , false } ,
	["mUSP40"] = { "HK .40 S&W USP Magazine" , 0 , 13 , false } ,
	["mUSP45"] = { "HK .45 ACP USP Magazine" , 0 , 12 , false } ,
	["mMP5"] = { "HK MP5 Magazine" , 0 , 30 , false } ,
	["m92FS"] = { "Beretta 9x19mm Magazine" , 0 , 15 , false } ,
	["mSIG556"] = { "5.56 SIG Magazine" , 0 , 30 , false } ,
	["mNATO556"] = { "5.56 STANAG" , 0 , 30 , false } ,
	["mNATO762"] = { "7.62 STANAG" , 0 , 20 , false } ,
	["m40x46"] = { "40x46mm NATO Grenade Launcher Shell" , 0 , 1 , true } ,
	["mM40"] = { "M40 Magazine" , 0 , 5 , false } ,
	["mBARMk2"] = { "BAR Mk.II Magazine" , 0 , 10 , false } ,
	["mShotgun"] = { "Shotgun Shell" , 0 , 1 , true } ,
	["mMakarov"] = { "PM Magazine" , 0 , 8 , false } ,
	["mBizon"] = { "PP-19 Magazine" , 0 , 64 , false } ,
	["mCZ75"] = { "CZ-75 Magazine" , 0 , 15 , false } ,
	["mGlock"] = { "Glock Magazine" , 0 , 15 , false } ,
	["m54539"] = { "5.45x39mm Warsaw Pact STANAG" , 0 , 30 , false } ,
	["m54539D"] = { "5.45x39mm Warsaw Pact Drum" , 0 , 75 , false } ,
	["m76225"] = { "7.62x25mm Warsaw Pact Pistol Mag" , 0 , 8 , false } ,
	["m76239"] = { "7.62x39mm Warsaw Pact STANAG" , 0 , 40 , false } ,
	["m76239D"] = { "7.62x39mm Warsaw Pact Drum" , 0 , 75 , false } ,
	["m76254"] = { "7.62x54mm Warsaw Pact STANAG" , 0 , 30 , false } ,
	["m76254C"] = { "7.62x54mm Warsaw Pact Stripper Clip" , 0 , 5 , true } ,
	["mFlashbang"] = { "Flashbang Grenade" , 0 , 1 , true } ,
	["mM18"] = { "M18 Grenade" , 0 , 1 , true } ,
	["mM61"] = { "M61 Grenade" , 0 , 1 , true } ,
	["mM67"] = { "M67 Grenade" , 0 , 1 , true } ,
	["mM249"] = { "M249 LMG Box Mag" , 0 , 100 , false } ,
	--	WWII
	["mMk2Frag"] = { "Mk.II Grenade" , 0 , 1 , true } ,
	["mM24Stielhandgranate"] = { "Stielhandgranate" , 0 , 1 , true } ,
	["mNb39Nebelhandgranate"] = { "Nebelhandgranate" , 0 , 1 , true } ,
	["m3006Stripper"] = { ".30-06 Stripper Clip" , 0 , 5 , true } ,
	["m3006Belt"] = { ".30-06 150-Round Belt" , 0 , 150 , true } ,
	["mM1911"] = { "M1911 Magazine" , 0 , 7 , false } ,
	["mM1Carbine15"] = { "M1 Carbine 15-Round Magazine" , 0 , 15 , false } ,
	["mM1Carbine30"] = { "M1 Carbine 30-Round Magazine" , 0 , 30 , false } ,
	["mBAR"] = { "BAR .30-06 Magazine" , 0 , 20 , false } ,
	["mGarand"] = { "M1 Garand .30-06 En Bloc Clip" , 0 , 8 , true } ,
	["mGarandNade"] = { "M1 Garand Rifle Grenade" , 0 , 1 , true } ,
	["mThompsonStick"] = { "M1A1 Thompson Stick Mag" , 0 , 30 , false } ,
	["mBazooka"] = { "M6A1 Rockets" , 0 , 1 , true } ,
	["mC96"] = { "C96 Magazine" , 0 , 10 , false } ,
	["mMP40Stick"] = { "MP 40 Stick Magazine" , 0 , 32 , false } ,
	["mStG44"] = { "StG 44 Magazine" , 0 , 30 , false } ,
	["mP38"] = { "Walther P38 Magazine" , 0 , 8 , false } ,
	["mMG42"] = { "MG 42 250-Round Belt" , 0 , 250 , true } ,
	["mK98k"] = { "Karabiner 98k Stripper Clip" , 0 , 5 , true } ,
	--	Western / FoF
	["m45Colt"] = { ".45 Colt Bullet" , 0 , 1 , true } ,
	-- Halo
	["mHaloM6"] = { "M6 Magnum Magazine" , 0 , 8 , false } ,
	["mHaloM7"] = { "M7 SMG Magazine" , 0 , 8 , false } ,
	["mHaloMA"] = { "MA Magazine" , 0 , 36 , false } ,
	["mHaloM392"] = { "M392 Magazine" , 0 , 36 , false } ,
	["mHaloBR55"] = { "BR55 Magazine" , 0 , 36 , false } ,
	["mHaloSK8GMagnum"] = { "8-Gauge Magnum Shells" , 0 , 1 , true } ,
	["mHaloSK8GBuck"] = { "8-Gauge Buckshot Shells" , 0 , 1 , true } ,
	-- HL2
	["mAR2"] = { "AR2 Magazine" , 0 , 30 , false } ,
	-- SiN
	["mSinTEKAR"] = { "Assault Rifle Magazine" , 0 , 45 , false } ,
	["mSinTEKMagnum"] = { "Auto Magnum Magazine" , 0 , 20 , false } ,
	["mSinTEKScattergun"] = { "Scattergun Magazine" , 0 , 6 , false } ,
}

---------------------------------------------
---------------------------------------------
--	Ammo Types
--	Used across all my weapon packs
--	Damage, Strong Material Penetration, Weak Material Penetration
---------------------------------------------
---------------------------------------------

VNTCB.Ammo = VNTCB.Ammo or {
	["a357Magnum"] = { 50 , 50 , 75 } ,
	["a357Sig"] = { 35 , 15 , 20 } ,
	["a38S"] = { 38 , 9 , 12 } ,
	["a40SW"] = { 40 , 12 , 15 } ,
	["a44Magnum"] = { 44 , 20 , 25 } ,
	["a45ACP"] = { 45 , 13 , 16 } ,
	["a45GAP"] = { 35 , 10 , 12 } ,
	["a50BMG"] = { 150 , 50 , 75 } ,
	["a86x70mm"] = { 100 , 35 , 50 } ,
	["a50AE"] = { 50 , 50 , 75 } ,
	["aMP7"] = { 30 , 9 , 12 } ,
	["a57"] = { 28 , 9 , 12 } ,
	["a556NATO"] = { 45 , 15 , 18 } ,
	["a762NATO"] = { 51 , 18 , 21 } ,
	["a9x19mmNATO"] = { 19 , 5 , 8 } ,
	["a10mmAuto"] = { 21 , 10 , 15 } ,
	["a68x43"] = { 48 , 16 , 19 } ,
	["a68cl"] = { 48 , 16 , 19 } ,
	["a10Auto"] = { 25 , 7 , 10 } ,
	["a12GBuck"] = { 16 , 15 , 18 } ,
	["a20GBuck"] = { 12 , 17 , 20 } ,
	["a9x18mmWP"] = { 18 , 5 , 8 } ,
	["a545x39mmWP"] = { 39 , 15 , 18 } ,
	["a762x25mmWP"] = { 25 , 12 , 15 } ,
	["a762x39mmWP"] = { 39 , 20 , 23 } ,
	["a762x54mmWP"] = { 39 , 23 , 26 } ,
	["aArmyMen"] = { 45 , 15 , 18 } ,
	["a40x46mm"] = { 1 , 1 , 1 } ,
	["aM61Frag"] = { 1 , 1 , 1 } ,
	["aM67Frag"] = { 1 , 1 , 1 } ,
	["aM18Smoke"] = { 1 , 1 , 1 } ,
	["aFlashbang"] = { 1 , 1 , 1 } ,
	["aVOG25"] = { 1 , 1 , 1 } ,
	-- WWII
	["a762x25Mauser"] = { 28 , 14 , 17 } ,
	["a792x33mmkurz"] = { 38 , 15 , 20 } ,
	["a792x57mmmauser"] = { 57 , 28 , 36 } ,
	["a30carb"] = { 33 , 15 , 20 } ,
	["a3006"] = { 92 , 45 , 50 } ,
	["agarandgrenade"] = { 1 , 1 , 1 } ,
	["aBazooka"] = { 1 , 1 , 1 } ,
	["a88mmrocket"] = { 1 , 1 , 1 } ,
	["aMk2Frag"] = { 1 , 1 , 1 } ,
	["aNb39Smoke"] = { 1 , 1 , 1 } ,
	-- Western / FoF
	["a45Colt"] = { 45 , 16 , 20 } ,
	["a4570Gov"] = { 70 , 25 , 45 } ,
	-- Utility
	["aMedBag"] = { 10 , 1 , 1 } ,
	["aAmmoBag"] = { 10 , 1 , 1 } ,
	["aBatteries"] = { 1 , 1 , 1 } ,
	-- Halo
	["aSK8GMagnum"] = { 32 , 16 , 24 } ,
	["aSK8GBuck"] = { 32 , 4 , 12 } ,
	["a95x40mm"] = { 50 , 18 , 21 } ,
	["a5x23mm"] = { 22 , 8 , 12 } ,
	-- HL2
	["aAR2"] = { 8 , 8 , 12 } ,
}

local AmmoTypes = {
	------------------------------------------------------
	------------------------------------------------------
	------------------------------------------------------
	--	Callibre
	------------------------------------------------------
	------------------------------------------------------
	------------------------------------------------------
	--[[[".357 Magnum"] = {
		name = "9x33mm",
		dmgtype = DMG_BULLET,
		tracer = TRACER_NONE,
	},--]]
	[".357 SIG"] = {
		name = "357sig",
		dmgtype = DMG_BULLET,
		tracer = TRACER_NONE,
	},
	[".38 Special"] = {
		name = "38special",
		dmgtype = DMG_BULLET,
		tracer = TRACER_NONE,
	},
	[".40 S&W"] = {
		name = "40sw",
		dmgtype = DMG_BULLET,
		tracer = TRACER_NONE,
	},
	[".44 Magnum"] = {
		name = "44magnum",
		dmgtype = DMG_BULLET,
		tracer = TRACER_NONE,
	},
	[".45 ACP"] = {
		name = "45acp",
		dmgtype = DMG_BULLET,
		tracer = TRACER_NONE,
	},
	[".45-70 Government"] = {
		name = "4570gov",
		dmgtype = DMG_BULLET,
		tracer = TRACER_NONE,
	},
	[".45 GAP"] = {
		name = "45gap",
		dmgtype = DMG_BULLET,
		tracer = TRACER_NONE,
	},
	[".45 Colt"] = {
		name = "45colt",
		dmgtype = DMG_BULLET,
		tracer = TRACER_NONE,
	},
	[".50 BMG"] = {
		name = "50bmg",
		dmgtype = DMG_BULLET,
		tracer = TRACER_NONE,
	},
	[".50 AE"] = {
		name = "50ae",
		dmgtype = DMG_BULLET,
		tracer = TRACER_NONE,
	},
	------------------------------------------------------
	------------------------------------------------------
	------------------------------------------------------
	--	Millimetre :: Modern-ish
	------------------------------------------------------
	------------------------------------------------------
	------------------------------------------------------
	["4.6x30mm"] = {
		name = "46x30mm",
		dmgtype = DMG_BULLET,
		tracer = TRACER_NONE,
	},
	["5.7x28mm"] = {
		name = "57x28mm",
		dmgtype = DMG_BULLET,
		tracer = TRACER_NONE,
	},
	["8.6x70mm"] = {
		name = "86x70mm",
		dmgtype = DMG_BULLET,
		tracer = TRACER_NONE,
	},
	["5.56x45mm NATO"] = {
		name = "556x45mmnato",
		dmgtype = DMG_BULLET,
		tracer = TRACER_NONE,
	},
	["7.62x51mm NATO"] = {
		name = "762x51mmnato",
		dmgtype = DMG_BULLET,
		tracer = TRACER_NONE,
	},
	["9x19mm NATO"] = {
		name = "9x19mmnato",
		dmgtype = DMG_BULLET,
		tracer = TRACER_NONE,
	},
	["6.8x43mm"] = {
		name = "68x43mm",
		dmgtype = DMG_BULLET,
		tracer = TRACER_NONE,
	},
	["6.8mm Caseless"] = {
		name = "68mmcl",
		dmgtype = DMG_BULLET,
		tracer = TRACER_NONE,
	},
	["10mm Auto"] = {
		name = "10mmauto",
		dmgtype = DMG_BULLET,
		tracer = TRACER_NONE,
	},
	------------------------------------------------------
	------------------------------------------------------
	------------------------------------------------------
	--	Warsaw Pact
	------------------------------------------------------
	------------------------------------------------------
	------------------------------------------------------
	["9x18mm Warsaw Pact"] = {
		name = "9x18mmwp",
		dmgtype = DMG_BULLET,
		tracer = TRACER_NONE,
	},
	["5.45x39mm Warsaw Pact"] = {
		name = "545x39mmwp",
		dmgtype = DMG_BULLET,
		tracer = TRACER_NONE,
	},
	["7.62x25mm Warsaw Pact"] = {
		name = "762x25mmwp",
		dmgtype = DMG_BULLET,
		tracer = TRACER_NONE,
	},
	["7.62x39mm Warsaw Pact"] = {
		name = "762x39mmwp",
		dmgtype = DMG_BULLET,
		tracer = TRACER_NONE,
	},
	["7.62x54mm Warsaw Pact"] = {
		name = "762x54mmwp",
		dmgtype = DMG_BULLET,
		tracer = TRACER_NONE,
	},
	["12.7x108mm"] = {
		name = "127x108mm",
		dmgtype = DMG_BULLET,
		tracer = TRACER_NONE,
	},
	
	------------------------------------------------------
	------------------------------------------------------
	------------------------------------------------------
	--	Shotguns
	------------------------------------------------------
	------------------------------------------------------
	------------------------------------------------------
	["12-Gauge Buckshot"] = {
		name = "12gauge",
		dmgtype = DMG_BULLET,
		tracer = TRACER_NONE,
	},
	["20-Gauge Buckshot"] = {
		name = "20gauge",
		dmgtype = DMG_BULLET,
		tracer = TRACER_NONE,
	},
	------------------------------------------------------
	------------------------------------------------------
	------------------------------------------------------
	--	Grenades
	------------------------------------------------------
	------------------------------------------------------
	------------------------------------------------------
	["40x46mm NATO Grenade"] = {
		name = "40x46mmgrenade",
		dmgtype = DMG_BLAST,
		tracer = TRACER_NONE,
	},
	["VOG-25 Grenade"] = {
		name = "vog25grenade",
		dmgtype = DMG_BLAST,
		tracer = TRACER_NONE,
	},
	["M61 Frag Grenade"] = {
		name = "m61grenade",
		dmgtype = DMG_BLAST,
		tracer = TRACER_NONE,
	},
	["M67 Frag Grenade"] = {
		name = "m67grenade",
		dmgtype = DMG_BLAST,
		tracer = TRACER_NONE,
	},
	["M18 Smoke Grenade"] = {
		name = "m18smoke",
		dmgtype = DMG_BLAST,
		tracer = TRACER_NONE,
	},
	["Flashbang"] = {
		name = "flashbang",
		dmgtype = DMG_BLAST,
		tracer = TRACER_NONE,
	},
	["M15 Anti-Vehicle Mines"] = {
		name = "m15mine",
		dmgtype = DMG_BLAST,
		tracer = TRACER_NONE,
	},
	["M18 Anti-Personnel Mines"] = {
		name = "m18mine",
		dmgtype = DMG_BLAST,
		tracer = TRACER_NONE,
	},

	------------------------------------------------------
	------------------------------------------------------
	------------------------------------------------------
	--	WWII
	------------------------------------------------------
	------------------------------------------------------
	------------------------------------------------------
	[".30-06"] = {
		name = "762x63mm",
		dmgtype = DMG_BULLET,
		tracer = TRACER_NONE,
	},
	[".30 Carbine"] = {
		name = "762x33mm",
		dmgtype = DMG_BULLET,
		tracer = TRACER_NONE,
	},
	["M1 Garand Rifle Grenade"] = {
		name = "garandgrenade",
		dmgtype = DMG_BLAST,
		tracer = TRACER_NONE,
	},
	["M6A1 HEAT Rocket"] = {
		name = "m6a1heatrocket",
		dmgtype = DMG_BLAST,
		tracer = TRACER_NONE,
	},
	["88mm Rocket"] = {
		name = "88mmrocket",
		dmgtype = DMG_BLAST,
		tracer = TRACER_NONE,
	},
	["7.62x25mm Mauser"] = {
		name = "762x25mmmauser",
		dmgtype = DMG_BULLET,
		tracer = TRACER_NONE,
	},
	["7.92x57mm Mauser"] = {
		name = "792x57mmmauser",
		dmgtype = DMG_BULLET,
		tracer = TRACER_NONE,
	},
	["7.92x33mm Kurz"] = {
		name = "792x33mmkurz",
		dmgtype = DMG_BULLET,
		tracer = TRACER_NONE,
	},
	["Mk.II Pineapple Grenade"] = {
		name = "mk2frag",
		dmgtype = DMG_BLAST,
		tracer = TRACER_NONE,
	},
	["Model 24 Stielhandgranate"] = {
		name = "m24stielhandgranate",
		dmgtype = DMG_BLAST,
		tracer = TRACER_NONE,
	},
	["Nb.39 Nebelhandgranate"] = {
		name = "nb39nebelhandgranate",
		dmgtype = DMG_BLAST,
		tracer = TRACER_NONE,
	},
	------------------------------------------------------
	------------------------------------------------------
	------------------------------------------------------
	--	Utility
	------------------------------------------------------
	------------------------------------------------------
	------------------------------------------------------
	["Medical Bag"] = {
		name = "medbag",
		dmgtype = DMG_CLUB,
		tracer = TRACER_NONE,
	},
	["Ammunition Bag"] = {
		name = "ammobag",
		dmgtype = DMG_CLUB,
		tracer = TRACER_NONE,
	},
	["Batteries"] = {
		name = "batteries",
		dmgtype = DMG_CLUB,
		tracer = TRACER_NONE,
	},
	["Parachutes"] = {
		name = "parachute",
		dmgtype = DMG_CLUB,
		tracer = TRACER_NONE,
	},
	["Gas Mask Filters"] = {
		name = "gasmaskfilters",
		dmgtype = DMG_CLUB,
		tracer = TRACER_NONE,
	},
	------------------------------------------------------
	------------------------------------------------------
	------------------------------------------------------
	-- Halo
	------------------------------------------------------
	------------------------------------------------------
	------------------------------------------------------
	["5x23mm"] = {
		name = "5x23mm",
		dmgtype = DMG_BULLET,
		tracer = TRACER_NONE,
	},
	["9.5x40mm"] = {
		name = "95x40mm",
		dmgtype = DMG_BULLET,
		tracer = TRACER_NONE,
	},
	["Soellkraft 8-Gauge Buckshot"] = {
		name = "sk8gaugebuck",
		dmgtype = DMG_BULLET,
		tracer = TRACER_NONE,
	},
	["Soellkraft 8-Gauge Magnum"] = {
		name = "sk8gaugemagnum",
		dmgtype = DMG_BULLET,
		tracer = TRACER_NONE,
	},
	
}

for K, V in pairs(AmmoTypes) do
	if CLIENT then
		language.Add(V.name .. "_ammo", K)
	end	
	game.AddAmmoType(V)
end

---------------------------------------------
---------------------------------------------
--	CLOTHING
---------------------------------------------
---------------------------------------------

local function VNT_Clothing_CleanUp()
	for _,_P in pairs( player.GetAll() ) do
		if ((!_P:Alive()) or (!IsValid(_P))) and _P:GetNWBool( "STALKER_PlyGasItemOn" ) != false then
			if CLIENT then
				RunConsoleCommand( "pp_mat_overlay", "" )
				_P.ItemMDL = _P.ItemMDL or ClientsideModel( "models/jessev92/stalker/weapons/gas_mask_sunrise_w.mdl", RENDERGROUP_BOTH)
				local ItemMDL = _P.ItemMDL	
				ItemMDL:Remove()
			end
			if SERVER then
				_P:SetNWBool( "STALKER_PlyGasItemOn", false )
			end
		end
	end
end

timer.Create( "VNT_Clothing_CleanUp", 2, 0, VNT_Clothing_CleanUp )
