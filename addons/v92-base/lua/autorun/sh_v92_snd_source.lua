
AddCSLuaFile()

sound.Add( {	name		= "VNT.Vehicles.Wipers",
	channel		= CHAN_BODY,
	volume		= 1.0,
	level		= 75,
	pitch		= { 110 },
	sound		= { "jessev92/vehicles/windscreen_wipers_loop.wav"}
} )
util.PrecacheSound("jessev92/vehicles/windscreen_wipers_loop.wav")

---------------------------------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------------------------
--	Counter-Strike: Source
---------------------------------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------------------------

------------------------------------------------------------------------------------------
--	CS:S :: Player
------------------------------------------------------------------------------------------
sound.Add( {
	name = "CSS.Player.Headshot" ,
	channel = CHAN_BODY ,
	volume = 1.0 ,
	level = 180 ,
	sound = { "player/headshot1.wav", "player/headshot2.wav" } ,
} )
util.PrecacheSound( "player/headshot1.wav" )
util.PrecacheSound( "player/headshot2.wav" )

------------------------------------------------------------------------------------------
--	CS:S :: Weapons
------------------------------------------------------------------------------------------

---------------------------------------------
---------------------------------------------
--	M4A1
---------------------------------------------
---------------------------------------------
sound.Add( {
	name = "CSS.M4A1.DetachSuppressor" ,
	channel = CHAN_ITEM ,
	volume = 1.0 ,
	level = 75 ,
	sound = { "jessev92/css/weapons/m4a1/suppressor_off.wav" } ,
} )
util.PrecacheSound( "jessev92/css/weapons/m4a1/suppressor_off.wav" )

sound.Add( {
	name = "CSS.M4A1.AttachSuppressor" ,
	channel = CHAN_ITEM ,
	volume = 1.0 ,
	level = 75 ,
	sound = { "jessev92/css/weapons/m4a1/suppressor_on.wav" } ,
} )
util.PrecacheSound( "jessev92/css/weapons/m4a1/suppressor_on.wav" )

---------------------------------------------
---------------------------------------------
--	USP
---------------------------------------------
---------------------------------------------
sound.Add( {
	name = "CSS.USP.DetachSuppressor" ,
	channel = CHAN_ITEM ,
	volume = 1.0 ,
	level = 75 ,
	sound = { "jessev92/css/weapons/usp/suppressor_off.wav" } ,
} )
util.PrecacheSound( "jessev92/css/weapons/usp/suppressor_off.wav" )

sound.Add( {
	name = "CSS.USP.AttachSuppressor" ,
	channel = CHAN_ITEM ,
	volume = 1.0 ,
	level = 75 ,
	sound = { "jessev92/css/weapons/usp/suppressor_on.wav" } ,
} )
util.PrecacheSound( "jessev92/css/weapons/usp/suppressor_on.wav" )

//G3SG1
util.PrecacheSound( "weapons/g3sg1/g3sg1-1.wav" )
util.PrecacheSound( "weapons/g3sg1/g3sg1_slide.wav" )
util.PrecacheSound( "weapons/g3sg1/g3sg1_clipout.wav" )
util.PrecacheSound( "weapons/g3sg1/g3sg1_clipin.wav" )

//GLOCK
util.PrecacheSound( "weapons/glock/glock18-1.wav" )
util.PrecacheSound( "weapons/glock/glock_clipout.wav" )
util.PrecacheSound( "weapons/glock/glock_clipin.wav" )
util.PrecacheSound( "weapons/glock/glock_sliderelease.wav" )
util.PrecacheSound( "weapons/glock/glock_slideback.wav" )

//GALIL
util.PrecacheSound( "weapons/galil/galil-1.wav" )
util.PrecacheSound( "weapons/galil/galil_clipout.wav" )
util.PrecacheSound( "weapons/galil/galil_clipin.wav" )
util.PrecacheSound( "weapons/galil/galil_boltpull.wav" )

//M249
util.PrecacheSound( "weapons/m249/m249-1.wav" )
util.PrecacheSound( "weapons/m249/m249_coverup.wav" )
util.PrecacheSound( "weapons/m249/m249_boxout.wav" )
util.PrecacheSound( "weapons/m249/m249_boxin.wav" )
util.PrecacheSound( "weapons/m249/m249_chain.wav" )
util.PrecacheSound( "weapons/m249/m249_coverdown.wav" )

//M1014
util.PrecacheSound( "weapons/xm1014/xm1014-1.wav" )
util.PrecacheSound( "weapons/xm1014/xm1014_insertshell.wav" )

//M3SUPER90
util.PrecacheSound( "weapons/m3/m3-1.wav" )
util.PrecacheSound( "weapons/m3/m3_insertshell.wav" )
util.PrecacheSound( "weapons/m3/m3_pump.wav" )

//MAC10
util.PrecacheSound( "weapons/mac10/mac10-1.wav" )
util.PrecacheSound( "weapons/mac10/mac10_clipout.wav" )
util.PrecacheSound( "weapons/mac10/mac10_clipin.wav" )
util.PrecacheSound( "weapons/mac10/mac10_boltpull.wav" )

//TMP
util.PrecacheSound( "weapons/tmp/tmp-1.wav" )
util.PrecacheSound( "weapons/tmp/tmp_clipin.wav" )
util.PrecacheSound( "weapons/tmp/tmp_clipout.wav" )

//MP5
util.PrecacheSound( "weapons/mp5navy/mp5-1.wav" )
util.PrecacheSound( "weapons/mp5navy/mp5_clipout.wav" )
util.PrecacheSound( "weapons/mp5navy/mp5_clipin.wav" )
util.PrecacheSound( "weapons/mp5navy/mp5_slideback.wav" )

//P228
util.PrecacheSound( "weapons/p228/p228-1.wav" )
util.PrecacheSound( "weapons/p228/p228_clipout.wav" )
//"Weapon_P228.Clipin"
util.PrecacheSound( "weapons/p228/p228_clipin.wav" )
//"Weapon_P228.Sliderelease"
util.PrecacheSound( "weapons/p228/p228_sliderelease.wav" )
//"Weapon_P228.Slidepull"
util.PrecacheSound( "weapons/p228/p228_slidepull.wav" )
//"Weapon_P228.Slideback"
util.PrecacheSound( "weapons/p228/p228_slideback.wav" )

//P90
//"Weapon_P90.Single"
util.PrecacheSound( "weapons/p90/p90-1.wav" )
//"Weapon_P90.Cliprelease"
util.PrecacheSound( "weapons/p90/p90_cliprelease.wav" )
//"Weapon_P90.Clipout"
util.PrecacheSound( "weapons/p90/p90_clipout.wav" )
//"Weapon_P90.Clipin"
util.PrecacheSound( "weapons/p90/p90_clipin.wav" )
//"Weapon_P90.Boltpull"
util.PrecacheSound( "weapons/p90/p90_boltpull.wav" )

//SCOUT
//"Weapon_Scout.Single"
util.PrecacheSound( "weapons/scout/scout_fire-1.wav" )
//"Weapon_Scout.Bolt"
util.PrecacheSound( "weapons/scout/scout_bolt.wav" )
//"Weapon_Scout.Clipout"
util.PrecacheSound( "weapons/scout/scout_clipout.wav" )
//"Weapon_Scout.Clipin"
util.PrecacheSound( "weapons/scout/scout_clipin.wav" )

//SG550
//"Weapon_SG550.Single"
util.PrecacheSound( "weapons/sg550/sg550-1.wav" )
//"Weapon_SG550.Clipout"
util.PrecacheSound( "weapons/sg550/sg550_clipout.wav" )
//"Weapon_SG550.Clipin"
util.PrecacheSound( "weapons/sg550/sg550_clipin.wav" )
//"Weapon_SG550.Boltpull"
util.PrecacheSound( "weapons/sg550/sg550_boltpull.wav" )

//SG552
//"Weapon_SG552.Single"
util.PrecacheSound( "weapons/sg552/sg552-1.wav" )
//"Weapon_SG552.Clipout"
util.PrecacheSound( "weapons/sg552/sg552_clipout.wav" )
//"Weapon_SG552.Clipin"
util.PrecacheSound( "weapons/sg552/sg552_clipin.wav" )
//"Weapon_SG552.Boltpull"
util.PrecacheSound( "weapons/sg552/sg552_boltpull.wav" )

//FIVESEVEN
//"Weapon_FiveSeven.Single"
util.PrecacheSound( "weapons/fiveseven/fiveseven-1.wav" )
//"Weapon_FiveSeven.Clipout"
util.PrecacheSound( "weapons/fiveseven/fiveseven_clipout.wav" )
//"Weapon_FiveSeven.Clipin"
util.PrecacheSound( "weapons/fiveseven/fiveseven_clipin.wav" )
//"Weapon_FiveSeven.Sliderelease"
util.PrecacheSound( "weapons/fiveseven/fiveseven_sliderelease.wav" )
//"Weapon_FiveSeven.Slidepull"
util.PrecacheSound( "weapons/fiveseven/fiveseven_slidepull.wav" )
//"Weapon_FiveSeven.Slideback"
util.PrecacheSound( "weapons/fiveseven/fiveseven_slideback.wav" )

//UMP45
//"Weapon_UMP45.Single"
util.PrecacheSound( "weapons/ump45/ump45-1.wav" )
//"Weapon_UMP45.Clipout"
util.PrecacheSound( "weapons/ump45/ump45_clipout.wav" )
//"Weapon_UMP45.Clipin"
util.PrecacheSound( "weapons/ump45/ump45_clipin.wav" )
//"Weapon_UMP45.Boltslap"
util.PrecacheSound( "weapons/ump45/ump45_boltslap.wav" )

//USP
//"Weapon_USP.Single"
util.PrecacheSound( "weapons/usp/usp_unsil-1.wav" )
//"Weapon_USP.SilencedShot"
util.PrecacheSound( "weapons/usp/usp1.wav" )
//"Weapon_USP.DetachSilencer"
util.PrecacheSound( "weapons/usp/usp_silencer_off.wav" )
//"Weapon_USP.AttachSilencer"
util.PrecacheSound( "weapons/usp/usp_silencer_on.wav" )
//"Weapon_USP.Clipout"
util.PrecacheSound( "weapons/usp/usp_clipout.wav" )
//"Weapon_USP.Clipin"
util.PrecacheSound( "weapons/usp/usp_clipin.wav" )
//"Weapon_USP.Sliderelease"
util.PrecacheSound( "weapons/usp/usp_sliderelease.wav" )
//"Weapon_USP.Slideback"
util.PrecacheSound( "weapons/usp/usp_slideback.wav" )
//"Weapon_USP.Slideback2"
util.PrecacheSound( "weapons/usp/usp_slideback2.wav" )

//M4A1
//"Weapon_M4A1.Single"
util.PrecacheSound( "weapons/m4a1/m4a1_unsil-1.wav" )
//"Weapon_M4A1.Silenced"
util.PrecacheSound( "weapons/m4a1/m4a1-1.wav" )
//"Weapon_M4A1.Silencer_Off"
util.PrecacheSound( "weapons/m4a1/m4a1_silencer_off.wav" )
//"Weapon_M4A1.Silencer_On"
util.PrecacheSound( "weapons/m4a1/m4a1_silencer_on.wav" )
//"Weapon_M4A1.Clipout"
util.PrecacheSound( "weapons/m4a1/m4a1_clipout.wav" )
//"Weapon_M4A1.Clipin"
util.PrecacheSound( "weapons/m4a1/m4a1_clipin.wav" )
//"Weapon_M4A1.Boltpull"
util.PrecacheSound( "weapons/m4a1/m4a1_boltpull.wav" )
//"Weapon_M4A1.Deploy"
util.PrecacheSound( "weapons/m4a1/m4a1_deploy.wav" )

//ELITES
//"Weapon_ELITE.Single"
util.PrecacheSound( "weapons/elite/elite-1.wav" )
//"Weapon_ELITE.Reloadstart"
util.PrecacheSound( "weapons/elite/elite_reloadstart.wav" )
//"Weapon_ELITE.Lclipin"
util.PrecacheSound( "weapons/elite/elite_leftclipin.wav" )
//"Weapon_ELITE.Clipout"
util.PrecacheSound( "weapons/elite/elite_clipout.wav" )
//"Weapon_ELITE.Sliderelease"
util.PrecacheSound( "weapons/elite/elite_sliderelease.wav" )
//"Weapon_ELITE.Rclipin"
util.PrecacheSound( "weapons/elite/elite_rightclipin.wav" )
//"Weapon_ELITE.Deploy"
util.PrecacheSound( "weapons/elite/elite_deploy.wav" )

//FAMAS
//"Weapon_FAMAS.Single"
util.PrecacheSound( "weapons/famas/famas-1.wav" )
//"Weapon_FAMAS.Clipout"
util.PrecacheSound( "weapons/famas/famas_clipout.wav" )
//"Weapon_FAMAS.Clipin"
util.PrecacheSound( "weapons/famas/famas_clipin.wav" )
//"Weapon_FAMAS.Forearm"
util.PrecacheSound( "weapons/famas/famas_forearm.wav" )

//AUG
//"Weapon_AUG.Single"
util.PrecacheSound( "weapons/aug/aug-1.wav" )
//"Weapon_AUG.Boltpull"
util.PrecacheSound( "weapons/aug/aug_boltpull.wav" )
//"Weapon_AUG.Clipout"
util.PrecacheSound( "weapons/aug/aug_clipout.wav" )
//"Weapon_AUG.Clipin"
util.PrecacheSound( "weapons/aug/aug_clipin.wav" )
//"Weapon_AUG.Boltslap"
util.PrecacheSound( "weapons/aug/aug_boltslap.wav" )
//"Weapon_AUG.Forearm"
util.PrecacheSound( "weapons/aug/aug_forearm.wav" )

//AWP
//"Weapon_AWP.Single"
util.PrecacheSound( "weapons/awp/awp1.wav" )
//"Weapon_AWP.Clipin"
util.PrecacheSound( "weapons/awp/awp_clipin.wav" )
//"Weapon_AWP.Clipout"
util.PrecacheSound( "weapons/awp/awp_clipout.wav" )
//"Weapon_AWP.Bolt"
util.PrecacheSound( "weapons/awp/awp_bolt.wav" )

//KNIFE
//"Weapon_Knife.Deploy"
util.PrecacheSound( "indeploy1.wav" )
//"Weapon_Knife.Hit"
util.PrecacheSound( "inhit1.wav" )
util.PrecacheSound( "inhit2.wav" )
util.PrecacheSound( "inhit3.wav" )
util.PrecacheSound( "inhit4.wav" )
//"Weapon_Knife.HitWall"
util.PrecacheSound( "inhitwall1.wav" )
//"Weapon_Knife.Slash"
util.PrecacheSound( "inslash1.wav" )
util.PrecacheSound( "inslash2.wav" )
//"Weapon_Knife.Stab"
util.PrecacheSound( "instab.wav" )

//DEAGLE
//"Weapon_DEagle.Single"
util.PrecacheSound( "weapons/DEagle/deagle-1.wav" )
//"Weapon_DEagle.Clipout"
util.PrecacheSound( "weapons/DEagle/de_clipout.wav" )
//"Weapon_DEagle.Clipin"
util.PrecacheSound( "weapons/DEagle/de_clipin.wav" )
//"Weapon_DEagle.Deploy"
util.PrecacheSound( "weapons/DEagle/de_deploy.wav" )
//"Weapon_DEagle.Slideback"
util.PrecacheSound( "weapons/DEagle/de_slideback.wav" )

//AK47
//"Weapon_AK47.Single"
util.PrecacheSound( "weapons/ak47/ak47-1.wav" )
//"3rd_Weapon_AK47.Single"
util.PrecacheSound( "weapons/ak47/ak47-1.wav" )
//"Weapon_AK47.BoltPull"
util.PrecacheSound( "weapons/ak47/ak47_boltpull.wav" )
//"Weapon_AK47.Clipin"
util.PrecacheSound( "weapons/ak47/ak47_clipin.wav" )
//"Weapon_AK47.Clipout"
util.PrecacheSound( "weapons/ak47/ak47_clipout.wav" )

//"Default.PullPin_Grenade"
util.PrecacheSound( "weapons/pinpull.wav" )

//"Flashbang.Explode"
util.PrecacheSound( "weapons/flashbang/flashbang_explode1.wav" )	
util.PrecacheSound( "weapons/flashbang/flashbang_explode2.wav" )	
//"Flashbang.Bounce"
util.PrecacheSound( "weapons/flashbang/grenade_hit1.wav" )

//"SmokeGrenade.Bounce"
util.PrecacheSound( "weapons/smokegrenade/grenade_hit1.wav" )

//"HEGrenade.Bounce"
util.PrecacheSound( "weapons/hegrenade/he_bounce-1.wav" )

//"Default.ClipEmpty_Rifle"
util.PrecacheSound( "weapons/ClipEmpty_Rifle.wav" )
//"Default.ClipEmpty_Pistol"
util.PrecacheSound( "weapons/ClipEmpty_Pistol.wav" )
//"Default.Zoom"
util.PrecacheSound( "weapons/zoom.wav" )

//"BaseGrenade.Explode"
util.PrecacheSound( "weapons/hegrenade/explode3.wav" )
util.PrecacheSound( "weapons/hegrenade/explode4.wav" )
util.PrecacheSound( "weapons/hegrenade/explode5.wav" )

//"BaseExplosionEffect.Sound"
util.PrecacheSound( "weapons/debris1.wav" )
util.PrecacheSound( "weapons/debris2.wav" )
//"BaseSmokeEffect.Sound"
util.PrecacheSound( "weapons/smokegrenade/sg_explode.wav" )

//"c4.disarmstart"
util.PrecacheSound( "weapons/c4/c4_disarm.wav" )
//"c4.disarmfinish"
util.PrecacheSound( "weapons/c4/c4_disarm.wav" )
//"c4.explode"
util.PrecacheSound( "weapons/c4/c4_explode1.wav" )
//"c4.click"
util.PrecacheSound( "weapons/c4/c4_click.wav" )
//"c4.plant"
util.PrecacheSound( "weapons/c4/c4_plant.wav" )
//"C4.PlantSound"
util.PrecacheSound( "weapons/c4/c4_beep1.wav" )

//"defuser.equip"
util.PrecacheSound( "items/defuser_equip.wav" )
-------------------------------------------------------
--	Half-Life 2 + EP1 + EP2 + HL2DM + Lost Coast Audio
--	Encoded by V92
--	Profile Link:	http://steamcommunity.com/id/JesseVanover/
--	Workshop Link:	http://steamcommunity.com/sharedfiles/filedetails/?id=655569086
-------------------------------------------------------

---------------------------------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------------------------
--	NPC
---------------------------------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------------------------

------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------
--	ROLLEMINE
------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------

sound.Add( { ["name"] = "HL2.Rollermine.RemoteYes",
	["channel"] = CHAN_STATIC,
	["volume"] = 1.0 ,
	["level"] = 85 ,
	["pitch"] = { 95 , 105 } ,
	["sound"] = { 
		"^)npc/roller/remote_yes.wav" ,
	} ,
} )

------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------
--	COMBINE SOLDIER
------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------

---------------------------------------------
---------------------------------------------
---------------------------------------------
--	ZIPLINE
---------------------------------------------
---------------------------------------------
---------------------------------------------

sound.Add({	["name"] = "Combine_Soldier.ZipLine.Clip" ,
	["channel"] = CHAN_STATIC ,
	["volume"] = 1.0 ,
	["level"] = 100 ,
	["pitch"] = { 90, 110 } ,
	["sound"] = { 
		"jessev92/lostcoast/npc/combine_soldier/zipline_clip1.wav" ,
		"jessev92/lostcoast/npc/combine_soldier/zipline_clip2.wav" ,
	} ,
} )

------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------
--	VEHICLES
------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------

---------------------------------------------
---------------------------------------------
--	V8
---------------------------------------------
---------------------------------------------

sound.Add( {	["name"] = "HL2.Vehicles.V8.Gear4_Cruise" ,
	["channel"] = CHAN_STATIC ,
	["volume"] = 1 ,
	["level"] = 100 ,
	["pitch"] = { 100 } ,
	["sound"] = { 
		"vehicles/v8/fourth_cruise_loop2.wav" ,
	} ,
} )
util.PrecacheSound( "vehicles/v8/fourth_cruise_loop2.wav" )

---------------------------------------------
---------------------------------------------
--	EP2 Charger
---------------------------------------------
---------------------------------------------
sound.Add( {	["name"] = "EP2.Charger.RadarPing" ,
	["channel"] = CHAN_ITEM ,
	["volume"] = 0.05 ,
	["level"] = 10 ,
	["pitch"] = { 90 , 110 } ,
	["sound"] = { "^)vehicles/junker/radar_ping_friendly1.wav" }
} )
util.PrecacheSound( "vehicles/junker/radar_ping_friendly1.wav" )

sound.Add( {	["name"] = "EP2.V8.Impact_Heavy" ,
	["channel"] = CHAN_ITEM ,
	["volume"] = 1 ,
	["level"] = 90 ,
	["pitch"] = { 90 , 110 } ,
	["sound"] = { 
		"^)vehicles/v8/vehicle_impact_heavy1.wav" ,
		"^)vehicles/v8/vehicle_impact_heavy2.wav" ,
		"^)vehicles/v8/vehicle_impact_heavy3.wav" ,
		"^)vehicles/v8/vehicle_impact_heavy4.wav" ,
	} ,
} )
util.PrecacheSound( "vehicles/v8/vehicle_impact_heavy1.wav" )
util.PrecacheSound( "vehicles/v8/vehicle_impact_heavy2.wav" )
util.PrecacheSound( "vehicles/v8/vehicle_impact_heavy3.wav" )
util.PrecacheSound( "vehicles/v8/vehicle_impact_heavy4.wav" )

------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------
--	VOICE OVERS
------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------

---------------------------------------------
---------------------------------------------
--	Player Coughing
---------------------------------------------
---------------------------------------------

sound.Add( {	["name"] = "HL2.VO.Player.Cough" ,
	["channel"] = CHAN_VOICE ,
	["volume"] = 1.0 ,
	["level"] = 75 ,
	["pitch"] = { 95 , 105 } ,
	["sound"] = { "ambient/voices/cough1.wav" , "ambient/voices/cough2.wav" , "ambient/voices/cough3.wav" , "ambient/voices/cough4.wav" }
} )
util.PrecacheSound( "ambient/voices/cough1.wav" )
util.PrecacheSound( "ambient/voices/cough2.wav" )
util.PrecacheSound( "ambient/voices/cough3.wav" )
util.PrecacheSound( "ambient/voices/cough4.wav" )

------------------------------------------------------------------------------------------
--	HL2 :: Physics
------------------------------------------------------------------------------------------

sound.Add( {	["name"] = "HL2.Physics.Concrete.Impact.Bullet" ,
	["channel"] = CHAN_BODY ,
	["level"] = 75 ,
	["volume"] = 1 ,
	["pitch"] = { 85, 115 } ,
	["sound"] = { 
		"physics/concrete/concrete_impact_bullet1.wav" , 
		"physics/concrete/concrete_impact_bullet2.wav" , 
		"physics/concrete/concrete_impact_bullet3.wav" , 
		"physics/concrete/concrete_impact_bullet4.wav" , 
	} ,
} )
util.PrecacheSound( "physics/concrete/concrete_impact_bullet1.wav" )
util.PrecacheSound( "physics/concrete/concrete_impact_bullet2.wav" )
util.PrecacheSound( "physics/concrete/concrete_impact_bullet3.wav" )
util.PrecacheSound( "physics/concrete/concrete_impact_bullet4.wav" )

sound.Add( {	["name"] = "HL2.Physics.Glass.Impact.Bullet" ,
	["channel"] = CHAN_BODY ,
	["level"] = 75 ,
	["volume"] = 1 ,
	["pitch"] = { 85, 115 } ,
	["sound"] = { 
		"physics/Glass/glass_impact_bullet1.wav" , 
		"physics/Glass/glass_impact_bullet2.wav" , 
		"physics/Glass/glass_impact_bullet3.wav" , 
		"physics/Glass/glass_impact_bullet4.wav" , 
	} ,
} )
util.PrecacheSound( "physics/Glass/glass_impact_bullet1.wav" )
util.PrecacheSound( "physics/Glass/glass_impact_bullet2.wav" )
util.PrecacheSound( "physics/Glass/glass_impact_bullet3.wav" )
util.PrecacheSound( "physics/Glass/glass_impact_bullet4.wav" )

sound.Add( {	["name"] = "HL2.Physics.Flesh.Impact.Bullet" ,
	["channel"] = CHAN_BODY ,
	["level"] = 75 ,
	["volume"] = 1 ,
	["pitch"] = { 85, 115 } ,
	["sound"] = { 
		"physics/flesh/flesh_impact_bullet1.wav" , 
		"physics/flesh/flesh_impact_bullet2.wav" , 
		"physics/flesh/flesh_impact_bullet3.wav" , 
		"physics/flesh/flesh_impact_bullet4.wav" , 
		"physics/flesh/flesh_impact_bullet5.wav" , 
	} ,
} )
util.PrecacheSound( "physics/flesh/flesh_impact_bullet1.wav" )
util.PrecacheSound( "physics/flesh/flesh_impact_bullet2.wav" )
util.PrecacheSound( "physics/flesh/flesh_impact_bullet3.wav" )
util.PrecacheSound( "physics/flesh/flesh_impact_bullet4.wav" )
util.PrecacheSound( "physics/flesh/flesh_impact_bullet5.wav" )

sound.Add( {	["name"] = "HL2.Physics.Computer.Impact.Bullet" ,
	["channel"] = CHAN_BODY ,
	["level"] = 75 ,
	["volume"] = 1 ,
	["pitch"] = { 85, 115 } ,
	["sound"] = { 
		"physics/metal/metal_computer_impact_bullet1.wav" , 
		"physics/metal/metal_computer_impact_bullet2.wav" , 
		"physics/metal/metal_computer_impact_bullet3.wav" , 
	} ,
} )
util.PrecacheSound( "physics/metal/metal_computer_impact_bullet1.wav" )
util.PrecacheSound( "physics/metal/metal_computer_impact_bullet2.wav" )
util.PrecacheSound( "physics/metal/metal_computer_impact_bullet3.wav" )

sound.Add( {	["name"] = "HL2.Physics.Metal.Box.Impact.Bullet" ,
	["channel"] = CHAN_BODY ,
	["level"] = 75 ,
	["volume"] = 1 ,
	["pitch"] = { 85, 115 } ,
	["sound"] = { 
		"physics/metal/metal_box_impact_bullet1.wav" , 
		"physics/metal/metal_box_impact_bullet2.wav" , 
	} ,
} )
util.PrecacheSound( "physics/metal/metal_box_impact_bullet1.wav" )
util.PrecacheSound( "physics/metal/metal_box_impact_bullet2.wav" )

sound.Add( {
	["name"] = "HL2.Physics.Metal.Sheet.Impact.Bullet" ,
	["channel"] = CHAN_BODY ,
	["level"] = 75 ,
	["volume"] = 1 ,
	["pitch"] = { 85, 115 } ,
	["sound"] = { 
		"physics/metal/metal_sheet_impact_bullet1.wav" , 
		"physics/metal/metal_sheet_impact_bullet2.wav" , 
	} ,
} )
util.PrecacheSound( "physics/metal/metal_sheet_impact_bullet1.wav" )
util.PrecacheSound( "physics/metal/metal_sheet_impact_bullet2.wav" )

sound.Add( {
	["name"] = "HL2.Physics.Plastic.Box.Impact.Bullet" ,
	["channel"] = CHAN_BODY ,
	["level"] = 75 ,
	["volume"] = 1 ,
	["pitch"] = { 85, 115 } ,
	["sound"] = { 
		"physics/plastic/plastic_box_impact_bullet1.wav" , 
		"physics/plastic/plastic_box_impact_bullet2.wav" , 
		"physics/plastic/plastic_box_impact_bullet3.wav" , 
		"physics/plastic/plastic_box_impact_bullet4.wav" , 
		"physics/plastic/plastic_box_impact_bullet5.wav" , 
	} ,
} )
util.PrecacheSound( "physics/plastic/plastic_box_impact_bullet1.wav" )
util.PrecacheSound( "physics/plastic/plastic_box_impact_bullet2.wav" )
util.PrecacheSound( "physics/plastic/plastic_box_impact_bullet3.wav" )
util.PrecacheSound( "physics/plastic/plastic_box_impact_bullet4.wav" )
util.PrecacheSound( "physics/plastic/plastic_box_impact_bullet5.wav" )

sound.Add( {
	["name"] = "HL2.Physics.Plastic.Barrel.Impact.Bullet" ,
	["channel"] = CHAN_BODY ,
	["level"] = 75 ,
	["volume"] = 1 ,
	["pitch"] = { 85, 115 } ,
	["sound"] = { 
		"physics/plastic/plastic_barrel_impact_bullet1.wav" , 
		"physics/plastic/plastic_barrel_impact_bullet2.wav" , 
		"physics/plastic/plastic_barrel_impact_bullet3.wav" , 
		"physics/plastic/plastic_barrel_impact_bullet4.wav" , 
	} ,
} )
util.PrecacheSound( "physics/plastic/plastic_barrel_impact_bullet1.wav" )
util.PrecacheSound( "physics/plastic/plastic_barrel_impact_bullet2.wav" )
util.PrecacheSound( "physics/plastic/plastic_barrel_impact_bullet3.wav" )
util.PrecacheSound( "physics/plastic/plastic_barrel_impact_bullet4.wav" )

sound.Add( {
	["name"] = "HL2.Physics.Metal.Solid.Impact.Bullet" ,
	["channel"] = CHAN_BODY ,
	["level"] = 75 ,
	["volume"] = 1 ,
	["pitch"] = { 85, 115 } ,
	["sound"] = { 
		"physics/metal/metal_solid_impact_bullet1.wav" , 
		"physics/metal/metal_solid_impact_bullet2.wav" , 
		"physics/metal/metal_solid_impact_bullet3.wav" , 
		"physics/metal/metal_solid_impact_bullet4.wav" , 
	} ,
} )
util.PrecacheSound( "physics/metal/metal_solid_impact_bullet1.wav" )
util.PrecacheSound( "physics/metal/metal_solid_impact_bullet2.wav" )
util.PrecacheSound( "physics/metal/metal_solid_impact_bullet3.wav" )
util.PrecacheSound( "physics/metal/metal_solid_impact_bullet4.wav" )

sound.Add( {
	["name"] = "HL2.Physics.Sand.Impact.Bullet" ,
	["channel"] = CHAN_BODY ,
	["level"] = 75 ,
	["volume"] = 1 ,
	["pitch"] = { 85, 115 } ,
	["sound"] = { 
		"physics/surfaces/sand_impact_bullet1.wav" , 
		"physics/surfaces/sand_impact_bullet2.wav" , 
		"physics/surfaces/sand_impact_bullet3.wav" , 
		"physics/surfaces/sand_impact_bullet4.wav" , 
	} ,
} )
util.PrecacheSound( "physics/surfaces/sand_impact_bullet1.wav" )
util.PrecacheSound( "physics/surfaces/sand_impact_bullet2.wav" )
util.PrecacheSound( "physics/surfaces/sand_impact_bullet3.wav" )
util.PrecacheSound( "physics/surfaces/sand_impact_bullet4.wav" )

sound.Add( {
	["name"] = "HL2.Physics.Underwater.Impact.Bullet" ,
	["channel"] = CHAN_BODY ,
	["level"] = 75 ,
	["volume"] = 1 ,
	["pitch"] = { 85, 115 } ,
	["sound"] = { 
		"physics/surfaces/underwater_impact_bullet1.wav" , 
		"physics/surfaces/underwater_impact_bullet2.wav" , 
		"physics/surfaces/underwater_impact_bullet3.wav" ,
	} ,
} )
util.PrecacheSound( "physics/surfaces/underwater_impact_bullet1.wav" )
util.PrecacheSound( "physics/surfaces/underwater_impact_bullet2.wav" )
util.PrecacheSound( "physics/surfaces/underwater_impact_bullet3.wav" )

sound.Add( {
	["name"] = "HL2.Physics.Tile.Impact.Bullet" ,
	["channel"] = CHAN_BODY ,
	["level"] = 75 ,
	["volume"] = 1 ,
	["pitch"] = { 85, 115 } ,
	["sound"] = { 
		"physics/surfaces/tile_impact_bullet1.wav" , 
		"physics/surfaces/tile_impact_bullet2.wav" , 
		"physics/surfaces/tile_impact_bullet3.wav" ,
		"physics/surfaces/tile_impact_bullet4.wav" ,
	} ,
} )
util.PrecacheSound( "physics/surfaces/tile_impact_bullet1.wav" )
util.PrecacheSound( "physics/surfaces/tile_impact_bullet2.wav" )
util.PrecacheSound( "physics/surfaces/tile_impact_bullet3.wav" )
util.PrecacheSound( "physics/surfaces/tile_impact_bullet4.wav" )

sound.Add( {
	["name"] = "HL2.Physics.Wood.Box.Impact.Bullet" ,
	["channel"] = CHAN_BODY ,
	["level"] = 75 ,
	["volume"] = 1 ,
	["pitch"] = { 85, 115 } ,
	["sound"] = { 
		"physics/wood/wood_box_impact_bullet1.wav" , 
		"physics/wood/wood_box_impact_bullet2.wav" , 
		"physics/wood/wood_box_impact_bullet3.wav" ,
		"physics/wood/wood_box_impact_bullet4.wav" ,
	} ,
} )
util.PrecacheSound( "physics/wood/wood_box_impact_bullet1.wav" )
util.PrecacheSound( "physics/wood/wood_box_impact_bullet2.wav" )
util.PrecacheSound( "physics/wood/wood_box_impact_bullet3.wav" )
util.PrecacheSound( "physics/wood/wood_box_impact_bullet4.wav" )

sound.Add( {
	["name"] = "HL2.Physics.Wood.Solid.Impact.Bullet" ,
	["channel"] = CHAN_BODY ,
	["level"] = 75 ,
	["volume"] = 1 ,
	["pitch"] = { 85, 115 } ,
	["sound"] = { 
		"physics/wood/wood_solid_impact_bullet1.wav" , 
		"physics/wood/wood_solid_impact_bullet2.wav" , 
		"physics/wood/wood_solid_impact_bullet3.wav" ,
		"physics/wood/wood_solid_impact_bullet4.wav" ,
		"physics/wood/wood_solid_impact_bullet5.wav" ,
	} ,
} )
util.PrecacheSound( "physics/wood/wood_solid_impact_bullet1.wav" )
util.PrecacheSound( "physics/wood/wood_solid_impact_bullet2.wav" )
util.PrecacheSound( "physics/wood/wood_solid_impact_bullet3.wav" )
util.PrecacheSound( "physics/wood/wood_solid_impact_bullet4.wav" )
util.PrecacheSound( "physics/wood/wood_solid_impact_bullet5.wav" )

sound.Add( {
	["name"] = "HL2.Ambient.Energy.Zap" ,
	["channel"] = CHAN_BODY ,
	["level"] = 75 ,
	["volume"] = 1 ,
	["pitch"] = { 85, 115 } ,
	["sound"] = { 
		"ambient/energy/zap1.wav" ,
		"ambient/energy/zap2.wav" ,
		"ambient/energy/zap3.wav" ,
		"ambient/energy/zap4.wav" ,
		"ambient/energy/zap5.wav" ,
		"ambient/energy/zap6.wav" ,
		"ambient/energy/zap7.wav" ,
		"ambient/energy/zap8.wav" ,
		"ambient/energy/zap9.wav" ,
	} ,
} )
util.PrecacheSound( "ambient/energy/zap1.wav" )
util.PrecacheSound( "ambient/energy/zap2.wav" )
util.PrecacheSound( "ambient/energy/zap3.wav" )
util.PrecacheSound( "ambient/energy/zap4.wav" )
util.PrecacheSound( "ambient/energy/zap5.wav" )
util.PrecacheSound( "ambient/energy/zap6.wav" )
util.PrecacheSound( "ambient/energy/zap7.wav" )
util.PrecacheSound( "ambient/energy/zap8.wav" )
util.PrecacheSound( "ambient/energy/zap9.wav" )

sound.Add( { ["name"] = "HL2.Physics.Wood.Box.Impact.Hard" ,
	["channel"] = CHAN_BODY ,
	["level"] = 75 ,
	["volume"] = 1 ,
	["pitch"] = { 85, 115 } ,
	["sound"] = { 
		"physics/wood/wood_box_impact_hard1.wav" , 
		"physics/wood/wood_box_impact_hard2.wav" , 
		"physics/wood/wood_box_impact_hard3.wav" , 
		"physics/wood/wood_box_impact_hard4.wav" , 
		"physics/wood/wood_box_impact_hard5.wav" , 
		"physics/wood/wood_box_impact_hard6.wav" , 
	} ,
} )
util.PrecacheSound( "physics/wood/wood_box_impact_hard1.wav" )
util.PrecacheSound( "physics/wood/wood_box_impact_hard2.wav" )
util.PrecacheSound( "physics/wood/wood_box_impact_hard3.wav" )
util.PrecacheSound( "physics/wood/wood_box_impact_hard4.wav" )
util.PrecacheSound( "physics/wood/wood_box_impact_hard5.wav" )
util.PrecacheSound( "physics/wood/wood_box_impact_hard6.wav" )

---------------------------------------------
---------------------------------------------
--	Wood Furniture Break
---------------------------------------------
---------------------------------------------
sound.Add( {
	["name"] = "HL2.Physics.Wood.Furniture.Break" ,
	["channel"] = CHAN_ITEM ,
	["volume"] = 1.0 ,
	["level"] = 75 ,
	["pitch"] = { 70 , 140 } ,
	["sound"] = { "physics/wood/wood_furniture_break1.wav" , "physics/wood/wood_furniture_break2.wav" }
} )
util.PrecacheSound( "physics/wood/wood_furniture_break1.wav" )
util.PrecacheSound( "physics/wood/wood_furniture_break2.wav" )

------------------------------------------------------------------------------------------
--	HL2 :: Weapons
------------------------------------------------------------------------------------------

---------------------------------------------
---------------------------------------------
-- SLAM
---------------------------------------------
---------------------------------------------

sound.Add( { ["name"] = "HL2.SLAM.MineMode",
	["channel"] = CHAN_STATIC,
	["volume"] = 1.0 ,
	["level"] = 85 ,
	["pitch"] = { 95 , 105 } ,
	["sound"] = { 
		"^)weapons/slam/mine_mode.wav" ,
	} ,
} )

---------------------------------------------
---------------------------------------------
--	Generic Explosion
---------------------------------------------
---------------------------------------------
sound.Add( {
	["name"] = "HL2.Generic.Explosion" ,
	["channel"] = CHAN_WEAPON ,
	["volume"] = 1.0 ,
	["level"] = 75 ,
	["pitch"] = { 80 , 140 } ,
	["sound"] = { "weapons/explode3.wav" , "weapons/explode4.wav" , "weapons/explode5.wav" }
} )
util.PrecacheSound( "weapons/explode3.wav" )
util.PrecacheSound( "weapons/explode4.wav" )
util.PrecacheSound( "weapons/explode5.wav" )

-------------------------------------------------------
--	Black Mesa (Source) Audio
--	Encoded by V92
--	Profile Link:	http://steamcommunity.com/id/JesseVanover/
--	Workshop Link:	http://steamcommunity.com/sharedfiles/filedetails/?id=
-------------------------------------------------------

---------------------------------------------------------------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------------------------------------------------------
--	WEAPONS
---------------------------------------------------------------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------------------------------------------------------

--------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------
--	HANDHELD
--------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------

-------------------------------------------------------
-------------------------------------------------------
--	TORCH
-------------------------------------------------------
-------------------------------------------------------

sound.Add( { ["name"] = "BMS.Torch.Draw",
	["channel"] = CHAN_STATIC,
	["volume"] = 1.0 ,
	["level"] = 85 ,
	["pitch"] = { 95 , 105 } ,
	["sound"] = { 
		"jessev92/weapons/univ/draw1.wav" ,
		"jessev92/weapons/univ/draw2.wav" ,
	} ,
} )

sound.Add( { ["name"] = "BMS.Torch.Holster",
	["channel"] = CHAN_STATIC,
	["volume"] = 1.0 ,
	["level"] = 85 ,
	["pitch"] = { 95 , 105 } ,
	["sound"] = { 
		"jessev92/weapons/univ/holster1.wav" ,
	} ,
} )

sound.Add( { ["name"] = "BMS.Torch.ClickOn",
	["channel"] = CHAN_WEAPON,
	["volume"] = 1.0 ,
	["level"] = 85 ,
	["pitch"] = { 95 , 105 } ,
	["sound"] = { 
		"jessev92/items/flashlight/on1.wav" ,
		"jessev92/items/flashlight/on2.wav" ,
	} ,
} )

sound.Add( { ["name"] = "BMS.Torch.ClickOff",
	["channel"] = CHAN_WEAPON,
	["volume"] = 1.0 ,
	["level"] = 85 ,
	["pitch"] = { 95 , 105 } ,
	["sound"] = { 
		"jessev92/items/flashlight/off1.wav" ,
		"jessev92/items/flashlight/off2.wav" ,
	} ,
} )

sound.Add( { ["name"] = "BMS.Torch.Shake",
	["channel"] = CHAN_STATIC,
	["volume"] = 1.0 ,
	["level"] = 85 ,
	["pitch"] = { 95 , 105 } ,
	["sound"] = {
		"weapons/shotgun/shotgun_empty.wav" ,
	} ,
} )

--------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------
--	EMPLACEMENTS
--------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------

-------------------------------------------------------
-------------------------------------------------------
--	M2HB .50 Cal
-------------------------------------------------------
-------------------------------------------------------

sound.Add( { ["name"] = "BMS.Weapons.50Cal" ,
	["channel"] = CHAN_WEAPON ,
	["volume"] = 1.0 ,
	["level"] = 100 ,
	["pitch"] = { 95 , 105 } ,
	["sound"] = { 
		"jessev92/bms/weapons/m2hb/50cal_single1.wav" , 
		"jessev92/bms/weapons/m2hb/50cal_single2.wav" , 
		"jessev92/bms/weapons/m2hb/50cal_single3.wav" ,
	}
} )

util.PrecacheSound( "jessev92/bms/weapons/m2hb/50cal_single1.wav" )
util.PrecacheSound( "jessev92/bms/weapons/m2hb/50cal_single2.wav" )
util.PrecacheSound( "jessev92/bms/weapons/m2hb/50cal_single3.wav" )

---------------------------------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------------------------
--	SiN Episodes
---------------------------------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------------------------

------------------------------------------------------------------------------------------
--	Weapons
------------------------------------------------------------------------------------------

---------------------------------------------
--	Auto Magnum
--	.44 Magnum
---------------------------------------------

sound.Add( {
	["name"] = "SiN.AutoMagnum.Raise" ,
	["channel"] = CHAN_AUTO ,
	["volume"] = 0.7 ,
	["level"] = 65 ,
	["pitch"] = { 95 , 100 } ,
	["sound"] = "^)jessev92/sin/weapons/pistol/pistol_raise.wav"
} )
util.PrecacheSound( "sound/jessev92/sin/weapons/pistol/pistol_raise.wav" )

sound.Add( {
	["name"] = "SiN.AutoMagnum.Single" ,
	["channel"] = CHAN_AUTO ,
	["volume"] = 1.0 ,
	["level"] = 140 ,
	["pitch"] = { 95 , 103 } ,
	["sound"] = { 
		"^)jessev92/sin/weapons/pistol/pistol_fire1.wav" ,
		"^)jessev92/sin/weapons/pistol/pistol_fire2.wav" ,
	} ,
} )
util.PrecacheSound( "sound/jessev92/sin/weapons/pistol/pistol_fire1.wav" )
util.PrecacheSound( "sound/jessev92/sin/weapons/pistol/pistol_fire2.wav" )

sound.Add( {
	["name"] = "SiN.AutoMagnum.Empty" ,
	["channel"] = CHAN_AUTO ,
	["volume"] = 0.6 ,
	["level"] = 140 ,
	["pitch"] = { 97 , 102 } ,
	["sound"] = { 
		"^)jessev92/sin/weapons/pistol/pistol_empty.wav"
	} ,
} )
util.PrecacheSound( "sound/jessev92/sin/weapons/pistol/pistol_empty.wav" )

sound.Add( {
	["name"] = "SiN.Pistol.Empty" ,
	["channel"] = CHAN_AUTO ,
	["volume"] = 0.6 ,
	["level"] = 140 ,
	["pitch"] = { 97 , 102 } ,
	["sound"] = { 
		"^)jessev92/sin/weapons/pistol/pistol_empty.wav"
	} ,
} )
util.PrecacheSound( "sound/jessev92/sin/weapons/pistol/pistol_empty.wav" )

sound.Add( {
	["name"] = "SiN.AutoMagnum.Reload" ,
	["channel"] = CHAN_AUTO ,
	["volume"] = 0.7 ,
	["level"] = 65 ,
	-- ["pitch"] = { 97 , 102 } ,
	["sound"] = { 
		"^)jessev92/sin/weapons/pistol/pistol_reload1.wav"
	} ,
} )
util.PrecacheSound( "sound/jessev92/sin/weapons/pistol/pistol_reload1.wav" )

sound.Add( {
	["name"] = "SiN.AutoMagnum.ReloadOne" ,
	["channel"] = CHAN_AUTO ,
	["volume"] = 0.7 ,
	["level"] = 65 ,
	-- ["pitch"] = { 97 , 102 } ,
	["sound"] = { 
		"^)jessev92/sin/weapons/pistol/pistol_reload1.wav"
	} ,
} )
util.PrecacheSound( "sound/jessev92/sin/weapons/pistol/pistol_reload1.wav" )

sound.Add( {
	["name"] = "SiN.AutoMagnum.ReloadTwo" ,
	["channel"] = CHAN_AUTO ,
	["volume"] = 0.7 ,
	["level"] = 65 ,
	-- ["pitch"] = { 97 , 102 } ,
	["sound"] = { 
		"^)jessev92/sin/weapons/pistol/pistol_reload1.wav"
	} ,
} )
util.PrecacheSound( "sound/jessev92/sin/weapons/pistol/pistol_reload1.wav" )

sound.Add( {
	["name"] = "SiN.AutoMagnum.NPC_Reload" ,
	["channel"] = CHAN_AUTO ,
	["volume"] = 0.7 ,
	["level"] = 65 ,
	-- ["pitch"] = { 97 , 102 } ,
	["sound"] = { 
		"^)jessev92/sin/weapons/pistol/pistol_reload1.wav"
	} ,
} )
util.PrecacheSound( "sound/jessev92/sin/weapons/pistol/pistol_reload1.wav" )

sound.Add( {
	["name"] = "SiN.AutoMagnum.NPC_Single" ,
	["channel"] = CHAN_AUTO ,
	["volume"] = 0.9 ,
	["level"] = 65 ,
	["pitch"] = { 95 , 105 } ,
	["sound"] = { 
		"^)jessev92/sin/weapons/pistol/pistol_NPC_fire.wav"
	} ,
} )
util.PrecacheSound( "sound/jessev92/sin/weapons/pistol/pistol_NPC_fire.wav" )

sound.Add( {
	["name"] = "SiN.AutoMagnum.Double" ,
	["channel"] = CHAN_AUTO ,
	["volume"] = 1.0 ,
	["level"] = 140 ,
	["pitch"] = { 100 , 101 } ,
	["sound"] = { 
		"^)jessev92/sin/weapons/pistol/pistol_uraniumshot.wav"
	} ,
} )
util.PrecacheSound( "sound/jessev92/sin/weapons/pistol/pistol_uraniumshot.wav" )

sound.Add( {
	["name"] = "SiN.AutoMagnum.Melee_Miss" ,
	["channel"] = CHAN_AUTO ,
	["volume"] = 0.3 ,
	["level"] = 140 ,
	["pitch"] = { 95 , 100 } ,
	["sound"] = { 
		"^)jessev92/sin/weapons/melee/melee_move3.wav"
	} ,
} )
util.PrecacheSound( "sound/jessev92/sin/weapons/melee/melee_move3.wav" )

sound.Add( {
	["name"] = "SiN.AutoMagnum.Melee_Hit_World" ,
	["channel"] = CHAN_AUTO ,
	["volume"] = 0.8 ,
	["level"] = 140 ,
	["pitch"] = { 90 , 115 } ,
	["sound"] = { 
		"^)jessev92/sin/weapons/melee/melee_hit_magnum.wav"
	} ,
} )
util.PrecacheSound( "sound/jessev92/sin/weapons/melee/melee_hit_magnum.wav" )

sound.Add( {
	["name"] = "SiN.AutoMagnum.Melee_Hit" ,
	["channel"] = CHAN_AUTO ,
	["volume"] = 0.9 ,
	["level"] = 140 ,
	["pitch"] = { 90 , 115 } ,
	["sound"] = { 
		"^)jessev92/sin/weapons/melee/melee_hit_magnum.wav"
	} ,
} )
util.PrecacheSound( "sound/jessev92/sin/weapons/melee/melee_hit_magnum.wav" )

---------------------------------------------
--	Assault Rifle
--	5.56x45mm NATO
---------------------------------------------

sound.Add( {
	["name"] = "SiN.AssaultRifle.Raise" ,
	["channel"] = CHAN_AUTO ,
	["volume"] = 0.7 ,
	["level"] = 65 ,
	["pitch"] = { 95 , 100 } ,
	["sound"] = "^)jessev92/sin/weapons/assault/assault_raise.wav"
} )
util.PrecacheSound( "sound/jessev92/sin/weapons/assault/assault_raise.wav" )

sound.Add( {
	["name"] = "SiN.AssaultRifle.Reload" ,
	["channel"] = CHAN_AUTO ,
	["volume"] = 1.0 ,
	["level"] = 65 ,
	-- ["pitch"] = { 95 , 100 } ,
	["sound"] = "^)jessev92/sin/weapons/assault/assault_reload1.wav"
} )
util.PrecacheSound( "sound/jessev92/sin/weapons/assault/assault_reload1.wav" )

sound.Add( {
	["name"] = "SiN.AssaultRifle.ReloadOne" ,
	["channel"] = CHAN_AUTO ,
	["volume"] = 1.0 ,
	["level"] = 65 ,
	-- ["pitch"] = { 95 , 100 } ,
	["sound"] = "^)jessev92/sin/weapons/assault/assault_reload1.wav"
} )
util.PrecacheSound( "sound/jessev92/sin/weapons/assault/assault_reload1.wav" )

sound.Add( {
	["name"] = "SiN.AssaultRifle.ReloadTwo" ,
	["channel"] = CHAN_AUTO ,
	["volume"] = 1.0 ,
	["level"] = 65 ,
	-- ["pitch"] = { 95 , 100 } ,
	["sound"] = "^)jessev92/sin/weapons/assault/_assault_reload1.wav"
} )
util.PrecacheSound( "sound/jessev92/sin/weapons/assault/_assault_reload1.wav" )

sound.Add( {
	["name"] = "SiN.AssaultRifle.NPC_Reload" ,
	["channel"] = CHAN_AUTO ,
	["volume"] = 0.7 ,
	["level"] = 65 ,
	-- ["pitch"] = { 95 , 100 } ,
	["sound"] = "^)jessev92/sin/weapons/assault/assault_reload1.wav"
} )
util.PrecacheSound( "sound/jessev92/sin/weapons/assault/assault_reload1.wav" )

sound.Add( {
	["name"] = "SiN.AssaultRifle.Empty" ,
	["channel"] = CHAN_AUTO ,
	["volume"] = 0.7 ,
	["level"] = 65 ,
	-- ["pitch"] = { 95 , 100 } ,
	["sound"] = "^)jessev92/sin/weapons/pistol/pistol_empty.wav"
} )
util.PrecacheSound( "sound/jessev92/sin/weapons/pistol/pistol_empty.wav" )

sound.Add( {
	["name"] = "SiN.AssaultRifle.Single" ,
	["channel"] = CHAN_AUTO ,
	["volume"] = 0.7 ,
	["level"] = 140 ,
	["pitch"] = { 98 , 102 } ,
	["sound"] = "^)jessev92/sin/weapons/assault/assault_fire1.wav"
} )
util.PrecacheSound( "sound/jessev92/sin/weapons/assault/assault_fire1.wav" )

sound.Add( {
	["name"] = "SiN.AssaultRifle.Double" ,
	["channel"] = CHAN_AUTO ,
	["volume"] = 0.8 ,
	["level"] = 140 ,
	["pitch"] = { 90 , 112 } ,
	["sound"] = "^)jessev92/sin/weapons/assault/assault_grenade.wav"
} )
util.PrecacheSound( "sound/jessev92/sin/weapons/assault/assault_grenade.wav" )

sound.Add( {
	["name"] = "SiN.AssaultRifle.NPC_Single" ,
	["channel"] = CHAN_AUTO ,
	["volume"] = 0.5 ,
	["level"] = 140 ,
	["pitch"] = { 95 , 105 } ,
	["sound"] = "^)jessev92/sin/weapons/assault/npc_assault_fire1.wav"
} )
util.PrecacheSound( "sound/jessev92/sin/weapons/assault/npc_assault_fire1.wav" )

sound.Add( {
	["name"] = "SiN.AssaultRifle.Special1" ,
	["channel"] = CHAN_AUTO ,
	["volume"] = 0.7 ,
	["level"] = 65 ,
	-- ["pitch"] = { 95 , 105 } ,
	["sound"] = "^)jessev92/sin/weapons/assault/assault_switch.wav"
} )
util.PrecacheSound( "sound/jessev92/sin/weapons/assault/assault_switch.wav" )

sound.Add( {
	["name"] = "SiN.AssaultRifle.Special2" ,
	["channel"] = CHAN_AUTO ,
	["volume"] = 0.7 ,
	["level"] = 65 ,
	-- ["pitch"] = { 95 , 105 } ,
	["sound"] = "^)jessev92/sin/weapons/assault/assault_switch.wav"
} )
util.PrecacheSound( "sound/jessev92/sin/weapons/assault/assault_switch.wav" )

sound.Add( {
	["name"] = "SiN.AssaultRifle.Burst" ,
	["channel"] = CHAN_AUTO ,
	["volume"] = 0.7 ,
	["level"] = 140 ,
	["pitch"] = { 98 , 105 } ,
	["sound"] = "^)jessev92/sin/weapons/assault/assault_burst.wav"
} )
util.PrecacheSound( "sound/jessev92/sin/weapons/assault/assault_burst.wav" )

sound.Add( {
	["name"] = "SiN.AssaultRifle.Melee_Miss" ,
	["channel"] = CHAN_AUTO ,
	["volume"] = 0.6 ,
	["level"] = 140 ,
	-- ["pitch"] = { 98 , 105 } ,
	["sound"] = "^)jessev92/sin/weapons/melee/melee_move2.wav"
} )
util.PrecacheSound( "sound/jessev92/sin/weapons/melee/melee_move2.wav" )

sound.Add( {
	["name"] = "SiN.AssaultRifle.Melee_Hit_World" ,
	["channel"] = CHAN_AUTO ,
	["volume"] = 0.8 ,
	["level"] = 140 ,
	["pitch"] = { 90 , 115 } ,
	["sound"] = "^)jessev92/sin/weapons/melee/melee_hit_assault.wav"
} )
util.PrecacheSound( "sound/jessev92/sin/weapons/melee/melee_hit_assault.wav" )

sound.Add( {
	["name"] = "SiN.AssaultRifle.Melee_Hit" ,
	["channel"] = CHAN_AUTO ,
	["volume"] = 0.9 ,
	["level"] = 140 ,
	["pitch"] = { 90 , 115 } ,
	["sound"] = "^)jessev92/sin/weapons/melee/melee_hit_assault.wav"
} )
util.PrecacheSound( "sound/jessev92/sin/weapons/melee/melee_hit_assault.wav" )

---------------------------------------------
--	Scattergun
--	12-Gauge
---------------------------------------------

sound.Add( {
	["name"] = "SiN.Scattergun.Raise" ,
	["channel"] = CHAN_AUTO ,
	["volume"] = 0.7 ,
	["level"] = 65 ,
	["pitch"] = { 95 , 100 } ,
	["sound"] = "^)jessev92/sin/weapons/shotgun/shotgun_raise.wav"
} )
util.PrecacheSound( "sound/jessev92/sin/weapons/shotgun/shotgun_raise.wav" )

sound.Add( {
	["name"] = "SiN.Scattergun.Reload" ,
	["channel"] = CHAN_AUTO ,
	["volume"] = 0.7 ,
	["level"] = 65 ,
	-- ["pitch"] = { 95 , 100 } ,
	["sound"] = "^)jessev92/sin/weapons/shotgun/shotgun_full_reload.wav"
} )
util.PrecacheSound( "sound/jessev92/sin/weapons/shotgun/shotgun_full_reload.wav" )

sound.Add( {
	["name"] = "SiN.Scattergun.ReloadOne" ,
	["channel"] = CHAN_AUTO ,
	["volume"] = 0.7 ,
	["level"] = 65 ,
	-- ["pitch"] = { 95 , 100 } ,
	["sound"] = "^)jessev92/sin/weapons/shotgun/shotgun_full_reload.wav"
} )
util.PrecacheSound( "sound/jessev92/sin/weapons/shotgun/shotgun_full_reload.wav" )

sound.Add( {
	["name"] = "SiN.Scattergun.ReloadTwo" ,
	["channel"] = CHAN_AUTO ,
	["volume"] = 0.7 ,
	["level"] = 65 ,
	-- ["pitch"] = { 95 , 100 } ,
	["sound"] = "^)jessev92/sin/weapons/shotgun/shotgun_reloadTwo.wav"
} )
util.PrecacheSound( "sound/jessev92/sin/weapons/shotgun/shotgun_reloadTwo.wav" )

sound.Add( {
	["name"] = "SiN.Scattergun.NPC_Reload" ,
	["channel"] = CHAN_AUTO ,
	["volume"] = 0.5 ,
	["level"] = 65 ,
	-- ["pitch"] = { 95 , 100 } ,
	["sound"] = {	
		"^)jessev92/sin/weapons/shotgun/shotgun_reload1.wav" ,
		"^)jessev92/sin/weapons/shotgun/shotgun_reload2.wav" ,
		"^)jessev92/sin/weapons/shotgun/shotgun_reload3.wav" ,
	} ,
} )
util.PrecacheSound( "sound/jessev92/sin/weapons/shotgun/shotgun_reload1.wav" )
util.PrecacheSound( "sound/jessev92/sin/weapons/shotgun/shotgun_reload2.wav" )
util.PrecacheSound( "sound/jessev92/sin/weapons/shotgun/shotgun_reload3.wav" )

sound.Add( {
	["name"] = "SiN.Scattergun.Empty" ,
	["channel"] = CHAN_AUTO ,
	["volume"] = 0.7 ,
	["level"] = 65 ,
	-- ["pitch"] = { 95 , 100 } ,
	["sound"] = "^)jessev92/sin/weapons/pistol/pistol_empty.wav"
} )
util.PrecacheSound( "sound/jessev92/sin/weapons/pistol/pistol_empty.wav" )

sound.Add( {
	["name"] = "SiN.Scattergun.Special1" ,
	["channel"] = CHAN_AUTO ,
	["volume"] = 0.7 ,
	["level"] = 65 ,
	-- ["pitch"] = { 95 , 100 } ,
	["sound"] = "^)jessev92/sin/weapons/shotgun/shotgun_cock.wav"
} )
util.PrecacheSound( "sound/jessev92/sin/weapons/shotgun/shotgun_cock.wav" )

sound.Add( {
	["name"] = "SiN.Scattergun.Single" ,
	["channel"] = CHAN_AUTO ,
	["volume"] = 0.86 ,
	["level"] = 140 ,
	["pitch"] = { 98 , 101 } ,
	["sound"] = "^)jessev92/sin/weapons/shotgun/shotgun_fire7.wav"
} )
util.PrecacheSound( "sound/jessev92/sin/weapons/shotgun/shotgun_fire7.wav" )

sound.Add( {
	["name"] = "SiN.Scattergun.Double" ,
	["channel"] = CHAN_AUTO ,
	["volume"] = 0.9 ,
	["level"] = 140 ,
	["pitch"] = { 90 , 95 } ,
	["sound"] = "^)jessev92/sin/weapons/shotgun/shotgun_dbl_fire7.wav"
} )
util.PrecacheSound( "sound/jessev92/sin/weapons/shotgun/shotgun_dbl_fire7.wav" )

sound.Add( {
	["name"] = "SiN.Scattergun.NPC_Double" ,
	["channel"] = CHAN_AUTO ,
	["volume"] = 0.9 ,
	["level"] = 140 ,
	["pitch"] = { 90 , 95 } ,
	["sound"] = "^)jessev92/sin/weapons/shotgun/shotgun_NPC_dbl_fire7.wav"
} )
util.PrecacheSound( "sound/jessev92/sin/weapons/shotgun/shotgun_NPC_dbl_fire7.wav" )

sound.Add( {
	["name"] = "SiN.Scattergun.NPC_Single" ,
	["channel"] = CHAN_AUTO ,
	["volume"] = 0.65 ,
	["level"] = 140 ,
	["pitch"] = { 98 , 101 } ,
	["sound"] = "^)jessev92/sin/weapons/shotgun/shotgun_fire_NPC.wav"
} )
util.PrecacheSound( "sound/jessev92/sin/weapons/shotgun/shotgun_fire_NPC.wav" )

sound.Add( {
	["name"] = "SiN.Scattergun.Special1" ,
	["channel"] = CHAN_AUTO ,
	["volume"] = 0.7 ,
	["level"] = 65 ,
	-- ["pitch"] = { 95 , 105 } ,
	["sound"] = "^)jessev92/sin/weapons/shotgun/assault_switch.wav"
} )
util.PrecacheSound( "sound/jessev92/sin/weapons/shotgun/assault_switch.wav" )

sound.Add( {
	["name"] = "SiN.Scattergun.Special2" ,
	["channel"] = CHAN_AUTO ,
	["volume"] = 0.7 ,
	["level"] = 65 ,
	-- ["pitch"] = { 95 , 105 } ,
	["sound"] = "^)jessev92/sin/weapons/shotgun/assault_switch.wav"
} )
util.PrecacheSound( "sound/jessev92/sin/weapons/shotgun/assault_switch.wav" )

sound.Add( {
	["name"] = "SiN.Scattergun.Burst" ,
	["channel"] = CHAN_AUTO ,
	["volume"] = 0.7 ,
	["level"] = 140 ,
	["pitch"] = { 98 , 105 } ,
	["sound"] = "^)jessev92/sin/weapons/shotgun/assault_burst.wav"
} )
util.PrecacheSound( "sound/jessev92/sin/weapons/shotgun/assault_burst.wav" )

sound.Add( {
	["name"] = "SiN.Scattergun.Melee_Miss" ,
	["channel"] = CHAN_AUTO ,
	["volume"] = 0.6 ,
	["level"] = 140 ,
	-- ["pitch"] = { 98 , 105 } ,
	["sound"] = "^)jessev92/sin/weapons/melee/melee_move1.wav"
} )
util.PrecacheSound( "sound/jessev92/sin/weapons/melee/melee_move1.wav" )

sound.Add( {
	["name"] = "SiN.Scattergun.Melee_Hit_World" ,
	["channel"] = CHAN_AUTO ,
	["volume"] = 0.8 ,
	["level"] = 140 ,
	["pitch"] = { 90 , 115 } ,
	["sound"] = "^)jessev92/sin/weapons/melee/melee_hit_shotgun.wav"
} )
util.PrecacheSound( "sound/jessev92/sin/weapons/melee/melee_hit_shotgun.wav" )

sound.Add( {
	["name"] = "SiN.Scattergun.Melee_Hit" ,
	["channel"] = CHAN_AUTO ,
	["volume"] = 0.9 ,
	["level"] = 140 ,
	["pitch"] = { 90 , 115 } ,
	["sound"] = "^)jessev92/sin/weapons/melee/melee_hit_shotgun.wav"
} )
util.PrecacheSound( "sound/jessev92/sin/weapons/melee/melee_hit_shotgun.wav" )

---------------------------------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------------------------
--	Day of Defeat: Source
---------------------------------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------------------------

------------------------------------------------------------------------------------------
--	DoD:S :: Player
------------------------------------------------------------------------------------------

sound.Add( {
	name = "DODS.Player.ReloadRustle" ,
	channel = CHAN_WEAPON ,
	volume = 1.0 ,
	level = 75 ,
	pitch = { 95 , 100 } ,
	sound = "^)jessev92/dods/player/reload_rustle.wav"
} )
util.PrecacheSound( "sound/jessev92/dods/player/reload_rustle.wav" )

------------------------------------------------------------------------------------------
--	DoD:S :: Weapons
------------------------------------------------------------------------------------------

---------------------------------------------
--	"30Cal"
--	M1919A4 Browning Machine Gun
--	.30-06
---------------------------------------------

sound.Add( {
	name = "DODS.30Cal.Shoot" ,
	channel = CHAN_WEAPON ,
	volume = 1.0 ,
	level = 150 ,
	pitch = { 95 , 100 } ,
	sound = "^)jessev92/dods/weapons/30Cal/30Cal_shoot.wav"
} )
util.PrecacheSound( "sound/jessev92/dods/weapons/30Cal/30Cal_shoot.wav" )

sound.Add( {
	name = "DODS.30Cal.BoltBack" ,
	channel = CHAN_ITEM ,
	volume = 1.0 ,
	level = 75 ,
	//pitch = { 95 , 100 } ,
	sound = "^)jessev92/dods/weapons/30Cal/30Cal_boltback.wav"
} )
util.PrecacheSound( "sound/jessev92/dods/weapons/30Cal/30Cal_boltback.wav" )

sound.Add( {
	name = "DODS.30Cal.BoltForward" ,
	channel = CHAN_ITEM ,
	volume = 1.0 ,
	level = 75 ,
	//pitch = { 95 , 100 } ,
	sound = "^)jessev92/dods/weapons/30Cal/30Cal_boltforward.wav"
} )
util.PrecacheSound( "sound/jessev92/dods/weapons/30Cal/30Cal_boltforward.wav" )

sound.Add( {
	name = "DODS.30Cal.BulletChain1" ,
	channel = CHAN_ITEM ,
	volume = 1.0 ,
	level = 75 ,
	pitch = { 95 , 100 } ,
	sound = "^)jessev92/dods/weapons/30Cal/30Cal_bulletchain1.wav"
} )
util.PrecacheSound( "sound/jessev92/dods/weapons/30Cal/30Cal_bulletchain1.wav" )

sound.Add( {
	name = "DODS.30Cal.BulletChain2" ,
	channel = CHAN_ITEM ,
	volume = 1.0 ,
	level = 75 ,
	pitch = { 95 , 100 } ,
	sound = "^)jessev92/dods/weapons/30Cal/30Cal_bulletchain2.wav"
} )
util.PrecacheSound( "sound/jessev92/dods/weapons/30Cal/30Cal_bulletchain2.wav" )

sound.Add( {
	name = "DODS.30Cal.CoverUp" ,
	channel = CHAN_ITEM ,
	volume = 1.0 ,
	level = 75 ,
	pitch = { 98 , 102 } ,
	sound = "^)jessev92/dods/weapons/30Cal/30Cal_coverup.wav"
} )
util.PrecacheSound( "sound/jessev92/dods/weapons/30Cal/30Cal_coverup.wav" )

sound.Add( {
	name = "DODS.30Cal.CoverDown" ,
	channel = CHAN_ITEM ,
	volume = 1.0 ,
	level = 75 ,
	pitch = { 98 , 102 } ,
	sound = "^)jessev92/dods/weapons/30Cal/30Cal_coverdown.wav"
} )
util.PrecacheSound( "sound/jessev92/dods/weapons/30Cal/30Cal_coverdown.wav" )

sound.Add( {
	name = "DODS.30Cal.Draw" ,
	channel = CHAN_ITEM ,
	volume = 1.0 ,
	level = 75 ,
	pitch = { 90 , 110 } ,
	sound = "^)jessev92/dods/weapons/common/draw_rifle.wav"
} )
util.PrecacheSound( "sound/jessev92/dods/weapons/common/draw_rifle.wav" )

sound.Add( {
	name = "DODS.30Cal.WorldReload" ,
	channel = CHAN_ITEM ,
	volume = 0.77 ,
	level = 65 ,
	pitch = { 90 , 110 } ,
	sound = "^)jessev92/dods/weapons/30Cal/30Cal_worldreload.wav"
} )
util.PrecacheSound( "sound/jessev92/dods/weapons/30Cal/30Cal_worldreload.wav" )

---------------------------------------------
--	"BAR"
--	M1918A2 Browning Auto Rifle
--	.30-06
---------------------------------------------

sound.Add( {
	name = "DODS.BAR.Single" ,
	channel = CHAN_WEAPON ,
	volume = 1.0 ,
	level = 150 ,
	pitch = { 95 , 100 } ,
	sound = "^)jessev92/dods/weapons/bar/bar_shoot.wav"
} )
util.PrecacheSound( "sound/jessev92/dods/weapons/bar/bar_shoot.wav" )

sound.Add( {
	name = "DODS.BAR.BoltBack" ,
	channel = CHAN_ITEM ,
	volume = 1.0 ,
	level = 75 ,
	pitch = { 95 , 100 } ,
	sound = "^)jessev92/dods/weapons/bar/bar_boltback.wav"
} )
util.PrecacheSound( "sound/jessev92/dods/weapons/bar/bar_boltback.wav" )

sound.Add( {
	name = "DODS.BAR.BoltForward" ,
	channel = CHAN_ITEM ,
	volume = 1.0 ,
	level = 75 ,
	pitch = { 95 , 100 } ,
	sound = "^)jessev92/dods/weapons/bar/bar_boltforward.wav"
} )
util.PrecacheSound( "sound/jessev92/dods/weapons/bar/bar_boltforward.wav" )

sound.Add( {
	name = "DODS.BAR.ClipIn1" ,
	channel = CHAN_ITEM ,
	volume = 1.0 ,
	level = 75 ,
	pitch = { 95 , 100 } ,
	sound = "^)jessev92/dods/weapons/bar/bar_ClipIn1.wav"
} )
util.PrecacheSound( "sound/jessev92/dods/weapons/bar/bar_ClipIn1.wav" )

sound.Add( {
	name = "DODS.BAR.ClipIn2" ,
	channel = CHAN_ITEM ,
	volume = 1.0 ,
	level = 75 ,
	pitch = { 95 , 100 } ,
	sound = "^)jessev92/dods/weapons/bar/bar_ClipIn2.wav"
} )
util.PrecacheSound( "sound/jessev92/dods/weapons/bar/bar_ClipIn2.wav" )

sound.Add( {
	name = "DODS.BAR.Switch" ,
	channel = CHAN_ITEM ,
	volume = 1.0 ,
	level = 75 ,
	//pitch = { 95 , 100 } ,
	sound = "^)jessev92/dods/weapons/bar/bar_selectorswitch.wav"
} )
util.PrecacheSound( "sound/jessev92/dods/weapons/bar/bar_selectorswitch.wav" )

sound.Add( {
	name = "DODS.BAR.Draw" ,
	channel = CHAN_WEAPON ,
	volume = 1.0 ,
	level = 75 ,
	pitch = { 95 , 100 } ,
	sound = "^)jessev92/dods/weapons/common/draw_rifle.wav"
} )

sound.Add( {
	name = "DODS.BAR.WorldReload" ,
	channel = CHAN_ITEM ,
	volume = 0.77 ,
	level = 65 ,
	//pitch = { 95 , 100 } ,
	sound = "^)jessev92/dods/weapons/bar/bar_worldreload.wav"
} )
util.PrecacheSound( "sound/jessev92/dods/weapons/bar/bar_worldreload.wav" )

---------------------------------------------
--	"Bazooka"
--	M1A1 Bazooka
--	M6A1 HEAT Rockets
---------------------------------------------

sound.Add( {
	name = "DODS.Bazooka.Shoot" ,
	channel = CHAN_WEAPON ,
	volume = 1.0 ,
	level = 150 ,
	pitch = { 95 , 100 } ,
	sound = "^)jessev92/dods/weapons/rocket/rocket1.wav"
} )

sound.Add( {
	name = "DODS.Bazooka.ClipIn" ,
	channel = CHAN_WEAPON ,
	volume = 1.0 ,
	level = 75 ,
	//pitch = { 95 , 100 } ,
	sound = "^)jessev92/dods/weapons/rocket/rocket_clipin.wav"
} )
util.PrecacheSound( "sound/jessev92/dods/weapons/rocket/rocket_clipin.wav" )

sound.Add( {
	name = "DODS.Bazooka.Draw" ,
	channel = CHAN_WEAPON ,
	volume = 1.0 ,
	level = 75 ,
	//pitch = { 95 , 100 } ,
	sound = "^)jessev92/dods/weapons/common/draw_rifle.wav"
} )
util.PrecacheSound( "sound/jessev92/dods/weapons/common/draw_rifle.wav" )

sound.Add( {
	name = "DODS.Bazooka.WorldReload" ,
	channel = CHAN_ITEM ,
	volume = 0.77 ,
	level = 65 ,
	//pitch = { 95 , 100 } ,
	sound = "^)jessev92/dods/weapons/rocket/rocket_worldreload.wav"
} )
util.PrecacheSound( "sound/jessev92/dods/weapons/rocket/rocket_worldreload.wav" )

---------------------------------------------
--	"Carbine"
--	M1 Carbine
--	.30 Carbine
---------------------------------------------

sound.Add( {
	name = "DODS.Carbine.Shoot" ,
	channel = CHAN_ITEM ,
	volume = 1.0 ,
	level = 150 ,
	pitch = { 95 , 100 } ,
	sound = "^)jessev92/dods/weapons/m1carbine/m1carbine_shoot.wav"
} )
util.PrecacheSound( "sound/jessev92/dods/weapons/m1carbine/m1carbine_shoot.wav" )

sound.Add( {
	name = "DODS.Carbine.BoltBack" ,
	channel = CHAN_ITEM ,
	volume = 1.0 ,
	level = 75 ,
	//pitch = { 95 , 100 } ,
	sound = "^)jessev92/dods/weapons/m1carbine/m1carbine_boltback.wav"
} )
util.PrecacheSound( "sound/jessev92/dods/weapons/m1carbine/m1carbine_boltback.wav" )

sound.Add( {
	name = "DODS.Carbine.BoltBack" ,
	channel = CHAN_ITEM ,
	volume = 1.0 ,
	level = 75 ,
	//pitch = { 95 , 100 } ,
	sound = "^)jessev92/dods/weapons/m1carbine/m1carbine_boltforward.wav"
} )
util.PrecacheSound( "sound/jessev92/dods/weapons/m1carbine/m1carbine_boltforward.wav" )

sound.Add( {
	name = "DODS.Carbine.ClipIn1" ,
	channel = CHAN_ITEM ,
	volume = 1.0 ,
	level = 75 ,
	//pitch = { 95 , 100 } ,
	sound = "^)jessev92/dods/weapons/m1carbine/m1carbine_clipin1.wav"
} )
util.PrecacheSound( "sound/jessev92/dods/weapons/m1carbine/m1carbine_clipin1.wav" )

sound.Add( {
	name = "DODS.Carbine.ClipIn2" ,
	channel = CHAN_ITEM ,
	volume = 1.0 ,
	level = 75 ,
	//pitch = { 95 , 100 } ,
	sound = "^)jessev92/dods/weapons/m1carbine/m1carbine_clipin2.wav"
} )
util.PrecacheSound( "sound/jessev92/dods/weapons/m1carbine/m1carbine_clipin2.wav" )

sound.Add( {
	name = "DODS.Carbine.ClipOut" ,
	channel = CHAN_ITEM ,
	volume = 1.0 ,
	level = 75 ,
	//pitch = { 95 , 100 } ,
	sound = "^)jessev92/dods/weapons/m1carbine/m1carbine_clipout.wav"
} )
util.PrecacheSound( "sound/jessev92/dods/weapons/m1carbine/m1carbine_clipout.wav" )

sound.Add( {
	name = "DODS.Carbine.Draw" ,
	channel = CHAN_ITEM ,
	volume = 1.0 ,
	level = 75 ,
	//pitch = { 95 , 100 } ,
	sound = "^)jessev92/dods/weapons/common/draw_rifle.wav"
} )
util.PrecacheSound( "sound/jessev92/dods/weapons/common/draw_rifle.wav" )

sound.Add( {
	name = "DODS.Carbine.WorldReload" ,
	channel = CHAN_ITEM ,
	volume = 0.77 ,
	level = 65 ,
	//pitch = { 95 , 100 } ,
	sound = "^)jessev92/dods/weapons/m1carbine/m1carbine_worldreload.wav"
} )
util.PrecacheSound( "sound/jessev92/dods/weapons/m1carbine/m1carbine_worldreload.wav" )

---------------------------------------------
--	"Colt"
--	M1911
--	.45 ACP
---------------------------------------------
sound.Add( {
	name = "DODS.Colt.Shoot" ,
	channel = CHAN_WEAPON ,
	volume = 1.0 ,
	level = 140 ,
	pitch = { 95 , 100 } ,
	sound = "^)jessev92/dods/weapons/1911/colt_shoot.wav"
} )
util.PrecacheSound( "sound/jessev92/dods/weapons/1911/colt_shoot.wav" )

sound.Add( {
	name = "DODS.Colt.BoltBack" ,
	channel = CHAN_ITEM ,
	volume = 1.0 ,
	level = 75 ,
	pitch = { 95 , 100 } ,
	sound = "^)jessev92/dods/weapons/1911/colt_boltback.wav"
} )
util.PrecacheSound( "sound/jessev92/dods/weapons/1911/colt_boltback.wav" )

sound.Add( {
	name = "DODS.Colt.BoltForward" ,
	channel = CHAN_ITEM ,
	volume = 1.0 ,
	level = 75 ,
	pitch = { 95 , 100 } ,
	sound = "^)jessev92/dods/weapons/1911/colt_boltforward.wav"
} )
util.PrecacheSound( "sound/jessev92/dods/weapons/1911/colt_boltforward.wav" )

sound.Add( {
	name = "DODS.Colt.ClipIn" ,
	channel = CHAN_ITEM ,
	volume = 1.0 ,
	level = 75 ,
	pitch = { 95 , 100 } ,
	sound = "^)jessev92/dods/weapons/1911/colt_clipin.wav"
} )
util.PrecacheSound( "sound/jessev92/dods/weapons/1911/colt_clipin.wav" )

sound.Add( {
	name = "DODS.Colt.ClipOut" ,
	channel = CHAN_ITEM ,
	volume = 1.0 ,
	level = 75 ,
	pitch = { 95 , 100 } ,
	sound = "^)jessev92/dods/weapons/1911/colt_clipout.wav"
} )
util.PrecacheSound( "sound/jessev92/dods/weapons/1911/colt_clipout.wav" )

sound.Add( {
	name = "DODS.Colt.WorldReload" ,
	channel = CHAN_BODY ,
	volume = 0.77 ,
	level = 65 ,
	pitch = { 95 , 100 } ,
	sound = "^)jessev92/dods/weapons/1911/colt_worldreload.wav"
} )
util.PrecacheSound( "sound/jessev92/dods/weapons/1911/colt_worldreload.wav" )

---------------------------------------------
--	"C96"
--	Mauser C96
--	.45 ACP
---------------------------------------------
sound.Add( {
	name = "DODS.C96.Shoot" ,
	channel = CHAN_WEAPON ,
	volume = 1.0 ,
	level = 140 ,
	pitch = { 95 , 100 } ,
	sound = "^)jessev92/dods/weapons/c96/c96_shoot.wav"
} )
util.PrecacheSound( "sound/jessev92/dods/weapons/c96/c96_shoot.wav" )

sound.Add( {
	name = "DODS.C96.ClipIn1" ,
	channel = CHAN_ITEM ,
	volume = 1.0 ,
	level = 75 ,
	pitch = { 95 , 100 } ,
	sound = "^)jessev92/dods/weapons/C96/c96_clipin1.wav"
} )
util.PrecacheSound( "sound/jessev92/dods/weapons/C96/c96_clipin1.wav" )

sound.Add( {
	name = "DODS.C96.ClipIn2" ,
	channel = CHAN_ITEM ,
	volume = 1.0 ,
	level = 75 ,
	pitch = { 95 , 100 } ,
	sound = "^)jessev92/dods/weapons/C96/c96_clipin2.wav"
} )
util.PrecacheSound( "sound/jessev92/dods/weapons/C96/c96_clipin2.wav" )

sound.Add( {
	name = "DODS.C96.ClipOut" ,
	channel = CHAN_ITEM ,
	volume = 1.0 ,
	level = 75 ,
	pitch = { 95 , 100 } ,
	sound = "^)jessev92/dods/weapons/C96/c96_clipout.wav"
} )
util.PrecacheSound( "sound/jessev92/dods/weapons/C96/c96_clipout.wav" )

sound.Add( {
	name = "DODS.C96.BoltBack" ,
	channel = CHAN_ITEM ,
	volume = 1.0 ,
	level = 75 ,
	pitch = { 95 , 100 } ,
	sound = "^)jessev92/dods/weapons/C96/c96_boltback.wav"
} )
util.PrecacheSound( "sound/jessev92/dods/weapons/C96/c96_boltback.wav" )

sound.Add( {
	name = "DODS.C96.BoltForward" ,
	channel = CHAN_ITEM ,
	volume = 1.0 ,
	level = 75 ,
	pitch = { 95 , 100 } ,
	sound = "^)jessev92/dods/weapons/C96/c96_boltforward.wav"
} )
util.PrecacheSound( "sound/jessev92/dods/weapons/C96/c96_boltforward.wav" )

sound.Add( {
	name = "DODS.C96.Draw" ,
	channel = CHAN_ITEM ,
	volume = 1.0 ,
	level = 75 ,
	pitch = { 95 , 100 } ,
	sound = "^)jessev92/dods/weapons/common/draw_pistol.wav"
} )
util.PrecacheSound( "sound/jessev92/dods/weapons/common/draw_pistol.wav" )

sound.Add( {
	name = "DODS.C96.WorldReload" ,
	channel = CHAN_BODY ,
	volume = 0.77 ,
	level = 65 ,
	pitch = { 95 , 100 } ,
	sound = "^)jessev92/dods/weapons/c96/c96_worldreload.wav"
} )
util.PrecacheSound( "sound/jessev92/dods/weapons/c96/c96_worldreload.wav" )

---------------------------------------------
--	"Garand"
--	M1 Garand Battle Rifle
--	.30-06
---------------------------------------------

sound.Add( {
	name = "DODS.Garand.Single" ,
	channel = CHAN_WEAPON ,
	volume = 1.0 ,
	level = 140 ,
	pitch = { 95 , 100 } ,
	sound = "^)jessev92/dods/weapons/garand/garand_shoot.wav"
} )
util.PrecacheSound( "sound/jessev92/dods/weapons/garand/garand_shoot.wav" )

sound.Add( {
	name = "DODS.Garand.BoltForward" ,
	channel = CHAN_ITEM ,
	volume = 1.0 ,
	level = 70 ,
	pitch = { 95 , 100 } ,
	sound = "^)jessev92/dods/weapons/garand/garand_boltforward.wav"
} )
util.PrecacheSound( "sound/jessev92/dods/weapons/garand/garand_boltforward.wav" )

sound.Add( {
	name = "DODS.Garand.ClipDing" ,
	channel = CHAN_ITEM ,
	volume = 1.0 ,
	level = 70 ,
	pitch = { 95 , 100 } ,
	sound = "^)jessev92/dods/weapons/garand/garand_clipding.wav"
} )
util.PrecacheSound( "sound/jessev92/dods/weapons/garand/garand_clipding.wav" )

sound.Add( {
	name = "DODS.Garand.ClipIn1" ,
	channel = CHAN_ITEM ,
	volume = 1.0 ,
	level = 70 ,
	pitch = { 95 , 100 } ,
	sound = "^)jessev92/dods/weapons/garand/garand_ClipIn1.wav"
} )
util.PrecacheSound( "sound/jessev92/dods/weapons/garand/garand_ClipIn1.wav" )

sound.Add( {
	name = "DODS.Garand.ClipIn2" ,
	channel = CHAN_ITEM ,
	volume = 1.0 ,
	level = 70 ,
	pitch = { 95 , 100 } ,
	sound = "^)jessev92/dods/weapons/garand/garand_ClipIn2.wav"
} )
util.PrecacheSound( "sound/jessev92/dods/weapons/garand/garand_ClipIn2.wav" )

sound.Add( {
	name = "DODS.Garand.Reload" ,
	channel = CHAN_BODY ,
	volume = 1.0 ,
	level = 70 ,
	//pitch = { 95 , 100 } ,
	sound = "^)jessev92/dods/weapons/garand/garand_worldreload.wav"
} )
util.PrecacheSound( "sound/jessev92/dods/weapons/garand/garand_worldreload.wav" )

---------------------------------------------
--	"K98"
--	Karabiner 98k
--	7.92x57mm Mauser
---------------------------------------------

sound.Add( {
	name = "DODS.K98.Shoot" ,
	channel = CHAN_WEAPON ,
	volume = 1.0 ,
	level = 150 ,
	pitch = { 95 , 100 } ,
	sound = "^)jessev92/dods/weapons/k98/k98_shoot.wav"
} )
util.PrecacheSound( "sound/jessev92/dods/weapons/k98/k98_shoot.wav" )

sound.Add( {
	name = "DODS.K98.BoltBack1" ,
	channel = CHAN_WEAPON ,
	volume = 1.0 ,
	level = 75 ,
	//pitch = { 95 , 100 } ,
	sound = "^)jessev92/dods/weapons/k98/k98_boltback1.wav"
} )
util.PrecacheSound( "sound/jessev92/dods/weapons/k98/k98_boltback1.wav" )

sound.Add( {
	name = "DODS.K98.BoltBack2" ,
	channel = CHAN_WEAPON ,
	volume = 1.0 ,
	level = 75 ,
	//pitch = { 95 , 100 } ,
	sound = "^)jessev92/dods/weapons/k98/k98_boltback2.wav"
} )
util.PrecacheSound( "sound/jessev92/dods/weapons/k98/k98_boltback2.wav" )

sound.Add( {
	name = "DODS.K98.BoltForward1" ,
	channel = CHAN_WEAPON ,
	volume = 1.0 ,
	level = 75 ,
	//pitch = { 95 , 100 } ,
	sound = "^)jessev92/dods/weapons/k98/k98_boltforward1.wav"
} )
util.PrecacheSound( "sound/jessev92/dods/weapons/k98/k98_boltforward1.wav" )

sound.Add( {
	name = "DODS.K98.BoltForward2" ,
	channel = CHAN_WEAPON ,
	volume = 1.0 ,
	level = 75 ,
	//pitch = { 95 , 100 } ,
	sound = "^)jessev92/dods/weapons/k98/k98_boltforward2.wav"
} )
util.PrecacheSound( "sound/jessev92/dods/weapons/k98/k98_boltforward2.wav" )

sound.Add( {
	name = "DODS.K98.ClipIn" ,
	channel = CHAN_WEAPON ,
	volume = 1.0 ,
	level = 75 ,
	//pitch = { 95 , 100 } ,
	sound = "^)jessev92/dods/weapons/k98/k98_clipin.wav"
} )
util.PrecacheSound( "sound/jessev92/dods/weapons/k98/k98_clipin.wav" )

sound.Add( {
	name = "DODS.K98.ClipIn2" ,
	channel = CHAN_WEAPON ,
	volume = 1.0 ,
	level = 75 ,
	//pitch = { 95 , 100 } ,
	sound = "^)jessev92/dods/weapons/k98/k98_clipin2.wav"
} )
util.PrecacheSound( "sound/jessev92/dods/weapons/k98/k98_clipin2.wav" )

sound.Add( {
	name = "DODS.K98.ClipOut" ,
	channel = CHAN_WEAPON ,
	volume = 1.0 ,
	level = 75 ,
	//pitch = { 95 , 100 } ,
	sound = "^)jessev92/dods/weapons/k98/k98_clipout.wav"
} )
util.PrecacheSound( "sound/jessev92/dods/weapons/k98/k98_clipout.wav" )

sound.Add( {
	name = "DODS.K98.Draw" ,
	channel = CHAN_WEAPON ,
	volume = 1.0 ,
	level = 75 ,
	//pitch = { 95 , 100 } ,
	sound = "^)jessev92/dods/weapons/common/draw_rifle.wav"
} )
util.PrecacheSound( "sound/jessev92/dods/weapons/common/draw_rifle.wav" )

sound.Add( {
	name = "DODS.K98.ClipOut" ,
	channel = CHAN_WEAPON ,
	volume = 1.0 ,
	level = 75 ,
	//pitch = { 95 , 100 } ,
	sound = "^)jessev92/dods/weapons/k98/k98_clipout.wav"
} )
util.PrecacheSound( "sound/jessev92/dods/weapons/k98/k98_clipout.wav" )

sound.Add( {
	name = "DODS.K98.SinglShotReload" ,
	channel = CHAN_WEAPON ,
	volume = 0.77 ,
	level = 65 ,
	//pitch = { 95 , 100 } ,
	sound = "^)jessev92/dods/weapons/k98/k98_singleshotreload.wav"
} )
util.PrecacheSound( "sound/jessev92/dods/weapons/k98/k98_singleshotreload.wav" )

sound.Add( {
	name = "DODS.K98.WorldReload" ,
	channel = CHAN_WEAPON ,
	volume = 0.77 ,
	level = 65 ,
	//pitch = { 95 , 100 } ,
	sound = "^)jessev92/dods/weapons/k98/k98_worldreload.wav"
} )
util.PrecacheSound( "sound/jessev92/dods/weapons/k98/k98_worldreload.wav" )

---------------------------------------------
--	"KarScoped"
--	Karabiner 98k
--	7.92x57mm Mauser
---------------------------------------------

sound.Add( {
	name = "DODS.KarScoped.Shoot" ,
	channel = CHAN_WEAPON ,
	volume = 1.0 ,
	level = 150 ,
	pitch = { 95 , 100 } ,
	sound = "^)jessev92/dods/weapons/k98/k98scoped_shoot.wav"
} )
util.PrecacheSound( "sound/jessev92/dods/weapons/k98/k98scoped_shoot.wav" )

---------------------------------------------
--	"P38"
--	Walther P38
--	9x19mm Parabellum
---------------------------------------------

sound.Add( {
	name = "DODS.P38.Shoot" ,
	channel = CHAN_WEAPON ,
	volume = 1.0 ,
	level = 150 ,
	pitch = { 95 , 100 } ,
	sound = "^)jessev92/dods/weapons/p38/p38_shoot.wav"
} )
util.PrecacheSound( "sound/jessev92/dods/weapons/p38/p38_shoot.wav" )

sound.Add( {
	name = "DODS.P38.ClipIn" ,
	channel = CHAN_WEAPON ,
	volume = 1.0 ,
	level = 75 ,
	//pitch = { 95 , 100 } ,
	sound = "^)jessev92/dods/weapons/p38/p38_clipin.wav"
} )
util.PrecacheSound( "sound/jessev92/dods/weapons/p38/p38_clipin.wav" )

sound.Add( {
	name = "DODS.P38.ClipOut" ,
	channel = CHAN_WEAPON ,
	volume = 1.0 ,
	level = 75 ,
	//pitch = { 95 , 100 } ,
	sound = "^)jessev92/dods/weapons/p38/p38_clipout.wav"
} )
util.PrecacheSound( "sound/jessev92/dods/weapons/p38/p38_clipout.wav" )

sound.Add( {
	name = "DODS.P38.BoltBack" ,
	channel = CHAN_WEAPON ,
	volume = 1.0 ,
	level = 75 ,
	//pitch = { 95 , 100 } ,
	sound = "^)jessev92/dods/weapons/p38/p38_boltback.wav"
} )
util.PrecacheSound( "sound/jessev92/dods/weapons/p38/p38_boltback.wav" )

sound.Add( {
	name = "DODS.P38.BoltForward" ,
	channel = CHAN_WEAPON ,
	volume = 1.0 ,
	level = 75 ,
	//pitch = { 95 , 100 } ,
	sound = "^)jessev92/dods/weapons/p38/p38_boltforward.wav"
} )
util.PrecacheSound( "sound/jessev92/dods/weapons/p38/p38_boltforward.wav" )

sound.Add( {
	name = "DODS.P38.Draw" ,
	channel = CHAN_WEAPON ,
	volume = 1.0 ,
	level = 75 ,
	//pitch = { 95 , 100 } ,
	sound = "^)jessev92/dods/weapons/common/draw_pistol.wav"
} )
util.PrecacheSound( "sound/jessev92/dods/weapons/common/draw_pistol.wav" )

sound.Add( {
	name = "DODS.P38.WorldReload" ,
	channel = CHAN_WEAPON ,
	volume = 0.77 ,
	level = 65 ,
	//pitch = { 95 , 100 } ,
	sound = "^)jessev92/dods/weapons/p38/p38_worldreload.wav"
} )
util.PrecacheSound( "sound/jessev92/dods/weapons/p38/p38_worldreload.wav" )

---------------------------------------------
--	"MG42"
--	Maschinengewehr 42
--	7.92x57mm Mauser
---------------------------------------------

sound.Add( {
	name = "DODS.MG42.Shoot" ,
	channel = CHAN_WEAPON ,
	volume = 1.0 ,
	level = 150 ,
	pitch = { 95 , 100 } ,
	sound = "^)jessev92/dods/weapons/MG42/MG42_shoot.wav"
} )
util.PrecacheSound( "sound/jessev92/dods/weapons/MG42/MG42_shoot.wav" )

sound.Add( {
	name = "DODS.MG42.OverHeat" ,
	channel = CHAN_WEAPON ,
	volume = 1.0 ,
	level = 75 ,
	//pitch = { 95 , 100 } ,
	sound = "^)jessev92/dods/weapons/MG42/MG42_overheat.wav"
} )
util.PrecacheSound( "sound/jessev92/dods/weapons/MG42/MG42_overheat.wav" )

sound.Add( {
	name = "DODS.MG42.CoverUp" ,
	channel = CHAN_WEAPON ,
	volume = 1.0 ,
	level = 75 ,
	//pitch = { 95 , 100 } ,
	sound = "^)jessev92/dods/weapons/MG42/MG42_coverup.wav"
} )
util.PrecacheSound( "sound/jessev92/dods/weapons/MG42/MG42_coverup.wav" )

sound.Add( {
	name = "DODS.MG42.CoverDown" ,
	channel = CHAN_WEAPON ,
	volume = 1.0 ,
	level = 75 ,
	//pitch = { 95 , 100 } ,
	sound = "^)jessev92/dods/weapons/MG42/MG42_coverdown.wav"
} )
util.PrecacheSound( "sound/jessev92/dods/weapons/MG42/MG42_coverdown.wav" )

sound.Add( {
	name = "DODS.MG42.BulletChain1" ,
	channel = CHAN_WEAPON ,
	volume = 1.0 ,
	level = 75 ,
	//pitch = { 95 , 100 } ,
	sound = "^)jessev92/dods/weapons/MG42/MG42_bulletchain1.wav"
} )
util.PrecacheSound( "sound/jessev92/dods/weapons/MG42/MG42_bulletchain1.wav" )

sound.Add( {
	name = "DODS.MG42.BulletChain2" ,
	channel = CHAN_WEAPON ,
	volume = 1.0 ,
	level = 75 ,
	//pitch = { 95 , 100 } ,
	sound = "^)jessev92/dods/weapons/MG42/MG42_bulletchain2.wav"
} )
util.PrecacheSound( "sound/jessev92/dods/weapons/MG42/MG42_bulletchain2.wav" )

sound.Add( {
	name = "DODS.MG42.BoltBack" ,
	channel = CHAN_WEAPON ,
	volume = 1.0 ,
	level = 75 ,
	//pitch = { 95 , 100 } ,
	sound = "^)jessev92/dods/weapons/MG42/MG42_boltback.wav"
} )
util.PrecacheSound( "sound/jessev92/dods/weapons/MG42/MG42_boltback.wav" )

sound.Add( {
	name = "DODS.MG42.BoltForward" ,
	channel = CHAN_WEAPON ,
	volume = 1.0 ,
	level = 75 ,
	//pitch = { 95 , 100 } ,
	sound = "^)jessev92/dods/weapons/MG42/MG42_boltforward.wav"
} )
util.PrecacheSound( "sound/jessev92/dods/weapons/MG42/MG42_boltforward.wav" )

sound.Add( {
	name = "DODS.MG42.Draw" ,
	channel = CHAN_WEAPON ,
	volume = 1.0 ,
	level = 75 ,
	//pitch = { 95 , 100 } ,
	sound = "^)jessev92/dods/weapons/common/draw_rifle.wav"
} )
util.PrecacheSound( "sound/jessev92/dods/weapons/common/draw_rifle.wav" )

sound.Add( {
	name = "DODS.MG42.DeployBipod" ,
	channel = CHAN_WEAPON ,
	volume = 1.0 ,
	level = 75 ,
	//pitch = { 95 , 100 } ,
	sound = "^)jessev92/dods/weapons/MG42/MG42_deploybipod.wav"
} )
util.PrecacheSound( "sound/jessev92/dods/weapons/MG42/MG42_deploybipod.wav" )

sound.Add( {
	name = "DODS.MG42.RaiseBipod" ,
	channel = CHAN_WEAPON ,
	volume = 1.0 ,
	level = 75 ,
	//pitch = { 95 , 100 } ,
	sound = "^)jessev92/dods/weapons/MG42/MG42_raisebipod.wav"
} )
util.PrecacheSound( "sound/jessev92/dods/weapons/MG42/MG42_raisebipod.wav" )

sound.Add( {
	name = "DODS.MG42.WorldReload" ,
	channel = CHAN_WEAPON ,
	volume = 0.77 ,
	level = 65 ,
	//pitch = { 95 , 100 } ,
	sound = "^)jessev92/dods/weapons/MG42/MG42_worldreload.wav"
} )
util.PrecacheSound( "sound/jessev92/dods/weapons/MG42/MG42_worldreload.wav" )

---------------------------------------------
--	"MP40"
--	Maschinenpistole 40
--	9x19mm Parabellum
---------------------------------------------

sound.Add( {
	name = "DODS.MP40.Shoot" ,
	channel = CHAN_WEAPON ,
	volume = 1.0 ,
	level = 150 ,
	pitch = { 95 , 100 } ,
	sound = "^)jessev92/dods/weapons/MP40/MP40_shoot.wav"
} )
util.PrecacheSound( "sound/jessev92/dods/weapons/MP40/MP40_shoot.wav" )

sound.Add( {
	name = "DODS.MP40.Draw" ,
	channel = CHAN_WEAPON ,
	volume = 1.0 ,
	level = 75 ,
	//pitch = { 95 , 100 } ,
	sound = "^)jessev92/dods/weapons/common/draw_rifle.wav"
} )
util.PrecacheSound( "sound/jessev92/dods/weapons/common/draw_rifle.wav" )

sound.Add( {
	name = "DODS.MP40.BoltBack" ,
	channel = CHAN_WEAPON ,
	volume = 1.0 ,
	level = 75 ,
	//pitch = { 95 , 100 } ,
	sound = "^)jessev92/dods/weapons/MP40/MP40_boltback.wav"
} )
util.PrecacheSound( "sound/jessev92/dods/weapons/MP40/MP40_boltback.wav" )

sound.Add( {
	name = "DODS.MP40.ClipIn" ,
	channel = CHAN_WEAPON ,
	volume = 1.0 ,
	level = 75 ,
	//pitch = { 95 , 100 } ,
	sound = "^)jessev92/dods/weapons/MP40/MP40_clipin.wav"
} )
util.PrecacheSound( "sound/jessev92/dods/weapons/MP40/MP40_clipin.wav" )

sound.Add( {
	name = "DODS.MP40.ClipOut" ,
	channel = CHAN_WEAPON ,
	volume = 1.0 ,
	level = 75 ,
	//pitch = { 95 , 100 } ,
	sound = "^)jessev92/dods/weapons/MP40/MP40_clipout.wav"
} )
util.PrecacheSound( "sound/jessev92/dods/weapons/MP40/MP40_clipout.wav" )

sound.Add( {
	name = "DODS.MP40.BoltForward" ,
	channel = CHAN_WEAPON ,
	volume = 1.0 ,
	level = 75 ,
	//pitch = { 95 , 100 } ,
	sound = "^)jessev92/dods/weapons/MP40/MP40_boltforward.wav"
} )
util.PrecacheSound( "sound/jessev92/dods/weapons/MP40/MP40_boltforward.wav" )

sound.Add( {
	name = "DODS.MP40.WorldReload" ,
	channel = CHAN_WEAPON ,
	volume = 0.77 ,
	level = 65 ,
	//pitch = { 95 , 100 } ,
	sound = "^)jessev92/dods/weapons/MP40/MP40_worldreload.wav"
} )
util.PrecacheSound( "sound/jessev92/dods/weapons/MP40/MP40_worldreload.wav" )

---------------------------------------------
--	"StG44"
--	Sturmgewehr 44
--	7.92×33mm Kurz
---------------------------------------------

sound.Add( {
	name = "DODS.StG44.Shoot" ,
	channel = CHAN_WEAPON ,
	volume = 1.0 ,
	level = 150 ,
	pitch = { 95 , 100 } ,
	sound = "^)jessev92/dods/weapons/stg44/StG44_shoot.wav"
} )
util.PrecacheSound( "sound/jessev92/dods/weapons/stg44/StG44_shoot.wav" )

sound.Add( {
	name = "DODS.StG44.BoltBack" ,
	channel = CHAN_WEAPON ,
	volume = 1.0 ,
	level = 75 ,
	//pitch = { 95 , 100 } ,
	sound = "^)jessev92/dods/weapons/stg44/StG44_boltback.wav"
} )
util.PrecacheSound( "sound/jessev92/dods/weapons/stg44/StG44_boltback.wav" )

sound.Add( {
	name = "DODS.StG44.BoltForward" ,
	channel = CHAN_WEAPON ,
	volume = 1.0 ,
	level = 75 ,
	//pitch = { 95 , 100 } ,
	sound = "^)jessev92/dods/weapons/stg44/StG44_boltforward.wav"
} )
util.PrecacheSound( "sound/jessev92/dods/weapons/stg44/StG44_boltforward.wav" )

sound.Add( {
	name = "DODS.StG44.ClipIn1" ,
	channel = CHAN_WEAPON ,
	volume = 1.0 ,
	level = 75 ,
	//pitch = { 95 , 100 } ,
	sound = "^)jessev92/dods/weapons/stg44/StG44_clipin1.wav"
} )
util.PrecacheSound( "sound/jessev92/dods/weapons/stg44/StG44_clipin1.wav" )

sound.Add( {
	name = "DODS.StG44.ClipIn2" ,
	channel = CHAN_WEAPON ,
	volume = 1.0 ,
	level = 75 ,
	//pitch = { 95 , 100 } ,
	sound = "^)jessev92/dods/weapons/stg44/StG44_clipin2.wav"
} )
util.PrecacheSound( "sound/jessev92/dods/weapons/stg44/StG44_clipin2.wav" )

sound.Add( {
	name = "DODS.StG44.ClipOut" ,
	channel = CHAN_WEAPON ,
	volume = 1.0 ,
	level = 75 ,
	//pitch = { 95 , 100 } ,
	sound = "^)jessev92/dods/weapons/stg44/StG44_clipout.wav"
} )
util.PrecacheSound( "sound/jessev92/dods/weapons/stg44/StG44_clipout.wav" )

sound.Add( {
	name = "DODS.StG44.Draw" ,
	channel = CHAN_WEAPON ,
	volume = 1.0 ,
	level = 75 ,
	//pitch = { 95 , 100 } ,
	sound = "^)jessev92/dods/weapons/common/draw_rifle.wav"
} )
util.PrecacheSound( "sound/jessev92/dods/weapons/common/draw_rifle.wav" )

sound.Add( {
	name = "DODS.StG44.WorldReload" ,
	channel = CHAN_WEAPON ,
	volume = 0.77 ,
	level = 65 ,
	//pitch = { 95 , 100 } ,
	sound = "^)jessev92/dods/weapons/stg44/StG44_worldreload.wav"
} )
util.PrecacheSound( "sound/jessev92/dods/weapons/stg44/StG44_worldreload.wav" )

sound.Add( {
	name = "DODS.StG44.SelectorSwitch" ,
	channel = CHAN_WEAPON ,
	volume = 1.0 ,
	level = 75 ,
	//pitch = { 95 , 100 } ,
	sound = "^)jessev92/dods/weapons/stg44/StG44_selectorswitch.wav"
} )
util.PrecacheSound( "sound/jessev92/dods/weapons/stg44/StG44_selectorswitch.wav" )

---------------------------------------------
--	"Panzerschreck"
--	Panzerschreck
--	88mm Rocket
---------------------------------------------

sound.Add( {
	name = "DODS.Panzerschreck.Shoot" ,
	channel = CHAN_WEAPON ,
	volume = 1.0 ,
	level = 150 ,
	pitch = { 95 , 100 } ,
	sound = "^)jessev92/dods/weapons/rocket/rocket1.wav"
} )
util.PrecacheSound( "sound/jessev92/dods/weapons/rocket/rocket1.wav" )

sound.Add( {
	name = "DODS.Panzerschreck.BoltBack" ,
	channel = CHAN_WEAPON ,
	volume = 1.0 ,
	level = 75 ,
	//pitch = { 95 , 100 } ,
	sound = "^)jessev92/dods/weapons/rocket/rocket_clipin.wav"
} )
util.PrecacheSound( "sound/jessev92/dods/weapons/rocket/rocket_clipin.wav" )

sound.Add( {
	name = "DODS.Panzerschreck.Draw" ,
	channel = CHAN_WEAPON ,
	volume = 1.0 ,
	level = 75 ,
	pitch = { 95 , 100 } ,
	sound = "^)jessev92/dods/weapons/common/draw_rifle.wav"
} )
util.PrecacheSound( "sound/jessev92/dods/weapons/common/draw_rifle.wav" )

sound.Add( {
	name = "DODS.Panzerschreck.WorldReload" ,
	channel = CHAN_WEAPON ,
	volume = 0.77 ,
	level = 65 ,
	//pitch = { 95 , 100 } ,
	sound = "^)jessev92/dods/weapons/rocket/rocket_worldreload.wav"
} )
util.PrecacheSound( "sound/jessev92/dods/weapons/rocket/rocket_worldreload.wav" )

---------------------------------------------
--	"Springfield"
--	M1903 Springfield
--	.30-06
---------------------------------------------

sound.Add( {
	name = "DODS.Springfield.Shoot" ,
	channel = CHAN_WEAPON ,
	volume = 1.0 ,
	level = 150 ,
	pitch = { 95 , 100 } ,
	sound = "^)jessev92/dods/weapons/k98/k98scoped_shoot.wav"
} )
util.PrecacheSound( "sound/jessev92/dods/weapons/k98/k98scoped_shoot.wav" )

sound.Add( {
	name = "DODS.Springfield.SinglShotReload" ,
	channel = CHAN_WEAPON ,
	volume = 1.0 ,
	level = 75 ,
	//pitch = { 95 , 100 } ,
	sound = "^)jessev92/dods/weapons/k98/k98_singleshotreload.wav"
} )
util.PrecacheSound( "sound/jessev92/dods/weapons/k98/k98_singleshotreload.wav" )

sound.Add( {
	name = "DODS.Springfield.Draw" ,
	channel = CHAN_WEAPON ,
	volume = 1.0 ,
	level = 75 ,
	//pitch = { 95 , 100 } ,
	sound = "^)jessev92/dods/weapons/common/draw_rifle.wav"
} )
util.PrecacheSound( "sound/jessev92/dods/weapons/common/draw_rifle.wav" )

sound.Add( {
	name = "DODS.Springfield.WorldReload" ,
	channel = CHAN_WEAPON ,
	volume = 0.77 ,
	level = 65 ,
	//pitch = { 95 , 100 } ,
	sound = "^)jessev92/dods/weapons/k98/k98_worldreload.wav"
} )
util.PrecacheSound( "sound/jessev92/dods/weapons/k98/k98_worldreload.wav" )

sound.Add( {
	name = "DODS.Springfield.BoltBack1" ,
	channel = CHAN_WEAPON ,
	volume = 1.0 ,
	level = 75 ,
	//pitch = { 95 , 100 } ,
	sound = "^)jessev92/dods/weapons/k98/k98_boltback1.wav"
} )
util.PrecacheSound( "sound/jessev92/dods/weapons/k98/k98_boltback1.wav" )

sound.Add( {
	name = "DODS.Springfield.BoltBack2" ,
	channel = CHAN_WEAPON ,
	volume = 1.0 ,
	level = 75 ,
	//pitch = { 95 , 100 } ,
	sound = "^)jessev92/dods/weapons/k98/k98_boltback2.wav"
} )
util.PrecacheSound( "sound/jessev92/dods/weapons/k98/k98_boltback2.wav" )

sound.Add( {
	name = "DODS.Springfield.BoltForward1" ,
	channel = CHAN_WEAPON ,
	volume = 1.0 ,
	level = 75 ,
	//pitch = { 95 , 100 } ,
	sound = "^)jessev92/dods/weapons/k98/k98_boltforward1.wav"
} )
util.PrecacheSound( "sound/jessev92/dods/weapons/k98/k98_boltforward1.wav" )

sound.Add( {
	name = "DODS.Springfield.BoltForward2" ,
	channel = CHAN_WEAPON ,
	volume = 1.0 ,
	level = 75 ,
	//pitch = { 95 , 100 } ,
	sound = "^)jessev92/dods/weapons/k98/k98_boltforward2.wav"
} )
util.PrecacheSound( "sound/jessev92/dods/weapons/k98/k98_boltforward2.wav" )

sound.Add( {
	name = "DODS.Springfield.ClipIn" ,
	channel = CHAN_WEAPON ,
	volume = 1.0 ,
	level = 75 ,
	//pitch = { 95 , 100 } ,
	sound = "^)jessev92/dods/weapons/k98/k98_clipin.wav"
} )
util.PrecacheSound( "sound/jessev92/dods/weapons/k98/k98_clipin.wav" )

sound.Add( {
	name = "DODS.Springfield.ClipIn2" ,
	channel = CHAN_WEAPON ,
	volume = 1.0 ,
	level = 75 ,
	//pitch = { 95 , 100 } ,
	sound = "^)jessev92/dods/weapons/k98/k98_clipin2.wav"
} )
util.PrecacheSound( "sound/jessev92/dods/weapons/k98/k98_clipin2.wav" )

sound.Add( {
	name = "DODS.Springfield.ClipOut" ,
	channel = CHAN_WEAPON ,
	volume = 1.0 ,
	level = 75 ,
	//pitch = { 95 , 100 } ,
	sound = "^)jessev92/dods/weapons/k98/k98_clipout.wav"
} )
util.PrecacheSound( "sound/jessev92/dods/weapons/k98/k98_clipout.wav" )

sound.Add( {
	name = "DODS.Springfield.ClipOut" ,
	channel = CHAN_WEAPON ,
	volume = 1.0 ,
	level = 75 ,
	//pitch = { 95 , 100 } ,
	sound = "^)jessev92/dods/weapons/k98/k98_clipout.wav"
} )
util.PrecacheSound( "sound/jessev92/dods/weapons/k98/k98_clipout.wav" )

---------------------------------------------
--	"Thompson"
--	M1A1 Thompson
--	.45 ACP
---------------------------------------------

sound.Add( {
	name = "DODS.Thompson.Shoot" ,
	channel = CHAN_WEAPON ,
	volume = 1.0 ,
	level = 150 ,
	pitch = { 95 , 105 } ,
	sound = "^)jessev92/dods/weapons/thompson/thompson_shoot.wav"
} )
util.PrecacheSound( "sound/jessev92/dods/weapons/thompson/thompson_shoot.wav" )

sound.Add( {
	name = "DODS.Thompson.BoltForward" ,
	channel = CHAN_ITEM ,
	volume = 1.0 ,
	level = 75 ,
	pitch = { 97 , 103 } ,
	sound = "^)jessev92/dods/weapons/thompson/thompson_boltforward.wav"
} )
util.PrecacheSound( "sound/jessev92/dods/weapons/thompson/thompson_boltforward.wav" )

sound.Add( {
	name = "DODS.Thompson.BoltBack" ,
	channel = CHAN_ITEM ,
	volume = 1.0 ,
	level = 75 ,
	pitch = { 95 , 100 } ,
	sound = "^)jessev92/dods/weapons/thompson/thompson_boltback.wav"
} )
util.PrecacheSound( "sound/jessev92/dods/weapons/thompson/thompson_boltback.wav" )

sound.Add( {
	name = "DODS.Thompson.ClipIn" ,
	channel = CHAN_ITEM ,
	volume = 1.0 ,
	level = 70 ,
	pitch = { 97 , 103 } ,
	sound = "^)jessev92/dods/weapons/thompson/thompson_ClipIn.wav"
} )
util.PrecacheSound( "sound/jessev92/dods/weapons/thompson/thompson_ClipIn.wav" )

sound.Add( {
	name = "DODS.Thompson.ClipOut" ,
	channel = CHAN_ITEM ,
	volume = 1.0 ,
	level = 70 ,
	pitch = { 97 , 103 } ,
	sound = "^)jessev92/dods/weapons/thompson/thompson_ClipOut.wav"
} )
util.PrecacheSound( "sound/jessev92/dods/weapons/thompson/thompson_ClipOut.wav" )

sound.Add( {
	name = "DODS.Thompson.Draw" ,
	channel = CHAN_BODY ,
	volume = 1.0 ,
	level = 75 ,
	pitch = { 97 , 103 } ,
	sound = "^)jessev92/dods/weapons/common/draw_rifle.wav"
} )
util.PrecacheSound( "sound/jessev92/dods/weapons/common/draw_rifle.wav" )

sound.Add( {
	name = "DODS.Thompson.WorldReload" ,
	channel = CHAN_BODY ,
	volume = 0.77 ,
	level = 65 ,
	//pitch = { 95 , 100 } ,
	sound = "^)jessev92/dods/weapons/thompson/thompson_worldreload.wav"
} )
util.PrecacheSound( "sound/jessev92/dods/weapons/thompson/thompson_worldreload.wav" )

---------------------------------------------
--	"Knife"
--	Mark I Trench Knife
---------------------------------------------

sound.Add( {
	name = "DODS.Knife.Draw" ,
	channel = CHAN_WEAPON ,
	volume = 1.0 ,
	level = 75 ,
	pitch = { 95 , 100 } ,
	sound = "^)jessev92/dods/weapons/melee/draw_knife.wav"
} )
util.PrecacheSound( "sound/jessev92/dods/weapons/melee/draw_knife.wav" )

sound.Add( {
	name = "DODS.Knife.Swing" ,
	channel = CHAN_WEAPON ,
	volume = 1.0 ,
	level = 75 ,
	pitch = { 90 , 110 } ,
	sound = "^)jessev92/dods/weapons/melee/blade_swing.wav"
} )
util.PrecacheSound( "sound/jessev92/dods/weapons/melee/blade_swing.wav" )

sound.Add( {
	name = "DODS.Knife.SlashPlayer" ,
	channel = CHAN_WEAPON ,
	volume = 1.0 ,
	level = 75 ,
	pitch = { 90 , 110 } ,
	sound = {
		"^)jessev92/dods/weapons/melee/blade_hit1.wav" ,
		"^)jessev92/dods/weapons/melee/blade_hit2.wav" ,
		"^)jessev92/dods/weapons/melee/blade_hit3.wav" ,
		"^)jessev92/dods/weapons/melee/blade_hit4.wav" ,
	} ,
} )
util.PrecacheSound( "sound/jessev92/dods/weapons/melee/blade_hit1.wav" )
util.PrecacheSound( "sound/jessev92/dods/weapons/melee/blade_hit2.wav" )
util.PrecacheSound( "sound/jessev92/dods/weapons/melee/blade_hit3.wav" )
util.PrecacheSound( "sound/jessev92/dods/weapons/melee/blade_hit4.wav" )

sound.Add( {
	name = "DODS.Knife.SlashWorld" ,
	channel = CHAN_WEAPON ,
	volume = 1.0 ,
	level = 75 ,
	pitch = { 90 , 110 } ,
	sound = {
		"^)jessev92/dods/weapons/melee/blade_hitworld.wav" ,
	} ,
} )
util.PrecacheSound( "sound/jessev92/dods/weapons/melee/blade_hitworld.wav" )

---------------------------------------------
--	Punch
---------------------------------------------

sound.Add( {
	name = "DODS.Punch.HitPlayer" ,
	channel = CHAN_WEAPON ,
	volume = 1.0 ,
	level = 75 ,
	pitch = { 90 , 110 } ,
	sound = {
		"^)jessev92/dods/weapons/melee/punch_hit_player2.wav" ,
		"^)jessev92/dods/weapons/melee/punch_hit_player3.wav" ,
		"^)jessev92/dods/weapons/melee/punch_hit_player4.wav" ,
	} ,
} )
util.PrecacheSound( "sound/jessev92/dods/weapons/melee/punch_hit_player2.wav" )
util.PrecacheSound( "sound/jessev92/dods/weapons/melee/punch_hit_player3.wav" )
util.PrecacheSound( "sound/jessev92/dods/weapons/melee/punch_hit_player4.wav" )

sound.Add( {
	name = "DODS.Punch.HitWorld" ,
	channel = CHAN_WEAPON ,
	volume = 1.0 ,
	level = 75 ,
	pitch = { 90 , 110 } ,
	sound = {
		"^)jessev92/dods/weapons/melee/punch_hit_player2.wav" ,
		"^)jessev92/dods/weapons/melee/punch_hit_player3.wav" ,
		"^)jessev92/dods/weapons/melee/punch_hit_player4.wav" ,
	} ,
} )
util.PrecacheSound( "sound/jessev92/dods/weapons/melee/punch_hit_player2.wav" )
util.PrecacheSound( "sound/jessev92/dods/weapons/melee/punch_hit_player3.wav" )
util.PrecacheSound( "sound/jessev92/dods/weapons/melee/punch_hit_player4.wav" )

---------------------------------------------
--	Spade
---------------------------------------------

sound.Add( {
	name = "DODS.Spade.Draw" ,
	channel = CHAN_WEAPON ,
	volume = 1.0 ,
	level = 75 ,
	pitch = { 95 , 100 } ,
	sound = "^)jessev92/dods/weapons/melee/draw_spade.wav"
} )
util.PrecacheSound( "sound/jessev92/dods/weapons/melee/draw_spade.wav" )

sound.Add( {
	name = "DODS.Spade.SlashPlayer" ,
	channel = CHAN_WEAPON ,
	volume = 1.0 ,
	level = 75 ,
	pitch = { 90 , 110 } ,
	sound = {
		"^)jessev92/dods/weapons/melee/blade_hit1.wav" ,
		"^)jessev92/dods/weapons/melee/blade_hit2.wav" ,
		"^)jessev92/dods/weapons/melee/blade_hit3.wav" ,
		"^)jessev92/dods/weapons/melee/blade_hit4.wav" ,
	} ,
} )
util.PrecacheSound( "sound/jessev92/dods/weapons/melee/blade_hit1.wav" )
util.PrecacheSound( "sound/jessev92/dods/weapons/melee/blade_hit2.wav" )
util.PrecacheSound( "sound/jessev92/dods/weapons/melee/blade_hit3.wav" )
util.PrecacheSound( "sound/jessev92/dods/weapons/melee/blade_hit4.wav" )

sound.Add( {
	name = "DODS.Spade.SlashWorld" ,
	channel = CHAN_WEAPON ,
	volume = 1.0 ,
	level = 75 ,
	pitch = { 90 , 110 } ,
	sound = {
		"^)jessev92/dods/weapons/melee/blade_hitworld.wav" ,
	} ,
} )
util.PrecacheSound( "sound/jessev92/dods/weapons/melee/blade_hitworld.wav" )

---------------------------------------------
--	Grenades
---------------------------------------------

sound.Add( {
	name = "DODS.SmokeGrenade.Explode" ,
	channel = CHAN_WEAPON ,
	volume = 0.75 ,
	level = 75 ,
	pitch = { 95 , 100 } ,
	sound = "^)jessev92/dods/weapons/explode/explode_smoke.wav"
} )
util.PrecacheSound( "sound/jessev92/dods/weapons/explode/explode_smoke.wav" )

sound.Add( {
	name = "DODS.Explode.Debris" ,
	channel = CHAN_WEAPON ,
	volume = 0.75 ,
	level = 75 ,
	pitch = { 95 , 100 } ,
	sound = {
	
		"^)jessev92/dods/weapons/explode/debris2.wav",
		"^)jessev92/dods/weapons/explode/debris4.wav",
		
	} ,
} )
util.PrecacheSound( "sound/jessev92/dods/weapons/explode/debris2.wav" )
util.PrecacheSound( "sound/jessev92/dods/weapons/explode/debris4.wav" )

sound.Add( {
	name = "DODS.Explode" ,
	channel = CHAN_WEAPON ,
	volume = 0.75 ,
	level = 75 ,
	pitch = { 95 , 100 } ,
	sound = {
	
		"^)jessev92/dods/weapons/explode/explode3.wav",
		"^)jessev92/dods/weapons/explode/explode4.wav",
		"^)jessev92/dods/weapons/explode/explode5.wav",
		
	} ,
} )
util.PrecacheSound( "sound/jessev92/dods/weapons/explode/explode3.wav" )
util.PrecacheSound( "sound/jessev92/dods/weapons/explode/explode4.wav" )
util.PrecacheSound( "sound/jessev92/dods/weapons/explode/explode5.wav" )

sound.Add( {
	name = "DODS.Grenade.Spoon" ,
	channel = CHAN_WEAPON ,
	volume = 0.75 ,
	level = 75 ,
	pitch = { 95 , 100 } ,
	sound = "^)jessev92/dods/weapons/grenade/grenade_spoon.wav"
} )
util.PrecacheSound( "sound/jessev92/dods/weapons/grenade/grenade_spoon.wav" )

sound.Add( {
	name = "DODS.Grenade.PinPull" ,
	channel = CHAN_WEAPON ,
	volume = 0.5 ,
	level = 75 ,
	pitch = { 95 , 100 } ,
	sound = "^)jessev92/dods/weapons/grenade/grenade_pinpull.wav"
} )
util.PrecacheSound( "sound/jessev92/dods/weapons/grenade/grenade_pinpull.wav" )

sound.Add( {
	name = "DODS.Grenade.String" ,
	channel = CHAN_WEAPON ,
	volume = 1 ,
	level = 75 ,
	pitch = { 95 , 100 } ,
	sound = "^)jessev92/dods/weapons/grenade/grenade_string.wav"
} )
util.PrecacheSound( "sound/jessev92/dods/weapons/grenade/grenade_string.wav" )

sound.Add( {
	name = "DODS.Grenade.Throw" ,
	channel = CHAN_WEAPON ,
	volume = 1 ,
	level = 75 ,
	pitch = { 95 , 100 } ,
	sound = "^)jessev92/dods/weapons/grenade/grenade_throw.wav"
} )
util.PrecacheSound( "sound/jessev92/dods/weapons/grenade/grenade_throw.wav" )

sound.Add( {
	name = "DODS.Grenade.Draw" ,
	channel = CHAN_WEAPON ,
	volume = 1 ,
	level = 75 ,
	pitch = { 95 , 100 } ,
	sound = "^)jessev92/dods/weapons/common/draw_grenade.wav"
} )
util.PrecacheSound( "sound/jessev92/dods/weapons/common/draw_grenade.wav" )

---------------------------------------------
--	Rifle Grenades
---------------------------------------------

sound.Add( {
	name = "DODS.Grenade.Shoot" ,
	channel = CHAN_WEAPON ,
	volume = 1 ,
	level = 75 ,
	pitch = { 95 , 100 } ,
	sound = "^)jessev92/dods/weapons/grenade/grenade_shoot.wav"
} )
util.PrecacheSound( "sound/jessev92/dods/weapons/grenade/grenade_shoot.wav" )

sound.Add( {
	name = "DODS.Grenade.WorldReloadGarand" ,
	channel = CHAN_WEAPON ,
	volume = 0.77 ,
	level = 65 ,
	pitch = { 95 , 100 } ,
	sound = "^)jessev92/dods/weapons/garand/grenade_worldreloadgarand.wav"
} )
util.PrecacheSound( "sound/jessev92/dods/weapons/garand/grenade_worldreloadgarand.wav" )

sound.Add( {
	name = "DODS.Grenade.ReloadGarand1" ,
	channel = CHAN_WEAPON ,
	volume = 1 ,
	level = 75 ,
	pitch = { 95 , 100 } ,
	sound = "^)jessev92/dods/weapons/garand/grenade_reloadgarand1.wav"
} )
util.PrecacheSound( "sound/jessev92/dods/weapons/garand/grenade_reloadgarand1.wav" )

sound.Add( {
	name = "DODS.Grenade.ReloadGarand2" ,
	channel = CHAN_WEAPON ,
	volume = 1 ,
	level = 75 ,
	pitch = { 95 , 100 } ,
	sound = "^)jessev92/dods/weapons/garand/grenade_reloadgarand2.wav"
} )
util.PrecacheSound( "sound/jessev92/dods/weapons/garand/grenade_reloadgarand2.wav" )

sound.Add( {
	name = "DODS.Grenade.ReloadGarand3" ,
	channel = CHAN_WEAPON ,
	volume = 1 ,
	level = 75 ,
	pitch = { 95 , 100 } ,
	sound = "^)jessev92/dods/weapons/garand/grenade_reloadgarand3.wav"
} )
util.PrecacheSound( "sound/jessev92/dods/weapons/garand/grenade_reloadgarand3.wav" )

sound.Add( {
	name = "DODS.Grenade.ReloadGarand4" ,
	channel = CHAN_WEAPON ,
	volume = 1 ,
	level = 75 ,
	pitch = { 95 , 100 } ,
	sound = "^)jessev92/dods/weapons/garand/grenade_reloadgarand4.wav"
} )
util.PrecacheSound( "sound/jessev92/dods/weapons/garand/grenade_reloadgarand4.wav" )

sound.Add( {
	name = "DODS.Grenade.WorldReloadK98" ,
	channel = CHAN_WEAPON ,
	volume = 0.77 ,
	level = 65 ,
	pitch = { 95 , 100 } ,
	sound = "^)jessev92/dods/weapons/k98/grenade_worldreloadk98.wav"
} )
util.PrecacheSound( "sound/jessev92/dods/weapons/k98/grenade_worldreloadk98.wav" )

---------------------------------------------
--	TNT / Satchel
---------------------------------------------

sound.Add( {
	name = "DODS.TNT.Draw" ,
	channel = CHAN_WEAPON ,
	volume = 1.0 ,
	level = 75 ,
	pitch = { 95 , 100 } ,
	sound = "^)jessev92/dods/weapons/melee/draw_spade.wav"
} )
util.PrecacheSound( "sound/jessev92/dods/weapons/melee/draw_spade.wav" )

-------------------------------------------------------
--	Resistance & Liberation (Mod) Audio
--	Encoded by V92
--	Profile Link:	http://steamcommunity.com/id/JesseVanover/
--	Workshop Link:	http://steamcommunity.com/sharedfiles/filedetails/?id=655569086
-------------------------------------------------------

---------------------------------------------------------------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------------------------------------------------------
--	WEAPONS
---------------------------------------------------------------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------------------------------------------------------

--------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------
--	GERMAN
--------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------

-------------------------------------------------------
-------------------------------------------------------
--	MG 42
-------------------------------------------------------
-------------------------------------------------------

sound.Add( {
	name = "RnL.MG42.Fire" ,
	channel = CHAN_WEAPON ,
	volume = 1.0 ,
	level = 140 ,
	pitch = { 95 , 105 } ,
	sound = { "jessev92/weapons/MG42/MG42_fire_1.wav" , "jessev92/weapons/MG42/MG42_fire_2.wav" }
} )
util.PrecacheSound( "jessev92/weapons/MG42/MG42_fire_1.wav" )
util.PrecacheSound( "jessev92/weapons/MG42/MG42_fire_2.wav" )

---------------------------------------------------------------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------------------------------------------------------
--	VEHICLES
---------------------------------------------------------------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------------------------------------------------------

--------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------
--	GERMAN
--------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------

-------------------------------------------------------
-------------------------------------------------------
--	WIRBELWIND
-------------------------------------------------------
-------------------------------------------------------

sound.Add( {
	name = "RnL.Wirbelwind.Fire" ,
	channel = CHAN_WEAPON ,
	volume = 1.0 ,
	level = 75 ,
	pitch = { 95 , 105 } ,
	sound = { "jessev92/vehicles/spg/wirbelwind_fire_01.wav" , "jessev92/vehicles/spg/wirbelwind_fire_02.wav" , "jessev92/vehicles/spg/wirbelwind_fire_03.wav" , "jessev92/vehicles/spg/wirbelwind_fire_04.wav" }
} )
util.PrecacheSound( "jessev92/vehicles/spg/wirbelwind_fire_01.wav" )
util.PrecacheSound( "jessev92/vehicles/spg/wirbelwind_fire_02.wav" )
util.PrecacheSound( "jessev92/vehicles/spg/wirbelwind_fire_03.wav" )
util.PrecacheSound( "jessev92/vehicles/spg/wirbelwind_fire_04.wav" )

---------------------------------------------------------------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------------------------------------------------------
--	AMBIENT
---------------------------------------------------------------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------------------------------------------------------

--------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------
--	WIND
--------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------

-------------------------------------------------------
-------------------------------------------------------
--	PARACHUTE
-------------------------------------------------------
-------------------------------------------------------

sound.Add({	["name"] = "RNL.Parachute.Deploy",
	["channel"] = CHAN_STATIC,
	["volume"] = 1.0,
	["level"] = 120,
	["pitch"] = { 105, 110 },
	["sound"] = {"jessev92/rnl/ambient/parachute/parachute_deploy.wav"},
})

sound.Add({	["name"] = "RNL.Parachute.Wind",
	["channel"] = CHAN_STATIC,
	["volume"] = 1.0,
	["level"] = 70,
	["pitch"] = { 105, 110 },
	["sound"] = {"jessev92/rnl/ambient/wind/parachute-wind.wav"},
})

sound.Add({	["name"] = "RNL.Parachute.Wind.NoFlap",
	["channel"] = CHAN_STATIC,
	["volume"] = 1.0,
	["level"] = 70,
	["pitch"] = { 105, 110 },
	["sound"] = {"jessev92/rnl/ambient/wind/parachute-wind-noflap.wav"},
})

---------------------------------------------------------------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------------------------------------------------------
--	L4D2
---------------------------------------------------------------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------------------------------------------------------

--------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------
--	PLAYER
--------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------

-------------------------------------------------------
-------------------------------------------------------
--	MELEE
-------------------------------------------------------
-------------------------------------------------------

---------------------------------------------
---------------------------------------------
--	SWING WEAPON
---------------------------------------------
---------------------------------------------

sound.Add( {
	name = "VNTCB.SWep.Melee.WhipHit" ,
	channel = CHAN_WEAPON ,
	level = 75 ,
	volume = 1.0 ,
	pitch = { 95 , 105 } ,
	sound = { "^)jessev92/l4d2/player/melee/swing_weapon1.wav" , "^)jessev92/l4d2/player/melee/swing_weapon2.wav" }
} )
util.PrecacheSound( "jessev92/l4d2/player/melee/swing_weapon1.wav" )
util.PrecacheSound( "jessev92/l4d2/player/melee/swing_weapon2.wav" )

sound.Add( {
	name = "VNTCB.SWep.Melee.WhipMiss" ,
	channel = CHAN_WEAPON ,
	level = 75 ,
	volume = 1.0 ,
	pitch = { 95 , 105 } ,
	sound = { "^)jessev92/l4d2/player/melee/swing_miss1.wav" , "^)jessev92/l4d2/player/melee/swing_miss2.wav" }
} )
util.PrecacheSound( "jessev92/l4d2/player/melee/swing_miss1.wav" )
util.PrecacheSound( "jessev92/l4d2/player/melee/swing_miss2.wav" )

---------------------------------------------------------------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------------------------------------------------------
--	SMOD: TACTICAL
---------------------------------------------------------------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------------------------------------------------------

--------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------
--	WEAPONS
--------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------

-------------------------------------------------------
-------------------------------------------------------
--	KNIFE
-------------------------------------------------------
-------------------------------------------------------

---------------------------------------------
---------------------------------------------
--	HIT
---------------------------------------------
---------------------------------------------

sound.Add( {
	name = "VNTCB.SWep.Melee.HitFlesh" ,
	channel = CHAN_WEAPON ,
	level = 75 ,
	volume = 1.0 ,
	pitch = { 95 , 105 } ,
	sound = { "^)jessev92/smodtac/weapons/knife/hit1.wav" , "^)jessev92/smodtac/weapons/knife/hit2.wav" , "^)jessev92/smodtac/weapons/knife/hit3.wav" , "^)jessev92/smodtac/weapons/knife/hit4.wav" }
} )
util.PrecacheSound( "jessev92/smodtac/weapons/knife/hit1.wav" )
util.PrecacheSound( "jessev92/smodtac/weapons/knife/hit2.wav" )
util.PrecacheSound( "jessev92/smodtac/weapons/knife/hit3.wav" )
util.PrecacheSound( "jessev92/smodtac/weapons/knife/hit4.wav" )

sound.Add( {
	name = "VNTCB.SWep.Melee.HitWall" ,
	channel = CHAN_WEAPON ,
	level = 75 ,
	volume = 1.0 ,
	pitch = { 95 , 105 } ,
	sound = { "^)jessev92/smodtac/weapons/knife/hitwall.wav" }
} )
util.PrecacheSound( "jessev92/smodtac/weapons/knife/hitwall.wav" )

---------------------------------------------
---------------------------------------------
--	MISS
---------------------------------------------
---------------------------------------------

sound.Add( {
	name = "VNTCB.SWep.Melee.Slash" ,
	channel = CHAN_WEAPON ,
	level = 75 ,
	volume = 1.0 ,
	pitch = { 85 , 115 } ,
	sound = { "^)jessev92/smodtac/weapons/knife/slash1.wav" , "^)jessev92/smodtac/weapons/knife/slash2.wav" }
} )
util.PrecacheSound( "jessev92/smodtac/weapons/knife/slash1.wav" )
util.PrecacheSound( "jessev92/smodtac/weapons/knife/slash2.wav" )

-------------------------------------------------------
--	SCar Audio
--	Encoded by V92
--	Profile Link:	http://steamcommunity.com/id/JesseVanover/
--	Workshop Link:	http://steamcommunity.com/sharedfiles/filedetails/?id=
-------------------------------------------------------

---------------------------------------------------------------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------------------------------------------------------
--	WEAPONS
---------------------------------------------------------------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------------------------------------------------------

--------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------
--	COMMON
--------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------

-------------------------------------------------------
-------------------------------------------------------
--	M16A1
-------------------------------------------------------
-------------------------------------------------------

sound.Add( { ["name"] = "VNT.M16A1.MagSpank" ,
	["channel"] = CHAN_STATIC ,
	["volume"] = 1.0 ,
	["level"] = 85 ,
	["pitch"] = { 95 , 105 } ,
	["sound"] = { 
		"jessev92/weapons/m16a1/magspank.wav" ,
	}
} )

-------------------------------------------------------
-------------------------------------------------------
--	Flare Gun
-------------------------------------------------------
-------------------------------------------------------

sound.Add( { ["name"] = "VNT.FlareGun.Fire" ,
	["channel"] = CHAN_STATIC ,
	["volume"] = 1.0 ,
	["level"] = 95 ,
	["pitch"] = { 90 , 110 } ,
	["sound"] = { 
		"jessev92/weapons/flaregun/fire.wav" ,
	}
} )

---------------------------------------------------------------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------------------------------------------------------
--	VEHICLES
---------------------------------------------------------------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------------------------------------------------------

--------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------
--	COMMON
--------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------

-------------------------------------------------------
-------------------------------------------------------
--	ENGINES
-------------------------------------------------------
-------------------------------------------------------

sound.Add( { ["name"] = "SCars.Engine.1" ,
	["channel"] = CHAN_STATIC ,
	["volume"] = 1.0 ,
	["level"] = 100 ,
	["pitch"] = { 100 } ,
	["sound"] = { 
		"scarenginesounds/1.wav" ,
	}
} )

sound.Add( { ["name"] = "SCars.Engine.2" ,
	["channel"] = CHAN_STATIC ,
	["volume"] = 1.0 ,
	["level"] = 100 ,
	["pitch"] = { 100 } ,
	["sound"] = { 
		"scarenginesounds/2.wav" ,
	}
} )

sound.Add( { ["name"] = "SCars.Engine.3" ,
	["channel"] = CHAN_STATIC ,
	["volume"] = 1.0 ,
	["level"] = 100 ,
	["pitch"] = { 100 } ,
	["sound"] = { 
		"scarenginesounds/3.wav" ,
	}
} )

sound.Add( { ["name"] = "SCars.Engine.4" ,
	["channel"] = CHAN_STATIC ,
	["volume"] = 1.0 ,
	["level"] = 100 ,
	["pitch"] = { 100 } ,
	["sound"] = { 
		"scarenginesounds/4.wav" ,
	}
} )

sound.Add( { ["name"] = "SCars.Engine.5" ,
	["channel"] = CHAN_STATIC ,
	["volume"] = 1.0 ,
	["level"] = 100 ,
	["pitch"] = { 100 } ,
	["sound"] = { 
		"scarenginesounds/5.wav" ,
	}
} )

sound.Add( { ["name"] = "SCars.Engine.6" ,
	["channel"] = CHAN_STATIC ,
	["volume"] = 1.0 ,
	["level"] = 100 ,
	["pitch"] = { 100 } ,
	["sound"] = { 
		"scarenginesounds/6.wav" ,
	}
} )

sound.Add( { ["name"] = "SCars.Engine.7" ,
	["channel"] = CHAN_STATIC ,
	["volume"] = 1.0 ,
	["level"] = 100 ,
	["pitch"] = { 100 } ,
	["sound"] = { 
		"scarenginesounds/7.wav" ,
	}
} )

sound.Add( { ["name"] = "SCars.Engine.8" ,
	["channel"] = CHAN_STATIC ,
	["volume"] = 1.0 ,
	["level"] = 100 ,
	["pitch"] = { 100 } ,
	["sound"] = { 
		"scarenginesounds/8.wav" ,
	}
} )

sound.Add( { ["name"] = "SCars.Engine.9" ,
	["channel"] = CHAN_STATIC ,
	["volume"] = 1.0 ,
	["level"] = 100 ,
	["pitch"] = { 100 } ,
	["sound"] = { 
		"scarenginesounds/9.wav" ,
	}
} )

sound.Add( { ["name"] = "SCars.Engine.10" ,
	["channel"] = CHAN_STATIC ,
	["volume"] = 1.0 ,
	["level"] = 100 ,
	["pitch"] = { 100 } ,
	["sound"] = { 
		"scarenginesounds/10.wav" ,
	}
} )

sound.Add( { ["name"] = "SCars.Engine.11" ,
	["channel"] = CHAN_STATIC ,
	["volume"] = 1.0 ,
	["level"] = 100 ,
	["pitch"] = { 100 } ,
	["sound"] = { 
		"scarenginesounds/11.wav" ,
	}
} )

sound.Add( { ["name"] = "SCars.Engine.12" ,
	["channel"] = CHAN_STATIC ,
	["volume"] = 1.0 ,
	["level"] = 100 ,
	["pitch"] = { 100 } ,
	["sound"] = { 
		"scarenginesounds/12.wav" ,
	}
} )

sound.Add( { ["name"] = "SCars.Engine.13" ,
	["channel"] = CHAN_STATIC ,
	["volume"] = 1.0 ,
	["level"] = 100 ,
	["pitch"] = { 100 } ,
	["sound"] = { 
		"scarenginesounds/13.wav" ,
	}
} )

sound.Add( { ["name"] = "SCars.Engine.14" ,
	["channel"] = CHAN_STATIC ,
	["volume"] = 1.0 ,
	["level"] = 100 ,
	["pitch"] = { 100 } ,
	["sound"] = { 
		"scarenginesounds/14.wav" ,
	}
} )

util.PrecacheSound( "scarenginesounds/1.wav" )
util.PrecacheSound( "scarenginesounds/2.wav" )
util.PrecacheSound( "scarenginesounds/3.wav" )
util.PrecacheSound( "scarenginesounds/4.wav" )
util.PrecacheSound( "scarenginesounds/5.wav" )
util.PrecacheSound( "scarenginesounds/6.wav" )
util.PrecacheSound( "scarenginesounds/7.wav" )
util.PrecacheSound( "scarenginesounds/8.wav" )
util.PrecacheSound( "scarenginesounds/9.wav" )
util.PrecacheSound( "scarenginesounds/10.wav" )
util.PrecacheSound( "scarenginesounds/11.wav" )
util.PrecacheSound( "scarenginesounds/12.wav" )
util.PrecacheSound( "scarenginesounds/13.wav" )
util.PrecacheSound( "scarenginesounds/14.wav" )

-------------------------------------------------------
-------------------------------------------------------
--	CAR
-------------------------------------------------------
-------------------------------------------------------

sound.Add( { ["name"] = "SCars.Siren.ToyCop" ,
	["channel"] = CHAN_STATIC ,
	["volume"] = 1.0 ,
	["level"] = 120 ,
	["pitch"] = { 100 } ,
	["sound"] = { 
		"car/siren.wav" ,
	}
} )

util.PrecacheSound( "car/siren.wav" )

-------------------------------------------------------
-------------------------------------------------------
--	CAR HORNS
-------------------------------------------------------
-------------------------------------------------------

sound.Add( { ["name"] = "SCars.Horn.Car1" ,
	["channel"] = CHAN_STATIC ,
	["volume"] = 1.0 ,
	["level"] = 90 ,
	["pitch"] = { 100 } ,
	["sound"] = { 
		"scarhorns/horn 1.wav" ,
	}
} )

sound.Add( { ["name"] = "SCars.Horn.Car2" ,
	["channel"] = CHAN_STATIC ,
	["volume"] = 1.0 ,
	["level"] = 90 ,
	["pitch"] = { 100 } ,
	["sound"] = { 
		"scarhorns/horn 2.wav" ,
	}
} )

sound.Add( { ["name"] = "SCars.Horn.Car3" ,
	["channel"] = CHAN_STATIC ,
	["volume"] = 1.0 ,
	["level"] = 90 ,
	["pitch"] = { 100 } ,
	["sound"] = { 
		"scarhorns/horn 3.wav" ,
	}
} )

sound.Add( { ["name"] = "SCars.Horn.Car4" ,
	["channel"] = CHAN_STATIC ,
	["volume"] = 1.0 ,
	["level"] = 90 ,
	["pitch"] = { 100 } ,
	["sound"] = { 
		"scarhorns/horn 4.wav" ,
	}
} )

sound.Add( { ["name"] = "SCars.Horn.Car5" ,
	["channel"] = CHAN_STATIC ,
	["volume"] = 1.0 ,
	["level"] = 90 ,
	["pitch"] = { 100 } ,
	["sound"] = { 
		"scarhorns/horn 5.wav" ,
	}
} )

sound.Add( { ["name"] = "SCars.Horn.Car6" ,
	["channel"] = CHAN_STATIC ,
	["volume"] = 1.0 ,
	["level"] = 90 ,
	["pitch"] = { 100 } ,
	["sound"] = { 
		"scarhorns/horn 6.wav" ,
	}
} )

sound.Add( { ["name"] = "SCars.Horn.Car7" ,
	["channel"] = CHAN_STATIC ,
	["volume"] = 1.0 ,
	["level"] = 90 ,
	["pitch"] = { 100 } ,
	["sound"] = { 
		"scarhorns/horn 7.wav" ,
	}
} )

sound.Add( { ["name"] = "SCars.Horn.Car8" ,
	["channel"] = CHAN_STATIC ,
	["volume"] = 1.0 ,
	["level"] = 90 ,
	["pitch"] = { 100 } ,
	["sound"] = { 
		"scarhorns/horn 8.wav" ,
	}
} )

sound.Add( { ["name"] = "SCars.Horn.Car9" ,
	["channel"] = CHAN_STATIC ,
	["volume"] = 1.0 ,
	["level"] = 90 ,
	["pitch"] = { 100 } ,
	["sound"] = { 
		"scarhorns/horn 9.wav" ,
	}
} )

sound.Add( { ["name"] = "SCars.Horn.Car10" ,
	["channel"] = CHAN_STATIC ,
	["volume"] = 1.0 ,
	["level"] = 90 ,
	["pitch"] = { 100 } ,
	["sound"] = { 
		"scarhorns/horn 10.wav" ,
	}
} )

util.PrecacheSound( "scarhorns/horn 1.wav" )
util.PrecacheSound( "scarhorns/horn 2.wav" )
util.PrecacheSound( "scarhorns/horn 3.wav" )
util.PrecacheSound( "scarhorns/horn 4.wav" )
util.PrecacheSound( "scarhorns/horn 5.wav" )
util.PrecacheSound( "scarhorns/horn 6.wav" )
util.PrecacheSound( "scarhorns/horn 7.wav" )
util.PrecacheSound( "scarhorns/horn 8.wav" )
util.PrecacheSound( "scarhorns/horn 9.wav" )
util.PrecacheSound( "scarhorns/horn 10.wav" )

-------------------------------------------------------
-------------------------------------------------------
--	TRUCK HORNS
-------------------------------------------------------
-------------------------------------------------------

sound.Add( { ["name"] = "SCars.Horn.Truck1" ,
	["channel"] = CHAN_STATIC ,
	["volume"] = 1.0 ,
	["level"] = 140 ,
	["pitch"] = { 100 } ,
	["sound"] = { 
		"scarhorns/truckhorn 1.wav" ,
	}
} )

sound.Add( { ["name"] = "SCars.Horn.Truck2" ,
	["channel"] = CHAN_STATIC ,
	["volume"] = 1.0 ,
	["level"] = 140 ,
	["pitch"] = { 100 } ,
	["sound"] = { 
		"scarhorns/truckhorn 2.wav" ,
	}
} )

sound.Add( { ["name"] = "SCars.Horn.Truck3" ,
	["channel"] = CHAN_STATIC ,
	["volume"] = 1.0 ,
	["level"] = 140 ,
	["pitch"] = { 100 } ,
	["sound"] = { 
		"scarhorns/truckhorn 3.wav" ,
	}
} )

sound.Add( { ["name"] = "SCars.Horn.Truck4" ,
	["channel"] = CHAN_STATIC ,
	["volume"] = 1.0 ,
	["level"] = 140 ,
	["pitch"] = { 100 } ,
	["sound"] = { 
		"scarhorns/truckhorn 4.wav" ,
	}
} )

sound.Add( { ["name"] = "SCars.Horn.Truck5" ,
	["channel"] = CHAN_STATIC ,
	["volume"] = 1.0 ,
	["level"] = 140 ,
	["pitch"] = { 100 } ,
	["sound"] = { 
		"scarhorns/truckhorn 5.wav" ,
	}
} )

util.PrecacheSound( "scarhorns/truckhorn 1.wav" )
util.PrecacheSound( "scarhorns/truckhorn 2.wav" )
util.PrecacheSound( "scarhorns/truckhorn 3.wav" )
util.PrecacheSound( "scarhorns/truckhorn 4.wav" )
util.PrecacheSound( "scarhorns/truckhorn 5.wav" )

-------------------------------------------------------
-------------------------------------------------------
--	SPECIAL HORNS
-------------------------------------------------------
-------------------------------------------------------

sound.Add( { ["name"] = "SCars.Horn.GeneralLee" ,
	["channel"] = CHAN_STATIC ,
	["volume"] = 1.0 ,
	["level"] = 90 ,
	["pitch"] = { 100 } ,
	["sound"] = { 
		"scarhorns/general lee horn.wav" ,
	}
} )

util.PrecacheSound( "scarhorns/general lee horn.wav" )

sound.Add( { ["name"] = "SCars.Horn.Siren" ,
	["channel"] = CHAN_STATIC ,
	["volume"] = 1.0 ,
	["level"] = 140 ,
	["pitch"] = { 100 } ,
	["sound"] = { 
		"scarhorns/sirenhorn.wav" ,
	}
} )

util.PrecacheSound( "scarhorns/sirenhorn.wav" )

--------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------
--	SPECIFIC VEHICLES
--------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------

-------------------------------------------------------
-------------------------------------------------------
--	DEFAULT SC TANK
-------------------------------------------------------
-------------------------------------------------------

sound.Add( { ["name"] = "SCar.Tank.Cannon" ,
	["channel"] = CHAN_STATIC ,
	["volume"] = 1.0 ,
	["level"] = 140 ,
	["pitch"] = { 90 , 115 } ,
	["sound"] = { 
		"scartank/fire.wav" ,
	}
} )

sound.Add( { ["name"] = "SCar.Tank.Smoke" ,
	["channel"] = CHAN_STATIC ,
	["volume"] = 1.0 ,
	["level"] = 140 ,
	["pitch"] = { 90 , 115 } ,
	["sound"] = { 
		"scartank/fire_smoke.wav" ,
	}
} )

sound.Add( { ["name"] = "SCars.Tank.Cannon_Reload" ,
	["channel"] = CHAN_STATIC ,
	["volume"] = 1.0 ,
	["level"] = 100 ,
	["pitch"] = { 100 } ,
	["sound"] = { 
		"scartank/reload.wav" ,
	}
} )

sound.Add( { ["name"] = "SCars.Tank.Smoke_Reload" ,
	["channel"] = CHAN_STATIC ,
	["volume"] = 1.0 ,
	["level"] = 100 ,
	["pitch"] = { 100 } ,
	["sound"] = { 
		"scartank/reload_smoke.wav" ,
	}
} )

sound.Add( { ["name"] = "SCars.Tank.Engine" ,
	["channel"] = CHAN_STATIC ,
	["volume"] = 1.0 ,
	["level"] = 100 ,
	["pitch"] = { 100 } ,
	["sound"] = { 
		"scartank/tankengine.wav" ,
	}
} )

sound.Add( { ["name"] = "SCars.Tank.Tracks" ,
	["channel"] = CHAN_STATIC ,
	["volume"] = 1.0 ,
	["level"] = 100 ,
	["pitch"] = { 100 } ,
	["sound"] = { 
		"scartank/tracksmove.wav" ,
	}
} )

sound.Add( { ["name"] = "SCars.Tank.Coax" ,
	["channel"] = CHAN_STATIC ,
	["volume"] = 1.0 ,
	["level"] = 100 ,
	["pitch"] = { 90 , 110 } ,
	["sound"] = { 
		"scartank/turretfire.wav" ,
	}
} )

sound.Add( { ["name"] = "SCars.Tank.TurretRot.Yaw" ,
	["channel"] = CHAN_STATIC ,
	["volume"] = 1.0 ,
	["level"] = 75 ,
	["pitch"] = { 100 } ,
	["sound"] = { 
		"scartank/turretmove.wav" ,
	}
} )

sound.Add( { ["name"] = "SCars.Tank.TurretRot.Pitch" ,
	["channel"] = CHAN_STATIC ,
	["volume"] = 1.0 ,
	["level"] = 75 ,
	["pitch"] = { 100 } ,
	["sound"] = { 
		"scartank/turretmove2.wav" ,
	}
} )

util.PrecacheSound( "scartank/fire.wav" )
util.PrecacheSound( "scartank/fire_smoke.wav" )
util.PrecacheSound( "scartank/reload.wav" )
util.PrecacheSound( "scartank/reload_smoke.wav" )
util.PrecacheSound( "scartank/tankengine.wav" )
util.PrecacheSound( "scartank/tracksmove.wav" )
util.PrecacheSound( "scartank/turretfire.wav" )
util.PrecacheSound( "scartank/turretmove.wav" )
util.PrecacheSound( "scartank/turretmove2.wav" )
