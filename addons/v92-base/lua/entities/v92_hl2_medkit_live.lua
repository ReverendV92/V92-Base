
AddCSLuaFile()

ENT.Type = "anim"
ENT.Base = VNT_BASE_DEPLOYABLE_MEDICAL

ENT.PrintName = "Active Medical Kit"
ENT.Category = VNT_CATEGORY_HL2
ENT.Author = VNTCB.Info.author
ENT.Purpose = VNTCB.Info.purpose
ENT.Instructions = VNTCB.Info.instructions
ENT.Contact = VNTCB.Info.contact
ENT.Spawnable = true
ENT.AdminOnly = true

ENT.RemainingCharge = GetConVarNumber("VNT_SWep_Deployable_Charge")
ENT.ActionDelay = CurTime()
ENT.PickupSound = Sound( "HealthKit.Touch" )
ENT.PickupModel = Model( "models/weapons/w_medkit.mdl" )
ENT.RemoveTime = 300
