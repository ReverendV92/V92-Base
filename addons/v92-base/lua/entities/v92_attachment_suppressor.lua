
AddCSLuaFile( )

if not VNTCB then return false end

ENT.Base = VNT_BASE_WEAPON_ATTACHMENT
ENT.Type = "anim"

ENT.PrintName = "Suppressor"
ENT.Author = VNTCB.author
ENT.Information = "Gives a suppressor"
ENT.Category = VNT_CATEGORY_VNT

ENT.Spawnable = true
ENT.AdminOnly = false

ENT.SelfEntity = "v92_attachment_suppressor" -- (String) Name of this entity
ENT.PartSlot = VNT_WEAPON_ATTACHMENT_SUPPRESSOR -- (Enumeration: VNT Weapon Attachments)
ENT.PartCrateModel = Model( "models/Items/BoxMRounds.mdl" ) -- (String) Model to use
ENT.CollisionSound = Sound( "BaseCombatWeapon.WeaponDrop" ) -- Physics collisions
ENT.PickupSound = Sound( "BaseCombatCharacter.AmmoPickup" ) -- Pickup sounds
ENT.BagCollisionSound = Sound( "Rubber.ImpactHard" ) -- Physics collisions
