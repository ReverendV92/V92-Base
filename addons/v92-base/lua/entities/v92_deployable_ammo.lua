
AddCSLuaFile()

ENT.Type = "anim"
--ENT.Base = "v92_base_deployable"

ENT.PrintName = "Ammunition Bag"
ENT.Category = VNT_CATEGORY_VNT
ENT.Author = VNTCB.Info.author
ENT.Purpose = VNTCB.Info.purpose
ENT.Instructions = VNTCB.Info.instructions
ENT.Contact = VNTCB.Info.contact
ENT.Spawnable = false
ENT.AdminOnly = true

ENT.RemainingCharge = GetConVarNumber("VNT_SWep_Deployable_Ammo_Charge")
ENT.ActionDelay = CurTime()
ENT.PickupSound = Sound( "BF2.Common.Resupply" )
ENT.PickupModel = Model( "models/Items/BoxMRounds.mdl" )
ENT.RemoveTime = 300
ENT.BlacklistedAmmoTypes = {
	"medbag" ,
	"ammobag" ,
	"rpg_rocket" ,
	"gasmaskfilters" ,
	"slam" ,
	"c4" ,
}
ENT.BlacklistedWeapons = {
	"gmod_tool" ,
	"camera" ,
	"weapon_physcannon" ,
	"weapon_physgun" ,
	"weapon_crowbar" ,
	"weapon_stunstick" ,
	"weapon_rpg" ,
	"weapon_slam" ,
	"weapon_frag" ,
	"weapon_grenade" ,
	"v92_hl2_ammokit" ,
	"v92_hl2_medkit" ,
	"v92_bf2_ammobag" ,
	"v92_bf2_medicbag" ,
}

if CLIENT then

	function ENT:Initialize() end
	function ENT:Draw() self:DrawModel() end
	function ENT:Think() end 

end

if SERVER then

	function ENT:Initialize()

		self:SetModel( self.PickupModel )

		self:PhysicsInit( SOLID_VPHYSICS ) -- Set physics type
		self:SetSolid( SOLID_VPHYSICS ) -- Set solid
		self:SetMoveType( MOVETYPE_VPHYSICS ) -- Set movetype
		self:SetCollisionGroup( COLLISION_GROUP_INTERACTIVE ) -- Set our collision group
		self:SetUseType( SIMPLE_USE ) -- Set a use type so it doesn't spam it
		self:SetOwner( self ) -- Set our owner
		self:DrawShadow( true ) -- Draw a shadow

		local Physics = self:GetPhysicsObject() -- Get our physics

		if Physics:IsValid() then -- If the physics are valid

			Physics:SetMass( self.Mass or Physics:GetMass( ) ) -- Set our mass to either the defined mass, or the prop's mass
			Physics:Wake() -- Wake up Mr. Cheeseman

		end

		timer.Simple( self.RemoveTime + 0.92 , function( ) 

			if IsValid( self ) then

				self:Remove()

			end

		end )

	end

	function ENT:OnTakeDamage(dmginfo)

		self:GetPhysicsObject():AddVelocity(dmginfo:GetDamageForce() * 0.1)
		
	end

	function ENT:Use( activator )

		if IsValid( self ) and IsValid( activator ) then

			activator:ChatPrint( "Self and user are valid" )

			if activator:IsPlayer( ) and activator:Alive( ) then

				activator:ChatPrint( "user is a player and it's alive" )

				if self.RemainingCharge > 0 then

					activator:ChatPrint( "charge is > 0" )

					local wep = activator:GetActiveWeapon()
					
					if wep == ( "weapon_physcannon" ) then 
					
						return false
						
					end

					if table.HasValue( self.BlacklistedWeapons , wep ) then

						activator:ChatPrint( "Blacklisted weapon!" )

						return false
					
					end

					if wep.Primary and wep.Primary.ClipSize > -1 then

						if not table.HasValue( self.BlacklistedAmmoTypes , wep:GetPrimaryAmmoType( ) ) then

							activator:GiveAmmo( GetConVarNumber("VNT_SWep_Deployable_Ammo_Apply") , wep:Ammo1( ) )

							self.RemainingCharge = ( self.RemainingCharge - GetConVarNumber("VNT_SWep_Deployable_Ammo_Apply") )

							activator:ChatPrint( "Gave ammo to activator; remaining charge: " .. self.RemainingCharge )

						else

							activator:ChatPrint( "Blacklisted primary ammo type!" )
							
							return false

						end

					end

					if wep.Secondary and wep.Secondary.ClipSize > -1 then

						if not table.HasValue( self.BlacklistedAmmoTypes , wep:GetSecondaryAmmoType( ) ) then

							activator:GiveAmmo( GetConVarNumber("VNT_SWep_Deployable_Ammo_Apply") , wep:Ammo1( ) )

							self.RemainingCharge = ( self.RemainingCharge - GetConVarNumber("VNT_SWep_Deployable_Ammo_Apply") )

							activator:ChatPrint( "Gave ammo to activator; remaining charge: " .. self.RemainingCharge )

						else

							activator:ChatPrint( "Blacklisted Secondary ammo type!" )

						end

					end

					self:EmitSound( self.PickupSound )

					self.ActionDelay = CurTime( ) + GetConVarNumber("VNT_SWep_Deployable_Ammo_GiveDelay")

				end

			else
			
				self:Remove()

			end

		end

	end

	function ENT:OnRemove() 

		return false	

	end 

	function ENT:Think()

		if SERVER then

			if CurTime() > self.ActionDelay and self.RemainingCharge <= 0 then

				SafeRemoveEntity( self )
				
				return false
				
			end

		end

	end

end
