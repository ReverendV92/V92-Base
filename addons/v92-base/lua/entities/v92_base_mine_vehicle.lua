
AddCSLuaFile()

ENT.Type = "anim"
ENT.PrintName = "Anti-Vehicle Mine"
ENT.Author = "V92"
ENT.Spawnable = false
ENT.AdminOnly = true

ENT.MineModel = Model( "models/props_junk/sawblade001a.mdl" )
ENT.TimeToArm = 3
ENT.DeployedSound = Sound( "HL2.SLAM.MineMode" )
ENT.ArmedSound = Sound( "HL2.Rollermine.RemoteYes" )
ENT.ExplosionSound = Sound( "HL2.Generic.Explosion" )

-- Nothing beyond this needs to be included in child entities

if CLIENT then

	function ENT:Initialize() 

	end

	function ENT:Draw() 
	
		self:DrawModel() 
		
	end

	function ENT:Think() 

		return false

	end 

end

if SERVER then

	function ENT:Initialize( )

		self:SetModel( self.MineModel )
		self:PhysicsInit( SOLID_VPHYSICS )
		self:SetMoveType( MOVETYPE_VPHYSICS )
		self:SetSolid( SOLID_VPHYSICS )
		self:SetCollisionGroup( COLLISION_GROUP_INTERACTIVE )

		self:DrawShadow( true )
		self:SetNoDraw( false )

		self:EmitSound( self.DeployedSound )
		
		local phys = self:GetPhysicsObject( )

		if phys:IsValid( ) then	
			
			phys:Wake( )

		end

		ArmDelay = CurTime() + self.TimeToArm

		constraint.Keepupright( self , Angle( 0 , 0 , 0 ) , 0 , 5000 )

		timer.Simple( 3 , function( )
		
			if IsValid( self ) then

				self:EmitSound( self.ArmedSound )
				
			end
			
		end )

		timer.Simple( 5 , function( )

			if IsValid( self ) then

				self:SetMoveType( MOVETYPE_NONE )
				
			end

		end)

	end

	function ENT:Think()

		if ArmDelay <= CurTime() then

			for key , EntitiesFound in pairs ( ents.FindInSphere( self:GetPos(), 32 ) ) do

				local _TRACE = { }

					_TRACE.start = self:GetPos( )
					_TRACE.endpos = EntitiesFound:GetPos( )
					_TRACE.filter = self

				local _TR = util.TraceLine( _TRACE )

				if _TR.Entity:IsVehicle( ) then

					self:Explosion( )

				end

			end

		end

		self:NextThink( CurTime( ) )

		return false

	end

	function ENT:Explosion( )

		local trace = { }
		trace.start = self:GetPos( ) + Vector(0, 0, 32)
		trace.endpos = self:GetPos( ) - Vector(0, 0, 128)
		trace.Entity = self
		trace.mask  = 16395
		local Normal = util.TraceLine(trace).HitNormal

		self.Scale = 1
		self.EffectScale = self.Scale ^ 0.65

		local effectdata = EffectData()
			effectdata:SetOrigin(self:GetPos())
		util.Effect("HelicopterMegaBomb", effectdata)

		local explo = ents.Create("env_explosion")
			explo:SetOwner(self.Owner)
			explo:SetPos(self:GetPos())
			explo:SetKeyValue("iMagnitude", "200")
			explo:Spawn()
			explo:Activate()
			explo:Fire("Explode", "", 0)
		
		local shake = ents.Create("env_shake")
			shake:SetOwner(self.Owner)
			shake:SetPos(self:GetPos())
			shake:SetKeyValue("amplitude", "2000") // Power of the shake
			shake:SetKeyValue("radius", "1250") 	// Radius of the shake
			shake:SetKeyValue("duration", "2.5") // Time of shake
			shake:SetKeyValue("frequency", "255") // How har should the screenshake be
			shake:SetKeyValue("spawnflags", "4") // Spawnflags(In Air)
			shake:Spawn()
			shake:Activate()
			shake:Fire("StartShake", "", 0)

		local ar2Explo = ents.Create("env_ar2explosion")
			ar2Explo:SetOwner(self.Owner)
			ar2Explo:SetPos(self:GetPos())
			ar2Explo:Spawn()
			ar2Explo:Activate()
			ar2Explo:Fire("Explode", "", 0)

		self:EmitSound( self.ExplosionSound )

		self:Remove( )


		if GetConVarNumber( "VNT_Mine_ExplodeUnweld" ) != 0 then

			for key , EntitiesFound in pairs ( ents.FindInSphere( self:GetPos(), 32 ) ) do

				if IsValid( v:GetPhysicsObject( ) ) then

					-- Break Prop Welds

					if ( math.random( 1 , 100 ) < 45 ) then

						v:Fire( "enablemotion" , "" , 0 )

						constraint.RemoveAll( v )

					end

				end

			end

		end

	end

	function ENT:OnTakeDamage( dmginfo )

		local GoodLuck = math.random( 1 , 3 )

		if GoodLuck == 1 then

			self:Explosion( )

		end

	end

	function ENT:Use( activator , caller , type , value ) end

	function ENT:StartTouch( entity ) end

	function ENT:Touch( entity ) 
	
		if IsValid( self ) and IsValid( entity ) then
	
			if entity:IsVehicle( ) then

				self:Explosion( )

			end

		end

	end

	function ENT:EndTouch( entity ) end

end
