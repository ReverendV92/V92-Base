
AddCSLuaFile()

ENT.Type = "anim"
ENT.Base = VNT_BASE_DEPLOYABLE_AMMO

ENT.PrintName = "Active Ammo Box"
ENT.Category = VNT_CATEGORY_HL2
ENT.Author = VNTCB.Info.author
ENT.Purpose = VNTCB.Info.purpose
ENT.Instructions = VNTCB.Info.instructions
ENT.Contact = VNTCB.Info.contact
ENT.Spawnable = true
ENT.AdminOnly = true

ENT.RemainingCharge = GetConVarNumber("VNT_SWep_Deployable_Ammo_Charge")
ENT.ActionDelay = CurTime()
ENT.PickupSound = Sound( "BF2.Common.Resupply" )
ENT.PickupModel = Model( "models/Items/BoxMRounds.mdl" )
ENT.RemoveTime = 300
