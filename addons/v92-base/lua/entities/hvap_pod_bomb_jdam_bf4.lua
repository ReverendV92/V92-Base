AddCSLuaFile()

--if wac then return false end
if not hvap then return false end

ENT.Base = "hvap_pod_bomb_base"
ENT.PrintName = "JDAM"
ENT.Author = VNTCB.author
ENT.Category = VNTCB.Category.VNT
ENT.Contact = VNTCB.contact
ENT.Purpose = VNTCB.purpose
ENT.Instructions = VNTCB.instructions

ENT.Spawnable = false
ENT.AdminOnly = true

ENT.Name = ENT.PrintName
ENT.Ammo = 2000
ENT.FireRate = 60

ENT.DisableRearm = false
ENT.PhysOrd = true
ENT.Rocket = false

ENT.Belt = 1

ENT.AmmoBelt = {
	{
		"he",
	},
}

ENT.AmmoData = {
	["he"] = {
		class = "hvap_bullet_bomb_he",
		info = {
			model = "models/jessev92/bf4/vehicles/missiles/jdam.mdl",
			mass = 250,
			Radius = 1280,
			Damage = 640
		}
	},
	
}
