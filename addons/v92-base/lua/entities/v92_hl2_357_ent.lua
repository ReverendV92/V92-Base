
AddCSLuaFile( )

ENT.PrintName = ".357 Magnum"

if not VNTCB then

	Error( "V92 Content Bases not mounted; Removing Weapon: " .. self.PrintName .. "\n" )

	return false

end

ENT.Base = VNT_BASE_WEAPON_ENTITY
ENT.Type = "anim"
ENT.Author = VNTCB.author
ENT.Information = "Uses .357 Magnum Ammo"
ENT.Category = VNT_CATEGORY_HL2
ENT.Spawnable = true
ENT.AdminOnly = false
ENT.SWepName = "v92_hl2_357" -- (String) Name of the weapon entity in Lua/weapons/swep_name.lua
ENT.WeaponName = ENT.SWepName .. "_ent"	-- (String) Name of this entity
ENT.SEntModel = Model( "models/weapons/w_357.mdl" ) -- (String) Model to use