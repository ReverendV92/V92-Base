
AddCSLuaFile()

ENT.Type = "anim"
--ENT.Base = "v92_base_deployable"

ENT.PrintName = "Medical Kit"
ENT.Category = VNT_CATEGORY_VNT
ENT.Author = VNTCB.Info.author
ENT.Purpose = VNTCB.Info.purpose
ENT.Instructions = VNTCB.Info.instructions
ENT.Contact = VNTCB.Info.contact
ENT.Spawnable = false
ENT.AdminOnly = true

ENT.RemainingCharge = GetConVarNumber("VNT_SWep_Deployable_Medical_Charge")
ENT.ActionDelay = CurTime()
ENT.PickupSound = Sound( "BF2.Common.Heal" )
ENT.PickupModel = Model( "models/weapons/w_medkit.mdl" )
ENT.RemoveTime = 300
ENT.Mass = 2

if CLIENT then

	function ENT:Initialize() end
	function ENT:Draw() self:DrawModel() end
	function ENT:Think() end 

end

if SERVER then

	function ENT:Initialize()

		self:SetModel( self.PickupModel )

		self:PhysicsInit( SOLID_VPHYSICS ) -- Set physics type
		self:SetSolid( SOLID_VPHYSICS ) -- Set solid
		self:SetMoveType( MOVETYPE_VPHYSICS ) -- Set movetype
		self:SetCollisionGroup( COLLISION_GROUP_INTERACTIVE ) -- Set our collision group
		self:SetUseType( SIMPLE_USE ) -- Set a use type so it doesn't spam it
		self:SetOwner( self ) -- Set our owner
		self:DrawShadow( true ) -- Draw a shadow

		local Physics = self:GetPhysicsObject() -- Get our physics

		if Physics:IsValid() then -- If the physics are valid

			Physics:SetMass( self.Mass or Physics:GetMass( ) ) -- Set our mass to either the defined mass, or the prop's mass
			Physics:Wake() -- Wake up Mr. Cheeseman

		end

		timer.Simple( self.RemoveTime + 0.92 , function( ) 

			if IsValid( self ) then

				self:Remove()

			end

		end )

	end

	function ENT:OnTakeDamage(dmginfo)

		self:GetPhysicsObject():AddVelocity(dmginfo:GetDamageForce() * 0.1)
		
	end

	function ENT:Use( ply )

		if ply:IsPlayer( ) and not ply:Health( ) == ply:GetMaxHealth( ) then

			if not ply:KeyDown( IN_SPEED ) then

				ply:PickupObject( self )

			else 

				if self.PickupSound != nil then

					ply:EmitSound( self.PickupSound )

				end

				ply:SetHealth( ply:GetMaxHealth( ) )

				if IsValid( self ) then
				
					self:Remove()
					
				end

			end

		end

	end

	function ENT:OnRemove() return false	end 

	function ENT:Think()

		if SERVER then

			if CurTime() > self.ActionDelay then

				if self.RemainingCharge <= 0 then

					SafeRemoveEntity( self )
					
					return false
					
				end

				for Key , FoundEnts in pairs( ents.FindInSphere( self:GetPos( ) , GetConVarNumber("VNT_SWep_Deployable_FindRadius") ) ) do

					if IsValid( FoundEnts ) and ( 
						( FoundEnts:IsPlayer( ) and FoundEnts:Alive( ) )
						or ( FoundEnts:IsNPC( ) )
					)
						and FoundEnts:Health() < FoundEnts:GetMaxHealth()
					then
					
						if GetConVarNumber("VNT_SWep_Deployable_Medical_Charge") > 0 then

							if ( ( FoundEnts:GetMaxHealth() - FoundEnts:Health() ) < GetConVarNumber( "VNT_SWep_Deployable_Medical_Apply" ) ) then

								-- print( "Health Pack: Health to Apply Would Overflow! Clamping!" )
								FoundEnts:SetHealth( FoundEnts:Health() + ( math.Clamp( FoundEnts:Health() , 1 , ( FoundEnts:GetMaxHealth() - FoundEnts:Health()  ) ) ) )
								self.RemainingCharge = ( self.RemainingCharge - ( FoundEnts:GetMaxHealth() - FoundEnts:Health()  ) )
								
								FoundEnts:EmitSound( self.PickupSound )

								return
						
							elseif ( ( FoundEnts:GetMaxHealth( ) - FoundEnts:Health() ) > GetConVarNumber( "VNT_SWep_Deployable_Medical_Charge" ) ) then

								-- print( "Health Pack: Missing Health is MORE than Remaining Charge!" )
								FoundEnts:SetHealth( FoundEnts:Health() + GetConVarNumber("VNT_SWep_Deployable_Medical_Charge") )

							else

								-- print( "Health Pack: Missing Health is LESS than Remaining Charge!" )
								FoundEnts:SetHealth( FoundEnts:Health() + GetConVarNumber("VNT_SWep_Deployable_Medical_Apply") )

							end
							
							self.RemainingCharge = ( self.RemainingCharge - GetConVarNumber("VNT_SWep_Deployable_Medical_Apply") )
							-- print( "Health Pack: Remaining Charge is: " .. self.RemainingCharge )
							FoundEnts:EmitSound( self.PickupSound )

						else
						
							SafeRemoveEntity( self )

						end

					end

				end

				self.ActionDelay = CurTime() + GetConVarNumber("VNT_SWep_Deployable_GiveDelay")

			end

		end

	end

end