
---------------------------------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------------------------
--	Half-Life 2: Expanded - Melee
---------------------------------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------------------------

--	Catch-22 Diabolis Ex Machina Prevention

if SERVER then
	resource.AddWorkshop("505106454") 	--	V92 Code Bases
end

-- SOUND GUIDE
----------
-- Channel = Typically CHAN_WEAPON or CHAN_ITEM for gunshots and gun parts respectively; CHAN_BODY for foley
-- Volume = Leave at 1.0
-- Pitch = Typically between 95 and 105 for gunshots
-- Level = For gunshots, follow this estimation table as a LOOSE guideline:
----------
--	.22LR = 40dB
--	9x19mm = 60dB
--	.38 Special = 70dB
--	5.56x45mm = 70dB
--	7.62x51mm = 90dB
--	.45 ACP = 90dB
--	.50 AE = 95dB
--	.50 BMG = 100dB
--	12-Gauge Buckshot = 120dB
--	Explosion = 200dB
--	Suppressed = 2/3 of normal
--	Empty = 60dB 
--	Weapon sounds = ~50dB (use your head)
--	Foley = ~30dB (use your head)
----------
----------

if not VNTCB then
	Error( "V92 Content Bases not mounted :: Removing Autorun File :: HL2X Melee\n" )

	return false
end

if VNTCB.Plugins then
	table.insert( VNTCB.Plugins, 0, "VNT :: HL2X Melee Pack" )
end

------------------------------------------------------------------------------------------
--	HL2X :: Melee
------------------------------------------------------------------------------------------

---------------------------------------------
--	SOG Fasthawk
---------------------------------------------

sound.Add( {
	name = "HL2_Fasthawk.Draw" ,
	channel = CHAN_BODY ,
	volume = 1.0 ,
	level = 35 ,
	pitch = { 95 , 105 } ,
	sound = "jessev92/weapons/univ/draw1.wav"
} )

util.PrecacheSound( "jessev92/weapons/univ/draw1.wav" )

sound.Add( {
	name = "HL2_Fasthawk.Holster" ,
	channel = CHAN_BODY ,
	volume = 1.0 ,
	level = 35 ,
	pitch = { 95 , 105 } ,
	sound = "jessev92/weapons/univ/holster1.wav"
} )

util.PrecacheSound( "jessev92/weapons/univ/holster1.wav" )

sound.Add( {
	name = "HL2_Fasthawk.Foley" ,
	channel = CHAN_BODY ,
	volume = 1.0 ,
	level = 35 ,
	pitch = { 95 , 105 } ,
	sound = "jessev92/weapons/univ/quickmove1.wav"
} )
util.PrecacheSound( "jessev92/weapons/univ/quickmove1.wav" )

---------------------------------------------
--	Colt Tactical Knife
---------------------------------------------

sound.Add( {
	name = "HL2_ColtKnife.Draw" ,
	channel = CHAN_BODY ,
	volume = 1.0 ,
	level = 25 ,
	pitch = { 95 , 105 } ,
	sound = { "jessev92/weapons/knife_smodt/deploy.wav" }
} )
util.PrecacheSound( "jessev92/weapons/univ/draw1.wav" )

sound.Add( {
	name = "HL2_ColtKnife.Holster" ,
	channel = CHAN_BODY ,
	volume = 1.0 ,
	level = 25 ,
	pitch = { 95 , 105 } ,
	sound = "jessev92/weapons/univ/holster1.wav"
} )
util.PrecacheSound( "jessev92/weapons/univ/holster1.wav" )

sound.Add( {
	name = "HL2_ColtKnife.Foley" ,
	channel = CHAN_BODY ,
	volume = 1.0 ,
	level = 40 ,
	pitch = { 95 , 105 } ,
	sound = "jessev92/weapons/univ/quickmove1.wav"
} )
util.PrecacheSound( "jessev92/weapons/univ/quickmove1.wav" )

---------------------------------------------
--	Harpoon
---------------------------------------------
sound.Add( {
	name = "HL2_Harpoon.Draw" ,
	channel = CHAN_BODY ,
	volume = 1.0 ,
	level = 45 ,
	pitch = { 95 , 105 } ,
	sound = "jessev92/weapons/univ/draw1.wav"
} )
util.PrecacheSound( "jessev92/weapons/univ/draw1.wav" )

sound.Add( {
	name = "HL2_Harpoon.Holster" ,
	channel = CHAN_BODY ,
	volume = 1.0 ,
	level = 45 ,
	pitch = { 95 , 105 } ,
	sound = "jessev92/weapons/univ/holster1.wav"
} )
util.PrecacheSound( "jessev92/weapons/univ/holster1.wav" )

sound.Add( {
	name = "HL2_Harpoon.Foley" ,
	channel = CHAN_BODY ,
	volume = 1.0 ,
	level = 45 ,
	pitch = { 95 , 105 } ,
	sound = "jessev92/weapons/univ/quickmove1.wav"
} )
util.PrecacheSound( "jessev92/weapons/univ/quickmove1.wav" )