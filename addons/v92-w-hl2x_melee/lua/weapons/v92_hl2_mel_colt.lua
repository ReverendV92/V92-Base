AddCSLuaFile( )

if not VNTCB then
	Error( "V92 Content Bases not mounted; Removing Weapon:\n" )


	return false
end

------------------------------------------------------
--	Half-Life 2: Extras								--
--	Colt Tactical Knife								--
------------------------------------------------------
SWEP.Base = VNT_BASE_WEAPON_MELEE -- (String) Weapon base parent this is a child of
SWEP.Spawnable = true -- (Boolean) Can be spawned via the menu
SWEP.AdminOnly = false -- (Boolean) Admin only spawnable
------------------------------------------------------
--	Client Information								--	Info used in the client block of the weapon
------------------------------------------------------
SWEP.WeaponName = "v92_hl2_mel_colt" -- (String) Name of the weapon script
SWEP.WeaponEntityName = SWEP.WeaponName .. "_ent" -- (String) Name of the weapon entity in Lua/Entities/Entityname.lua
SWEP.PrintName = "Tactical Knife" -- (String) Printed name on menu
SWEP.Manufacturer = VNT_WEAPON_MANUFACTURER_COLT -- (String) Gun company that makes this weapon
SWEP.CountryOfOrigin = VNT_WEAPON_COUNTRY_UNITEDSTATES -- (String) Country of origin
SWEP.Category = VNT_CATEGORY_HL2EXPANDED -- (String) Category
SWEP.Instructions = "Slice'n'dice!" -- (String) Instruction
SWEP.Author = VNTCB.author -- (String) Author
SWEP.Contact = VNTCB.contact -- (String) Contact
SWEP.Slot = VNT_WEAPON_BUCKETPOS_MELEE -- (Integer) Bucket to place weapon in, 1 to 6
SWEP.SlotPos = VNT_WEAPON_SLOTPOS_MELEE -- (Integer) Bucket position
------------------------------------------------------
--	Model Information								--
------------------------------------------------------
SWEP.ViewModelFlip = false -- (Boolean) Only used for vanilla CS:S models
SWEP.ViewModel = Model( "models/jessev92/hl2/weapons/coltknife_v.mdl" ) -- (String) View model - v_*
SWEP.WorldModel = Model( "models/jessev92/hl2/weapons/coltknife_w.mdl" ) -- (String) World model - w_*
SWEP.HoldType = "knife" -- (String) Hold type for our weapon, refer to wiki for animation sets
------------------------------------------------------
--	Gun Types										--	Set the type of weapon
------------------------------------------------------------------------------------------------------------
--	Primary Fire Settings							--	Settings for the primary fire of the weapon
------------------------------------------------------
SWEP.Primary.ClipSize = -1 -- (Integer) Size of a magazine
SWEP.Primary.DefaultClip = 0 -- (Integer) Default number of ammo you spawn with
SWEP.Primary.Ammo = "none" -- (String) Primary ammo used by the weapon, bullets probably
SWEP.Primary.RPM = 200 -- (Integer) Go to a wikipedia page and look at the RPM of the weapon, then put that here - the base will do the math
SWEP.Primary.PureDmg = 25 -- (Integer) Base damage, put one number here and the base will do the rest
------------------------------------------------------
--	Secondary Fire Settings							--	Settings for the Secondary fire of the weapon
------------------------------------------------------
SWEP.Secondary.ClipSize = -1 -- (Integer) Size of a magazine
SWEP.Secondary.DefaultClip = 0 -- (Integer) Default number of ammo you spawn with
SWEP.Secondary.Ammo = "none" -- (String) Secondary ammo used by the weapon, bullets probably
SWEP.Secondary.RPM = 50 -- (Integer) Go to a wikipedia page and look at the RPM of the weapon, then put that here - the base will do the math
SWEP.Secondary.PureDmg = 50 -- (Integer) Base damage, put one number here and the base will do the rest
------------------------------------------------------
--	Gun Mechanics									--	Various things to tweak the effects and feedback
------------------------------------------------------
SWEP.Weight = 1 -- (Integer) The weight in Kilogrammes of our weapon - used in my weapon weight mod!
SWEP.StoppageRate = 0 -- (Integer) Rate of stoppages in the weapon, look up the real world number estimations and just throw that in here.
------------------------------------------------------
--	Melee Settings									--
------------------------------------------------------
SWEP.MeleeAnimType = 0 -- (Integer) Melee type; 0=holdtype animation, 1=pistol whip, 2=rifle butt
SWEP.MeleeRange = 55 -- (Integer) Range of melee weapon swings
SWEP.HasMeleeAttack = false -- (Boolean) Does this weapon have a pistol whip or rifle butt animation?
SWEP.AltFireMelee = true -- (Boolean) Is the alt fire a melee attack?
SWEP.IsBladedMelee = true -- (Boolean) Is the melee a blade?
------------------------------------------------------
--	Ironsight & Run Positions						--	Set our model transforms for running and ironsights
------------------------------------------------------
SWEP.IronSightsPos = Vector( 0 , 0 , 0 ) -- (Vector) Ironsight XYZ Transform
SWEP.IronSightsAng = Vector( 0 , 0 , 0 ) -- (Vector) Ironsight XYZ Rotation
SWEP.RunArmOffset = Vector( -10 , -14 , -1.5 ) -- (Vector) Sprinting XYZ Transform
SWEP.RunArmAngle = Vector( -25 , 0 , -50 ) -- (Vector) Sprinting XYZ Rotation

------------------------------------------------------
--	Setup Clientside Info							--	This block must be in every weapon!
------------------------------------------------------
if CLIENT then
	SWEP.WepSelectIcon = surface.GetTextureID( "vgui/hud/" .. SWEP.WeaponName )
	SWEP.RenderGroup = RENDERGROUP_BOTH
	language.Add( SWEP.WeaponName , SWEP.PrintName )
	killicon.Add( SWEP.WeaponName , "vgui/entities/" .. SWEP.WeaponName , Color( 255 , 255 , 255 ) )
end
------------------------------------------------------
--	SWEP:Initialize() 							--	Called when the weapon is first loaded
------------------------------------------------------
function SWEP:Initialize( )
	self.HoldMeRight = VNTCB.HoldType.Knife -- (String) Hold type table for our weapon, Lua/autorun/sh_v92_base_swep.Lua
end

SWEP.SeqPrimary = { "misscenter1" }
SWEP.SeqIdle = { "idle01" }
SWEP.SeqDraw = { "draw" }
SWEP.SeqHolster = { "holster" }
SWEP.SeqHitCenter = { "hitcenter1" , "hitcenter2" , "hitcenter3" }
SWEP.SeqHitCenter2 = { "hitcenter1" , "hitcenter2" , "hitcenter3" }
SWEP.SeqMissCenter = { "misscenter1" , "misscenter2" }
SWEP.SeqMissCenter2 = { "misscenter1" , "misscenter2" }
