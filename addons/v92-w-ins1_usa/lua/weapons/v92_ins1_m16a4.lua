
------------------------------------------------------
--	Insurgency: Modern Infantry Combat
--	M16A4
--	5.56x45mm NATO assault rifle
------------------------------------------------------

AddCSLuaFile()

SWEP.PrintName = "M16A4" -- (String) Printed name on menu

if not VNTCB then
	Error( "V92 Content Bases not mounted; Removing Weapon: " .. SWEP.PrintName .. "\n" )
	return false
end

SWEP.Base = VNTCB.Bases.Wep -- (String) Weapon base parent this is a child of
SWEP.Spawnable = true -- (Boolean) Can be spawned via the menu
SWEP.AdminOnly = false -- (Boolean) Admin only spawnable

------------------------------------------------------
--	Client Information
------------------------------------------------------

SWEP.WeaponName	= "v92_ins1_m16a4" -- (String) Name of the weapon script
SWEP.WeaponEntityName = SWEP.WeaponName .. "_ent" -- (String) Name of the weapon entity in Lua/Entities/Entityname.lua
SWEP.Manufacturer = VNTCB.Manufacturer.CLT -- (String) Gun company that makes this weapon
SWEP.CountryOfOrigin = VNTCB.Country.USA -- (String) Country of origin
SWEP.MagazineName = VNTCB.Magazine.mSTANAG -- (String) The name of the magazine the weapon uses - used in my Weapon Magazine System
SWEP.Category = VNTCB.Category.INS1 -- (String) Category
SWEP.Instructions = VNTCB.instructions -- (String) Instruction
SWEP.Author = VNTCB.author -- (String) Author
SWEP.Contact = VNTCB.contact -- (String) Contact
SWEP.Slot = VNTCB.Bucket.Rifle -- (Integer) Bucket to place weapon in, 1 to 6
SWEP.SlotPos = VNTCB.Slot.Rifle -- (Integer) Bucket position
SWEP.ViewModelFOV = 60 -- (Integer) First-person field of view
SWEP.WorkshopID = "" -- (Integer) Workshop ID number of the upload that contains this file.

------------------------------------------------------
--	Model Information
------------------------------------------------------

SWEP.ViewModelFlip = false -- (Boolean) Only used for vanilla CS:S models
SWEP.ViewModel = Model("models/weapons/ins_m16a4_v.mdl") -- (String) View model - v_*
SWEP.WorldModel = Model("models/weapons/ins_m16a4_w.mdl") -- (String) World model - w_*
SWEP.HoldType = "ar2" -- (String) Hold type for our weapon, refer to wiki for animation sets

------------------------------------------------------
--	Gun Types
------------------------------------------------------

-- SWEP.DeployableType = 0 -- (Integer) 0=None, 1=Grenade Launcher, 2=Bipod

------------------------------------------------------
--	Primary Fire Settings
------------------------------------------------------

SWEP.Primary.ClipSize = 30 -- (Integer) Size of a magazine
SWEP.Primary.DefaultClip = 30 -- (Integer) Default number of ammo you spawn with
SWEP.Primary.Ammo = "556x45mmnato" -- (String) Primary ammo used by the weapon, bullets probably
SWEP.Primary.PureDmg = VNTCB.Ammo.a556NATO[1] -- (Integer) Base damage, put one number here and the base will do the rest
SWEP.Primary.RPM = 950 -- (Integer) Go to a wikipedia page and look at the RPM of the weapon, then put that here - the base will do the math

------------------------------------------------------
--	Secondary Fire Settings
------------------------------------------------------

-- SWEP.Secondary.ClipSize = -1 -- (Integer) Size of a secondary magazine; if no alt-fire, set to -1
-- SWEP.Secondary.DefaultClip = 3 -- (Integer) Default number of projectiles in the alt fire mag; if none, set to -1
-- SWEP.Secondary.Ammo = "40x46mmgrenade" -- (String) Ammo used by the grenade launcher, if it doesn't have one, leave at "none"

------------------------------------------------------
--	Gun Mechanics
------------------------------------------------------

SWEP.FireMode = { true, true, true, false } -- (Table: Boolean, Boolean, Boolean, Boolean ) Enable different fire modes on the weapon; Has modes, Has Single, Has Burst, Has Auto - in that order. You can have more than one, but the first must be true
SWEP.CurrentMode = 1 -- (Integer) Current fire mode of the weapon; used to set the default mode; corresponds to the FireMode table
SWEP.Weight = 9	-- (Integer) The weight in Kilogrammes of our weapon - used in my weapon weight mod!
SWEP.CanFireUnderwater = false -- (Boolean) Can we shoot underwater?
SWEP.StrongPenetration = VNTCB.Ammo.a556NATO[2] -- (Integer) Max penetration
SWEP.WeakPenetration = VNTCB.Ammo.a556NATO[3] -- (Integer) Max wood penetration
SWEP.EffectiveRange = 200 -- (Integer) Effective range of the weapon in metres.
SWEP.Settings.Jamming.MeanRateOfFailure = 5000 -- (Integer) Rate of stoppages in the weapon, look up the real world number estimations and just throw that in here.
SWEP.Settings.FireModes.Burst.Count = 3 -- (Integer) Amounts of shots to be fired by burst
SWEP.BurstDelay = 0.05 -- (Float) Time between bolt cycles in the burst

------------------------------------------------------
--	Special FX
------------------------------------------------------

SWEP.MuzzleAttach = 1 -- (Integer) The number of the attachment point for muzzle flashes, typically "1"
SWEP.MuzzleFlashType = 6 -- (Integer) The number of the muzzle flash to use; see Lua/Effects/fx_muzzleflash.Lua
SWEP.ShellAttach = 2 -- (String) The name of the attachment point for shell ejections, typically "2" or "eject"
SWEP.ShellType = 14 -- (Integer) The shell to use, see Lua/Effects/FX_ShellEject for integers
SWEP.ShellDelay = 0 -- (Float) 	Time between shot firing and shell ejection; useful for bolt-actions and things like that that need a delay

------------------------------------------------------
--	Custom Sounds
------------------------------------------------------

SWEP.Sounds = {
	["Primary"] = Sound("HL2_OldSch_MP5Navy.Single") , --	(String) Primary shoot sound
	["Primary_Dry"] = Sound("VNTCB.SWep.Empty2") ,  -- (String) Primary dry fire sound
	["GrenadeLauncher"] = Sound("HL2_OldSch_MP5Navy.GL") ,  -- (String) Grenade launcher sound
	["Reload"] = Sound("HL2_OldSch_MP5Navy.Reload") , --	(String) Reload sound

	["Noise_Close"] = Sound( "BF3.BulletCraft.Noise.Forest.Close" ) , 
	["Noise_Distant"] = Sound( "BF3.BulletCraft.Noise.Forest.Distant" ) , 
	["Noise_Far"] = Sound( "BF3.BulletCraft.Noise.Forest.Far" ) , 
	["CoreBass_Close"] = Sound( "BF3.BulletCraft.CoreBass.Close.LMG_16" ) , 
	["CoreBass_Distant"] = Sound( "BF3.BulletCraft.CoreBass.Distant.LMG_13" ) , 
	["HiFi"] = Sound( "BF3.BulletCraft.HiFi.MP5" ) , 
	["Reflection"] = Sound( "BF3.BulletCraft.Reflection.Sniper" ) , 
}

SWEP.SelectorSwitchSNDType = 1 -- (Integer) 1=US , 2=RU
SWEP.UsesSuperSonicAmmo = false -- (Boolean) Is the weapon using supersonic or subsonic ammo?

------------------------------------------------------
--	Ironsight & Run Positions
------------------------------------------------------

SWEP.IronSightsPos = Vector (-3.13, -10, 2.15)
SWEP.IronSightsAng = Vector (-1, 0, 0)
SWEP.RunArmOffset = Vector (3, -3, 0)
SWEP.RunArmAngle = Vector (-13, 46, -26)

------------------------------------------------------
------------------------------------------------------
--	Setup Clientside Info
------------------------------------------------------
------------------------------------------------------

if CLIENT then
	SWEP.WepSelectIcon = surface.GetTextureID("vgui/hud/" .. SWEP.WeaponName )
	SWEP.RenderGroup = RENDERGROUP_BOTH
	language.Add( SWEP.WeaponName, SWEP.PrintName )
	killicon.Add( SWEP.WeaponName, "vgui/entities/".. SWEP.WeaponName , Color( 255, 255, 255 ) )
elseif SERVER then
	resource.AddWorkshop( SWEP.WorkshopID )
end

------------------------------------------------------
--	SWEP:Initialize()
------------------------------------------------------

function SWEP:Initialize()
	self.HoldMeRight = VNTCB.HoldType.Rifle -- (String) Hold type table for our weapon, Lua/autorun/sh_v92_base_swep.Lua
end

SWEP.Sequences.Draw = { "draw" }
SWEP.Sequences.Primary = { "fire01" , "fire02" , "fire03" , "fire04" }
SWEP.Sequences.Primary_Dry = { "dryfire" }
SWEP.Sequences.Idle = { "idle01" }
SWEP.Sequences.Reload = { "reload" }
SWEP.Sequences.LowIdle = { "lowidle" }
SWEP.Sequences.LowToIdle = { "lowtoidle" }
SWEP.Sequences.IdleToLow = { "idletolow" }
SWEP.Sequences.ReloadM203 = { "reload_gl" }
SWEP.Sequences.Secondary = { "altfire" }
