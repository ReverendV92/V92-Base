
AddCSLuaFile()
if not VNTCB then 
	Error("V92 Content Bases not mounted: Removing Weapon")
	return false 
end
------------------------------------------------------
--	Halo: Reach										--
--	Medical Kit										--
------------------------------------------------------
SWEP.Base 			= VNTBases.Wep					--	(String)	Weapon base parent this is a child of
SWEP.Spawnable 		= true							--	(Boolean)	Can be spawned via the menu
SWEP.AdminOnly 		= false							--	(Boolean)	Admin only spawnable
------------------------------------------------------
--	Client Information								--	Info used in the client block of the weapon
------------------------------------------------------
SWEP.WeaponName = "v92_reach_medkit" --	(String)	Name of the weapon script
SWEP.WeaponEntityName = SWEP.WeaponName .. "_ent"	--	(String)	Name of the weapon entity in Lua/Entities/Entityname.lua
SWEP.GrenadeLauncherEntity = "v92_reach_medkit_healer" --	(String)	Name of the grenade launcher shell entity in Lua/Entities/Entityname.lua
SWEP.GrenadeLauncherForce = 200 --	(Integer)	Force of grenade launchers and shell throwers like that.
SWEP.PrintName = "Medkit" --	(String)	Printed name on menu
SWEP.Manufacturer = VNTMaker.VAR --	(String)	Gun company that makes this weapon
SWEP.CountryOfOrigin = VNTCountry.VAR --	(String)	Country of origin
SWEP.MagazineName = "Medkit" --	(String)	The name of the magazine the weapon uses - used in my Weapon Magazine System
SWEP.Category = VNTCategory.Reach --	(String)	Category
SWEP.Instructions = "Uses medbag ammo" --	(String)	Instruction
SWEP.Purpose = VNTCB.purpose -- (String) Purpose of the weapon
SWEP.Author = VNTCB.author --	(String)	Author
SWEP.Contact = VNTCB.contact --	(String)	Contact
SWEP.Slot = VNTBucket.Support --	(Integer)	Bucket to place weapon in, 1 to 6
SWEP.SlotPos = VNTSlot.Support --	(Integer)	Bucket position
SWEP.ViewModelFOV = 70 --	(Integer)	First-person field of view
SWEP.WorkshopID = "503422951" --	(Integer)	Workshop ID number of the upload that contains this file.
------------------------------------------------------
--	Model Information								--
------------------------------------------------------
SWEP.ViewModelFlip = false --	(Boolean)	Only used for vanilla CS:S models
SWEP.ViewModel = Model( "models/jessev92/haloreach/weapons/medkit_c.mdl" ) --	(String)	View model - v_*
SWEP.WorldModel = Model( "models/jessev92/haloreach/weapons/medkit_w.mdl" ) --	(String)	World model - w_*
SWEP.UseHands = true --	(Boolean)	Leave at false unless the model uses C_Arms
SWEP.HoldType = "slam" --	(String)	Hold type for our weapon, refer to wiki for animation sets
------------------------------------------------------
--	Gun Types										--	Set the type of weapon - ONLY PICK ONE!
------------------------------------------------------
SWEP.WeaponType = VNTWType.GLauncher --	(Integer)	1=Melee, 2=Pistol, 3=Rifle, 4=Shotgun, 5=Sniper, 6=Grenade Launcher, 7=Rocket Launcher
------------------------------------------------------
--	Primary Fire Settings							--	Settings for the primary fire of the weapon
------------------------------------------------------
SWEP.Primary.ClipSize = -1 --	(Integer)	Size of a magazine
SWEP.Primary.DefaultClip = 1 --	(Integer)	Default number of ammo you spawn with
SWEP.Primary.Ammo = "medbag" --	(String)	Primary ammo used by the weapon, bullets probably
SWEP.Primary.Recoil = 1 --	(Float)		Recoil factor, Good ranges are from 0.5 to 5; this MULTIPLIES with the CVar!
SWEP.Primary.RPM = 6 --	(Integer)	Go to a wikipedia page and look at the RPM of the weapon, then put that here - the base will do the math
SWEP.Primary.Cone = 0.005 --	(Float)		Accuracy of the weapon; 0.004 is the average for rifles
SWEP.Primary.Delay = ( 60 / SWEP.Primary.RPM )
SWEP.Primary.PureDmg = VNTAmmo.aMedBag[1] --	(Integer)	Base damage, put one number here and the base will do the rest
SWEP.Primary.Damage = math.random( SWEP.Primary.PureDmg - 3 , SWEP.Primary.PureDmg + 3 )
SWEP.CanChamber = true --	(Boolean)	Can we load a round into the chamber?
------------------------------------------------------
--	Gun Mechanics									--	Various things to tweak the effects and feedback
------------------------------------------------------
SWEP.FireMode = { false, true, false, false } --	(Table: Boolean, Boolean, Boolean, Boolean ) Enable different fire modes on the weapon; Has modes, Has Single, Has Burst, Has Auto - in that order. You can have more than one, but the first must be true
SWEP.CurrentMode = 1 --	(Integer)	Current fire mode of the weapon; used to set the default mode; corresponds to the FireMode table
SWEP.Weight = 2 --	(Integer)	The weight in Kilogrammes of our weapon - used in my weapon weight mod!
SWEP.MaxPenetration = VNTAmmo.aMedBag[2] --	(Integer)	Max penetration
SWEP.MaxWoodPenetration = VNTAmmo.aMedBag[3] --	(Integer)	Max wood penetration
------------------------------------------------------
--	Melee Settings									--
------------------------------------------------------
SWEP.MeleeAnimType = 2 --	(Integer)	Melee type; 0=holdtype animation, 1=pistol whip, 2=rifle butt
SWEP.MeleeRange = 70 --	(Integer)	Range of melee weapon swings
SWEP.HasMeleeAttack = true -- (Boolean) Does this weapon have a pistol whip or rifle butt animation?
SWEP.AltFireMelee = true -- (Boolean) Is the alt fire a melee attack?
------------------------------------------------------
--	Special FX										--	Muzzle flashes, shell casings, etc
------------------------------------------------------
SWEP.ShellEffect = VNTAmmo.aMedBag[4] --	(String)	Name of the file in Lua/effects/shelleject.lua to use for shell ejections; leave blank for none
------------------------------------------------------
--	Custom Sounds									--		Setup sounds here!
------------------------------------------------------
SWEP.SND_PrimaryFire = Sound("common/null.wav") --	(String)	Primary shoot sound
SWEP.SND_PrimaryDryFire = Sound("common/null.wav") --	(String)	Primary dry fire sound
SWEP.SND_Reload = Sound("common/null.wav") --	(String)	Reload sound
SWEP.SND_PistolWhipHit = Sound( "REACH.Medkit.Melee" ) --	(String)	Sound for pistol whip hits
SWEP.SND_PistolWhipMiss = Sound( "REACH.Medkit.MeleeMiss" ) --	(String)	Sound for pistol whip misses
------------------------------------------------------
--	Ironsight & Run Positions						--	Set our model transforms for running and ironsights
------------------------------------------------------
SWEP.IronSightsPos = Vector (-6.98,12, 3.085) --	(Vector)	Ironsight XYZ Transform
SWEP.IronSightsAng = Vector (0, 0, 0) --	(Vector)	Ironsight XYZ Rotation
SWEP.RunArmOffset = Vector (1, -6, 0) --	(Vector)	Sprinting XYZ Transform
SWEP.RunArmAngle = Vector (-7, 22, 0) --	(Vector)	Sprinting XYZ Rotation
------------------------------------------------------
--	Setup Clientside Info							--	This block must be in every weapon!
------------------------------------------------------
if CLIENT then
	SWEP.CSMuzzleFlashes	= false
	SWEP.WepSelectIcon 		= surface.GetTextureID("vgui/hud/" .. SWEP.WeaponName )
	SWEP.RenderGroup 		= RENDERGROUP_BOTH
	language.Add( SWEP.WeaponName, SWEP.PrintName )
	killicon.Add( SWEP.WeaponName, "vgui/entities/".. SWEP.WeaponName , Color( 255, 255, 255 ) )
elseif SERVER then
	resource.AddWorkshop( SWEP.WorkshopID )
end

------------------------------------------------------
--	SWEP:Initialize()								--	Called when the weapon is first loaded
------------------------------------------------------
function SWEP:Initialize()
	--self:SetWeaponHoldType(self.HoldType)
end

function SWEP:CustomThink()
if not IsFirstTimePredicted() then return end
	local vm = self.Owner:GetViewModel()
	if self.Owner:KeyPressed( IN_ATTACK ) and	self.ActionDelay <= CurTime() and self:Ammo1() > 0 then
		if	self == nil or self.Owner:GetEyeTrace().HitPos:Distance(self.Owner:GetShootPos()) <= 30 then	return	end
		self:SendWeaponAnim( ACT_VM_HITCENTER2 )
		self.Owner:SetAnimation( PLAYER_ATTACK1 )
		if SERVER then
			local _ENT = ents.Create( self.GrenadeLauncherEntity )
			_ENT:SetPos(self.Owner:EyePos() + self.Owner:GetAimVector() * 16 + self.Owner:GetRight() * 2 + self.Owner:GetUp() * -3)
			_ENT:SetAngles(self.Owner:EyeAngles() - Angle(45, 0, 0))
			_ENT:SetOwner(self.Owner)
			_ENT:Spawn()
			
			if _ENT:GetPhysicsObject():IsValid() then
				_ENT:GetPhysicsObject():SetMass(5)
				_ENT:GetPhysicsObject():SetVelocity(self.Owner:GetAimVector() * self.GrenadeLauncherForce)
			end
			
			SafeRemoveEntity(self.Owner.CurMedkitEnt)
			self.Owner.CurMedkitEnt = _ENT
		
			timer.Simple(1, function()
				if _ENT == NULL then	return	end				
				_ENT:SetOwner(NULL)
			end)
		end
		self.ActionDelay = CurTime() + GetConVarNumber("VNT_SWep_MedicBag_PlaceDelay")
		self:TakePrimaryAmmo(1)
	elseif self.Owner:KeyPressed( IN_ATTACK ) and self.ActionDelay <= CurTime() and self:Ammo1() <= 0 then
		self.ActionDelay = CurTime() + GetConVarNumber("VNT_SWep_MedicBag_PlaceDelay")
		self.Owner:ChatPrint( "Out of Medic Kits!" )
	end
end