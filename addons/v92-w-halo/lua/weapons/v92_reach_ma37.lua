AddCSLuaFile( )

if not VNTCB then
	Error( "V92 Content Bases not mounted: Removing Weapon" )

	return false
end

------------------------------------------------------
--	Halo: Reach										--
--	MA37 Assault Rifle								--
------------------------------------------------------
SWEP.Base = VNTBases.Wep --	(String)	Weapon base parent this is a child of
SWEP.Spawnable = true --	(Boolean)	Can be spawned via the menu
SWEP.AdminOnly = false --	(Boolean)	Admin only spawnable
------------------------------------------------------
--	Client Information								--	Info used in the client block of the weapon
------------------------------------------------------
SWEP.WeaponName = "v92_reach_ma37" --	(String)	Name of the weapon script
SWEP.WeaponEntityName = SWEP.WeaponName .. "_ent" --	(String)	Name of the weapon entity in Lua/Entities/Entityname.lua
SWEP.PrintName = "MA37" --	(String)	Printed name on menu
SWEP.Manufacturer = VNTMaker.MIS --	(String)	Gun company that makes this weapon
SWEP.CountryOfOrigin = VNTCountry.MAR --	(String)	Country of origin
SWEP.MagazineName = "MA5 Magazine" --	(String)	The name of the magazine the weapon uses - used in my Weapon Magazine System
SWEP.Category = VNTCategory.Reach --	(String)	Category
SWEP.Instructions = "Uses 7.62 NATO ammo and MA5 magazines" --	(String)	Instruction
SWEP.Purpose = VNTCB.purpose -- (String) Purpose of the weapon
SWEP.Author = VNTCB.author --	(String)	Author
SWEP.Contact = VNTCB.contact --	(String)	Contact
SWEP.Slot = VNTBucket.Rifle --	(Integer)	Bucket to place weapon in, 1 to 6
SWEP.SlotPos = VNTSlot.Rifle --	(Integer)	Bucket position
SWEP.ViewModelFOV = 54 --	(Integer)	First-person field of view
SWEP.WorkshopID = "503422951" --	(Integer)	Workshop ID number of the upload that contains this file.
------------------------------------------------------
--	Model Information								--
------------------------------------------------------
SWEP.ViewModelFlip = false --	(Boolean)	Only used for vanilla CS:S models
SWEP.ViewModel = Model( "models/jessev92/haloreach/weapons/unsc/ma37_v.mdl" ) --	(String)	View model - v_*
SWEP.WorldModel = Model( "models/jessev92/haloreach/weapons/unsc/ma37_w.mdl" ) --	(String)	World model - w_*
SWEP.UseHands = false --	(Boolean)	Leave at false unless the model uses C_Arms
SWEP.HoldType = "ar2" --	(String)	Hold type for our weapon, refer to wiki for animation sets
------------------------------------------------------
--	Gun Types										--	Set the type of weapon - ONLY PICK ONE!
------------------------------------------------------
SWEP.WeaponType = VNTWType.Rifle --	(Integer)	1=Melee, 2=Pistol, 3=Rifle, 4=Shotgun, 5=Sniper, 6=Grenade Launcher, 7=Rocket Launcher
------------------------------------------------------
--	Primary Fire Settings							--	Settings for the primary fire of the weapon
------------------------------------------------------
SWEP.Primary.ClipSize = 32 --	(Integer)	Size of a magazine
SWEP.Primary.DefaultClip = 32 --	(Integer)	Default number of ammo you spawn with
SWEP.Primary.Ammo = "762x51mmnato" --	(String)	Primary ammo used by the weapon, bullets probably
SWEP.Primary.Recoil = 1 --	(Float)		Recoil factor, Good ranges are from 0.5 to 5; this MULTIPLIES with the CVar!
SWEP.Primary.Cone = 0.005 --	(Float)		Accuracy of the weapon; 0.004 is the average for rifles
SWEP.Primary.RPM = 700 --	(Integer)	Go to a wikipedia page and look at the RPM of the weapon, then put that here - the base will do the math
SWEP.Primary.Delay = ( 60 / SWEP.Primary.RPM )
SWEP.Primary.PureDmg = VNTAmmo.a762NATO[ 1 ] --	(Integer)	Base damage, put one number here and the base will do the rest
SWEP.Primary.Damage = math.random( SWEP.Primary.PureDmg - 3 , SWEP.Primary.PureDmg + 3 )
------------------------------------------------------
--	Gun Mechanics									--	Various things to tweak the effects and feedback
------------------------------------------------------
SWEP.FireMode = { true , true , false , true } --	(Table: Boolean, Boolean, Boolean, Boolean ) Enable different fire modes on the weapon; Has modes, Has Single, Has Burst, Has Auto - in that order. You can have more than one, but the first must be true
SWEP.CurrentMode = 1 --	(Integer)	Current fire mode of the weapon; used to set the default mode; corresponds to the FireMode table
SWEP.Weight = 5 --	(Integer)	The weight in Kilogrammes of our weapon - used in my weapon weight mod!
SWEP.MaxPenetration = VNTAmmo.a762NATO[ 2 ] --	(Integer)	Max penetration
SWEP.MaxWoodPenetration = VNTAmmo.a762NATO[ 3 ] --	(Integer)	Max wood penetration
------------------------------------------------------
--	Special FX										--	Muzzle flashes, shell casings, etc
------------------------------------------------------
SWEP.MuzzleAttach = 2 --	(Integer)	The number of the attachment point for muzzle flashes, typically "1"
SWEP.ShellAttach = 1 --	(Integer)	The number of the attachment point for shell ejections, typically "2"
SWEP.ShellEffect = VNTAmmo.a762NATO[ 4 ] --	(String)	Name of the file in Lua/effects/shelleject.lua to use for shell ejections; leave blank for none
------------------------------------------------------
--	Custom Sounds									--		Setup sounds here!
------------------------------------------------------
SWEP.SND_PrimaryFire = Sound( "REACH.MA37.Single" ) --	(String)	Primary shoot sound
SWEP.SND_PrimaryDryFire = Sound( "REACH.MA37.Dry" ) --	(String)	Primary dry fire sound
SWEP.SND_Reload = Sound( "REACH.MA37.Deploy" ) --	(String)	Reload sound
------------------------------------------------------
--	Ironsight & Run Positions						--	Set our model transforms for running and ironsights
------------------------------------------------------
SWEP.IronSightsPos = Vector( -3.29 , -5 , -0.3 ) --	(Vector)	Ironsight XYZ Transform
SWEP.IronSightsAng = Vector( 0 , 0 , 0 ) --	(Vector)	Ironsight XYZ Rotation
SWEP.RunArmOffset = Vector( 4.0928 , 0.4246 , 2.3712 ) --	(Vector)	Sprinting XYZ Transform
SWEP.RunArmAngle = Vector( -18.4406 , 33.1846 , 0 ) --	(Vector)	Sprinting XYZ Rotation

------------------------------------------------------
--	Setup Clientside Info							--	This block must be in every weapon!
------------------------------------------------------
if CLIENT then
	SWEP.Category = SWEP.Category
	SWEP.PrintName = SWEP.PrintName
	SWEP.Author = SWEP.Author
	SWEP.Contact = SWEP.Contact
	SWEP.Instructions = SWEP.Instructions
	SWEP.Slot = SWEP.Slot - 1
	SWEP.SlotPos = SWEP.SlotPos
	SWEP.ViewModelFOV = SWEP.ViewModelFOV
	SWEP.ViewModelFlip = SWEP.ViewModelFlip
	SWEP.DrawCrosshair = true
	SWEP.AccurateCrosshair = true
	SWEP.CSMuzzleFlashes = true
	SWEP.WepSelectIcon = surface.GetTextureID( "vgui/hud/" .. SWEP.WeaponName )
	SWEP.SwayScale = SWEP.SwayScale
	SWEP.BobScale = SWEP.BobScale
	SWEP.RenderGroup = RENDERGROUP_BOTH
	language.Add( SWEP.WeaponName , SWEP.PrintName )
	killicon.Add( SWEP.WeaponName , "vgui/entities/" .. SWEP.WeaponName , Color( 255 , 255 , 255 ) )
elseif SERVER then
	resource.AddWorkshop( SWEP.WorkshopID )
end

------------------------------------------------------
--	SWEP:Initialize()								--	Called when the weapon is first loaded
------------------------------------------------------
function SWEP:Initialize( )
--self:SetWeaponHoldType(self.HoldType)
end

function SWEP:CustomThink( )

	local ViewModel = self.Owner:GetViewModel( )

	local compassAng = (self.Owner:GetAimVector():Angle().y)
	if compassAng > 22 and compassAng < 68		then 
		ViewModel:SetSubMaterial( 15, "models/jessev92/haloreach/weapons/common/compass_nw" )
	elseif compassAng > 67 and compassAng < 113 	then 
		ViewModel:SetSubMaterial( 15, "models/jessev92/haloreach/weapons/common/compass_w" )
	elseif compassAng > 112 and compassAng < 157 	then 
		ViewModel:SetSubMaterial( 15, "models/jessev92/haloreach/weapons/common/compass_sw" )
	elseif compassAng > 156 and compassAng < 202	then 
		ViewModel:SetSubMaterial( 15, "models/jessev92/haloreach/weapons/common/compass_s" )
	elseif compassAng > 201 and compassAng < 246	then 
		ViewModel:SetSubMaterial( 15, "models/jessev92/haloreach/weapons/common/compass_se" )
	elseif compassAng > 245 and compassAng < 291	then 
		ViewModel:SetSubMaterial( 15, "models/jessev92/haloreach/weapons/common/compass_e" )
	elseif compassAng > 290 and compassAng < 336	then 
		ViewModel:SetSubMaterial( 15, "models/jessev92/haloreach/weapons/common/compass_ne" )
	else
		ViewModel:SetSubMaterial( 15, "models/jessev92/haloreach/weapons/common/compass_n" ) 
	end
	
	if CLIENT then
		if self:IsCarriedByLocalPlayer() == false then
			ViewModel:SetSubMaterial( 15, "" )
		end
	end	

	local ammoString = tostring( self:Clip1() )
	local ammoOnes = string.Right( ammoString, 1 )
	local ammoTens = string.Left( ammoString, 1 )
	
	if self:Clip1() < 10 then
		ammoTens = "0"
	end
	
	ViewModel:SetBodygroup( 6 , tonumber( ammoTens ) )
	ViewModel:SetBodygroup( 5 , tonumber( ammoOnes ) )
	
	--[[
	if self.Weapon:Clip1( ) == 40 then
		ViewModel:SetBodygroup( 6 , 4 )
		ViewModel:SetBodygroup( 5 , 0 )
	elseif self.Weapon:Clip1( ) == 39 then
		ViewModel:SetBodygroup( 6 , 3 )
		ViewModel:SetBodygroup( 5 , 9 )
	elseif self.Weapon:Clip1( ) == 38 then
		ViewModel:SetBodygroup( 6 , 3 )
		ViewModel:SetBodygroup( 5 , 8 )
	elseif self.Weapon:Clip1( ) == 37 then
		ViewModel:SetBodygroup( 6 , 3 )
		ViewModel:SetBodygroup( 5 , 7 )
	elseif self.Weapon:Clip1( ) == 36 then
		ViewModel:SetBodygroup( 6 , 3 )
		ViewModel:SetBodygroup( 5 , 6 )
	elseif self.Weapon:Clip1( ) == 35 then
		ViewModel:SetBodygroup( 6 , 3 )
		ViewModel:SetBodygroup( 5 , 5 )
	elseif self.Weapon:Clip1( ) == 34 then
		ViewModel:SetBodygroup( 6 , 3 )
		ViewModel:SetBodygroup( 5 , 4 )
	elseif self.Weapon:Clip1( ) == 33 then
		ViewModel:SetBodygroup( 6 , 3 )
		ViewModel:SetBodygroup( 5 , 3 )
	elseif self.Weapon:Clip1( ) == 32 then
		ViewModel:SetBodygroup( 6 , 3 )
		ViewModel:SetBodygroup( 5 , 2 )
	elseif self.Weapon:Clip1( ) == 31 then
		ViewModel:SetBodygroup( 6 , 3 )
		ViewModel:SetBodygroup( 5 , 1 )
	elseif self.Weapon:Clip1( ) == 30 then
		ViewModel:SetBodygroup( 6 , 3 )
		ViewModel:SetBodygroup( 5 , 0 )
	elseif self.Weapon:Clip1( ) == 29 then
		ViewModel:SetBodygroup( 6 , 2 )
		ViewModel:SetBodygroup( 5 , 8 )
	elseif self.Weapon:Clip1( ) == 28 then
		ViewModel:SetBodygroup( 6 , 2 )
		ViewModel:SetBodygroup( 5 , 8 )
	elseif self.Weapon:Clip1( ) == 27 then
		ViewModel:SetBodygroup( 6 , 2 )
		ViewModel:SetBodygroup( 5 , 7 )
	elseif self.Weapon:Clip1( ) == 26 then
		ViewModel:SetBodygroup( 6 , 2 )
		ViewModel:SetBodygroup( 5 , 6 )
	elseif self.Weapon:Clip1( ) == 25 then
		ViewModel:SetBodygroup( 6 , 2 )
		ViewModel:SetBodygroup( 5 , 5 )
	elseif self.Weapon:Clip1( ) == 24 then
		ViewModel:SetBodygroup( 6 , 2 )
		ViewModel:SetBodygroup( 5 , 4 )
	elseif self.Weapon:Clip1( ) == 23 then
		ViewModel:SetBodygroup( 6 , 2 )
		ViewModel:SetBodygroup( 5 , 3 )
	elseif self.Weapon:Clip1( ) == 22 then
		ViewModel:SetBodygroup( 6 , 2 )
		ViewModel:SetBodygroup( 5 , 2 )
	elseif self.Weapon:Clip1( ) == 21 then
		ViewModel:SetBodygroup( 6 , 2 )
		ViewModel:SetBodygroup( 5 , 1 )
	elseif self.Weapon:Clip1( ) == 20 then
		ViewModel:SetBodygroup( 6 , 2 )
		ViewModel:SetBodygroup( 5 , 0 )
	elseif self.Weapon:Clip1( ) == 19 then
		ViewModel:SetBodygroup( 6 , 1 )
		ViewModel:SetBodygroup( 5 , 9 )
	elseif self.Weapon:Clip1( ) == 18 then
		ViewModel:SetBodygroup( 6 , 1 )
		ViewModel:SetBodygroup( 5 , 8 )
	elseif self.Weapon:Clip1( ) == 17 then
		ViewModel:SetBodygroup( 6 , 1 )
		ViewModel:SetBodygroup( 5 , 7 )
	elseif self.Weapon:Clip1( ) == 16 then
		ViewModel:SetBodygroup( 6 , 1 )
		ViewModel:SetBodygroup( 5 , 6 )
	elseif self.Weapon:Clip1( ) == 15 then
		ViewModel:SetBodygroup( 6 , 1 )
		ViewModel:SetBodygroup( 5 , 5 )
	elseif self.Weapon:Clip1( ) == 14 then
		ViewModel:SetBodygroup( 6 , 1 )
		ViewModel:SetBodygroup( 5 , 4 )
	elseif self.Weapon:Clip1( ) == 13 then
		ViewModel:SetBodygroup( 6 , 1 )
		ViewModel:SetBodygroup( 5 , 3 )
	elseif self.Weapon:Clip1( ) == 12 then
		ViewModel:SetBodygroup( 6 , 1 )
		ViewModel:SetBodygroup( 5 , 2 )
	elseif self.Weapon:Clip1( ) == 11 then
		ViewModel:SetBodygroup( 6 , 1 )
		ViewModel:SetBodygroup( 5 , 1 )
	elseif self.Weapon:Clip1( ) == 10 then
		ViewModel:SetBodygroup( 6 , 1 )
		ViewModel:SetBodygroup( 5 , 0 )
	elseif self.Weapon:Clip1( ) == 9 then
		ViewModel:SetBodygroup( 6 , 0 )
		ViewModel:SetBodygroup( 5 , 9 )
	elseif self.Weapon:Clip1( ) == 8 then
		ViewModel:SetBodygroup( 6 , 0 )
		ViewModel:SetBodygroup( 5 , 8 )
	elseif self.Weapon:Clip1( ) == 7 then
		ViewModel:SetBodygroup( 6 , 0 )
		ViewModel:SetBodygroup( 5 , 7 )
	elseif self.Weapon:Clip1( ) == 6 then
		ViewModel:SetBodygroup( 6 , 0 )
		ViewModel:SetBodygroup( 5 , 6 )
	elseif self.Weapon:Clip1( ) == 5 then
		ViewModel:SetBodygroup( 6 , 0 )
		ViewModel:SetBodygroup( 5 , 5 )
	elseif self.Weapon:Clip1( ) == 4 then
		ViewModel:SetBodygroup( 6 , 0 )
		ViewModel:SetBodygroup( 5 , 4 )
	elseif self.Weapon:Clip1( ) == 3 then
		ViewModel:SetBodygroup( 6 , 0 )
		ViewModel:SetBodygroup( 5 , 3 )
	elseif self.Weapon:Clip1( ) == 2 then
		ViewModel:SetBodygroup( 6 , 0 )
		ViewModel:SetBodygroup( 5 , 2 )
	elseif self.Weapon:Clip1( ) == 1 then
		ViewModel:SetBodygroup( 6 , 0 )
		ViewModel:SetBodygroup( 5 , 1 )
	else
		ViewModel:SetBodygroup( 6 , 0 )
		ViewModel:SetBodygroup( 5 , 0 )
	end
	--]]
end