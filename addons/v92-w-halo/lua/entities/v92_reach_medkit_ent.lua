
AddCSLuaFile()
------------------------------------------------------
if not VNTCB then return false end					--	Prevent this file from loading if for some odd reason the base Lua isn't loaded
------------------------------------------------------
ENT.Base				= VNTBases.WepEnt			--	Weapon Entity Base
ENT.Type				= "anim"					--	Type
ENT.PrintName			= "Medkit"					--	Printed Name
ENT.Author				= VNTCB.author				--	Author
ENT.Information			= "Uses Medbags"	--	Info like ammo and magazine
ENT.Category			= VNTCategory.Reach			--	Category
ENT.Spawnable			= true						--	Spawnable?
ENT.AdminOnly			= false						--	Admin Only?
--	(String)	Name of the weapon entity in Lua/weapons/swep_name.lua
ENT.SWEPNAME			= "v92_reach_medkit"			
--	(String)	Name of this entity				
ENT.WeaponName			= ENT.SWEPNAME .. "_ent"
--	(String)	Model to use
ENT.SENTMODEL			= Model( "models/jessev92/haloreach/weapons/medkit_w.mdl" )
