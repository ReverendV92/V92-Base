
AddCSLuaFile()
------------------------------------------------------
if not VNTCB then return false end					--	Prevent this file from loading if for some odd reason the base Lua isn't loaded
------------------------------------------------------
ENT.Base				= VNTBases.WepEnt			--	Weapon Entity Base
ENT.Type				= "anim"					--	Type
ENT.PrintName			= "MA37"					--	Printed Name
ENT.Author				= VNTCB.author				--	Author
ENT.Information			= "Uses 7.62 NATO ammo and MA5 magazines"	--	Info like ammo and magazine
ENT.Category			= VNTCategory.Reach			--	Category
ENT.Spawnable			= true						--	Spawnable?
ENT.AdminOnly			= false						--	Admin Only?
ENT.SWEPNAME			= "v92_reach_ma37" --	(String)	Name of the weapon entity in Lua/weapons/swep_name.lua
ENT.WeaponName			= ENT.SWEPNAME .. "_ent" --	(String)	Name of this entity	
ENT.SENTMODEL			= Model( "models/jessev92/haloreach/weapons/unsc/ma37_w.mdl" ) --	(String)	Model to use
