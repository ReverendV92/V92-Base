
AddCSLuaFile()

ENT.Type = "anim"
ENT.Base = "base_gmodentity"
ENT.PrintName = "Medkit"
ENT.Author = "-"
ENT.Spawnable = false
ENT.AdminOnly = false 

if CLIENT then

	function ENT:Initialize()
	end

	function ENT:Draw()
		self.Entity:DrawModel()
	end

	function ENT:Think()
	end 

elseif SERVER then

	function ENT:Initialize()
		self.Entity:SetModel("models/JesseV92/haloreach/weapons/medkit_w.mdl") 
		self.Entity:PhysicsInit(SOLID_VPHYSICS)
		self.Entity:SetMoveType(MOVETYPE_VPHYSICS)
		self.Entity:SetSolid(SOLID_VPHYSICS)
		self.Entity:SetCollisionGroup(COLLISION_GROUP_DEBRIS)
		self.Entity.CanHurt = true
		local phys = self.Entity:GetPhysicsObject()

		if phys and phys:IsValid() then
			phys:Wake()
		end
		
		self.Entity.HealthCharge = 250
		self.Entity.HealDelay = CurTime()
		timer.Simple(300, function()
			SafeRemoveEntity(self)
		end)
	end

	function ENT:Use()
	return false
	end

	function ENT:OnTakeDamage(dmginfo)
		self.Entity:GetPhysicsObject():AddVelocity(dmginfo:GetDamageForce() * 0.1)
	end

	function ENT:Think()
		if SERVER then
			if CurTime() > self.Entity.HealDelay then
				for k, v in pairs(ents.FindInSphere(self.Entity:GetPos(), 150)) do
					if v:IsPlayer() and v:Alive() then
						if self.Entity.HealthCharge > 0 then
							if v:Health() < v:GetMaxHealth() then
								v:SetHealth(v:Health() + 1)
								v:EmitSound("jessev92/bf2/items/pick_up_medipack.wav", 60, 100)
								self.Entity.HealthCharge = self.Entity.HealthCharge - 1
							end
						else
							SafeRemoveEntity(self.Entity)
						end
					end
				end
				self.Entity.HealDelay = CurTime() + 0.01
			end
		end
	end

	function ENT:OnRemove()
	return false
	end 

end