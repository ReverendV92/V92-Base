
------------------------------------------------------
--	Half-Life 2: Expanded							--
--	Franci SPAS-12 (Stockless) 						--
--	.12 Gauge Pump-Action shotgun					--
------------------------------------------------------

AddCSLuaFile( )

SWEP.PrintName = "SPAS-12 (Stockless)" -- (String) Printed name on menu

if not VNTCB then
	Error( "V92 Content Bases not mounted; Removing Weapon: " .. SWEP.PrintName .. "\n" )
	return false
end

SWEP.Base = VNTCB.Bases.WepShell -- (String) Weapon base parent this is a child of
SWEP.Spawnable = true -- (Boolean) Can be spawned via the menu
SWEP.AdminOnly = false -- (Boolean) Admin only spawnable

------------------------------------------------------
--	Client Information								--	Info used in the client block of the weapon
------------------------------------------------------

SWEP.WeaponName = "v92_hl2_sg_spas12_sless" -- (String) Name of the weapon script
SWEP.WeaponEntityName = SWEP.WeaponName .. "_ent" -- (String) Name of the weapon entity in Lua/Entities/Entityname.lua
SWEP.Manufacturer = VNTCB.Manufacturer.FRC -- (String) Gun company that makes this weapon
SWEP.CountryOfOrigin = VNTCB.Country.ITA -- (String) Country of origin
SWEP.MagazineName = VNTCB.Magazine.mShotgun -- (String) The name of the magazine the weapon uses - used in my Weapon Magazine System
SWEP.Category = VNTCB.Category.HL2X -- (String) Category
SWEP.Instructions = VNTCB.instructions -- (String) Instruction
SWEP.Author = VNTCB.author -- (String) Author
SWEP.Contact = VNTCB.contact -- (String) Contact
SWEP.Slot = VNTCB.Bucket.Shotgun -- (Integer) Bucket to place weapon in, 1 to 6
SWEP.SlotPos = VNTCB.Slot.Shotgun -- (Integer) Bucket position
SWEP.ViewModelFOV = 60 -- (Integer) First-person field of view

------------------------------------------------------
--	Model Information								--
------------------------------------------------------

SWEP.ViewModel = Model( "models/jessev92/hl2/weapons/spas12_stockless_v.mdl" ) -- (String) View model - v_*
SWEP.WorldModel = Model( "models/jessev92/hl2/weapons/spas12_stockless_w.mdl" ) -- (String) World model - w_*
--SWEP.UseHands = true
SWEP.HoldType = "shotgun" -- (String) Hold type for our weapon, refer to wiki for animation sets

------------------------------------------------------
--	Gun Types										--	Set the type of weapon - ONLY PICK ONE!
------------------------------------------------------

SWEP.WeaponType = VNTCB.WeaponType.Shotgun -- (Integer) 1=Melee, 2=Pistol, 3=Rifle, 4=Shotgun, 5=Sniper, 6=Grenade Launcher, 7=Rocket Launcher

------------------------------------------------------
--	Shotgun Settings								--	Settings for shotguns
------------------------------------------------------

SWEP.PumpAction = true -- (Boolean) Is the shotgun pump action?

------------------------------------------------------
--	Primary Fire Settings							--	Settings for the primary fire of the weapon
------------------------------------------------------

SWEP.Primary.ClipSize = 8 -- (Integer) Size of a magazine
SWEP.Primary.DefaultClip = 8 -- (Integer) Default number of ammo you spawn with
SWEP.Primary.Ammo = "12gauge" -- (String) Primary ammo used by the weapon, bullets probably
SWEP.Primary.PureDmg = VNTCB.Ammo.a12GBuck[ 1 ] -- (Integer) Base damage, put one number here and the base will do the rest
SWEP.Primary.RPM = 250 -- (Integer) Go to a wikipedia page and look at the RPM of the weapon, then put that here - the base will do the math

------------------------------------------------------
--	Gun Mechanics									--	Various things to tweak the effects and feedback
------------------------------------------------------

SWEP.FireMode = { false , true , false , false } -- (Table: Boolean, Boolean, Boolean, Boolean ) Enable different fire modes on the weapon; Has modes, Has Single, Has Burst, Has Auto - in that order. You can have more than one, but the first must be true
SWEP.CurrentMode = 1 -- (Integer) Current fire mode of the weapon; used to set the default mode; corresponds to the FireMode table
SWEP.Primary.NumShots = 9 -- (Integer) How many bullets per shot; think of this as a shotgun shell, higher integer, more pellets
SWEP.Weight = 5 -- (Integer) The weight in Kilogrammes of our weapon - used in my weapon weight mod!
SWEP.StrongPenetration = VNTCB.Ammo.a12GBuck[ 2 ] -- (Integer) Max penetration
SWEP.WeakPenetration = VNTCB.Ammo.a12GBuck[ 3 ] -- (Integer) Max wood penetration
SWEP.EffectiveRange = 40 -- (Integer) Effective range of the weapon in metres.
SWEP.BlackFriday = true -- (Boolean) Black Friday Doorbuster Sales
SWEP.StoppageRate = 0 -- (Integer) Rate of stoppages in the weapon, look up the real world number estimations and just throw that in here.

------------------------------------------------------
--	Special FX										--	Muzzle flashes, shell casings, etc
------------------------------------------------------

SWEP.ShellType = 2 -- (Integer) The shell to use, see Lua/Effects/FX_ShellEject for integers
SWEP.ShellDelay = 0.5 -- (Float) 	Time between shot firing and shell ejection; useful for bolt-actions and things like that that need a delay
SWEP.MuzzleFlashType = 5 -- (Integer) The number of the muzzle flash to use; see Lua/Effects/fx_muzzleflash.Lua

------------------------------------------------------
--	Custom Sounds									--
--		Setup sounds here!							--
------------------------------------------------------

SWEP.Sounds = {
	["Primary"] = Sound( "HL2X.SPAS12.Single" ),
	["Primary_Dry"] = Sound("VNTCB.SWep.Empty3"),  -- (String) Primary dry fire sound
	["Reload"] = Sound( "HL2X.SPAS12.Foley" ),  -- (String) Primary dry fire sound

	["Noise_Close"] = Sound( "BF3.BulletCraft.Noise.Shotgun.Close" ),
	["Noise_Distant"] = Sound( "BF3.BulletCraft.Noise.Forest.Distant" ) ,
	["Noise_Far"] = Sound( "BF3.BulletCraft.Noise.Forest.Far" ) ,
	["CoreBass_Close"] = Sound( "BF3.BulletCraft.CoreBass.Close.OneShot_2" ),
	["CoreBass_Distant"] = Sound( "BF3.BulletCraft.CoreBass.Distant.OneShot_1" ),
	["HiFi"] = Sound( "BF3.BulletCraft.HiFi.Shotgun" ),
	["Reflection_Close"] = Sound( "BF3.BulletCraft.Reflection.Forest.Close" ) , 
	["Reflection_Far"] = Sound( "BF3.BulletCraft.Reflection.Forest.Far" ) ,
}

SWEP.SelectorSwitchSNDType = 1 -- (Integer) 1=US , 2=RU
SWEP.UsesSuperSonicAmmo = true -- (Boolean) Is the weapon using supersonic or subsonic ammo?

------------------------------------------------------
--	Ironsight & Run Positions						--	Set our model transforms for running and ironsights
------------------------------------------------------	

SWEP.IronSightsPos = Vector( -5.7 , -9.447 , 3.417 ) -- (Vector) Ironsight XYZ Transfor
SWEP.IronSightsAng = Vector( 0 , -0.5 , 0 ) -- (Vector) Ironsight XYZ Rotation
SWEP.RunArmOffset = Vector( 4 , 0.4 , 2.4 ) -- (Vector) Sprinting XYZ Transform
SWEP.RunArmAngle = Vector( -18 , 33 , 0 ) -- (Vector) Sprinting XYZ Rotation

------------------------------------------------------
--	Setup Clientside Info							--	This block must be in every weapon!
------------------------------------------------------

if CLIENT then
	SWEP.WepSelectIcon = surface.GetTextureID( "vgui/hud/" .. SWEP.WeaponName )
	SWEP.RenderGroup = RENDERGROUP_BOTH
	language.Add( SWEP.WeaponName , SWEP.PrintName )
	killicon.Add( SWEP.WeaponName , "vgui/entities/" .. SWEP.WeaponName , Color( 255 , 255 , 255 ) )
end

------------------------------------------------------
--	SWEP:Initialize() 							--	Called when the weapon is first loaded
------------------------------------------------------

function SWEP:Initialize( )
	self.HoldMeRight = VNTCB.HoldType.Shotgun -- (String) Hold type table for our weapon, Lua/autorun/sh_v92_base_swep.Lua
	--self:SetHoldType(self.HoldType)
end

SWEP.Sequences.Draw = { "draw" }
SWEP.Sequences.Holster = { "holster" }
SWEP.Sequences.Primary = { "fire1" }
SWEP.Sequences.Primary_Dry = { "dryfire" }
SWEP.Sequences.Idle = { "idle" }
SWEP.Sequences.ReloadStart = { "reload1" }
SWEP.Sequences.ReloadLoop = { "reload2" }
SWEP.Sequences.ReloadEnd = { "reload3" }
SWEP.Sequences.LowIdle = { "lowered" }
SWEP.Sequences.LowToIdle = { "lowered_to_idle" }
SWEP.Sequences.IdleToLow = { "idle_to_lowered" }
