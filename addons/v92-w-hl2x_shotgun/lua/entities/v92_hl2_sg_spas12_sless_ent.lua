AddCSLuaFile( )

if not VNTCB then
	Error( "V92 Content Bases not mounted: Removing Entity" )

	return false
end

ENT.Base = VNT_BASE_WEAPON_ENTITY
ENT.Type = "anim"
ENT.PrintName = "SPAS-12 (Stockless)"
ENT.Author = VNTCB.author
ENT.Information = "Uses .12 Gauge Buckshot ammo"
ENT.Category = VNT_CATEGORY_HL2EXPANDED
ENT.Spawnable = true
ENT.AdminOnly = false
ENT.WeaponName = "v92_hl2_sg_spas12_sless_ent" -- (String) Name of this entity
ENT.SWepName = "v92_hl2_sg_spas12_sless" -- (String) Name of the weapon entity in Lua/weapons/swep_name.lua
ENT.SEntModel = Model( "models/jessev92/hl2/weapons/spas12_stockless_w.mdl" ) -- (String) Model to use