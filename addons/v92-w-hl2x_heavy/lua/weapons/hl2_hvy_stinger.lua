
AddCSLuaFile()

SWEP.Base				= "v92_base_wep"

if CLIENT then
	local	_SELFENTNAME	= "hl2_hvy_stinger"
	local	_INFONAME		= "FIM-92"
	SWEP.Category			= "Tac-Life"
	SWEP.PrintName			= _INFONAME
	SWEP.Author   			= "V92"
	SWEP.Contact        	= "V92"
	SWEP.Instructions 		= "Uses RPG ammo"
	SWEP.Slot 				= 4
	SWEP.SlotPos 			= 1
	SWEP.ViewModelFOV		= 70
	SWEP.WepSelectIcon 		= surface.GetTextureID("vgui/hud/" .. _SELFENTNAME )

	language.Add( _SELFENTNAME, _INFONAME )	
	killicon.Add( _SELFENTNAME, "vgui/entities/".. _SELFENTNAME , Color( 255, 255, 255 ) )
end

SWEP.Weight					= 15			// Decides whether we should switch from/to this
SWEP.HoldType				= "shotgun"
SWEP.ViewModelFlip			= false
SWEP.UseHands				= false
SWEP.ViewModel				= Model("models/jessev92/weapons/hl2/hevmk4/stinger_v.mdl")
SWEP.WorldModel				= Model("models/jessev92/weapons/hl2/stinger_w.mdl")
SWEP.Spawnable				= true
SWEP.AdminOnly				= false

SWEP.Primary.Sound 			= Sound("NPC_Helicopter.FireRocket")
SWEP.Primary.Recoil			= 5.5
SWEP.Primary.Damage			= 0
SWEP.Primary.NumShots		= 0
SWEP.Primary.Cone			= 0
SWEP.Primary.Delay 			= 1
SWEP.Primary.ClipSize		= 1					// Size of a clip
SWEP.Primary.DefaultClip	= 2				// Default number of bullets in a clip
SWEP.Primary.Automatic		= false				// Automatic/Semi Auto
SWEP.Primary.Ammo			= "arpeegee"

SWEP.Secondary.ClipSize		= -1					// Size of a clip
SWEP.Secondary.DefaultClip	= -1					// Default number of bullets in a clip
SWEP.Secondary.Automatic	= false				// Automatic/Semi Auto
SWEP.Secondary.Ammo			= "none"

SWEP.ShellEffect			= "shell_12gauge"	

SWEP.Pistol					= false
SWEP.Rifle					= true
SWEP.Shotgun				= false
SWEP.Sniper					= false

SWEP.IronSightsPos			= Vector(-3.6, -14, -1.4)
SWEP.IronSightsAng			= Vector(0, 0, 0)
SWEP.RunArmOffset			= Vector(-2, -12, -3)
SWEP.RunArmAngle			= Vector(0, 60, -22)

SWEP.Speed 					= 0.25
SWEP.Mass					= 1
SWEP.WeaponName 			= "hl2_hvy_stinger"
SWEP.WeaponEntName 			= "hl2_hvy_stinger_ent"
SWEP.ScopeAfterShoot		= false 

function SWEP:PrimaryAttack()

	//check to see if jammed, running, reloading
	if (self.Owner:KeyDown(IN_SPEED)) then return end 
	if (not self.Owner:KeyDown(IN_RELOAD) and self.Owner:KeyDown(IN_USE)) then
		//if keying down E
		if (SERVER) then
			bHolsted = !self.Weapon:GetDTBool(0)
			self:SetHolsted(bHolsted)
		end
		// holster and push button after 1 second
		self.Weapon:SetNextPrimaryFire(CurTime() + 1.0)
		self.Weapon:SetNextSecondaryFire(CurTime() + 1.0)
		
		self:SetIronsights(false)

		return
	end
	// if the weapon cannot shoot then go to end
	if (not self:CanPrimaryAttack()) then 
		self:EmitSound("Buttons.snd14")
		return 
	end
	self:FireRocket()
	self:EmitSound(self.Primary.Sound)
	
	self.Reloadaftershoot = CurTime() + self.Primary.Delay
	self.ActionDelay = (CurTime() + self.Primary.Delay + 0.05)
	self.Weapon:SetNextPrimaryFire(CurTime() + self.Primary.Delay)
	self.Weapon:SetNextSecondaryFire(CurTime() + self.Primary.Delay)

	// take a single bullet
	self:TakePrimaryAmmo(1)
	//go to shoot bullet function
end

function SWEP:FireRocket()
	if SERVER then
		local Rocket = ents.Create("hl2_hvy_stinger_msl")
			Rocket:SetPos(self.Owner:GetShootPos() + self.Owner:EyeAngles():Right()*0)
			Rocket:SetAngles(self.Owner:EyeAngles())
			Rocket:SetOwner(self.Owner)
			Rocket:Spawn()
			Rocket:Activate()
			Rocket:SetVelocity(self.Owner:EyeAngles():Forward()*1500 + Vector(0,0,10))
	end
end
