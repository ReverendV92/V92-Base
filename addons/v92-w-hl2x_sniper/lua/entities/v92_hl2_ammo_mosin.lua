AddCSLuaFile( )
if not VNTCB then return false end
ENT.Type = "anim"
ENT.Base = VNT_BASE_AMMO_BOX
ENT.WeaponName = "v92_hl2_ammo_mosin"
ENT.AMMOTOGIVE = 5
ENT.AMMONAME = "7.62x54mm"
ENT.AMMOTYPE = "762x54mmwp"
ENT.AMMOMDL = Model( "models/jessev92/hl2/items/mosin_rounds.mdl" )
ENT.PrintName = ( "Clip of " .. ENT.AMMONAME )
ENT.Information = "Gives " .. ENT.AMMOTOGIVE .. " rounds on a " .. ENT.AMMONAME .. " stripper clip"
ENT.Author = VNTCB.author
ENT.Category = VNT_CATEGORY_HL2EXPANDED
ENT.Spawnable = true
ENT.AdminOnly = false