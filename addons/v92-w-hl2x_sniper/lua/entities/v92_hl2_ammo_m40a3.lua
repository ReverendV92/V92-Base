AddCSLuaFile( )
if not VNTCB then return false end
ENT.Type = "anim"
ENT.Base = VNT_BASE_AMMO_BOX
ENT.WeaponName = "v92_hl2_ammo_m40a3"
ENT.AMMOTOGIVE = 5
ENT.AMMONAME = "M40A3"
ENT.AMMOTYPE = "762x51mmnato"
ENT.AMMOMDL = Model( "models/jessev92/hl2/items/m40a3_mag.mdl" )
ENT.PrintName = ( ENT.AMMONAME .. " Magazine" )
ENT.Information = "Gives " .. ENT.AMMOTOGIVE .. " rounds of " .. ENT.AMMONAME .. " Ammo"
ENT.Author = VNTCB.author
ENT.Category = VNT_CATEGORY_HL2EXPANDED
ENT.Spawnable = true
ENT.AdminOnly = false