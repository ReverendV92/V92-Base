AddCSLuaFile( )
if not VNTCB then return false end
ENT.Base = VNT_BASE_WEAPON_ENTITY
ENT.Type = "anim"
ENT.PrintName = "Mosin-Nagant"
ENT.Author = VNTCB.author
ENT.Information = "Uses 7.62x54mm Warsaw Pact Ammo"
ENT.Category = VNT_CATEGORY_HL2EXPANDED
ENT.Spawnable = true
ENT.AdminOnly = false
ENT.WeaponName = "v92_hl2_sni_mosin_ent" -- (String) Name of this entity
ENT.SWepName = "v92_hl2_sni_mosin" -- (String) Name of the weapon entity in Lua/weapons/swep_name.lua
ENT.SEntModel = Model( "models/jessev92/hl2/weapons/mosin_w.mdl" ) -- (String) Model to use