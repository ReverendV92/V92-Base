
------------------------------------------------------
--	Counter-Strike: Source - HEAT					--
--	H&K SR9T										--
--	http://www.imfdb.org/wiki/Heat#Heckler_.26_Koch_SR9.28T.29
--	7.62x51mm Semi-Auto/Auto rifle					--
------------------------------------------------------

AddCSLuaFile( )

SWEP.PrintName = "SR-9T" -- (String) Printed name on menu

if not VNTCB then
	Error( "V92 Content Bases not mounted; Removing Weapon: " .. SWEP.PrintName .. "\n" )
	return false
end

SWEP.Base = VNT_BASE_WEAPON -- (String) Weapon base parent this is a child of
SWEP.Spawnable = true -- (Boolean) Can be spawned via the menu
SWEP.AdminOnly = false -- (Boolean) Admin only spawnable
------------------------------------------------------
--	Client Information								--	Info used in the client block of the weapon
------------------------------------------------------
SWEP.WeaponName = "v92_heat_sr9t" -- (String) Name of the weapon script
SWEP.WeaponEntityName = SWEP.WeaponName .. "_ent" -- (String) Name of the weapon entity in Lua/Entities/Entityname.lua
SWEP.Manufacturer = VNT_WEAPON_MANUFACTURER_HK -- (String) Gun company that makes this weapon
SWEP.CountryOfOrigin = VNT_WEAPON_COUNTRY_WESTGERMANY -- (String) Country of origin
SWEP.MagazineName = VNTCB.Magazine.mHK762 -- (String) The name of the magazine the weapon uses - used in my Weapon Magazine System
SWEP.Category = VNT_CATEGORY_HEAT -- (String) Category
SWEP.Instructions = VNTCB.instructions -- (String) Instruction
SWEP.Author = VNTCB.author -- (String) Author
SWEP.Contact = VNTCB.contact -- (String) Contact
SWEP.Slot = VNT_WEAPON_BUCKETPOS_SNIPER -- (Integer) Bucket to place weapon in, 1 to 6
SWEP.SlotPos = VNT_WEAPON_SLOTPOS_SNIPER -- (Integer) Bucket position
SWEP.ViewModelFOV = 85 -- (Integer) First-person field of view
SWEP.DrawWeaponInfoBox = false -- (Boolean) Draw a verbose info box in the HUD slots
SWEP.BounceWeaponIcon = false -- (Boolean) This causes that annoying icon bounce effect used on the Tool Gun
SWEP.DrawAmmo = true -- (Boolean) Draw our ammo, you can change this is it doesn't use ammo or you're hardcore
SWEP.DrawCrosshair = false -- (Boolean) You can change this is you're a git who can't aim
SWEP.AutoSwitchTo = false -- (Boolean) Auto-switch to this weapon when picked up? Leave to false - PLEASE
SWEP.AutoSwitchFrom = false -- (Boolean) Auto-switch away from this weapon when you pickup a new gun? Leave at false - PLEASE
SWEP.WorkshopID = "763862893" -- (Integer) Workshop ID number of the upload that contains this file.
------------------------------------------------------
--	Model Information								--
------------------------------------------------------
SWEP.ViewModelFlip = true -- (Boolean) Only used for vanilla CS:S models
SWEP.ViewModel = Model( "models/jessev92/css/weapons/heat/sr9t_v.mdl" ) -- (String) View model - v_*
SWEP.WorldModel = Model( "models/jessev92/css/weapons/heat/sr9t_w.mdl" ) -- (String) World model - w_*
SWEP.HoldType = "ar2" -- (String) Hold type for our weapon, refer to wiki for animation sets
------------------------------------------------------
--	Gun Types										--	Set the type of weapon - ONLY PICK ONE!
------------------------------------------------------

------------------------------------------------------
--	Sniper Settings									--	Settings for sniper rifles
------------------------------------------------------
SWEP.ScopeType = 2 -- (Integer) Type of scope, 0=none, 1=overlay, 2=Render Target
SWEP.ScopeMaterial = 6 -- (Integer) Type of overlay, 0=Red Dot, 1=EOTech, 2=ACOG, 3=SVD, 4=M14, 5=L42A1, 6=PSG1, 7=German tri-bar
SWEP.ScopeMaterialCustom = Material( "jessev92/ui/scope/scope_psg1" ) -- (String: Material) Path to the scope overlay you want to use if you use an overlay scope
SWEP.ScopeMultipliers = { 1 , 8 , 4 } -- (Table: Float, Float, Float) Zoom Muliplier, Default Zoom x, Close Zoom x
SWEP.DefaultZoom = 1 -- (Float) 	Current zoom, default to 1; DON'T CHANGE
SWEP.BoltAction = false -- (Boolean) Is this bolt action? Removes the player from the scope after firing
SWEP.ReturnToScope = false -- (Boolean) Return to scope after cycling the bolt?
SWEP.HasVariableZoom = true -- (Boolean) Does the weapon have variable zoom?
SWEP.IsCloseZoomed = false -- (Boolean) Don't touch
SWEP.ZoomAmount = nil
SWEP.HasRTScope = true -- Has RT scope?
SWEP.RTScopeCrosshair = Material( "jessev92/ui/scope/crosshair_psg1" ) -- The crosshair texture to use - check jessev92/ui/scope/ for textures
SWEP.RTScopeDirt = Material( "jessev92/ui/scope/scope_dirt" ) -- The dirt texture to use as an overlay
SWEP.RTScopeShadowMask = Material( "jessev92/ui/scope/scope_shadowmask" ) -- The shadow texture to use around the edges of the view
------------------------------------------------------
--	Primary Fire Settings							--	Settings for the primary fire of the weapon
------------------------------------------------------
SWEP.Primary.ClipSize = 20 -- (Integer) Size of a magazine
SWEP.Primary.DefaultClip = 20 -- (Integer) Default number of ammo you spawn with
SWEP.Primary.Ammo = "762x51mmnato" -- (String) Primary ammo used by the weapon, bullets probably
SWEP.Primary.RPM = 500 -- (Integer) Go to a wikipedia page and look at the RPM of the weapon, then put that here - the base will do the math
SWEP.Primary.PureDmg = VNTCB.Ammo.a762NATO[ 1 ] -- (Integer) Base damage, put one number here and the base will do the rest
------------------------------------------------------
--	Gun Mechanics									--	Various things to tweak the effects and feedback
------------------------------------------------------
SWEP.FireMode = { true , true , false , true } -- (Table: Boolean, Boolean, Boolean, Boolean ) Enable different fire modes on the weapon; Has modes, Has Single, Has Burst, Has Auto - in that order. You can have more than one, but the first must be true
SWEP.CurrentMode = 1 -- (Integer) Current fire mode of the weapon; used to set the default mode; corresponds to the FireMode table
SWEP.Weight = 5 -- (Integer) The weight in Kilogrammes of our weapon - used in my weapon weight mod!
SWEP.StrongPenetration = VNTCB.Ammo.a762NATO[ 2 ] -- (Integer) Max penetration
SWEP.WeakPenetration = VNTCB.Ammo.a762NATO[ 3 ] -- (Integer) Max wood penetration
SWEP.EffectiveRange = 400 -- (Integer) Effective range of the weapon in metres.
SWEP.StoppageRate = 5000 -- (Integer) Rate of stoppages in the weapon, look up the real world number estimations and just throw that in here.
------------------------------------------------------
--	Special FX										--	Muzzle flashes, shell casings, etc
------------------------------------------------------
SWEP.MuzzleAttach = 1 -- (Integer) The number of the attachment point for muzzle flashes, typically "1"
SWEP.MuzzleFlashType = 3 -- (Integer) The number of the muzzle flash to use; see Lua/Effects/fx_muzzleflash.Lua
SWEP.ShellAttach = 2 -- (Integer) The number of the attachment point for shell ejections, typically "2"
SWEP.ShellType = 18 -- (Integer) The shell to use, see Lua/Effects/FX_ShellEject for integers
SWEP.UseTracer = true -- (Boolean) Do we use tracers?
SWEP.TracerRandomizer = 3 -- (Integer) Between 0 and this number, chance to make a TracerType if UseTracer is true
------------------------------------------------------
--	Custom Sounds									--	Setup sounds here!
------------------------------------------------------
SWEP.Sounds = {
	["Primary"] = Sound( "HEAT_SR9T.Single" ),
	["PrimaryDry"] = Sound( "VNTCB.SWep.Empty5" ),
	["Reload"] = Sound( "HEAT_SR9T.Draw" ),

	["Noise_Close"] = Sound( "BF3.BulletCraft.Noise.Rifle.Close" ),
	["Noise_Distant"] = Sound( "BF3.BulletCraft.Noise.Forest.Distant" ) ,
	["Noise_Far"] = Sound( "BF3.BulletCraft.Noise.Forest.Far" ) ,
	["CoreBass_Close"] = Sound( "BF3.BulletCraft.CoreBass.Close.LMG_10" ),
	["CoreBass_Distant"] = Sound( "BF3.BulletCraft.CoreBass.Distant.LMG_4" ),
	["HiFi"] = Sound( "BF3.BulletCraft.HiFi.G3" ),
	["Reflection_Close"] = Sound( "BF3.BulletCraft.Reflection.Forest.Close" ) ,
	["Reflection_Far"] = Sound( "BF3.BulletCraft.Reflection.Sniper" ),
}

SWEP.SelectorSwitchSNDType = 1 -- (Integer) 1=US , 2=RU
SWEP.UsesSuperSonicAmmo = true -- (Boolean) Is the weapon using supersonic or subsonic ammo?
------------------------------------------------------
--	Ironsight & Run Positions						--	Set our model transforms for running and ironsights
------------------------------------------------------
SWEP.IronSightsPos = Vector( 2.7 , -3.8 , 0.895 ) -- (Vector) Ironsight XYZ Transform
SWEP.IronSightsAng = Vector( 0 , 0 , 0 ) -- (Vector) Ironsight XYZ Rotation
SWEP.RunArmOffset = Vector( 0 , -6.5 , -2.5 ) -- (Vector) Sprinting XYZ Transform
SWEP.RunArmAngle = Vector( -3.5 , -40 , 30 ) -- (Vector) Sprinting XYZ Rotation
------------------------------------------------------
--	Setup Clientside Info							--	This block must be in every weapon!
------------------------------------------------------
if CLIENT then

	local texFSB = render.GetSuperFPTex() -- Full screen render target
	local oldRT = nil
	local camdata = {}
	local scopeattach = nil
	local scopeaim = nil
	local viewmodel = nil

	local function HEATSR9TRTScope( )
		if LocalPlayer():GetActiveWeapon().HasRTScope then
			viewmodel = LocalPlayer():GetViewModel()
			scopeattach = viewmodel:GetAttachment( viewmodel:LookupAttachment( "scope_pov" ) )
			scopeaim = viewmodel:GetAttachment( viewmodel:LookupAttachment( "scope_align" ) )
			camdata = {
				x = 0,
				y = 0,
				w = ScrW(),
				h = ScrH(),
				angles = (scopeaim.Pos - scopeattach.Pos):Angle(),
				origin = scopeattach.Pos,
				fov = LocalPlayer():GetFOV() / (LocalPlayer():GetActiveWeapon().ZoomAmount or 4)*(LocalPlayer():GetActiveWeapon().ScopeMultipliers[1] or 1),
				drawhud = false,
				drawviewmodel = false,
				ortho = false,
				dopostprocess = false,
			}
			oldRT = render.GetRenderTarget()
			render.SetRenderTarget( texFSB )
			render.Clear( 0, 0, 0, 255 )
			render.SetViewPort( 0, 0, ScrW(), ScrH() )
			cam.Start2D()
				viewmodel:SetNoDraw( true )
				render.RenderView( camdata )
				render.SetMaterial( Material( "jessev92/ui/scope/crosshair_psg1.vmt" ) )
				render.DrawScreenQuadEx( camdata.x, camdata.y, camdata.w, camdata.h )
				viewmodel:SetNoDraw( false )
			cam.End2D()
			render.SetRenderTarget( oldRT )
			render.SetViewPort( 0, 0, ScrW(), ScrH() )
		end
	end
	hook.Add( "RenderScene", "HEATSR9TRTScope", HEATSR9TRTScope )
	
	function SWEP:CustomDrawHUD()
		local scopemat = Material( "models/jessev92/css/weapons/heat/hk91/lense_rt" )
		scopemat:SetTexture( "$basetexture", texFSB )
	end
	--]]

	SWEP.WepSelectIcon = surface.GetTextureID( "vgui/hud/" .. SWEP.WeaponName )
	SWEP.RenderGroup = RENDERGROUP_BOTH
	language.Add( SWEP.WeaponName , SWEP.PrintName )
	killicon.Add( SWEP.WeaponName , "vgui/entities/" .. SWEP.WeaponName , Color( 255 , 255 , 255 ) )
elseif SERVER then
	resource.AddWorkshop( SWEP.WorkshopID )
end

------------------------------------------------------
--	SWEP:Initialize() 							--	Called when the weapon is first loaded
------------------------------------------------------
function SWEP:Initialize( )
	self.HoldMeRight = VNTCB.HoldType.Sniper -- (String) Hold type table for our weapon, Lua/autorun/sh_v92_base_swep.Lua
end

SWEP.SeqDraw = { "draw" }
SWEP.SeqHolster = { "hoslter" }
SWEP.SeqIdle = { "idle" }
SWEP.SeqReload = { "reload" }
SWEP.SeqPrimary = { "shoot1" , "shoot2" }
