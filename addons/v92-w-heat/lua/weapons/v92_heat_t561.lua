
------------------------------------------------------
--	Counter-Strike: Source - HEAT					--
--	Norinco Type 56-1								--
--	http://www.imfdb.org/wiki/Heat#Norinco_Type_56-1 -
--	5.56x45mm Auto/Burst/Semi-Auto rifle			--
------------------------------------------------------

AddCSLuaFile( )

SWEP.PrintName = "Type 56-1" -- (String) Printed name on menu

if not VNTCB then
	Error( "V92 Content Bases not mounted; Removing Weapon: " .. SWEP.PrintName .. "\n" )
	return false
end

SWEP.Base = VNT_BASE_WEAPON -- (String) Weapon base parent this is a child of
SWEP.Spawnable = true -- (Boolean) Can be spawned via the menu
SWEP.AdminOnly = false -- (Boolean) Admin only spawnable
------------------------------------------------------
--	Client Information								--	Info used in the client block of the weapon
------------------------------------------------------
SWEP.WeaponName = "v92_heat_t561" -- (String) Name of the weapon script
SWEP.WeaponEntityName = SWEP.WeaponName .. "_ent" -- (String) Name of the weapon entity in Lua/Entities/Entityname.lua
SWEP.Manufacturer = VNT_WEAPON_MANUFACTURER_NORINCO -- (String) Gun company that makes this weapon
SWEP.CountryOfOrigin = VNT_WEAPON_COUNTRY_CHINA -- (String) Country of origin
SWEP.MagazineName = VNTCB.Magazine.m76239 -- (String) The name of the magazine the weapon uses - used in my Weapon Magazine System
SWEP.Category = VNT_CATEGORY_HEAT -- (String) Category
SWEP.Instructions = VNTCB.instructions -- (String) Instruction
SWEP.Author = VNTCB.author -- (String) Author
SWEP.Contact = VNTCB.contact -- (String) Contact
SWEP.Slot = VNT_WEAPON_BUCKETPOS_CARBINE -- (Integer) Bucket to place weapon in, 1 to 6
SWEP.SlotPos = VNT_WEAPON_SLOTPOS_RIFLE -- (Integer) Bucket position
SWEP.ViewModelFOV = 80 -- (Integer) First-person field of view
SWEP.WorkshopID = "763862893" -- (Integer) Workshop ID number of the upload that contains this file.
------------------------------------------------------
--	Model Information								--
------------------------------------------------------
SWEP.ViewModelFlip = true -- (Boolean) Only used for vanilla CS:S models
SWEP.ViewModel = Model( "models/jessev92/css/weapons/heat/type561_v.mdl" ) -- (String) View model - v_*
SWEP.WorldModel = Model( "models/jessev92/css/weapons/heat/type561_w.mdl" ) -- (String) World model - w_*
SWEP.HoldType = "ar2" -- (String) Hold type for our weapon, refer to wiki for animation sets
------------------------------------------------------
--	Gun Types										--	Set the type of weapon - ONLY PICK ONE!
------------------------------------------------------
SWEP.WeaponType = VNTCB.WeaponType.Carbine -- (Integer) 1=Melee, 2=Pistol, 3=Rifle, 4=Shotgun, 5=Sniper, 6=Grenade Launcher, 7=Rocket Launcher
------------------------------------------------------
--	Gun Mechanics									--	Various things to tweak the effects and feedback
------------------------------------------------------
SWEP.FireMode = { true , true , false , true } -- (Table: Boolean, Boolean, Boolean, Boolean ) Enable different fire modes on the weapon; Has modes, Has Single, Has Burst, Has Auto - in that order. You can have more than one, but the first must be true
SWEP.CurrentMode = 1 -- (Integer) Current fire mode of the weapon; used to set the default mode; corresponds to the FireMode table
SWEP.Weight = 4 -- (Integer) The weight in Kilogrammes of our weapon - used in my weapon weight mod!
SWEP.StrongPenetration = VNTCB.Ammo.a762x39mmWP[ 2 ] -- (Integer) Max penetration
SWEP.WeakPenetration = VNTCB.Ammo.a762x39mmWP[ 3 ] -- (Integer) Max wood penetration
SWEP.StoppageRate = 1000 -- (Integer) Rate of stoppages in the weapon, look up the real world number estimations and just throw that in here.
------------------------------------------------------
--	Primary Fire Settings							--	Settings for the primary fire of the weapon
------------------------------------------------------
SWEP.Primary.ClipSize = 40 -- (Integer) Size of a magazine
SWEP.Primary.DefaultClip = 40 -- (Integer) Default number of ammo you spawn with
SWEP.Primary.Ammo = "762x39mmwp" -- (String) Primary ammo used by the weapon, bullets probably
SWEP.Primary.RPM = 650 -- (Integer) Go to a wikipedia page and look at the RPM of the weapon, then put that here - the base will do the math
SWEP.Primary.PureDmg = VNTCB.Ammo.a762x39mmWP[ 1 ] -- (Integer) Base damage, put one number here and the base will do the rest
------------------------------------------------------
--	Special FX										--	Muzzle flashes, shell casings, etc
------------------------------------------------------
SWEP.MuzzleAttach = 1 -- (Integer) The number of the attachment point for muzzle flashes, typically "1"
SWEP.MuzzleFlashType = 3 -- (Integer) The number of the muzzle flash to use; see Lua/Effects/fx_muzzleflash.Lua
SWEP.ShellAttach = 2 -- (Integer) The number of the attachment point for shell ejections, typically "2"
SWEP.ShellType = 10 -- (Integer) The shell to use, see Lua/Effects/FX_ShellEject for integers
SWEP.EffectiveRange = 500 -- (Integer) Effective range of the weapon in metres.
------------------------------------------------------
--	Custom Sounds									--	Setup sounds here!
------------------------------------------------------
SWEP.Sounds = {
	["Primary"] = Sound( "HEAT_Type56-1.Single" ),
	["PrimaryDry"] = Sound("VNTCB.SWep.Empty4"),
	["Reload"] = Sound( "HEAT_Type56-1.Draw" ),

	["Noise_Close"] = Sound( "BF3.BulletCraft.Noise.Rifle.Close" ),
	["Noise_Distant"] = Sound( "BF3.BulletCraft.Noise.Forest.Distant" ) ,
	["Noise_Far"] = Sound( "BF3.BulletCraft.Noise.Forest.Far" ) ,
	["CoreBass_Close"] = Sound( "BF3.BulletCraft.CoreBass.Close.LMG_16" ) ,
	["CoreBass_Distant"] = Sound( "BF3.BulletCraft.CoreBass.Distant.LMG_13" ) ,
	["HiFi"] = Sound( "BF3.BulletCraft.HiFi.AK47_1" ),
	["Reflection_Close"] = Sound( "BF3.BulletCraft.Reflection.Forest.Close" ) ,
	["Reflection_Far"] = Sound( "BF3.BulletCraft.Reflection.Forest.Far" ) ,
}

SWEP.SelectorSwitchSNDType = 2 -- (Integer) 1=US , 2=RU
SWEP.UsesSuperSonicAmmo = true -- (Boolean) Is the weapon using supersonic or subsonic ammo?
------------------------------------------------------
--	Ironsight & Run Positions						--	Set our model transforms for running and ironsights
------------------------------------------------------
SWEP.IronSightsPos = Vector( 3.414 , -9 , 1.8 ) -- (Vector) Ironsight XYZ Transform
SWEP.IronSightsAng = Vector( 0 , 0 , 0 ) -- (Vector) Ironsight XYZ Rotation
SWEP.RunArmOffset = Vector( 0 , -6.5 , -2.5 ) -- (Vector) Sprinting XYZ Transform
SWEP.RunArmAngle = Vector( -3.5 , -40 , 30 ) -- (Vector) Sprinting XYZ Rotation

------------------------------------------------------
--	Setup Clientside Info							--	This block must be in every weapon!
------------------------------------------------------
if CLIENT then
	SWEP.CSMuzzleFlashes = true
	SWEP.CSMuzzleX = true
	SWEP.WepSelectIcon = surface.GetTextureID( "vgui/hud/" .. SWEP.WeaponName )
	SWEP.RenderGroup = RENDERGROUP_BOTH
	language.Add( SWEP.WeaponName , SWEP.PrintName )
	killicon.Add( SWEP.WeaponName , "vgui/entities/" .. SWEP.WeaponName , Color( 255 , 255 , 255 ) )
elseif SERVER then
	resource.AddWorkshop( SWEP.WorkshopID )
end
------------------------------------------------------
--	SWEP:Initialize() 							--	Called when the weapon is first loaded
------------------------------------------------------
function SWEP:Initialize( )
	self.HoldMeRight = VNTCB.HoldType.Rifle -- (String) Hold type table for our weapon, Lua/autorun/sh_v92_base_swep.Lua
end

SWEP.SeqDraw = { "draw" }
SWEP.SeqIdle = { "idle" }
SWEP.SeqReload = { "reload" }
SWEP.SeqPrimary = { "shoot1" , "shoot2" , "shoot3" }
