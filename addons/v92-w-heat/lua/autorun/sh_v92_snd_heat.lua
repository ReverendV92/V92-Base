
---------------------------------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------------------------
--	HEAT: A Garry's Mod Crime Saga
---------------------------------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------------------------

if not VNTCB then
	Error( "V92 Content Bases not mounted: Removing Autorun File" )

	return false
end

if VNTPlugins then
	table.insert( VNTPlugins, 0, "VNT :: HEAT Pack" )
end

------------------------------------------------------------------------------------------
--	HEAT :: Weapons :: Pistols / Handguns
------------------------------------------------------------------------------------------

---------------------------------------------
--	Colt Officer
---------------------------------------------
sound.Add( {
	name = "HEAT_Officer.Single" ,
	channel = CHAN_WEAPON ,
	volume = 1.0 ,
	level = 140 ,
	pitch = { 95 , 105 } ,
	sound = { "jessev92/css/weapons/heat/officer/single.wav" }
} )

sound.Add( {
	name = "HEAT_Officer.Dry" ,
	channel = CHAN_WEAPON ,
	volume = 1.0 ,
	level = 140 ,
	pitch = { 95 , 105 } ,
	sound = { "jessev92/weapons/univ/empty6.wav" }
} )

sound.Add( {
	name = "HEAT_Officer.Draw" ,
	channel = CHAN_BODY ,
	volume = 1.0 ,
	level = 75 ,
	pitch = { 95 , 105 } ,
	sound = { "jessev92/css/weapons/heat/officer/draw.wav" }
} )

sound.Add( {
	name = "HEAT_Officer.MagIn" ,
	channel = CHAN_ITEM ,
	volume = 1.0 ,
	level = 75 ,
	pitch = { 95 , 105 } ,
	sound = { "jessev92/css/weapons/heat/officer/magin.wav" }
} )

sound.Add( {
	name = "HEAT_Officer.MagOut" ,
	channel = CHAN_ITEM ,
	volume = 1.0 ,
	level = 75 ,
	pitch = { 95 , 105 } ,
	sound = { "jessev92/css/weapons/heat/officer/magout.wav" }
} )

sound.Add( {
	name = "HEAT_Officer.SlideRelease" ,
	channel = CHAN_ITEM ,
	volume = 1.0 ,
	level = 75 ,
	pitch = { 95 , 105 } ,
	sound = { "jessev92/css/weapons/heat/officer/sliderelease.wav" }
} )

sound.Add( {
	name = "HEAT_Officer.Forward" ,
	channel = CHAN_ITEM ,
	volume = 1.0 ,
	level = 75 ,
	pitch = { 95 , 105 } ,
	sound = { "jessev92/css/weapons/heat/officer/forward.wav" }
} )

sound.Add( {
	name = "HEAT_Officer.Back" ,
	channel = CHAN_ITEM ,
	volume = 1.0 ,
	level = 75 ,
	pitch = { 95 , 105 } ,
	sound = { "jessev92/css/weapons/heat/officer/back.wav" }
} )

---------------------------------------------
--	Star Megastar Akimbo
---------------------------------------------
sound.Add( {
	name = "HEAT_Megastar.Single" ,
	channel = CHAN_WEAPON ,
	volume = 1.0 ,
	level = 140 ,
	pitch = { 95 , 105 } ,
	sound = { "jessev92/css/weapons/heat/megastar/single.wav" }
} )

sound.Add( {
	name = "HEAT_Megastar.Draw" ,
	channel = CHAN_BODY ,
	volume = 1.0 ,
	level = 75 ,
	pitch = { 95 , 105 } ,
	sound = { "jessev92/weapons/univ/draw2.wav" }
} )

sound.Add( {
	name = "HEAT_Megastar.Ready" ,
	channel = CHAN_ITEM ,
	volume = 1.0 ,
	level = 75 ,
	pitch = { 95 , 105 } ,
	sound = { "jessev92/css/weapons/heat/megastar/ready.wav" }
} )

sound.Add( {
	name = "HEAT_Megastar.LeftMagIn" ,
	channel = CHAN_ITEM ,
	volume = 1.0 ,
	level = 75 ,
	pitch = { 95 , 105 } ,
	sound = { "jessev92/css/weapons/heat/megastar/left_mag_in.wav" }
} )

sound.Add( {
	name = "HEAT_Megastar.LeftMagOut" ,
	channel = CHAN_ITEM ,
	volume = 1.0 ,
	level = 75 ,
	pitch = { 95 , 105 } ,
	sound = { "jessev92/css/weapons/heat/megastar/left_mag_out.wav" }
} )

sound.Add( {
	name = "HEAT_Megastar.RightMagIn" ,
	channel = CHAN_ITEM ,
	volume = 1.0 ,
	level = 75 ,
	pitch = { 95 , 105 } ,
	sound = { "jessev92/css/weapons/heat/megastar/right_mag_in.wav" }
} )

sound.Add( {
	name = "HEAT_Megastar.RightMagOut" ,
	channel = CHAN_ITEM ,
	volume = 1.0 ,
	level = 75 ,
	pitch = { 95 , 105 } ,
	sound = { "jessev92/css/weapons/heat/megastar/right_mag_out.wav" }
} )

sound.Add( {
	name = "HEAT_Megastar.MagBash" ,
	channel = CHAN_ITEM ,
	volume = 1.0 ,
	level = 75 ,
	pitch = { 95 , 105 } ,
	sound = { "jessev92/css/weapons/heat/megastar/mag_bash.wav" }
} )

sound.Add( {
	name = "HEAT_Megastar.HammerBack" ,
	channel = CHAN_ITEM ,
	volume = 1.0 ,
	level = 75 ,
	pitch = { 95 , 105 } ,
	sound = { "jessev92/css/weapons/heat/megastar/hammer_back.wav" }
} )

sound.Add( {
	name = "HEAT_Megastar.SlideRelease" ,
	channel = CHAN_ITEM ,
	volume = 1.0 ,
	level = 75 ,
	pitch = { 95 , 105 } ,
	sound = { "jessev92/css/weapons/heat/megastar/slide_release.wav" }
} )

---------------------------------------------
--	Colt Detective Special
---------------------------------------------
sound.Add( {
	name = "HEAT_DetSpec.Single" ,
	channel = CHAN_WEAPON ,
	volume = 1.0 ,
	level = 140 ,
	pitch = { 95 , 105 } ,
	sound = "jessev92/css/weapons/heat/detspec/single_1.wav"
} )

sound.Add( {
	name = "HEAT_DetSpec.Draw" ,
	channel = CHAN_ITEM ,
	volume = 1.0 ,
	level = 75 ,
	pitch = { 95 , 105 } ,
	sound = "jessev92/weapons/univ/draw1.wav"
} )

sound.Add( {
	name = "HEAT_DetSpec.Reload" ,
	channel = CHAN_ITEM ,
	volume = 1.0 ,
	level = 75 ,
	--pitch			= { 95, 105 },
	sound = "jessev92/css/weapons/heat/detspec/reload.wav"
} )

sound.Add( {
	name = "HEAT_DetSpec.Spin" ,
	channel = CHAN_BODY ,
	volume = 1.0 ,
	level = 75 ,
	pitch = { 95 , 105 } ,
	sound = "jessev92/css/weapons/heat/detspec/draw.wav"
} )

sound.Add( {
	name = "HEAT_DetSpec.Holster" ,
	channel = CHAN_BODY ,
	volume = 1.0 ,
	level = 75 ,
	pitch = { 95 , 105 } ,
	sound = "jessev92/weapons/univ/holster1.wav"
} )

---------------------------------------------
--	SIG Sauer P220
---------------------------------------------
sound.Add( {
	name = "HEAT_P220.Single" ,
	channel = CHAN_WEAPON ,
	volume = 1.0 ,
	level = 140 ,
	pitch = { 95 , 105 } ,
	sound = { "jessev92/css/weapons/heat/p220/single.wav" }
} )

sound.Add( {
	name = "HEAT_P220.Draw" ,
	channel = CHAN_BODY ,
	volume = 1.0 ,
	level = 75 ,
	pitch = { 95 , 105 } ,
	sound = { "jessev92/weapons/univ/draw1.wav" }
} )

sound.Add( {
	name = "HEAT_P220.MagIn" ,
	channel = CHAN_ITEM ,
	volume = 1.0 ,
	level = 75 ,
	pitch = { 95 , 105 } ,
	sound = { "jessev92/css/weapons/heat/p220/mag_in.wav" }
} )

sound.Add( {
	name = "HEAT_P220.MagOut" ,
	channel = CHAN_ITEM ,
	volume = 1.0 ,
	level = 75 ,
	pitch = { 95 , 105 } ,
	sound = { "jessev92/css/weapons/heat/p220/mag_out.wav" }
} )

sound.Add( {
	name = "HEAT_P220.SlideRelease" ,
	channel = CHAN_ITEM ,
	volume = 1.0 ,
	level = 75 ,
	pitch = { 95 , 105 } ,
	sound = { "jessev92/css/weapons/heat/p220/slide_release.wav" }
} )

---------------------------------------------
--	H&K USP Compact
---------------------------------------------
sound.Add( {
	name = "HEAT_USPC.Single" ,
	channel = CHAN_WEAPON ,
	volume = 1.0 ,
	level = 140 ,
	pitch = { 95 , 105 } ,
	sound = { "jessev92/css/weapons/heat/uspc/single.wav" }
} )

sound.Add( {
	name = "HEAT_USPC.SingleSup" ,
	channel = CHAN_WEAPON ,
	volume = 1.0 ,
	level = 140 ,
	pitch = { 95 , 105 } ,
	sound = { "jessev92/css/weapons/heat/uspc/single_sup.wav" }
} )

sound.Add( {
	name = "HEAT_USPC.Draw" ,
	channel = CHAN_BODY ,
	volume = 1.0 ,
	level = 75 ,
	pitch = { 95 , 105 } ,
	sound = { "jessev92/css/weapons/heat/officer/draw.wav" }
} )

sound.Add( {
	name = "HEAT_USPC.MagIn" ,
	channel = CHAN_ITEM ,
	volume = 1.0 ,
	level = 75 ,
	pitch = { 95 , 105 } ,
	sound = { "jessev92/css/weapons/heat/uspc/magin.wav" }
} )

sound.Add( {
	name = "HEAT_USPC.MagOut" ,
	channel = CHAN_ITEM ,
	volume = 1.0 ,
	level = 75 ,
	pitch = { 95 , 105 } ,
	sound = { "jessev92/css/weapons/heat/uspc/magout.wav" }
} )

sound.Add( {
	name = "HEAT_USPC.SlideRelease" ,
	channel = CHAN_ITEM ,
	volume = 1.0 ,
	level = 75 ,
	pitch = { 95 , 105 } ,
	sound = { "jessev92/css/weapons/heat/uspc/sliderelease.wav" }
} )

sound.Add( {
	name = "HEAT_USPC.SlideBack1" ,
	channel = CHAN_ITEM ,
	volume = 1.0 ,
	level = 75 ,
	pitch = { 95 , 105 } ,
	sound = { "jessev92/css/weapons/heat/uspc/slideback1.wav" }
} )

sound.Add( {
	name = "HEAT_USPC.SlideBack2" ,
	channel = CHAN_ITEM ,
	volume = 1.0 ,
	level = 75 ,
	pitch = { 95 , 105 } ,
	sound = { "jessev92/css/weapons/heat/uspc/slideback2.wav" }
} )

sound.Add( {
	name = "HEAT_USPC.AttachSilencer" ,
	channel = CHAN_ITEM ,
	volume = 1.0 ,
	level = 75 ,
	pitch = { 95 , 105 } ,
	sound = { "jessev92/css/weapons/heat/uspc/sup_on.wav" }
} )

sound.Add( {
	name = "HEAT_USPC.DetachSilencer" ,
	channel = CHAN_ITEM ,
	volume = 1.0 ,
	level = 75 ,
	pitch = { 95 , 105 } ,
	sound = { "jessev92/css/weapons/heat/uspc/sup_off.wav" }
} )

---------------------------------------------
--	Beretta 92FS
---------------------------------------------
sound.Add( {
	name = "HEAT_92FS.Single" ,
	channel = CHAN_WEAPON ,
	volume = 1.0 ,
	level = 140 ,
	pitch = { 95 , 105 } ,
	sound = { "jessev92/css/weapons/heat/92fs/single.wav" }
} )

sound.Add( {
	name = "HEAT_92FS.Draw" ,
	channel = CHAN_BODY ,
	volume = 1.0 ,
	level = 75 ,
	pitch = { 95 , 105 } ,
	sound = { "jessev92/css/weapons/heat/car15/draw.wav" }
} )

sound.Add( {
	name = "HEAT_92FS.MagIn" ,
	channel = CHAN_ITEM ,
	volume = 1.0 ,
	level = 75 ,
	pitch = { 95 , 105 } ,
	sound = { "jessev92/css/weapons/heat/92fs/magin.wav" }
} )

sound.Add( {
	name = "HEAT_92FS.MagOut" ,
	channel = CHAN_ITEM ,
	volume = 1.0 ,
	level = 75 ,
	pitch = { 95 , 105 } ,
	sound = { "jessev92/css/weapons/heat/92fs/magout.wav" }
} )

sound.Add( {
	name = "HEAT_92FS.SlideBack" ,
	channel = CHAN_ITEM ,
	volume = 1.0 ,
	level = 75 ,
	pitch = { 95 , 105 } ,
	sound = { "jessev92/css/weapons/heat/92fs/slideback.wav" }
} )

sound.Add( {
	name = "HEAT_92FS.SlideForward" ,
	channel = CHAN_ITEM ,
	volume = 1.0 ,
	level = 75 ,
	pitch = { 95 , 105 } ,
	sound = { "jessev92/css/weapons/heat/92fs/slideforward.wav" }
} )

sound.Add( {
	name = "HEAT_92FS.SlideRelease" ,
	channel = CHAN_ITEM ,
	volume = 1.0 ,
	level = 75 ,
	pitch = { 95 , 105 } ,
	sound = { "jessev92/css/weapons/heat/92fs/sliderelease.wav" }
} )

------------------------------------------------------------------------------------------
--	HEAT :: Weapons :: SMG
------------------------------------------------------------------------------------------

---------------------------------------------
--	Steyr TMP
---------------------------------------------
sound.Add( {
	name = "HEAT_TMP.Single" ,
	channel = CHAN_WEAPON ,
	volume = 1.0 ,
	level = 140 ,
	pitch = { 95 , 105 } ,
	sound = "jessev92/css/weapons/heat/tmp/single.wav"
} )

sound.Add( {
	name = "HEAT_TMP.BoltPull" ,
	channel = CHAN_ITEM ,
	volume = 1.0 ,
	level = 75 ,
	sound = "jessev92/css/weapons/heat/tmp/boltpull.wav"
} )

sound.Add( {
	name = "HEAT_TMP.MagOut" ,
	channel = CHAN_ITEM ,
	volume = 1.0 ,
	level = 75 ,
	sound = "jessev92/css/weapons/heat/tmp/magout.wav"
} )

sound.Add( {
	name = "HEAT_TMP.MagIn" ,
	channel = CHAN_ITEM ,
	volume = 1.0 ,
	level = 75 ,
	sound = "jessev92/css/weapons/heat/tmp/magin.wav"
} )

sound.Add( {
	name = "HEAT_TMP.Draw" ,
	channel = CHAN_BODY ,
	volume = 1.0 ,
	level = 75 ,
	pitch = { 95 , 105 } ,
	sound = "jessev92/weapons/univ/draw1.wav"
} )

util.PrecacheSound( "jessev92/weapons/univ/draw1.wav" )

sound.Add( {
	name = "HEAT_TMP.Holster" ,
	channel = CHAN_BODY ,
	volume = 1.0 ,
	level = 75 ,
	pitch = { 95 , 105 } ,
	sound = "jessev92/weapons/univ/holster1.wav"
} )

util.PrecacheSound( "jessev92/weapons/univ/holster1.wav" )

---------------------------------------------
--	H&K MP5K
---------------------------------------------
sound.Add( {
	name = "HEAT_MP5K.Single" ,
	channel = CHAN_WEAPON ,
	volume = 1.0 ,
	level = 140 ,
	pitch = { 95 , 105 } ,
	sound = "jessev92/css/weapons/heat/mp5k/single_1.wav"
} )

sound.Add( {
	name = "HEAT_MP5K.BoltBack" ,
	channel = CHAN_ITEM ,
	volume = 1.0 ,
	level = 75 ,
	sound = "jessev92/css/weapons/heat/mp5k/bolt_back.wav"
} )

sound.Add( {
	name = "HEAT_MP5K.FiringPinForward" ,
	channel = CHAN_ITEM ,
	volume = 1.0 ,
	level = 75 ,
	sound = "jessev92/css/weapons/heat/mp5k/bolt_forward.wav"
} )

sound.Add( {
	name = "HEAT_MP5K.MagOut" ,
	channel = CHAN_ITEM ,
	volume = 1.0 ,
	level = 75 ,
	sound = "jessev92/css/weapons/heat/mp5k/mag_out.wav"
} )

sound.Add( {
	name = "HEAT_MP5K.MagIn" ,
	channel = CHAN_ITEM ,
	volume = 1.0 ,
	level = 75 ,
	sound = "jessev92/css/weapons/heat/mp5k/mag_in.wav"
} )

sound.Add( {
	name = "HEAT_MP5K.Draw" ,
	channel = CHAN_BODY ,
	volume = 1.0 ,
	level = 75 ,
	pitch = { 95 , 105 } ,
	sound = "jessev92/weapons/univ/draw1.wav"
} )

util.PrecacheSound( "jessev92/weapons/univ/draw1.wav" )

sound.Add( {
	name = "HEAT_MP5K.Holster" ,
	channel = CHAN_BODY ,
	volume = 1.0 ,
	level = 75 ,
	pitch = { 95 , 105 } ,
	sound = "jessev92/weapons/univ/holster1.wav"
} )

util.PrecacheSound( "jessev92/weapons/univ/holster1.wav" )

---------------------------------------------
--	H&K MP5A3
---------------------------------------------
sound.Add( {
	name = "HEAT_MP5A3.Single" ,
	channel = CHAN_WEAPON ,
	volume = 1.0 ,
	level = 140 ,
	pitch = { 95 , 105 } ,
	sound = "jessev92/css/weapons/heat/mp5a3/single.wav"
} )

sound.Add( {
	name = "HEAT_MP5A3.BoltPull" ,
	channel = CHAN_ITEM ,
	volume = 1.0 ,
	level = 75 ,
	sound = "jessev92/css/weapons/heat/mp5a3/boltpull.wav"
} )

sound.Add( {
	name = "HEAT_MP5A3.BoltSlap" ,
	channel = CHAN_ITEM ,
	volume = 1.0 ,
	level = 75 ,
	sound = "jessev92/css/weapons/heat/mp5a3/boltslap.wav"
} )

sound.Add( {
	name = "HEAT_MP5A3.MagOut" ,
	channel = CHAN_ITEM ,
	volume = 1.0 ,
	level = 75 ,
	sound = "jessev92/css/weapons/heat/mp5a3/magout.wav"
} )

sound.Add( {
	name = "HEAT_MP5A3.MagIn" ,
	channel = CHAN_ITEM ,
	volume = 1.0 ,
	level = 75 ,
	sound = "jessev92/css/weapons/heat/mp5a3/magin.wav"
} )

sound.Add( {
	name = "HEAT_MP5A3.Draw" ,
	channel = CHAN_BODY ,
	volume = 1.0 ,
	level = 75 ,
	pitch = { 95 , 105 } ,
	sound = "jessev92/weapons/univ/draw1.wav"
} )

util.PrecacheSound( "jessev92/weapons/univ/draw1.wav" )

sound.Add( {
	name = "HEAT_MP5A3.Holster" ,
	channel = CHAN_BODY ,
	volume = 1.0 ,
	level = 75 ,
	pitch = { 95 , 105 } ,
	sound = "jessev92/weapons/univ/holster1.wav"
} )

util.PrecacheSound( "jessev92/weapons/univ/holster1.wav" )

------------------------------------------------------------------------------------------
--	HEAT :: Weapons :: Carbines
------------------------------------------------------------------------------------------

---------------------------------------------
--	Norinco Type 56-1
---------------------------------------------
sound.Add( {
	name = "HEAT_Type56-1.Single" ,
	channel = CHAN_WEAPON ,
	volume = 1.0 ,
	level = 140 ,
	pitch = { 95 , 105 } ,
	sound = { "jessev92/css/weapons/heat/type56-1/single_1.wav" }
} )

sound.Add( {
	name = "HEAT_Type56-1.Draw" ,
	channel = CHAN_BODY ,
	volume = 1.0 ,
	level = 75 ,
	pitch = { 95 , 105 } ,
	sound = { "jessev92/css/weapons/heat/car15/draw.wav" }
} )

sound.Add( {
	name = "HEAT_Type56-1.MagIn" ,
	channel = CHAN_ITEM ,
	volume = 1.0 ,
	level = 75 ,
	pitch = { 95 , 105 } ,
	sound = { "jessev92/css/weapons/heat/type56-1/mag_in.wav" }
} )

sound.Add( {
	name = "HEAT_Type56-1.MagOut" ,
	channel = CHAN_ITEM ,
	volume = 1.0 ,
	level = 75 ,
	pitch = { 95 , 105 } ,
	sound = { "jessev92/css/weapons/heat/type56-1/mag_out.wav" }
} )

sound.Add( {
	name = "HEAT_Type56-1.BoltPull" ,
	channel = CHAN_ITEM ,
	volume = 1.0 ,
	level = 75 ,
	pitch = { 95 , 105 } ,
	sound = { "jessev92/css/weapons/heat/type56-1/bolt_pull.wav" }
} )

sound.Add( {
	name = "HEAT_Type56-1.Holster" ,
	channel = CHAN_ITEM ,
	volume = 1.0 ,
	level = 75 ,
	pitch = { 95 , 105 } ,
	sound = { "jessev92/weapons/univ/holster1.wav" }
} )

---------------------------------------------
--	Colt Model 733
---------------------------------------------
sound.Add( {
	name = "HEAT_CAR15.Single" ,
	channel = CHAN_WEAPON ,
	volume = 1.0 ,
	level = 140 ,
	pitch = { 95 , 105 } ,
	sound = { "jessev92/css/weapons/heat/car15/single_1.wav" , "jessev92/css/weapons/heat/car15/single_2.wav" }
} )

sound.Add( {
	name = "HEAT_CAR15.SingleSup" ,
	channel = CHAN_WEAPON ,
	volume = 1.0 ,
	level = 75 ,
	pitch = { 95 , 105 } ,
	sound = { "jessev92/css/weapons/heat/car15/single_sup_1.wav" , "jessev92/css/weapons/heat/car15/single_sup_2.wav" }
} )

sound.Add( {
	name = "HEAT_CAR15.Draw" ,
	channel = CHAN_BODY ,
	volume = 1.0 ,
	level = 75 ,
	pitch = { 95 , 105 } ,
	sound = { "jessev92/css/weapons/heat/car15/draw.wav" }
} )

sound.Add( {
	name = "HEAT_CAR15.MagIn" ,
	channel = CHAN_ITEM ,
	volume = 1.0 ,
	level = 75 ,
	pitch = { 95 , 105 } ,
	sound = { "jessev92/css/weapons/heat/car15/magin.wav" }
} )

sound.Add( {
	name = "HEAT_CAR15.MagOut" ,
	channel = CHAN_ITEM ,
	volume = 1.0 ,
	level = 75 ,
	pitch = { 95 , 105 } ,
	sound = { "jessev92/css/weapons/heat/car15/magout.wav" }
} )

sound.Add( {
	name = "HEAT_CAR15.BoltCatch" ,
	channel = CHAN_ITEM ,
	volume = 1.0 ,
	level = 75 ,
	pitch = { 95 , 105 } ,
	sound = { "jessev92/css/weapons/heat/car15/boltcatch.wav" }
} )

sound.Add( {
	name = "HEAT_CAR15.SuppressorOn" ,
	channel = CHAN_ITEM ,
	volume = 1.0 ,
	level = 75 ,
	pitch = { 95 , 105 } ,
	sound = { "jessev92/css/weapons/m4a1/Suppressor_On.wav" }
} )

sound.Add( {
	name = "HEAT_CAR15.SuppressorOff" ,
	channel = CHAN_ITEM ,
	volume = 1.0 ,
	level = 75 ,
	pitch = { 95 , 105 } ,
	sound = { "jessev92/css/weapons/m4a1/Suppressor_Off.wav" }
} )

------------------------------------------------------------------------------------------
--	HEAT :: Weapons :: Rifles
------------------------------------------------------------------------------------------

---------------------------------------------
--	Colt M16A1
---------------------------------------------
sound.Add( {
	name = "HEAT_M16A1.Single" ,
	channel = CHAN_WEAPON ,
	volume = 1.0 ,
	level = 140 ,
	pitch = { 95 , 105 } ,
	sound = { "jessev92/css/weapons/heat/m16a1/single.wav" }
} )

sound.Add( {
	name = "HEAT_M16A1.Draw" ,
	channel = CHAN_BODY ,
	volume = 1.0 ,
	level = 75 ,
	pitch = { 95 , 105 } ,
	sound = { "jessev92/css/weapons/heat/m16a1/draw.wav" }
} )

sound.Add( {
	name = "HEAT_M16A1.MagIn" ,
	channel = CHAN_ITEM ,
	volume = 1.0 ,
	level = 75 ,
	pitch = { 95 , 105 } ,
	sound = { "jessev92/css/weapons/heat/m16a1/magin.wav" }
} )

sound.Add( {
	name = "HEAT_M16A1.MagOut" ,
	channel = CHAN_ITEM ,
	volume = 1.0 ,
	level = 75 ,
	pitch = { 95 , 105 } ,
	sound = { "jessev92/css/weapons/heat/m16a1/magout.wav" }
} )

sound.Add( {
	name = "HEAT_M16A1.BoltPull1" ,
	channel = CHAN_ITEM ,
	volume = 1.0 ,
	level = 75 ,
	pitch = { 95 , 105 } ,
	sound = { "jessev92/css/weapons/heat/m16a1/boltpull1.wav" }
} )

sound.Add( {
	name = "HEAT_M16A1.BoltPull2" ,
	channel = CHAN_ITEM ,
	volume = 1.0 ,
	level = 75 ,
	pitch = { 95 , 105 } ,
	sound = { "jessev92/css/weapons/heat/m16a1/boltpull2.wav" }
} )

sound.Add( {
	name = "HEAT_M16A1.SlideRelease" ,
	channel = CHAN_ITEM ,
	volume = 1.0 ,
	level = 75 ,
	pitch = { 95 , 105 } ,
	sound = { "jessev92/css/weapons/heat/m16a1/sliderelease.wav" }
} )

---------------------------------------------
--	H&K HK91
---------------------------------------------
sound.Add( {
	name = "HEAT_HK91.Single" ,
	channel = CHAN_WEAPON ,
	volume = 1.0 ,
	level = 140 ,
	pitch = { 95 , 105 } ,
	sound = { "jessev92/css/weapons/heat/hk91/single_1.wav" }
} )

sound.Add( {
	name = "HEAT_HK91.Draw" ,
	channel = CHAN_BODY ,
	volume = 1.0 ,
	level = 75 ,
	pitch = { 95 , 105 } ,
	sound = { "jessev92/weapons/univ/draw1.wav" }
} )

sound.Add( {
	name = "HEAT_HK91.MagIn" ,
	channel = CHAN_ITEM ,
	volume = 1.0 ,
	level = 75 ,
	pitch = { 95 , 105 } ,
	sound = { "jessev92/css/weapons/heat/hk91/mag_in.wav" }
} )

sound.Add( {
	name = "HEAT_HK91.MagOut" ,
	channel = CHAN_ITEM ,
	volume = 1.0 ,
	level = 75 ,
	pitch = { 95 , 105 } ,
	sound = { "jessev92/css/weapons/heat/hk91/mag_out.wav" }
} )

sound.Add( {
	name = "HEAT_HK91.BoltRelease" ,
	channel = CHAN_ITEM ,
	volume = 1.0 ,
	level = 75 ,
	pitch = { 95 , 105 } ,
	sound = { "jessev92/css/weapons/heat/hk91/bolt_release.wav" }
} )

sound.Add( {
	name = "HEAT_HK91.Slide" ,
	channel = CHAN_ITEM ,
	volume = 1.0 ,
	level = 75 ,
	pitch = { 95 , 105 } ,
	sound = { "jessev92/css/weapons/heat/hk91/slide.wav" }
} )

sound.Add( {
	name = "HEAT_HK91.Holster" ,
	channel = CHAN_ITEM ,
	volume = 1.0 ,
	level = 75 ,
	pitch = { 95 , 105 } ,
	sound = { "jessev92/weapons/univ/holster1.wav" }
} )

---------------------------------------------
--	IMI Galil ARM
---------------------------------------------
sound.Add( {
	name = "HEAT_Galil.Single" ,
	channel = CHAN_WEAPON ,
	volume = 1.0 ,
	level = 140 ,
	pitch = { 95 , 105 } ,
	sound = { "jessev92/css/weapons/heat/galil/single_1.wav" }
} )

sound.Add( {
	name = "HEAT_Galil.Draw" ,
	channel = CHAN_BODY ,
	volume = 1.0 ,
	level = 75 ,
	pitch = { 95 , 105 } ,
	sound = { "jessev92/weapons/univ/draw1.wav" }
} )

sound.Add( {
	name = "HEAT_Galil.Unfold" ,
	channel = CHAN_ITEM ,
	volume = 1.0 ,
	level = 75 ,
	pitch = { 95 , 105 } ,
	sound = { "jessev92/css/weapons/heat/galil/unfold.wav" }
} )

sound.Add( {
	name = "HEAT_Galil.MagIn" ,
	channel = CHAN_ITEM ,
	volume = 1.0 ,
	level = 75 ,
	pitch = { 95 , 105 } ,
	sound = { "jessev92/css/weapons/heat/galil/mag_in.wav" }
} )

sound.Add( {
	name = "HEAT_Galil.MagOut" ,
	channel = CHAN_ITEM ,
	volume = 1.0 ,
	level = 75 ,
	pitch = { 95 , 105 } ,
	sound = { "jessev92/css/weapons/heat/galil/mag_out.wav" }
} )

sound.Add( {
	name = "HEAT_Galil.Cock" ,
	channel = CHAN_ITEM ,
	volume = 1.0 ,
	level = 75 ,
	pitch = { 95 , 105 } ,
	sound = { "jessev92/css/weapons/heat/galil/cock.wav" }
} )

---------------------------------------------
--	FN FNC-80
---------------------------------------------
sound.Add( {
	name = "HEAT_FNC.Single" ,
	channel = CHAN_WEAPON ,
	volume = 1.0 ,
	level = 140 ,
	pitch = { 95 , 105 } ,
	sound = { "jessev92/css/weapons/heat/fnc/single_1.wav" }
} )

sound.Add( {
	name = "HEAT_FNC.Draw" ,
	channel = CHAN_BODY ,
	volume = 1.0 ,
	level = 75 ,
	pitch = { 95 , 105 } ,
	sound = { "jessev92/css/weapons/heat/car15/draw.wav" }
} )

sound.Add( {
	name = "HEAT_FNC.MagIn" ,
	channel = CHAN_ITEM ,
	volume = 1.0 ,
	level = 75 ,
	pitch = { 95 , 105 } ,
	sound = { "jessev92/css/weapons/heat/fnc/mag_in.wav" }
} )

sound.Add( {
	name = "HEAT_FNC.MagOut" ,
	channel = CHAN_ITEM ,
	volume = 1.0 ,
	level = 75 ,
	pitch = { 95 , 105 } ,
	sound = { "jessev92/css/weapons/heat/fnc/mag_out.wav" }
} )

sound.Add( {
	name = "HEAT_FNC.Bolt" ,
	channel = CHAN_ITEM ,
	volume = 1.0 ,
	level = 75 ,
	pitch = { 95 , 105 } ,
	sound = { "jessev92/css/weapons/heat/fnc/bolt.wav" }
} )

sound.Add( {
	name = "HEAT_FNC.Stock" ,
	channel = CHAN_ITEM ,
	volume = 1.0 ,
	level = 75 ,
	pitch = { 95 , 105 } ,
	sound = { "jessev92/css/weapons/heat/fnc/stock.wav" }
} )

sound.Add( {
	name = "HEAT_FNC.Holster" ,
	channel = CHAN_ITEM ,
	volume = 1.0 ,
	level = 75 ,
	pitch = { 95 , 105 } ,
	sound = { "jessev92/weapons/univ/holster1.wav" }
} )

------------------------------------------------------------------------------------------
--	HEAT :: Weapons :: Shotguns
------------------------------------------------------------------------------------------

---------------------------------------------
--	Benelli M3 Super 90
---------------------------------------------
sound.Add( {
	name = "HEAT_M3S90.Dry" ,
	channel = CHAN_WEAPON ,
	volume = 1.0 ,
	level = 140 ,
	pitch = { 95 , 105 } ,
	sound = { "jessev92/weapons/univ/empty6.wav" }
} )

sound.Add( {
	name = "HEAT_M3S90.Single" ,
	channel = CHAN_WEAPON ,
	volume = 1.0 ,
	level = 140 ,
	pitch = { 95 , 105 } ,
	sound = "jessev92/css/weapons/heat/m3s90/single.wav"
} )

sound.Add( {
	name = "HEAT_M3S90.ShellInsert" ,
	channel = CHAN_WEAPON ,
	volume = 1.0 ,
	level = 140 ,
	pitch = { 95 , 105 } ,
	sound = "jessev92/css/weapons/heat/m3s90/insert.wav"
} )

sound.Add( {
	name = "HEAT_M3S90.Pump" ,
	channel = CHAN_WEAPON ,
	volume = 1.0 ,
	level = 140 ,
	pitch = { 95 , 105 } ,
	sound = "jessev92/css/weapons/heat/m3s90/pump.wav"
} )

---------------------------------------------
--	Remington Model 870
---------------------------------------------
sound.Add( {
	name = "HEAT_M870.Dry" ,
	channel = CHAN_WEAPON ,
	volume = 1.0 ,
	level = 140 ,
	pitch = { 95 , 105 } ,
	sound = { "jessev92/weapons/univ/empty6.wav" }
} )

sound.Add( {
	name = "HEAT_M870.Single" ,
	channel = CHAN_WEAPON ,
	volume = 1.0 ,
	level = 140 ,
	pitch = { 95 , 105 } ,
	sound = "jessev92/css/weapons/heat/m870/single_1.wav"
} )

sound.Add( {
	name = "HEAT_M870.InsertShell" ,
	channel = CHAN_WEAPON ,
	volume = 1.0 ,
	level = 140 ,
	pitch = { 95 , 105 } ,
	sound = "navaro/weapons/m1014/insertshell.wav"
} )

sound.Add( {
	name = "HEAT_M870.Pump" ,
	channel = CHAN_WEAPON ,
	volume = 1.0 ,
	level = 140 ,
	pitch = { 95 , 105 } ,
	sound = "jessev92/weapons/univ/cock.wav"
} )

------------------------------------------------------------------------------------------
--	HEAT :: Weapons :: Snipers
------------------------------------------------------------------------------------------

---------------------------------------------
--	H&K SR-9T
---------------------------------------------
sound.Add( {
	name = "HEAT_SR9T.Single" ,
	channel = CHAN_WEAPON ,
	volume = 1.0 ,
	level = 140 ,
	pitch = { 95 , 105 } ,
	sound = { "jessev92/css/weapons/heat/hk91/single_1.wav" }
} )

sound.Add( {
	name = "HEAT_SR9T.Safety" ,
	channel = CHAN_ITEM ,
	volume = 1.0 ,
	level = 140 ,
	pitch = { 95 , 105 } ,
	sound = { "jessev92/css/weapons/heat/hk91/safety.wav" }
} )

sound.Add( {
	name = "HEAT_SR9T.Draw" ,
	channel = CHAN_BODY ,
	volume = 1.0 ,
	level = 75 ,
	pitch = { 95 , 105 } ,
	sound = { "jessev92/weapons/univ/draw1.wav" }
} )

sound.Add( {
	name = "HEAT_SR9T.MagIn" ,
	channel = CHAN_ITEM ,
	volume = 1.0 ,
	level = 75 ,
	pitch = { 95 , 105 } ,
	sound = { "jessev92/css/weapons/heat/hk91/mag_in.wav" }
} )

sound.Add( {
	name = "HEAT_SR9T.MagOut" ,
	channel = CHAN_ITEM ,
	volume = 1.0 ,
	level = 75 ,
	pitch = { 95 , 105 } ,
	sound = { "jessev92/css/weapons/heat/hk91/mag_out.wav" }
} )

sound.Add( {
	name = "HEAT_SR9T.BoltRelease" ,
	channel = CHAN_ITEM ,
	volume = 1.0 ,
	level = 75 ,
	pitch = { 95 , 105 } ,
	sound = { "jessev92/css/weapons/heat/hk91/bolt_release.wav" }
} )

sound.Add( {
	name = "HEAT_SR9T.Slide" ,
	channel = CHAN_ITEM ,
	volume = 1.0 ,
	level = 75 ,
	pitch = { 95 , 105 } ,
	sound = { "jessev92/css/weapons/heat/hk91/slide.wav" }
} )

sound.Add( {
	name = "HEAT_SR9T.Holster" ,
	channel = CHAN_ITEM ,
	volume = 1.0 ,
	level = 75 ,
	pitch = { 95 , 105 } ,
	sound = { "jessev92/weapons/univ/holster1.wav" }
} )