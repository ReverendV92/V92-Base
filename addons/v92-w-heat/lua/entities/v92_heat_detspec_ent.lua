AddCSLuaFile( )
if not VNTCB then return false end
ENT.Base = VNT_BASE_WEAPON_ENTITY
ENT.Type = "anim"
ENT.PrintName = "Detective Special"
ENT.Author = VNTCB.author
ENT.Information = "Uses .38-Special Ammo"
ENT.Category = VNT_CATEGORY_HEAT
ENT.Spawnable = true
ENT.AdminOnly = false
ENT.WeaponName = "v92_heat_detspec_ent" -- (String) Name of this entity
ENT.SWepName = "v92_heat_detspec" -- (String) Name of the weapon entity in Lua/weapons/swep_name.lua
ENT.SEntModel = Model( "models/jessev92/css/weapons/heat/detspec_w.mdl" ) -- (String) Model to use