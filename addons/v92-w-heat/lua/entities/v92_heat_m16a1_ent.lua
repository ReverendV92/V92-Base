AddCSLuaFile( )
if not VNTCB then return false end
ENT.Base = VNT_BASE_WEAPON_ENTITY
ENT.Type = "anim"
ENT.PrintName = "M16A1"
ENT.Author = VNTCB.author
ENT.Information = "Uses 5.56x45mm NATO Ammo"
ENT.Category = VNT_CATEGORY_HEAT
ENT.Spawnable = true
ENT.AdminOnly = false
ENT.WeaponName = "v92_heat_m16a1_ent" -- (String) Name of this entity
ENT.SWepName = "v92_heat_m16a1" -- (String) Name of the weapon entity in Lua/weapons/swep_name.lua
ENT.SEntModel = Model( "models/jessev92/css/weapons/heat/m16a1_w.mdl" ) -- (String) Model to use