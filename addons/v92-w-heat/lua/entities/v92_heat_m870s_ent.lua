AddCSLuaFile( )
if not VNTCB then return false end
ENT.Base = VNT_BASE_WEAPON_ENTITY
ENT.Type = "anim"
ENT.PrintName = "M870 Polymer"
ENT.Author = VNTCB.author
ENT.Information = "Uses .12 Gauge Buckshot Ammo"
ENT.Category = VNT_CATEGORY_HEAT
ENT.Spawnable = true
ENT.AdminOnly = false
ENT.WeaponName = "v92_heat_m870s_ent" -- (String) Name of this entity
ENT.SWepName = "v92_heat_m870s" -- (String) Name of the weapon entity in Lua/weapons/swep_name.lua
ENT.SEntModel = Model( "models/jessev92/css/weapons/heat/m870_syn_w.mdl" ) -- (String) Model to use