
AddCSLuaFile( )
if not VNTCB then return false end
ENT.Base = VNT_BASE_KITBAG
ENT.Type = "anim"
ENT.PrintName = "The SWAT"
ENT.Author = VNTCB.author
ENT.Category = VNT_CATEGORY_HEAT
ENT.Information = "SWAT Units's Weapons During the Finale"
ENT.Spawnable = true
ENT.AdminOnly = false
ENT.RenderGroup = RENDERGROUP_BOTH
ENT.WeaponsToGive = { "v92_heat_92fs" , "v92_heat_mp5a3" } -- (String) Name of the weapon entity in Lua/weapons/swep_name.lua
ENT.AmmoToGive = { 
	["9x19mmnato"] = 75,
	["556x45mmnato"] = 300,
}
ENT.KitBagName = "v92_heat_kitbag_swat" -- (String) Name of this entity
ENT.KitBagModel = Model( "models/JesseV92/payday2/items/bag_loot_static.mdl" ) -- (String) Model to use
ENT.RemoveOnSpawn = false -- Remove the weapon on spawn, I.E. for the fists or unarmed SWeps
ENT.ContentCollisionSound = Sound( "BaseCombatWeapon.WeaponDrop" ) -- Physics collisions
ENT.BagCollisionSound = Sound( "Rubber.ImpactHard" ) -- Physics collisions
ENT.PickupSound = Sound( "VNTCB.Items.Zipper" ) -- Pickup sounds
