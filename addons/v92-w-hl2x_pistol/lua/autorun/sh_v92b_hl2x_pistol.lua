
---------------------------------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------------------------
--	Half-Life 2: Expanded - Pistols & Handguns
---------------------------------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------------------------

--	Catch-22 Diabolis Ex Machina Prevention

if SERVER then
	resource.AddWorkshop("505106454") 	--	V92 Code Bases
end

-- SOUND GUIDE
----------
-- Channel = Typically CHAN_WEAPON or CHAN_ITEM for gunshots and gun parts respectively; CHAN_BODY for foley
-- Volume = Leave at 1.0
-- Pitch = Typically between 95 and 105 for gunshots
-- Level = For sounds, follow this estimation table as a LOOSE guideline:
----------
--	.22LR = 60dB
--	9x19mm = 60dB
--	.38 Special = 70dB
--	5.56x45mm = 70dB
--	7.62x51mm = 90dB
--	.45 ACP = 90dB
--	.50 AE = 95dB
--	.50 BMG = 100dB
--	12-Gauge Buckshot = 120dB
--	Explosion = 200dB
--	Suppressed = 2/3 of normal
--	Empty = 60dB 
--	Weapon sounds = ~50dB (use your head)
--	Foley = ~30dB (use your head)
----------
----------


if not VNTCB then
	Error( "V92 Content Bases not mounted: Removing Autorun File" )

	return false
end

if VNTCB.Plugins then
	table.insert( VNTCB.Plugins, 0, "VNT :: HL2X Rifle Pack" )
end

------------------------------------------------------------------------------------------
--	HL2X :: Pistols & Handguns
------------------------------------------------------------------------------------------

---------------------------------------------
--	Makarov Pistol
---------------------------------------------

sound.Add( {
	name = "HL2_Makarov.Single" ,
	channel = CHAN_WEAPON ,
	volume = 1.0 ,
	level = 75 ,
	pitch = { 95 , 105 } ,
	sound = "^)jessev92/hl2/weapons/cz52/single.wav"
} )
util.PrecacheSound( "jessev92/hl2/weapons/cz52/single.wav" )

sound.Add( {
	name = "HL2_Makarov.SingleSup" ,
	channel = CHAN_WEAPON ,
	volume = 1.0 ,
	level = 60 ,
	pitch = { 95 , 105 } ,
	sound = "^)jessev92/hl2/weapons/mp5k/single_sil.wav"
} )
util.PrecacheSound( "jessev92/weapons/mp5k/single_sil.wav" )

sound.Add( {
	name = "HL2_Makarov.Reload" ,
	channel = CHAN_ITEM ,
	volume = 1.0 ,
	level = 40 ,
	--pitch = { 90 , 110 } ,
	sound = "^)jessev92/hl2/weapons/92fs/reload.wav"
} )
util.PrecacheSound( "jessev92/hl2/weapons/92fs/reload.wav" )

sound.Add( {
	name = "HL2_Makarov.Draw" ,
	channel = CHAN_BODY ,
	volume = 1.0 ,
	level = 35 ,
	pitch = { 90 , 110 } ,
	sound = "^)jessev92/weapons/univ/draw1.wav"
} )
util.PrecacheSound( "jessev92/weapons/univ/draw1.wav" )

sound.Add( {
	name = "HL2_Makarov.Holster" ,
	channel = CHAN_BODY ,
	volume = 1.0 ,
	level = 35 ,
	pitch = { 95 , 105 } ,
	sound = "^)jessev92/weapons/univ/holster1.wav"
} )
util.PrecacheSound( "jessev92/weapons/univ/holster1.wav" )

---------------------------------------------
--	H&K Mk.23 .45 ACP
---------------------------------------------

sound.Add( {
	name = "HL2_Mk23.Single" ,
	channel = CHAN_WEAPON ,
	volume = 1.0 ,
	level = 90 ,
	pitch = { 95 , 105 } ,
	sound = "^)jessev92/hl2/weapons/mk23/single.wav"
} )
util.PrecacheSound( "jessev92/hl2/weapons/mk23/single.wav" )

sound.Add( {
	name = "HL2_Mk23.Reload" ,
	channel = CHAN_ITEM ,
	volume = 1.0 ,
	level = 45 ,
	--pitch = { 95 , 105 } ,
	sound = "^)jessev92/hl2/weapons/mk23/reload.wav"
} )
util.PrecacheSound( "jessev92/hl2/weapons/mk23/reload.wav" )

sound.Add( {
	name = "HL2_Mk23.Safety" ,
	channel = CHAN_ITEM ,
	volume = 1.0 ,
	level = 35 ,
	pitch = { 95 , 105 } ,
	sound = "weapons_ins/1911/1911-safety.wav"
} )
util.PrecacheSound( "jessev92/hl2/weapons/mk23/.wav" )

sound.Add( {
	name = "HL2_Mk23.Draw" ,
	channel = CHAN_BODY ,
	volume = 1.0 ,
	level = 40 ,
	pitch = { 90 , 110 } ,
	sound = "^)jessev92/weapons/univ/draw1.wav"
} )
util.PrecacheSound( "jessev92/weapons/univ/draw1.wav" )

sound.Add( {
	name = "HL2_Mk23.Holster" ,
	channel = CHAN_BODY ,
	volume = 1.0 ,
	level = 40 ,
	pitch = { 95 , 105 } ,
	sound = "^)jessev92/weapons/univ/holster1.wav"
} )
util.PrecacheSound( "jessev92/weapons/univ/holster1.wav" )

---------------------------------------------
--	Kimber 1911 .45 ACP
---------------------------------------------

sound.Add( {
	name = "HL2_Kimber1911.Single" ,
	channel = CHAN_WEAPON ,
	volume = 1.0 ,
	level = 90 ,
	pitch = { 95 , 105 } ,
	sound = "^)jessev92/hl2/weapons/kimber1911/single.wav"
} )

util.PrecacheSound( "jessev92/hl2/weapons/kimber1911/single.wav" )

sound.Add( {
	name = "HL2_Kimber1911.MagIn" ,
	channel = CHAN_ITEM ,
	volume = 1.0 ,
	level = 40 ,
	--pitchstart			= 95,
	--pitchend			= 105,
	sound = "^)jessev92/hl2/weapons/kimber1911/magin.wav"
} )

util.PrecacheSound( "jessev92/hl2/weapons/kimber1911/magin.wav" )

sound.Add( {
	name = "HL2_Kimber1911.MagOut" ,
	channel = CHAN_ITEM ,
	volume = 1.0 ,
	level = 40 ,
	--pitch = { 95 , 105 } ,
	sound = "^)jessev92/hl2/weapons/kimber1911/magout.wav"
} )

util.PrecacheSound( "jessev92/hl2/weapons/kimber1911/magout.wav" )

sound.Add( {
	name = "HL2_Kimber1911.SlideForward" ,
	channel = CHAN_ITEM ,
	volume = 1.0 ,
	level = 45 ,
	--pitch = { 95 , 105 } ,
	sound = "^)jessev92/hl2/weapons/kimber1911/slideforward.wav"
} )

util.PrecacheSound( "jessev92/hl2/weapons/kimber1911/slideforward.wav" )

sound.Add( {
	name = "HL2_Kimber1911.SlideBack" ,
	channel = CHAN_ITEM ,
	volume = 1.0 ,
	level = 45 ,
	--pitch = { 95 , 105 } ,
	sound = "^)jessev92/hl2/weapons/kimber1911/slideback.wav"
} )

util.PrecacheSound( "jessev92/hl2/weapons/kimber1911/slideback.wav" )

sound.Add( {
	name = "HL2_Kimber1911.Safety" ,
	channel = CHAN_ITEM ,
	volume = 1.0 ,
	level = 35 ,
	pitch = { 95 , 105 } ,
	sound = "weapons_ins/1911/1911-safety.wav"
} )

sound.Add( {
	name = "HL2_Kimber1911.Draw" ,
	channel = CHAN_BODY ,
	volume = 1.0 ,
	level = 40 ,
	pitch = { 90 , 110 } ,
	sound = "^)jessev92/hl2/weapons/kimber1911/draw.wav"
} )

util.PrecacheSound( "jessev92/hl2/weapons/kimber1911/draw.wav" )

sound.Add( {
	name = "HL2_Kimber1911.Holster" ,
	channel = CHAN_BODY ,
	volume = 1.0 ,
	level = 40 ,
	pitch = { 95 , 105 } ,
	sound = "^)jessev92/hl2/weapons/kimber1911/Holster.wav"
} )
util.PrecacheSound( "jessev92/hl2/weapons/kimber1911/Holster.wav" )

---------------------------------------------
--	Glock 19
---------------------------------------------

sound.Add( {
	name = "HL2_Glock19.Single" ,
	channel = CHAN_WEAPON ,
	volume = 1.0 ,
	level = 70 ,
	pitch = { 95 , 105 } ,
	sound = "^)jessev92/hl2/weapons/glock19/single.wav"
} )
util.PrecacheSound( "jessev92/hl2/weapons/glock19/single.wav" )

sound.Add( {
	name = "HL2_Glock19.Reload" ,
	channel = CHAN_ITEM ,
	volume = 1.0 ,
	level = 45 ,
	--pitchstart			= 95,
	--pitchend			= 105,
	sound = "^)jessev92/hl2/weapons/glock19/reload.wav"
} )
util.PrecacheSound( "jessev92/hl2/weapons/glock19/reload.wav" )

sound.Add( {
	name = "HL2_Glock19.Dry" ,
	channel = CHAN_ITEM ,
	volume = 1.0 ,
	level = 60 ,
	pitch = { 95 , 105 } ,
	sound = "^)jessev92/hl2/weapons/glock19/dry.wav"
} )
util.PrecacheSound( "jessev92/hl2/weapons/glock19/dry.wav" )

sound.Add( {
	name = "HL2_Glock19.Draw" ,
	channel = CHAN_BODY ,
	volume = 1.0 ,
	level = 40 ,
	pitchstart = 90 ,
	pitchend = 110 ,
	sound = "^)jessev92/weapons/univ/draw1.wav"
} )
util.PrecacheSound( "jessev92/weapons/univ/draw1.wav" )

sound.Add( {
	name = "HL2_Glock19.Holster" ,
	channel = CHAN_BODY ,
	volume = 1.0 ,
	level = 40 ,
	pitch = { 95 , 105 } ,
	sound = "^)jessev92/weapons/univ/holster1.wav"
} )
util.PrecacheSound( "jessev92/weapons/univ/holster1.wav" )

---------------------------------------------
--	Beretta 92FS
---------------------------------------------

sound.Add( {
	name = "HL2_92FS.Single" ,
	channel = CHAN_WEAPON ,
	volume = 1.0 ,
	level = 75 ,
	pitch = { 95 , 105 } ,
	sound = "^)jessev92/hl2/weapons/92fs/single.wav"
} )
util.PrecacheSound( "jessev92/hl2/weapons/92fs/single.wav" )

sound.Add( {
	name = "HL2_92FS.Reload" ,
	channel = CHAN_ITEM ,
	volume = 1.0 ,
	level = 45 ,
	sound = "^)jessev92/hl2/weapons/92fs/reload.wav"
} )
util.PrecacheSound( "jessev92/hl2/weapons/92fs/reload.wav" )

sound.Add( {
	name = "HL2_92FS.Draw" ,
	channel = CHAN_BODY ,
	volume = 1.0 ,
	level = 40 ,
	pitch = { 95 , 105 } ,
	sound = "^)jessev92/weapons/univ/draw1.wav"
} )
util.PrecacheSound( "jessev92/weapons/univ/draw1.wav" )

sound.Add( {
	name = "HL2_92FS.Holster" ,
	channel = CHAN_BODY ,
	volume = 1.0 ,
	level = 40 ,
	pitch = { 95 , 105 } ,
	sound = "^)jessev92/weapons/univ/holster1.wav"
} )
util.PrecacheSound( "jessev92/weapons/univ/holster1.wav" )

---------------------------------------------
--	CZ-52
---------------------------------------------
sound.Add( {
	name = "HL2_CZ52.Single" ,
	channel = CHAN_WEAPON ,
	volume = 1.0 ,
	level = 75 ,
	pitch = { 95 , 105 } ,
	sound = "^)jessev92/hl2/weapons/cz52/single.wav"
} )

util.PrecacheSound( "jessev92/hl2/weapons/cz52/single.wav" )

sound.Add( {
	name = "HL2_CZ52.MagIn" ,
	channel = CHAN_ITEM ,
	volume = 1.0 ,
	level = 40 ,
	sound = "^)jessev92/hl2/weapons/cz52/magin.wav"
} )

util.PrecacheSound( "jessev92/hl2/weapons/cz52/magin.wav" )

sound.Add( {
	name = "HL2_CZ52.MagOut" ,
	channel = CHAN_ITEM ,
	volume = 1.0 ,
	level = 40 ,
	sound = "^)jessev92/hl2/weapons/cz52/MagOut.wav"
} )

util.PrecacheSound( "jessev92/hl2/weapons/cz52/MagOut.wav" )

sound.Add( {
	name = "HL2_CZ52.SlideRelease" ,
	channel = CHAN_ITEM ,
	volume = 1.0 ,
	level = 45 ,
	sound = "^)jessev92/hl2/weapons/cz52/SlideRelease.wav"
} )

util.PrecacheSound( "jessev92/hl2/weapons/cz52/SlideRelease.wav" )

sound.Add( {
	name = "HL2_CZ52.Draw" ,
	channel = CHAN_BODY ,
	volume = 1.0 ,
	level = 40 ,
	pitch = { 95 , 105 } ,
	sound = "^)jessev92/weapons/univ/draw1.wav"
} )

util.PrecacheSound( "jessev92/weapons/univ/draw1.wav" )

sound.Add( {
	name = "HL2_CZ52.Holster" ,
	channel = CHAN_BODY ,
	volume = 1.0 ,
	level = 40 ,
	pitch = { 95 , 105 } ,
	sound = "^)jessev92/weapons/univ/holster1.wav"
} )
util.PrecacheSound( "jessev92/weapons/univ/holster1.wav" )

---------------------------------------------
--	CZ-75
---------------------------------------------

sound.Add( {
	name = "HL2_CZ75.Single" ,
	channel = CHAN_WEAPON ,
	volume = 1.0 ,
	level = 75 ,
	pitch = { 95 , 105 } ,
	sound = "^)jessev92/hl2/weapons/cz75/single.wav"
} )
util.PrecacheSound( "jessev92/hl2/weapons/cz75/single.wav" )

sound.Add( {
	name = "HL2_CZ75.MagIn" ,
	channel = CHAN_ITEM ,
	volume = 1.0 ,
	level = 40 ,
	--pitchstart			= 95,
	--pitchend			= 105,
	sound = "^)jessev92/hl2/weapons/cz75/magin.wav"
} )
util.PrecacheSound( "jessev92/hl2/weapons/cz75/magin.wav" )

sound.Add( {
	name = "HL2_CZ75.MagOut" ,
	channel = CHAN_ITEM ,
	volume = 1.0 ,
	level = 40 ,
	--pitchstart			= 95,
	--pitchend			= 105,
	sound = "^)jessev92/hl2/weapons/cz75/magout.wav"
} )
util.PrecacheSound( "jessev92/hl2/weapons/cz75/magout.wav" )

sound.Add( {
	name = "HL2_CZ75.SlidePull" ,
	channel = CHAN_ITEM ,
	volume = 1.0 ,
	level = 45 ,
	--pitchstart			= 95,
	--pitchend			= 105,
	sound = "^)jessev92/hl2/weapons/cz75/SlidePull.wav"
} )
util.PrecacheSound( "jessev92/hl2/weapons/cz75/SlidePull.wav" )

sound.Add( {
	name = "HL2_CZ75.SlideRelease" ,
	channel = CHAN_ITEM ,
	volume = 1.0 ,
	level = 45 ,
	--pitchstart			= 95,
	--pitchend			= 105,
	sound = "^)jessev92/hl2/weapons/cz75/SlideRelease.wav"
} )
util.PrecacheSound( "jessev92/hl2/weapons/cz75/SlideRelease.wav" )

sound.Add( {
	name = "HL2_CZ75.Deploy2" ,
	channel = CHAN_BODY ,
	volume = 1.0 ,
	level = 40 ,
	pitchstart = 90 ,
	pitchend = 110 ,
	sound = "^)jessev92/hl2/weapons/cz75/deploy2.wav"
} )
util.PrecacheSound( "jessev92/hl2/weapons/cz75/deploy2.wav" )

sound.Add( {
	name = "HL2_CZ75.Deploy4" ,
	channel = CHAN_BODY ,
	volume = 1.0 ,
	level = 40 ,
	pitchstart = 90 ,
	pitchend = 110 ,
	sound = "^)jessev92/hl2/weapons/cz75/deploy4.wav"
} )
util.PrecacheSound( "jessev92/hl2/weapons/cz75/deploy4.wav" )

---------------------------------------------
--	H&K USP (9x19mm NATO)
---------------------------------------------

sound.Add( {
	name = "HL2_USP.Single" ,
	channel = CHAN_WEAPON ,
	volume = 1.0 ,
	level = 75 ,
	pitch = { 95 , 105 } ,
	sound = { "jessev92/hl2/weapons/usp/single1.wav" , "jessev92/hl2/weapons/usp/single2.wav" , "jessev92/hl2/weapons/usp/single3.wav" }
} )

util.PrecacheSound( "jessev92/hl2/weapons/usp/single1.wav" )
util.PrecacheSound( "jessev92/hl2/weapons/usp/single2.wav" )
util.PrecacheSound( "jessev92/hl2/weapons/usp/single3.wav" )

sound.Add( {
	name = "HL2_USP.MagIn" ,
	channel = CHAN_ITEM ,
	volume = 1.0 ,
	level = 40 ,
	--pitch = { 90 , 110 },,
	sound = { "jessev92/hl2/weapons/usp/magin1.wav" , "jessev92/hl2/weapons/usp/magin2.wav" , "jessev92/hl2/weapons/usp/magin3.wav" }
} )

util.PrecacheSound( "jessev92/hl2/weapons/usp/magin1.wav" )
util.PrecacheSound( "jessev92/hl2/weapons/usp/magin2.wav" )
util.PrecacheSound( "jessev92/hl2/weapons/usp/magin3.wav" )

sound.Add( {
	name = "HL2_USP.MagOut" ,
	channel = CHAN_ITEM ,
	volume = 1.0 ,
	level = 40 ,
	--pitch = { 90 , 110 },
	sound = { "jessev92/hl2/weapons/usp/MagOut1.wav" , "jessev92/hl2/weapons/usp/MagOut2.wav" , "jessev92/hl2/weapons/usp/MagOut3.wav" }
} )

util.PrecacheSound( "jessev92/hl2/weapons/usp/MagOut1.wav" )
util.PrecacheSound( "jessev92/hl2/weapons/usp/MagOut2.wav" )
util.PrecacheSound( "jessev92/hl2/weapons/usp/MagOut3.wav" )

sound.Add( {
	name = "HL2_USP.SlideBack" ,
	channel = CHAN_ITEM ,
	volume = 1.0 ,
	level = 45 ,
	--pitch = { 90 , 110 },
	sound = { "jessev92/hl2/weapons/usp/slideback1.wav" , "jessev92/hl2/weapons/usp/slideback2.wav" , "jessev92/hl2/weapons/usp/slideback1.wav" }
} )

util.PrecacheSound( "jessev92/hl2/weapons/usp/slideback1.wav" )
util.PrecacheSound( "jessev92/hl2/weapons/usp/slideback2.wav" )
util.PrecacheSound( "jessev92/hl2/weapons/usp/slideback3.wav" )

sound.Add( {
	name = "HL2_USP.SlideRelease" ,
	channel = CHAN_ITEM ,
	volume = 1.0 ,
	level = 45 ,
	--pitch = { 90 , 110 },
	sound = { "jessev92/hl2/weapons/usp/SlideForward1.wav" , "jessev92/hl2/weapons/usp/SlideForward2.wav" , "jessev92/hl2/weapons/usp/SlideForward3.wav" }
} )

util.PrecacheSound( "jessev92/hl2/weapons/usp/SlideForward1.wav" )
util.PrecacheSound( "jessev92/hl2/weapons/usp/SlideForward2.wav" )
util.PrecacheSound( "jessev92/hl2/weapons/usp/SlideForward3.wav" )

sound.Add( {
	name = "HL2_USP.Draw" ,
	channel = CHAN_BODY ,
	volume = 1.0 ,
	level = 30 ,
	pitch = { 90 , 110 },
	sound = "^)jessev92/weapons/univ/draw1.wav"
} )
util.PrecacheSound( "jessev92/weapons/univ/draw1.wav" )

sound.Add( {
	name = "HL2_USP.Holster" ,
	channel = CHAN_BODY ,
	volume = 1.0 ,
	level = 30 ,
	pitch = { 95 , 105 } ,
	sound = "^)jessev92/weapons/univ/holster1.wav"
} )
util.PrecacheSound( "jessev92/weapons/univ/holster1.wav" )

