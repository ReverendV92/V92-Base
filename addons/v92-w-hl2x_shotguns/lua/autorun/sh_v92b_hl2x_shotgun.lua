
---------------------------------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------------------------
--	Half-Life 2: Expanded - Shotguns
---------------------------------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------------------------

--	Catch-22 Diabolis Ex Machina Prevention

if SERVER then
	resource.AddWorkshop("505106454") 	--	V92 Code Bases
end

-- SOUND GUIDE
----------
-- Channel = Typically CHAN_WEAPON or CHAN_ITEM for gunshots and gun parts respectively; CHAN_BODY for foley
-- Volume = Leave at 1.0
-- Pitch = Typically between 95 and 105 for gunshots
-- Level = For gunshots, follow this estimation table as a LOOSE guideline:
----------
--	.22LR = 40dB
--	9x19mm = 60dB
--	.38 Special = 70dB
--	5.56x45mm = 70dB
--	7.62x51mm = 90dB
--	.45 ACP = 90dB
--	.50 AE = 95dB
--	.50 BMG = 100dB
--	12-Gauge Buckshot = 120dB
--	Explosion = 200dB
--	Suppressed = 2/3 of normal
--	Empty = 60dB 
--	Weapon sounds = ~50dB (use your head)
--	Foley = ~30dB (use your head)
----------
----------

if not VNTCB then
	Error( "V92 Content Bases not mounted :: Removing Autorun File :: HL2X Shotguns\n" )

	return false
end

if VNTCB.Plugins then
	table.insert( VNTCB.Plugins, 0, "VNT :: HL2X Shotgun Pack" )
end

------------------------------------------------------------------------------------------
--	HL2X :: Shotguns
------------------------------------------------------------------------------------------

---------------------------------------------
--	Franchi SPAS-12
---------------------------------------------

sound.Add( {
	name = "HL2X.SPAS12.Single" ,
	channel = CHAN_WEAPON ,
	volume = 1.0 ,
	level = 120 ,
	pitch = { 95 , 105 } ,
	sound = { "^)jessev92/hl2/weapons/spas12/single_1.wav" , "^)jessev92/hl2/weapons/spas12/single_2.wav" }
} )
util.PrecacheSound( "jessev92/hl2/weapons/spas12/single_1.wav" )
util.PrecacheSound( "jessev92/hl2/weapons/spas12/single_2.wav" )

sound.Add( {
	name = "HL2X.SPAS12.Cock" ,
	channel = CHAN_ITEM ,
	volume = 1.0 ,
	level = 45 ,
	pitch = { 95 , 105 } ,
	sound = "^)jessev92/hl2/weapons/spas12/cock.wav"
} )
util.PrecacheSound( "jessev92/hl2/weapons/spas12/cock.wav" )

sound.Add( {
	name = "HL2X.SPAS12.Draw" ,
	channel = CHAN_BODY ,
	volume = 1.0 ,
	level = 40 ,
	pitch = { 95 , 105 } ,
	sound = "^)jessev92/weapons/univ/throw1.wav"
} )
util.PrecacheSound( "jessev92/weapons/univ/throw1.wav" )

sound.Add( {
	name = "HL2X.SPAS12.Holster" ,
	channel = CHAN_BODY ,
	volume = 1.0 ,
	level = 40 ,
	pitch = { 95 , 105 } ,
	sound = "^)jessev92/weapons/univ/draw2.wav"
} )
util.PrecacheSound( "jessev92/weapons/univ/draw2.wav" )

sound.Add( {
	name = "HL2X.SPAS12.Foley" ,
	channel = CHAN_BODY ,
	volume = 1.0 ,
	level = 40 ,
	pitch = { 95 , 105 } ,
	sound = "^)jessev92/weapons/univ/draw1.wav"
} )
util.PrecacheSound( "jessev92/weapons/univ/draw1.wav" )

sound.Add( {
	name = "HL2X.SPAS12.ReloadStart" ,
	channel = CHAN_BODY ,
	volume = 1.0 ,
	level = 40 ,
	pitch = { 95 , 105 } ,
	sound = "^)jessev92/weapons/univ/iron_in1.wav"
} )
util.PrecacheSound( "jessev92/weapons/univ/iron_in1.wav" )

sound.Add( {
	name = "HL2X.SPAS12.ShellInsert" ,
	channel = CHAN_BODY ,
	volume = 1.0 ,
	level = 50 ,
	pitch = { 95 , 105 } ,
	sound = { "^)jessev92/hl2/weapons/spas12/shell_insert_1.wav" , "^)jessev92/hl2/weapons/spas12/shell_insert_2.wav" , "^)jessev92/hl2/weapons/spas12/shell_insert_3.wav" }
} )
util.PrecacheSound( "jessev92/hl2/weapons/spas12/shell_insert_1.wav" )
util.PrecacheSound( "jessev92/hl2/weapons/spas12/shell_insert_2.wav" )
util.PrecacheSound( "jessev92/hl2/weapons/spas12/shell_insert_3.wav" )

sound.Add( {
	name = "HL2X.SPAS12.ReloadEnd" ,
	channel = CHAN_BODY ,
	volume = 1.0 ,
	level = 40 ,
	pitch = { 95 , 105 } ,
	sound = "^)jessev92/weapons/univ/iron_out1.wav"
} )
util.PrecacheSound( "jessev92/weapons/univ/iron_out1.wav" )

---------------------------------------------
--	Benelli M3 Super 90
---------------------------------------------

sound.Add( {
	name = "HL2_M3S90.Single" ,
	channel = CHAN_WEAPON ,
	volume = 1.0 ,
	level = 120 ,
	pitch = { 95 , 105 } ,
	sound = { "^)jessev92/ins1/weapons/m1014/single_1.wav" , "^)jessev92/ins1/weapons/m1014/single_2.wav" }
} )
util.PrecacheSound( "jessev92/ins1/weapons/m1014/single_1.wav" )
util.PrecacheSound( "jessev92/ins1/weapons/m1014/single_2.wav" )

sound.Add( {
	name = "HL2_M3S90.Cock" ,
	channel = CHAN_ITEM ,
	volume = 1.0 ,
	level = 50 ,
	pitch = { 95 , 105 } ,
	sound = "^)jessev92/weapons/univ/cock.wav"
} )
util.PrecacheSound( "jessev92/weapons/univ/cock.wav" )

sound.Add( {
	name = "HL2_M3S90.Draw" ,
	channel = CHAN_BODY ,
	volume = 1.0 ,
	level = 45 ,
	pitch = { 95 , 105 } ,
	sound = "^)jessev92/weapons/univ/throw1.wav"
} )
util.PrecacheSound( "jessev92/weapons/univ/throw1.wav" )

sound.Add( {
	name = "HL2_M3S90.Holster" ,
	channel = CHAN_BODY ,
	volume = 1.0 ,
	level = 45 ,
	pitch = { 95 , 105 } ,
	sound = "^)jessev92/weapons/univ/draw2.wav"
} )
util.PrecacheSound( "jessev92/weapons/univ/draw2.wav" )

sound.Add( {
	name = "HL2_M3S90.Foley" ,
	channel = CHAN_BODY ,
	volume = 1.0 ,
	level = 45 ,
	pitch = { 95 , 105 } ,
	sound = "^)jessev92/weapons/univ/draw1.wav"
} )
util.PrecacheSound( "jessev92/weapons/univ/draw1.wav" )

sound.Add( {
	name = "HL2_M3S90.Shell1" ,
	channel = CHAN_BODY ,
	volume = 1.0 ,
	level = 45 ,
	pitch = { 95 , 105 } ,
	sound = "^)jessev92/weapons/univ/iron_in1.wav"
} )
util.PrecacheSound( "jessev92/weapons/univ/draw1.wav" )

sound.Add( {
	name = "HL2_M3S90.Shell2" ,
	channel = CHAN_BODY ,
	volume = 1.0 ,
	level = 45 ,
	pitch = { 95 , 105 } ,
	sound = { "^)jessev92/hl2/weapons/spas12/shell_insert_1.wav" , "^)jessev92/hl2/weapons/spas12/shell_insert_2.wav" , "^)jessev92/hl2/weapons/spas12/shell_insert_3.wav" }
} )
util.PrecacheSound( "jessev92/hl2/weapons/spas12/shell_insert_1.wav" )
util.PrecacheSound( "jessev92/hl2/weapons/spas12/shell_insert_2.wav" )
util.PrecacheSound( "jessev92/hl2/weapons/spas12/shell_insert_3.wav" )

sound.Add( {
	name = "HL2_M3S90.Shell3" ,
	channel = CHAN_BODY ,
	volume = 1.0 ,
	level = 45 ,
	pitch = { 95 , 105 } ,
	sound = "^)jessev92/weapons/univ/iron_out1.wav"
} )
util.PrecacheSound( "jessev92/weapons/univ/iron_out1.wav" )

---------------------------------------------
--	Remington Model 870 Shotgun
---------------------------------------------

sound.Add( {
	name = "HL2_M870.Single" ,
	channel = CHAN_WEAPON ,
	volume = 1.0 ,
	level = 120 ,
	pitch = { 95 , 105 } ,
	sound = { "^)weapons/shotgun/shotgun_fire6.wav" , "^)weapons/shotgun/shotgun_fire7.wav" }
} )
util.PrecacheSound( "weapons/shotgun/shotgun_fire6.wav" )
util.PrecacheSound( "weapons/shotgun/shotgun_fire7.wav" )

sound.Add( {
	name = "HL2_M870.Cock" ,
	channel = CHAN_ITEM ,
	volume = 1.0 ,
	level = 50 ,
	pitch = { 95 , 105 } ,
	sound = "^)jessev92/weapons/univ/cock.wav"
} )
util.PrecacheSound( "jessev92/weapons/univ/cock.wav" )

sound.Add( {
	name = "HL2_M870.Draw" ,
	channel = CHAN_BODY ,
	volume = 1.0 ,
	level = 45 ,
	pitch = { 95 , 105 } ,
	sound = "^)jessev92/weapons/univ/throw1.wav"
} )
util.PrecacheSound( "jessev92/weapons/univ/throw1.wav" )

sound.Add( {
	name = "HL2_M870.Holster" ,
	channel = CHAN_BODY ,
	volume = 1.0 ,
	level = 45 ,
	pitch = { 95 , 105 } ,
	sound = "^)jessev92/weapons/univ/draw2.wav"
} )
util.PrecacheSound( "jessev92/weapons/univ/draw2.wav" )

sound.Add( {
	name = "HL2_M870.Foley" ,
	channel = CHAN_BODY ,
	volume = 1.0 ,
	level = 45 ,
	pitch = { 95 , 105 } ,
	sound = "^)jessev92/weapons/univ/draw1.wav"
} )
util.PrecacheSound( "jessev92/weapons/univ/draw1.wav" )

sound.Add( {
	name = "HL2_M870.Shell1" ,
	channel = CHAN_BODY ,
	volume = 1.0 ,
	level = 45 ,
	pitch = { 95 , 105 } ,
	sound = "^)jessev92/weapons/univ/iron_in1.wav"
} )
util.PrecacheSound( "jessev92/weapons/univ/iron_in1.wav" )

sound.Add( {
	name = "HL2_M870.Shell2" ,
	channel = CHAN_BODY ,
	volume = 1.0 ,
	level = 40 ,
	pitch = { 95 , 105 } ,
	sound = { "^)jessev92/hl2/weapons/spas12/shell_insert_1.wav" , "^)jessev92/hl2/weapons/spas12/shell_insert_2.wav" , "^)jessev92/hl2/weapons/spas12/shell_insert_3.wav" }
} )
util.PrecacheSound( "jessev92/hl2/weapons/spas12/shell_insert_1.wav" )
util.PrecacheSound( "jessev92/hl2/weapons/spas12/shell_insert_2.wav" )
util.PrecacheSound( "jessev92/hl2/weapons/spas12/shell_insert_3.wav" )

sound.Add( {
	name = "HL2_M870.Shell3" ,
	channel = CHAN_BODY ,
	volume = 1.0 ,
	level = 45 ,
	pitch = { 95 , 105 } ,
	sound = "^)jessev92/weapons/univ/iron_out1.wav"
} )
util.PrecacheSound( "jessev92/weapons/univ/iron_out1.wav" )