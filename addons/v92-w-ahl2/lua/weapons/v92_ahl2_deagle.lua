
-------------------------------------------------------
-------------------------------------------------------
--	ACTION HALF-LIFE 2
--	DESERT EAGLE
--	.50 ACTION EXPRESS
-------------------------------------------------------
-------------------------------------------------------

SWEP.PrintName = "DESERT EAGLE" -- (String) Printed name on menu

if not VNTCB then
	Error( "V92 Content Bases not mounted; Removing Weapon: " .. SWEP.PrintName .. "\n" )
	return false
end

AddCSLuaFile( )

SWEP.Base = VNTCB.Bases.Wep -- (String) Weapon base parent this is a child of
SWEP.Spawnable = true -- (Boolean) Can be spawned via the menu
SWEP.AdminOnly = false -- (Boolean) Admin only spawnable

------------------------------------------------------
--	Client Information								--	Info used in the client block of the weapon
------------------------------------------------------

SWEP.WeaponName = "v92_ahl2_deagle" -- (String) Name of the weapon script
SWEP.WeaponEntityName = SWEP.WeaponName .. "_ent" -- (String) Name of the weapon entity in Lua/Entities/Entityname.lua
SWEP.Manufacturer = VNTCB.Manufacturer.IMI -- (String) Gun company that makes this weapon
SWEP.CountryOfOrigin = VNTCB.Country.ISR -- (String) Country of origin
SWEP.MagazineName = VNTCB.Magazine.mDEagle -- (String) The name of the magazine the weapon uses - used in my Weapon Magazine System
SWEP.Category = VNTCB.Category.AHL2 -- (String) Category
SWEP.Instructions = VNTCB.instructions -- (String) Instruction
SWEP.Author = VNTCB.author -- (String) Author
SWEP.Contact = VNTCB.contact -- (String) Contact
SWEP.Purpose = VNTCB.purpose -- (String) Purpose
SWEP.Slot = VNTCB.Bucket.Pistol -- (Integer) Bucket to place weapon in, 1 to 6
SWEP.SlotPos = VNTCB.Slot.Pistol -- (Integer) Bucket position
SWEP.ViewModelFOV = 64 -- (Integer) First-person field of view
SWEP.WorkshopID = "" -- (Integer) Workshop ID number of the upload that contains this file.

------------------------------------------------------
--	Model Information								--
------------------------------------------------------

SWEP.ViewModelFlip = false -- (Boolean) Only used for vanilla CS:S models
SWEP.ViewModel = Model( "models/jessev92/ahl2/weapons/deagle_v.mdl" ) -- (String) View model - v_*
SWEP.WorldModel = Model( "models/jessev92/ahl2/weapons/deagle_w.mdl" ) -- (String) World model - w_*
SWEP.ViewModelDefault = Model( "models/jessev92/ahl2/weapons/deagle_v.mdl" ) -- (String) View model - v_*
SWEP.WorldModelDefault = Model( "models/jessev92/ahl2/weapons/deagle_w.mdl" ) -- (String) World model - w_*
SWEP.UseHands = false -- (Boolean) Leave at false unless the model uses C_Arms
SWEP.HoldType = "ar2" -- (String) Hold type for our weapon, refer to wiki for animation sets

------------------------------------------------------
--	Gun Types										--	Set the type of weapon - ONLY PICK ONE!
------------------------------------------------------

SWEP.WeaponType = VNTCB.WeaponType.Pistol -- (Integer) 1=Melee, 2=Pistol, 3=Rifle, 4=Shotgun, 5=Sniper, 6=Grenade Launcher, 7=Rocket Launcher
SWEP.DeployableType = 6 -- (Integer) 0=None, 1=Grenade Launcher, 2=Bipod, 3=Model Swap Bipod, 4=Model Swap Stock Deploy, 5=Stock Deploy, 6=Deployable
SWEP.ViewModelLaser = Model( "models/jessev92/ahl2/weapons/deagle_v_laser.mdl" ) -- (String) View model - v_*
SWEP.WorldModelLaser = Model( "models/jessev92/ahl2/weapons/deagle_w_laser.mdl" ) -- (String) World model - w_*

------------------------------------------------------
--	Primary Fire Settings							--	Settings for the primary fire of the weapon
------------------------------------------------------

SWEP.Primary.ClipSize = VNTCB.Magazine.mDEagle[3] -- (Integer) Size of a magazine
SWEP.Primary.DefaultClip = VNTCB.Magazine.mDEagle[3] -- (Integer) Default number of ammo you spawn with
SWEP.Primary.Ammo = "50ae" -- (String) Primary ammo used by the weapon, bullets probably
SWEP.Primary.PureDmg = VNTCB.Ammo.a50AE[ 1 ] -- (Integer) Base damage, put one number here and the base will do the rest
SWEP.Primary.RPM = 175 -- (Integer) Go to a wikipedia page and look at the RPM of the weapon, then put that here - the base will do the math

------------------------------------------------------
--	Gun Mechanics									--	Various things to tweak the effects and feedback
------------------------------------------------------

SWEP.FireMode = { false , true , false , false } -- (Table: Boolean, Boolean, Boolean, Boolean ) Enable different fire modes on the weapon; Has modes, Has Single, Has Burst, Has Auto - in that order. You can have more than one, but the first must be true
SWEP.CurrentMode = 1 -- (Integer) Current fire mode of the weapon; used to set the default mode; corresponds to the FireMode table
SWEP.Weight = 2 -- (Integer) The weight in Kilogrammes of our weapon - used in my weapon weight mod!
SWEP.StrongPenetration = VNTCB.Ammo.a50AE[ 2 ] -- (Integer) Max penetration
SWEP.WeakPenetration = VNTCB.Ammo.a50AE[ 3 ] -- (Integer) Max wood penetration
SWEP.EffectiveRange = 200 -- (Integer) Effective range of the weapon in metres.
SWEP.Settings.Jamming.MeanRateOfFailure = 7000 -- (Integer) Rate of stoppages in the weapon, look up the real world number estimations and just throw that in here.

------------------------------------------------------
--	Special FX										--	Muzzle flashes, shell casings, etc
------------------------------------------------------

SWEP.MuzzleAttach = 1 -- (Integer) The number of the attachment point for muzzle flashes, typically "1"
SWEP.MuzzleFlashType = 4 -- (Integer) The number of the muzzle flash to use; see Lua/Effects/fx_muzzleflash.Lua
SWEP.ShellAttach = 2 -- (String) The name of the attachment point for shell ejections, typically "2" or "eject"
SWEP.ShellType = 5 -- (Integer) The shell to use, see Lua/Effects/FX_ShellEject for integers
SWEP.ShellDelay = 0 -- (Float) 	Time between shot firing and shell ejection; useful for bolt-actions and things like that that need a delay

------------------------------------------------------
--	Melee Settings									--
------------------------------------------------------

SWEP.MeleeAnimType = 1 -- (Integer) Melee type; 0=holdtype animation, 1=pistol whip, 2=rifle butt, 3=random between them
SWEP.MeleeRange = 70 -- (Integer) Range of melee weapon swings
SWEP.HasMeleeAttack = true -- (Boolean) Does this weapon have a pistol whip or rifle butt animation?

------------------------------------------------------
--	Custom Sounds									--	Setup sounds here!
------------------------------------------------------

SWEP.Sounds = {

	["Primary"] = Sound( "AHL2.DEagle.Fire" ), -- (String) Primary shoot sound
	["Primary_Dry"] = Sound( "AHL2.DEagle.FireEmpty" ), -- (String) Primary dry fire sound

	["WhipHitCharacter"] = Sound( "AHL2.Melee.HitBody" ) , -- (String) Sound for pistol whip hits
	["WhipHitWall"] = Sound( "AHL2.Melee.HitWorld" ) , -- (String) Sound for pistol whip hits
	["WhipMiss"] = Sound( "AHL2.Melee.Swing" ) , -- (String) Sound for pistol whip misses
	
	["Noise_Close"] = Sound( "BF3.BulletCraft.Noise.Forest.Close" ),
	["Noise_Distant"] = Sound( "BF3.BulletCraft.Noise.Forest.Distant" ),
	["Noise_Far"] = Sound( "BF3.BulletCraft.Noise.Forest.Far" ),
	["CoreBass_Close"] = Sound( "BF3.BulletCraft.CoreBass.Close.OneShot_2" ),
	["CoreBass_Distant"] = Sound( "BF3.BulletCraft.CoreBass.Distant.OneShot_1" ),
	["HiFi"] = Sound( "BF3.BulletCraft.HiFi.Pistol_1" ),
	["Reflection_Close"] = Sound( "BF3.BulletCraft.Reflection.Forest.Close" ),
	["Reflection_Far"] = Sound( "BF3.BulletCraft.Reflection.Forest.Far" ),
	
}

SWEP.ReloadSNDDelay = 0.14 -- (Float) Reload sound delay
SWEP.SelectorSwitchSNDType = 2 -- (Integer) 1=US , 2=RU
SWEP.UsesSuperSonicAmmo = true -- (Boolean) Is the weapon using supersonic or subsonic ammo?

------------------------------------------------------
--	Ironsight & Run Positions						--	Set our model transforms for running and ironsights
------------------------------------------------------

SWEP.IronSightsPos = Vector(-6.47, -7, 2.16)
SWEP.IronSightsAng = Vector(0, 0, 0)

SWEP.RunArmOffset = Vector(0, -20, -13)
SWEP.RunArmAngle = Vector(70, 0, 0)

------------------------------------------------------
--	Setup Clientside Info							--	This block must be in every weapon!
------------------------------------------------------

if CLIENT then

	SWEP.WepSelectIcon = surface.GetTextureID( "vgui/hud/" .. SWEP.WeaponName )
	SWEP.RenderGroup = RENDERGROUP_BOTH
	language.Add( SWEP.WeaponName , SWEP.PrintName )
	killicon.Add( SWEP.WeaponName , "vgui/entities/" .. SWEP.WeaponName , Color( 255 , 255 , 255 ) )

elseif SERVER then

	resource.AddWorkshop( SWEP.WorkshopID )

end

------------------------------------------------------
--	SWEP:Initialize() 							--	Called when the weapon is first loaded
------------------------------------------------------
function SWEP:Initialize( )

	self.HoldMeRight = VNTCB.HoldType.Pistol -- (String) Hold type table for our weapon, Lua/autorun/sh_v92_base_swep.Lua

end
