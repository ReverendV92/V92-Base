
-------------------------------------------------------
-------------------------------------------------------
--	ACTION HALF-LIFE 2
--	SUB-MACHINE GUN
--	9x19mm NATO
-------------------------------------------------------
-------------------------------------------------------

SWEP.PrintName = "SUBMACHINE GUN" -- (String) Printed name on menu

if not VNTCB then
	Error( "V92 Content Bases not mounted; Removing Weapon: " .. SWEP.PrintName .. "\n" )
	return false
end

AddCSLuaFile( )

SWEP.Base = VNTCB.Bases.Wep -- (String) Weapon base parent this is a child of
SWEP.Spawnable = true -- (Boolean) Can be spawned via the menu
SWEP.AdminOnly = false -- (Boolean) Admin only spawnable

------------------------------------------------------
--	Client Information								--	Info used in the client block of the weapon
------------------------------------------------------

SWEP.WeaponName = "v92_ahl2_mp5k" -- (String) Name of the weapon script
SWEP.WeaponEntityName = SWEP.WeaponName .. "_ent" -- (String) Name of the weapon entity in Lua/Entities/Entityname.lua
SWEP.Manufacturer = VNTCB.Manufacturer.HK -- (String) Gun company that makes this weapon
SWEP.CountryOfOrigin = VNTCB.Country.DEW -- (String) Country of origin
SWEP.MagazineName = VNTCB.Magazine.mMP5 -- (String) The name of the magazine the weapon uses - used in my Weapon Magazine System
SWEP.Category = VNTCB.Category.AHL2 -- (String) Category
SWEP.Instructions = VNTCB.instructions -- (String) Instruction
SWEP.Author = VNTCB.author -- (String) Author
SWEP.Contact = VNTCB.contact -- (String) Contact
SWEP.Purpose = VNTCB.purpose -- (String) Purpose
SWEP.Slot = VNTCB.Bucket.SMG -- (Integer) Bucket to place weapon in, 1 to 6
SWEP.SlotPos = VNTCB.Slot.SMG -- (Integer) Bucket position
SWEP.ViewModelFOV = 64 -- (Integer) First-person field of view
SWEP.WorkshopID = "" -- (Integer) Workshop ID number of the upload that contains this file.

------------------------------------------------------
--	Model Information								--
------------------------------------------------------

SWEP.ViewModelFlip = false -- (Boolean) Only used for vanilla CS:S models
SWEP.ViewModel = Model( "models/jessev92/ahl2/weapons/mp5k_v.mdl" ) -- (String) View model - v_*
SWEP.WorldModel = Model( "models/jessev92/ahl2/weapons/mp5k_w.mdl" ) -- (String) World model - w_*
SWEP.ViewModelDefault = Model( "models/jessev92/ahl2/weapons/mp5k_v.mdl" ) -- (String) View model - v_*
SWEP.WorldModelDefault = Model( "models/jessev92/ahl2/weapons/mp5k_w.mdl" ) -- (String) World model - w_*
SWEP.UseHands = false -- (Boolean) Leave at false unless the model uses C_Arms
SWEP.HoldType = "smg" -- (String) Hold type for our weapon, refer to wiki for animation sets

------------------------------------------------------
--	Gun Types										--	Set the type of weapon - ONLY PICK ONE!
------------------------------------------------------

SWEP.WeaponType = VNTCB.WeaponType.SMG -- (Integer) 1=Melee, 2=Pistol, 3=Rifle, 4=Shotgun, 5=Sniper, 6=Grenade Launcher, 7=Rocket Launcher
SWEP.DeployableType = 6 -- (Integer) 0=None, 1=Grenade Launcher, 2=Bipod, 3=Model Swap Bipod, 4=Model Swap Stock Deploy, 5=Stock Deploy, 6=Deployable
SWEP.ViewModelLaser = Model( "models/jessev92/ahl2/weapons/mp5k_v_laser.mdl" ) -- (String) Laser View model - v_*
SWEP.WorldModelLaser = Model( "models/jessev92/ahl2/weapons/mp5k_w_laser.mdl" ) -- (String) Laser World model - w_*
SWEP.SuppressorType = 1 -- (Integer) 0=No Suppressor, 1=Model Swap, 2=Animation
SWEP.ViewModelSuppressed = Model( "models/jessev92/ahl2/weapons/mp5k_v_suppressed.mdl" ) -- (String) The Suppressed view model
SWEP.WorldModelSuppressed = Model( "models/jessev92/ahl2/weapons/mp5k_w_suppressed.mdl" ) -- (String) The Suppressed world model
SWEP.ViewModelLaserSuppressed = Model( "models/jessev92/ahl2/weapons/mp5k_v_laser_suppressed.mdl" ) -- (String) The Laser AND Suppressed view model
SWEP.WorldModelLaserSuppressed = Model( "models/jessev92/ahl2/weapons/mp5k_w_laser_suppressed.mdl" ) -- (String) The Laser AND Suppressed world model

------------------------------------------------------
--	Primary Fire Settings							--	Settings for the primary fire of the weapon
------------------------------------------------------

SWEP.Primary.ClipSize = VNTCB.Magazine.mMP5[3] -- (Integer) Size of a magazine
SWEP.Primary.DefaultClip = VNTCB.Magazine.mMP5[3] -- (Integer) Default number of ammo you spawn with
SWEP.Primary.Ammo = "9x19mm_fmj" -- (String) Primary ammo used by the weapon, bullets probably
SWEP.Primary.PureDmg = VNTCB.Ammo.a9x19mmNATO[ 1 ] -- (Integer) Base damage, put one number here and the base will do the rest
SWEP.Primary.RPM = 900 -- (Integer) Go to a wikipedia page and look at the RPM of the weapon, then put that here - the base will do the math

------------------------------------------------------
--	Gun Mechanics									--	Various things to tweak the effects and feedback
------------------------------------------------------

SWEP.FireMode = { true , true , true , true } -- (Table: Boolean, Boolean, Boolean, Boolean ) Enable different fire modes on the weapon; Has modes, Has Single, Has Burst, Has Auto - in that order. You can have more than one, but the first must be true
SWEP.CurrentMode = 3 -- (Integer) Current fire mode of the weapon; used to set the default mode; corresponds to the FireMode table
SWEP.Weight = 2 -- (Integer) The weight in Kilogrammes of our weapon - used in my weapon weight mod!
SWEP.StrongPenetration = VNTCB.Ammo.a9x19mmNATO[ 2 ] -- (Integer) Max penetration
SWEP.WeakPenetration = VNTCB.Ammo.a9x19mmNATO[ 3 ] -- (Integer) Max wood penetration
SWEP.EffectiveRange = 200 -- (Integer) Effective range of the weapon in metres.
SWEP.Settings.Jamming.MeanRateOfFailure = 5000 -- (Integer) Rate of stoppages in the weapon, look up the real world number estimations and just throw that in here.
SWEP.Settings.FireModes.Burst.Count = 3 -- (Integer) Amounts of shots to be fired by burst
SWEP.BurstDelay = 0.05 -- (Float) Time between bolt cycles in the burst

------------------------------------------------------
--	Special FX										--	Muzzle flashes, shell casings, etc
------------------------------------------------------

SWEP.MuzzleAttach = 1 -- (Integer) The number of the attachment point for muzzle flashes, typically "1"
SWEP.MuzzleFlashType = 4 -- (Integer) The number of the muzzle flash to use; see Lua/Effects/fx_muzzleflash.Lua
SWEP.ShellAttach = 2 -- (String) The name of the attachment point for shell ejections, typically "2" or "eject"
SWEP.ShellType = 14 -- (Integer) The shell to use, see Lua/Effects/FX_ShellEject for integers
SWEP.ShellDelay = 0 -- (Float) 	Time between shot firing and shell ejection; useful for bolt-actions and things like that that need a delay

------------------------------------------------------
--	Melee Settings									--
------------------------------------------------------

SWEP.MeleeAnimType = 1 -- (Integer) Melee type; 0=holdtype animation, 1=pistol whip, 2=rifle butt, 3=random between them
SWEP.MeleeRange = 70 -- (Integer) Range of melee weapon swings
SWEP.HasMeleeAttack = true -- (Boolean) Does this weapon have a pistol whip or rifle butt animation?

------------------------------------------------------
--	Custom Sounds									--	Setup sounds here!
------------------------------------------------------

SWEP.Sounds = {

	["Primary"] = Sound( "AHL2.MP5k.Fire" ), -- (String) Primary shoot sound
	["PrimarySup"] = Sound( "AHL2.MP5k.FireSuppressed" ) , -- (String) Primary suppressed shoot sound
	["Primary_Dry"] = Sound( "AHL2.MP5k.FireEmpty" ), -- (String) Primary dry fire sound

	["WhipHitCharacter"] = Sound( "AHL2.Melee.HitBody" ) , -- (String) Sound for pistol whip hits
	["WhipHitWall"] = Sound( "AHL2.Melee.HitWorld" ) , -- (String) Sound for pistol whip hits
	["WhipMiss"] = Sound( "AHL2.Melee.Swing" ) , -- (String) Sound for pistol whip misses
	
	["Noise_Close"] = Sound( "BF3.BulletCraft.Noise.Forest.Close" ),
	["Noise_Distant"] = Sound( "BF3.BulletCraft.Noise.Forest.Distant" ),
	["Noise_Far"] = Sound( "BF3.BulletCraft.Noise.Forest.Far" ),
	["CoreBass_Close"] = Sound( "BF3.BulletCraft.CoreBass.Close.OneShot_2" ),
	["CoreBass_Distant"] = Sound( "BF3.BulletCraft.CoreBass.Distant.OneShot_1" ),
	["HiFi"] = Sound( "BF3.BulletCraft.HiFi.Pistol_1" ),
	["Reflection_Close"] = Sound( "BF3.BulletCraft.Reflection.Forest.Close" ),
	["Reflection_Far"] = Sound( "BF3.BulletCraft.Reflection.Forest.Far" ),
	
}

SWEP.ReloadSNDDelay = 0.14 -- (Float) Reload sound delay
SWEP.SelectorSwitchSNDType = 2 -- (Integer) 1=US , 2=RU
SWEP.UsesSuperSonicAmmo = true -- (Boolean) Is the weapon using supersonic or subsonic ammo?

------------------------------------------------------
--	Ironsight & Run Positions						--	Set our model transforms for running and ironsights
------------------------------------------------------

SWEP.IronSightsPos = Vector(-9.24, -9.849, 7.599)
SWEP.IronSightsAng = Vector(0, 0, -52.061)

SWEP.RunArmOffset = Vector(6.834, -14.271, 0)
SWEP.RunArmAngle = Vector(0, 70, 0)

------------------------------------------------------
--	Setup Clientside Info							--	This block must be in every weapon!
------------------------------------------------------

if CLIENT then

	SWEP.WepSelectIcon = surface.GetTextureID( "vgui/hud/" .. SWEP.WeaponName )
	SWEP.RenderGroup = RENDERGROUP_BOTH
	language.Add( SWEP.WeaponName , SWEP.PrintName )
	killicon.Add( SWEP.WeaponName , "vgui/entities/" .. SWEP.WeaponName , Color( 255 , 255 , 255 ) )

elseif SERVER then

	resource.AddWorkshop( SWEP.WorkshopID )

end

------------------------------------------------------
--	SWEP:Initialize() 							--	Called when the weapon is first loaded
------------------------------------------------------
function SWEP:Initialize( )

	self.HoldMeRight = VNTCB.HoldType.ForeGrip -- (String) Hold type table for our weapon, Lua/autorun/sh_v92_base_swep.Lua

end
