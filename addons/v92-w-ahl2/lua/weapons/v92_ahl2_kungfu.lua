
-------------------------------------------------------
--	ACTION HALF-LIFE 2
--	KUNG-FU
-------------------------------------------------------

SWEP.PrintName = "KUNG-FU" -- (String) Printed name on menu

if not VNTCB then
	Error( "V92 Content Bases not mounted; Removing Weapon: " .. SWEP.PrintName .. "\n" )
	return false
end

AddCSLuaFile( )

SWEP.Base = VNTCB.Bases.WepMelee -- (String) Weapon base parent this is a child of
SWEP.Spawnable = false -- (Boolean) Can be spawned via the menu
SWEP.AdminOnly = false -- (Boolean) Admin only spawnable

------------------------------------------------------
--	Client Information								--	Info used in the client block of the weapon
------------------------------------------------------

SWEP.WeaponName = "v92_ahl2_kungfu" -- (String) Name of the weapon script
SWEP.WeaponEntityName = SWEP.WeaponName .. "_ent" -- (String) Name of the weapon entity in Lua/Entities/Entityname.lua
SWEP.Manufacturer = VNTCB.Manufacturer.VAR -- (String) Gun company that makes this weapon
SWEP.CountryOfOrigin = VNTCB.Country.VAR -- (String) Country of origin
SWEP.Category = VNTCB.Category.AHL2 -- (String) Category
SWEP.Instructions = "Slice'n'dice!" -- (String) Instruction
SWEP.Author = VNTCB.author -- (String) Author
SWEP.Contact = VNTCB.contact -- (String) Contact
SWEP.Slot = VNTCB.Bucket.Melee -- (Integer) Bucket to place weapon in, 1 to 6
SWEP.SlotPos = VNTCB.Slot.Melee -- (Integer) Bucket position
SWEP.InventorySlot = VNTCB.Type.Melee -- (String) Weapon type, See autorun/sh_v92_code_base.lua; Primary, Secondary, Melee, GrenadeOne, GrenadeTwo, Mine, Supply, None
SWEP.ViewModelFOV = 54 -- (Integer) First-person field of view
SWEP.WorkshopID = "" -- (Integer) Workshop ID number of the upload that contains this file.

------------------------------------------------------
--	Model Information								--
------------------------------------------------------

SWEP.ViewModelFlip = false -- (Boolean) Only used for vanilla CS:S models
SWEP.ViewModel = Model( "models/jessev92/ahl2/weapons/kungfu_v.mdl" ) -- (String) View model - v_*
SWEP.WorldModel = ""  -- (String) World model - w_*
SWEP.UseHands = true -- (Boolean) Leave at false unless the model uses C_Arms
SWEP.HoldType = "fists" -- (String) Hold type for our weapon, refer to wiki for animation sets

------------------------------------------------------
--	Gun Types										--	Set the type of weapon
------------------------------------------------------

SWEP.WeaponType = VNTCB.WeaponType.Melee -- (Integer) 1=Melee, 2=Pistol, 3=Rifle, 4=Shotgun, 5=Sniper, 6=Grenade Launcher, 7=Rocket Launcher

------------------------------------------------------
--	Primary Fire Settings							--	Settings for the primary fire of the weapon
------------------------------------------------------

SWEP.Primary.ClipSize = -1 -- (Integer) Size of a magazine
SWEP.Primary.DefaultClip = 0 -- (Integer) Default number of ammo you spawn with
SWEP.Primary.Ammo = "none" -- (String) Primary ammo used by the weapon, bullets probably
SWEP.Primary.RPM = 200 -- (Integer) Go to a wikipedia page and look at the RPM of the weapon, then put that here - the base will do the math
SWEP.Primary.PureDmg = 25 -- (Integer) Base damage, put one number here and the base will do the rest

------------------------------------------------------
--	Secondary Fire Settings							--	Settings for the Secondary fire of the weapon
------------------------------------------------------

SWEP.Secondary.ClipSize = -1 -- (Integer) Size of a magazine
SWEP.Secondary.DefaultClip = 0 -- (Integer) Default number of ammo you spawn with
SWEP.Secondary.Ammo = "none" -- (String) Secondary ammo used by the weapon, bullets probably
SWEP.Secondary.RPM = 50 -- (Integer) Go to a wikipedia page and look at the RPM of the weapon, then put that here - the base will do the math
SWEP.Secondary.PureDmg = 50 -- (Integer) Base damage, put one number here and the base will do the rest

------------------------------------------------------
--	Gun Mechanics									--	Various things to tweak the effects and feedback
------------------------------------------------------

SWEP.Weight = 1 -- (Integer) The weight in Kilogrammes of our weapon - used in my weapon weight mod!
SWEP.EffectiveRange = 1 -- (Integer) Effective range of the weapon in metres.
SWEP.Settings.Jamming.MeanRateOfFailure = 0 -- (Integer) Rate of stoppages in the weapon, look up the real world number estimations and just throw that in here.

------------------------------------------------------
--	Melee Settings									--
------------------------------------------------------

SWEP.MeleeAnimType = 0 -- (Integer) Melee type; 0=holdtype animation, 1=pistol whip, 2=rifle butt
SWEP.MeleeRange = 55 -- (Integer) Range of melee weapon swings
SWEP.HasMeleeAttack = true -- (Boolean) Does this weapon have a pistol whip or rifle butt animation?
SWEP.AltFireMelee = false -- (Boolean) Is the alt fire a melee attack?
SWEP.IsBladedMelee = false -- (Boolean) Is the melee a blade?

------------------------------------------------------
--	Custom Sounds									--	Setup sounds here!
------------------------------------------------------

SWEP.Sounds = {

	["MeleeHitWall"] = Sound( "AHL2.KungFu.PunchWall" ) , --
	["MeleeHitCharacter"] = Sound( "AHL2.KungFu.PunchHit" ) , --
	["MeleeMissed"] = Sound( "AHL2.KungFu.Punch" ) , --

}

------------------------------------------------------
--	Ironsight & Run Positions						--	Set our model transforms for running and ironsights
------------------------------------------------------

SWEP.IronSightsPos = Vector( 0 , 0 , 0 ) -- (Vector) Ironsight XYZ Transform
SWEP.IronSightsAng = Vector( 0 , 0 , 0 ) -- (Vector) Ironsight XYZ Rotation
SWEP.RunArmOffset = Vector(0, 0, 0)
SWEP.RunArmAngle = Vector(-20, 0, 0)

------------------------------------------------------
--	Setup Clientside Info							--	This block must be in every weapon!
------------------------------------------------------
if CLIENT then

	SWEP.WepSelectIcon = surface.GetTextureID( "vgui/hud/" .. SWEP.WeaponName )
	SWEP.RenderGroup = RENDERGROUP_BOTH
	language.Add( SWEP.WeaponName , SWEP.PrintName )
	killicon.Add( SWEP.WeaponName , "vgui/entities/" .. SWEP.WeaponName , Color( 255 , 255 , 255 ) )
	
end
------------------------------------------------------
--	SWEP:Initialize() 							--	Called when the weapon is first loaded
------------------------------------------------------
function SWEP:Initialize( )

	self.HoldMeRight = VNTCB.HoldType.Fists -- (String) Hold type table for our weapon, Lua/autorun/sh_v92_base_swep.Lua

end
