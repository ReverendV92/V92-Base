
-------------------------------------------------------
-------------------------------------------------------
--	ACTION HALF-LIFE 2
--	SNIPER RIFLE
--	8.6x70mm LAPUA MAGNUM
-------------------------------------------------------
-------------------------------------------------------

SWEP.PrintName = "SNIPER RIFLE" -- (String) Printed name on menu

if not VNTCB then
	Error( "V92 Content Bases not mounted; Removing Weapon: " .. SWEP.PrintName .. "\n" )
	return false
end

AddCSLuaFile( )

SWEP.Base = VNT_BASE_WEAPON -- (String) Weapon base parent this is a child of
SWEP.Spawnable = true -- (Boolean) Can be spawned via the menu
SWEP.AdminOnly = false -- (Boolean) Admin only spawnable

------------------------------------------------------
--	Client Information								
--	Info used in the client block of the weapon
------------------------------------------------------

SWEP.Settings.Weapon = "v92_ahl2_sniper" -- (String) Name of the weapon script
SWEP.WeaponEntityName = SWEP.Settings.Weapon .. "_ent" -- (String) Name of the weapon entity in Lua/Entities/Entityname.lua
SWEP.Manufacturer = VNT_WEAPON_MANUFACTURER_ACCURACYINTL -- (String) Gun company that makes this weapon
SWEP.CountryOfOrigin = VNT_WEAPON_COUNTRY_UNITEDKINGDOM -- (String) Country of origin
SWEP.MagazineName = VNTCB.Magazine.mAWM -- (String) The name of the magazine the weapon uses - used in my Weapon Magazine System
SWEP.Category = VNT_CATEGORY_AHL2 -- (String) Category
SWEP.Instructions = VNTCB.instructions -- (String) Instruction
SWEP.Author = VNTCB.author -- (String) Author
SWEP.Contact = VNTCB.contact -- (String) Contact
SWEP.Purpose = VNTCB.purpose -- (String) Purpose
SWEP.Slot = VNT_WEAPON_BUCKETPOS_SNIPER -- (Integer) Bucket to place weapon in, 1 to 6
SWEP.SlotPos = VNT_WEAPON_SLOTPOS_SNIPER -- (Integer) Bucket position
SWEP.ViewModelFOV = 64 -- (Integer) First-person field of view
SWEP.Settings.WorkshopID = "" -- (Integer) Workshop ID number of the upload that contains this file.

------------------------------------------------------
--	Model Information								
--
------------------------------------------------------

SWEP.ViewModelFlip = false -- (Boolean) Only used for vanilla CS:S models
SWEP.ViewModel = Model( "models/jessev92/ahl2/weapons/sniper_v.mdl" ) -- (String) View model - v_*
SWEP.WorldModel = Model( "models/jessev92/ahl2/weapons/sniper_w.mdl" ) -- (String) World model - w_*
SWEP.ViewModelDefault = Model( "models/jessev92/ahl2/weapons/sniper_v.mdl" ) -- (String) View model - v_*
SWEP.WorldModelDefault = Model( "models/jessev92/ahl2/weapons/sniper_w.mdl" ) -- (String) World model - w_*
SWEP.UseHands = false -- (Boolean) Leave at false unless the model uses C_Arms
SWEP.HoldType = "ar2" -- (String) Hold type for our weapon, refer to wiki for animation sets

------------------------------------------------------
--	Gun Types										
--	Set the type of weapon - ONLY PICK ONE!
------------------------------------------------------


SWEP.SuppressorType = 1 -- (Integer) 0=No Suppressor, 1=Model Swap, 2=Animation
SWEP.ViewModelSuppressed = Model( "models/jessev92/ahl2/weapons/sniper_v_suppressor.mdl" ) -- (String) The Suppressed view model
SWEP.WorldModelSuppressed = Model( "models/jessev92/ahl2/weapons/sniper_w_suppressor.mdl" ) -- (String) The Suppressed world model

------------------------------------------------------
--	Sniper Settings									
--	Settings for sniper rifles
------------------------------------------------------

SWEP.ScopeType = 1 -- (Integer) Type of scope, 0=none, 1=overlay, 2=Render Target
SWEP.ScopeMaterial = 4 -- (Integer) Type of overlay, 0=Red Dot, 1=EOTech, 2=ACOG, 3=SVD, 4=M14, 5=L42A1
SWEP.ScopeMultipliers = { 1 , 0.2 , 0.6 } -- (Table: Float, Float, Float) Zoom Muliplier, Default Zoom x, Close Zoom x
SWEP.BoltAction = true -- (Boolean) Is this bolt action? Removes the player from the scope after firing
SWEP.ReturnToScope = true -- (Boolean) Return to scope after cycling the bolt?
SWEP.HasVariableZoom = true -- (Boolean) Does the weapon have variable zoom?
SWEP.ManualCocking = true -- (Boolean) Does the weapon have the bolt pull assigned to ACT_VM_PULLPIN

------------------------------------------------------
--	Primary Fire Settings							
--	Settings for the primary fire of the weapon
------------------------------------------------------

SWEP.Primary.ClipSize = VNTCB.Magazine.mAWM[3] -- (Integer) Size of a magazine
SWEP.Primary.DefaultClip = VNTCB.Magazine.mAWM[3] -- (Integer) Default number of ammo you spawn with
SWEP.Primary.Ammo = "86x70mm" -- (String) Primary ammo used by the weapon, bullets probably
SWEP.Primary.PureDmg = VNTCB.Ammo.a86x70mm[ 1 ] -- (Integer) Base damage, put one number here and the base will do the rest
SWEP.Primary.RPM = 150 -- (Integer) Go to a wikipedia page and look at the RPM of the weapon, then put that here - the base will do the math

------------------------------------------------------
--	Gun Mechanics									
--	Various things to tweak the effects and feedback
------------------------------------------------------

SWEP.FireMode = { false , true , false , false } -- (Table: Boolean, Boolean, Boolean, Boolean ) Enable different fire modes on the weapon; Has modes, Has Single, Has Burst, Has Auto - in that order. You can have more than one, but the first must be true
SWEP.CurrentMode = 1 -- (Integer) Current fire mode of the weapon; used to set the default mode; corresponds to the FireMode table
SWEP.Weight = 4 -- (Integer) The weight in Kilogrammes of our weapon - used in my weapon weight mod!
SWEP.StrongPenetration = VNTCB.Ammo.a86x70mm[ 2 ] -- (Integer) Max penetration
SWEP.WeakPenetration = VNTCB.Ammo.a86x70mm[ 3 ] -- (Integer) Max wood penetration
SWEP.EffectiveRange = 1500 -- (Integer) Effective range of the weapon in metres.
SWEP.Settings.Jamming.MeanRateOfFailure = 7000 -- (Integer) Rate of stoppages in the weapon, look up the real world number estimations and just throw that in here.
SWEP.InvertedReloadEnumNames = false -- (Boolean) Does this weapon have inverted reload enumerations? I.E. the empty reload is named ACT_VM_RELOAD and the normal is named ACT_VM_RELOAD_EMPTY

------------------------------------------------------
--	Special FX										
--	Muzzle flashes, shell casings, etc
------------------------------------------------------

SWEP.MuzzleAttach = 1 -- (Integer) The number of the attachment point for muzzle flashes, typically "1"
SWEP.MuzzleFlashType = 4 -- (Integer) The number of the muzzle flash to use; see Lua/Effects/fx_muzzleflash.Lua
SWEP.ShellAttach = 2 -- (String) The name of the attachment point for shell ejections, typically "2" or "eject"
SWEP.ShellType = 18 -- (Integer) The shell to use, see Lua/Effects/FX_ShellEject for integers
SWEP.ShellDelay = 0.5 -- (Float) 	Time between shot firing and shell ejection; useful for bolt-actions and things like that that need a delay

------------------------------------------------------
--	Attachment Settings
------------------------------------------------------

SWEP.EnableAttachments = true -- (Boolean) Toggle attachments
-- Table: DefaultToggle Boolean , Slot Enum , Table:{ View Model Bodygroup, View Model Subgroup Off , View Model Subgroup On} , Table:{ World Model Bodygroup, World Model Subgroup Off , World Model Subgroup On}
SWEP.AttachmentSuppressor = { false , VNT_WEAPON_ATTACHMENT_SUPPRESSOR , { 2 , 0 , 1 } , { 1 , 0 , 1 } }

------------------------------------------------------
--	Melee Settings
------------------------------------------------------

SWEP.MeleeAnimType = 1 -- (Integer) Melee type; 0=holdtype animation, 1=pistol whip, 2=rifle butt, 3=random between them
SWEP.MeleeRange = 70 -- (Integer) Range of melee weapon swings
SWEP.HasMeleeAttack = true -- (Boolean) Does this weapon have a pistol whip or rifle butt animation?

------------------------------------------------------
--	Custom Sounds								
--	Setup sounds here!
------------------------------------------------------

SWEP.Sounds = {

	["Primary"] = Sound( "AHL2.Sniper.Fire" ), -- (String) Primary shoot sound
	["PrimarySup"] = Sound( "AHL2.Sniper.FireSuppressed" ) , -- (String) Primary suppressed shoot sound
	["Primary_Dry"] = Sound( "AHL2.Sniper.FireEmpty" ), -- (String) Primary dry fire sound

	["WhipHitCharacter"] = Sound( "AHL2.Melee.HitBody" ) , -- (String) Sound for pistol whip hits
	["WhipHitWall"] = Sound( "AHL2.Melee.HitWorld" ) , -- (String) Sound for pistol whip hits
	["WhipMiss"] = Sound( "AHL2.Melee.Swing" ) , -- (String) Sound for pistol whip misses
	
	["ZoomIn"] = Sound( "AHL2.Sniper.Zoom" ) , -- (String) Sound used when zooming into a scope
	["ZoomOut"] = Sound( "AHL2.Sniper.Zoom" ) , -- (String) Sound used when zooming out of a scope
	["CycleZoomIn"] = Sound( "AHL2.Sniper.ZoomIn" ) , -- (String) Sound used when cycling the zoom level of a scope
	["CycleZoomOut"] = Sound( "AHL2.Sniper.ZoomOut" ) , -- (String) Sound used when cycling the zoom level of a scope

	["Noise_Close"] = Sound( "BF3.BulletCraft.Noise.Sniper.Close" ) , --
	["Noise_Distant"] = Sound( "BF3.BulletCraft.Noise.Forest.Distant" ) , --
	["Noise_Far"] = Sound( "BF3.BulletCraft.Noise.Forest.Far" ) , --
	["CoreBass_Close"] = Sound( "BF3.BulletCraft.CoreBass.Close.OneShot_3" ) , --
	["CoreBass_Distant"] = Sound( "BF3.BulletCraft.CoreBass.Distant.OneShot_1" ) , --
	["HiFi"] = Sound( "BF3.BulletCraft.HiFi.BoltAction" ) , --
	["Reflection_Close"] = Sound( "BF3.BulletCraft.Reflection.Forest.Sniper" ) , --
	["Reflection_Far"] = Sound( "common/null.wav" ) , --

}

--SWEP.ReloadSNDDelay = 0.14 -- (Float) Reload sound delay
SWEP.SelectorSwitchSNDType = 2 -- (Integer) 1=US , 2=RU
SWEP.UsesSuperSonicAmmo = true -- (Boolean) Is the weapon using supersonic or subsonic ammo?

------------------------------------------------------
--	Ironsight & Run Positions						
--	Set our model transforms for running and ironsights
------------------------------------------------------

SWEP.IronSightsPos = Vector(-6.881, -7.639, 1.759)
SWEP.IronSightsAng = Vector(1.5, -2.8, 0)
SWEP.StockIronSightsPos = Vector(-6.881, -7.639, 1.759)
SWEP.StockIronSightsAng = Vector(1.5, -2.8, 0 )
SWEP.RunArmOffset = Vector(6.834, -14.271, 0)
SWEP.RunArmAngle = Vector(0, 70, 0)

------------------------------------------------------
--	Setup Clientside Info							
--	This block must be in every weapon!
------------------------------------------------------

if CLIENT then

	SWEP.WepSelectIcon = surface.GetTextureID( "vgui/hud/" .. SWEP.Settings.Weapon )
	SWEP.RenderGroup = RENDERGROUP_BOTH
	language.Add( SWEP.Settings.Weapon , SWEP.PrintName )
	killicon.Add( SWEP.Settings.Weapon , "vgui/entities/" .. SWEP.Settings.Weapon , Color( 255 , 255 , 255 ) )

elseif SERVER then

	resource.AddWorkshop( SWEP.Settings.WorkshopID )

end

------------------------------------------------------
--	SWEP:Initialize() 							
--	Called when the weapon is first loaded
------------------------------------------------------

function SWEP:Initialize( )

	self.HoldMeRight = VNTCB.HoldType.Rifle -- (String) Hold type table for our weapon, Lua/autorun/sh_v92_base_swep.Lua

end

SWEP.Sequences.Primary = { "fire1" }
SWEP.Sequences.Primary_Suppressed = { "fire3" }

SWEP.Sequences.Idle = { "idle1" , "idle3" }
SWEP.Sequences.IdleEmpty = { "idle2" }

SWEP.Sequences.Primary_Last = { "fire2" }
SWEP.Sequences.Primary_LastSupp = { "fire" }

SWEP.Sequences.Holster = { "holster1" }
SWEP.Sequences.HolsterEmpty = { "holster2" }

SWEP.Sequences.Reload = { "reload1" , "reload2" } --"reload"

SWEP.Sequences.IronIn = { "idle2aim" }
SWEP.Sequences.IronOut = { "aim2idle" }
SWEP.Sequences.IronIdle = { "aim_idle" , "aim_idle2a" , "aim_idle2b" }
SWEP.Sequences.IronPrimary = { "aim_fire1" }
SWEP.Sequences.IronPrimary_Suppressed = { "aim_fire3" }
SWEP.Sequences.IronPrimary_Last = { "aim_fire2" }
SWEP.Sequences.IronPrimary_Suppressed_Last = { "aim_fire" }
SWEP.Sequences.IronPullPin = { "aim_rechamber" }

SWEP.Sequences.Draw = { "draw1" }
SWEP.Sequences.Draw_Empty = { "draw2" }

SWEP.Sequences.HitCenter = { "meleehit" }
SWEP.Sequences.HitCenter2 = { "meleehitempty" }
SWEP.Sequences.MissCenter = { "meleemiss" }
SWEP.Sequences.MissCenter2 = { "meleemissempty" }

SWEP.Sequences.PullPin = { "rechamber" }
