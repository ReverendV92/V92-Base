
AddCSLuaFile( )

if not VNTCB then return false end

ENT.Type = "anim"
ENT.Base = VNTCB.Bases.AmmoBox

ENT.WeaponName = "v92_ahl2_ammo_beretta"
ENT.AMMOTOGIVE = VNTCB.Magazine.m92FS[3]
ENT.AMMONAME = "9mm Beretta"
ENT.AMMOTYPE = "9x19mm_fmj"
ENT.AMMOMDL = Model( "models/jessev92/ahl2/ammo/beretta.mdl" )

ENT.PrintName = ( "Beretta Magazine" )
ENT.Information = "Gives " .. ENT.AMMOTOGIVE .. " rounds in a " .. ENT.AMMONAME .. " box-magazine"
ENT.Author = VNTCB.author
ENT.Category = VNTCB.Category.AHL2

ENT.Spawnable = true
ENT.AdminOnly = false
