
AddCSLuaFile( )

if not VNTCB then return false end

ENT.Type = "anim"
ENT.Base = VNT_BASE_AMMO_BOX

ENT.WeaponName = "v92_ahl2_ammo_12gauge"
ENT.AMMOTOGIVE = 8
ENT.AMMONAME = "12-Gauge"
ENT.AMMOTYPE = "12gauge"
ENT.AMMOMDL = Model( "models/jessev92/ahl2/ammo/12gauge.mdl" )

ENT.PrintName = ( "12-Gauge Pack" )
ENT.Information = "Gives " .. ENT.AMMOTOGIVE .. " rounds in a " .. ENT.AMMONAME .. " pack"
ENT.Author = VNTCB.author
ENT.Category = VNT_CATEGORY_AHL2

ENT.Spawnable = true
ENT.AdminOnly = false
