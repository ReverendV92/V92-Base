
AddCSLuaFile( )

if not VNTCB then return false end

ENT.Type = "anim"
ENT.Base = VNT_BASE_AMMO_BOX

ENT.WeaponName = "v92_ahl2_ammo_45colt"
ENT.AMMOTOGIVE = 36
ENT.AMMONAME = ".45 Colt"
ENT.AMMOTYPE = "45colt"
ENT.AMMOMDL = Model( "models/Items/357ammo.mdl" )

ENT.PrintName = ( ".45 Colt Box" )
ENT.Information = "Gives " .. ENT.AMMOTOGIVE .. " rounds in a " .. ENT.AMMONAME .. " carton"
ENT.Author = VNTCB.author
ENT.Category = VNT_CATEGORY_AHL2

ENT.Spawnable = true
ENT.AdminOnly = false
