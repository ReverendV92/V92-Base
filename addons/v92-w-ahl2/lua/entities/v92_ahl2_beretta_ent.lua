
-------------------------------------------------------
-------------------------------------------------------
--	ACTION HALF-LIFE 2
--	BERETTA
--	9x19mm NATO
-------------------------------------------------------
-------------------------------------------------------

ENT.PrintName = "BERETTA 92FS"

if not VNTCB then
	Error( "V92 Content Bases not mounted; Removing Weapon: " .. ENT.PrintName .. "\n" )
	return false
end

AddCSLuaFile( )

ENT.Base = VNT_BASE_WEAPON_ENTITY
ENT.Type = "anim"
ENT.Author = VNTCB.author
ENT.Information = "Uses 9x19mm NATO Ammo"
ENT.Category = VNT_CATEGORY_AHL2
ENT.Spawnable = true
ENT.AdminOnly = false
ENT.SWepName = "v92_ahl2_beretta" -- (String) Name of the weapon entity in Lua/weapons/swep_name.lua
ENT.WeaponName = ENT.SWepName .. "_ent"	-- (String) Name of this entity
ENT.SEntModel = Model( "models/jessev92/ahl2/weapons/beretta_w.mdl" ) -- (String) Model to use
