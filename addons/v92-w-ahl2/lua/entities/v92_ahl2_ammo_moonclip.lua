
AddCSLuaFile( )

if not VNTCB then return false end

ENT.Type = "anim"
ENT.Base = VNT_BASE_AMMO_BOX

ENT.WeaponName = "v92_ahl2_ammo_moonclip"
ENT.AMMOTOGIVE = VNTCB.Magazine.m357C[3]
ENT.AMMONAME = ".357 Moon Clip"
ENT.AMMOTYPE = "357"
ENT.AMMOMDL = Model( "models/jessev92/ahl2/ammo/moonclip.mdl" )

ENT.PrintName = ( ".357 Moon Clip" )
ENT.Information = "Gives " .. ENT.AMMOTOGIVE .. " rounds in a " .. ENT.AMMONAME
ENT.Author = VNTCB.author
ENT.Category = VNT_CATEGORY_AHL2

ENT.Spawnable = true
ENT.AdminOnly = false
