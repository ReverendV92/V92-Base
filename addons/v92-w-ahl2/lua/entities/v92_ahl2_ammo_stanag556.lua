
AddCSLuaFile( )

if not VNTCB then return false end

ENT.Type = "anim"
ENT.Base = VNT_BASE_AMMO_BOX

ENT.WeaponName = "v92_ahl2_ammo_stanag556"
ENT.AMMOTOGIVE = VNTCB.Magazine.mNATO556[3]
ENT.AMMONAME = "5.56 NATO"
ENT.AMMOTYPE = "556x45mmnato"
ENT.AMMOMDL = Model( "models/jessev92/ahl2/ammo/stanag_556.mdl" )

ENT.PrintName = ( "5.56 STANAG" )
ENT.Information = "Gives " .. ENT.AMMOTOGIVE .. " rounds in a " .. ENT.AMMONAME .. " STANAG"
ENT.Author = VNTCB.author
ENT.Category = VNT_CATEGORY_AHL2

ENT.Spawnable = true
ENT.AdminOnly = false
