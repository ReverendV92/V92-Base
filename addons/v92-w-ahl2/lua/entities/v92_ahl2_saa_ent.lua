
-------------------------------------------------------
-------------------------------------------------------
--	ACTION HALF-LIFE 2
--	SINGLE-ACTION ARMY
--	.45 Colt
-------------------------------------------------------
-------------------------------------------------------

ENT.PrintName = "SINGLE-ACTION ARMY" -- (String) Printed name on menu

if not VNTCB then
	Error( "V92 Content Bases not mounted; Removing Weapon: " .. ENT.PrintName .. "\n" )
	return false
end

AddCSLuaFile( )

ENT.Base = VNT_BASE_WEAPON_ENTITY
ENT.Type = "anim"
ENT.Author = VNTCB.author
ENT.Information = "Uses .45 Colt Ammo"
ENT.Category = VNT_CATEGORY_AHL2
ENT.Spawnable = true
ENT.AdminOnly = false
ENT.SWepName = "v92_ahl2_saa" -- (String) Name of the weapon entity in Lua/weapons/swep_name.lua
ENT.WeaponName = ENT.SWepName .. "_ent"	-- (String) Name of this entity
ENT.SEntModel = Model( "models/jessev92/ahl2/weapons/saa_w.mdl" ) -- (String) Model to use
