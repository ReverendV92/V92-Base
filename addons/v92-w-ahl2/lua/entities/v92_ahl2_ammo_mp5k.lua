
AddCSLuaFile( )

if not VNTCB then return false end

ENT.Type = "anim"
ENT.Base = VNTCB.Bases.AmmoBox

ENT.WeaponName = "v92_ahl2_ammo_mp5k"
ENT.AMMOTOGIVE = VNTCB.Magazine.mMP5[3]
ENT.AMMONAME = "9x19mm MP5k"
ENT.AMMOTYPE = "9x19mm_fmj"
ENT.AMMOMDL = Model( "models/jessev92/ahl2/ammo/mp5k.mdl" )

ENT.PrintName = ( "MP5k Magazine" )
ENT.Information = "Gives " .. ENT.AMMOTOGIVE .. " rounds in a " .. ENT.AMMONAME .. " magazine"
ENT.Author = VNTCB.author
ENT.Category = VNTCB.Category.AHL2

ENT.Spawnable = true
ENT.AdminOnly = false
