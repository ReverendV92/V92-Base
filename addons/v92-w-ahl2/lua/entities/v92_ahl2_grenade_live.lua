
-------------------------------------------------------
-------------------------------------------------------
--	ACTION HALF-LIFE 2
--	GRENADE
--	M67 GRENADES
-------------------------------------------------------
-------------------------------------------------------

ENT.PrintName = "Live GRENADE"

if not VNTCB then

	Error( "V92 Content Bases not mounted; Removing Weapon: " .. ENT.PrintName .. "\n" )
	return false

end

AddCSLuaFile()

ENT.Type = "anim"
ENT.Base = "v92_grenade_frag"

ENT.Type = "anim" -- Type of entity
ENT.Author = VNTCB.Info.author -- Author name
ENT.Contact = VNTCB.Info.contact -- Contact
ENT.Purpose = VNTCB.Info.purpose -- Purpose
ENT.Instructions = VNTCB.Info.instructions -- Instructions

-- Model to use
ENT.Model = Model( "models/jessev92/ahl2/weapons/grenade_w.mdl" ) -- (String) Model to use

-- Mass in KG
ENT.Mass = 5

-- Sound table
ENT.Sounds = {

	["BounceConcrete"] = Sound( "AHL2.Grenade.Bounce" ) ,
	["BounceMetal"] = Sound( "AHL2.Grenade.Bounce" ) ,
	["BounceSand"] = Sound( "AHL2.Grenade.Bounce" ) ,
	["BounceWood"] = Sound( "AHL2.Grenade.Bounce" ) ,
	["Debris"] = Sound( "BaseExplosionEffect.Sound" ) ,
	["Explosion"] = Sound( "AHL2.Grenade.Explode" ) ,
	["WaterExplosion"] = Sound( "WaterExplosionEffect.Sound" ) ,

}
