
AddCSLuaFile( )
if not VNTCB then return false end
ENT.Base = VNTCB.Bases.KitBag
ENT.Type = "anim"
ENT.PrintName = "Evidence Bag"
ENT.Author = VNTCB.author
ENT.Category = VNTCB.Category.AHL2
ENT.Information = "A police evidence bag filled with ammo"
ENT.Spawnable = true
ENT.AdminOnly = false
ENT.RenderGroup = RENDERGROUP_BOTH
ENT.WeaponsToGive = {} -- (String) Name of the weapon entity in Lua/weapons/swep_name.lua
ENT.AmmoToGive = { 
	["9x19mm_fmj"] = 15 ,
	["45colt"] = 6 ,
	["45acp"] = 7 ,
	["357"] = 6 ,
	["50ae"] = 7 ,
}
ENT.KitBagName = "v92_ahl2_ammo_sidearms" -- (String) Name of this entity
ENT.KitBagModel = Model( "models/JesseV92/ahl2/ammo/sidearms.mdl" ) -- (String) Model to use
ENT.RemoveOnSpawn = false -- Remove the weapon on spawn, I.E. for the fists or unarmed SWeps
ENT.ContentCollisionSound = Sound( "BaseCombatWeapon.WeaponDrop" ) -- Physics collisions
ENT.BagCollisionSound = Sound( "Rubber.ImpactHard" ) -- Physics collisions
ENT.PickupSound = Sound( "jessev92/fx/zipper1.wav" ) -- Pickup sounds
