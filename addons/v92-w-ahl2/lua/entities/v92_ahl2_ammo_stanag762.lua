
AddCSLuaFile( )

if not VNTCB then return false end

ENT.Type = "anim"
ENT.Base = VNT_BASE_AMMO_BOX

ENT.WeaponName = "v92_ahl2_ammo_stanag762"
ENT.AMMOTOGIVE = VNTCB.Magazine.mAWM[3]
ENT.AMMONAME = "8.6x70mm Lapua Magnum" --"7.62 NATO"
ENT.AMMOTYPE = "86x70mm" --"762x5ammnato"
ENT.AMMOMDL = Model( "models/jessev92/ahl2/ammo/stanag_762.mdl" )

ENT.PrintName = ( "8.6x70mm Magazine" )
ENT.Information = "Gives " .. ENT.AMMOTOGIVE .. " rounds in a " .. ENT.AMMONAME .. " magazine"
ENT.Author = VNTCB.author
ENT.Category = VNT_CATEGORY_AHL2

ENT.Spawnable = true
ENT.AdminOnly = false
