
AddCSLuaFile( )

if not VNTCB then return false end

ENT.Type = "anim"
ENT.Base = VNT_BASE_AMMO_BOX

ENT.WeaponName = "v92_ahl2_ammo_1911"
ENT.AMMOTOGIVE = VNTCB.Magazine.mM1911[3]
ENT.AMMONAME = ".45 ACP M1911"
ENT.AMMOTYPE = "45acp"
ENT.AMMOMDL = Model( "models/jessev92/ahl2/ammo/1911.mdl" )

ENT.PrintName = ( "1911 Magazine" )
ENT.Information = "Gives " .. ENT.AMMOTOGIVE .. " rounds in a " .. ENT.AMMONAME .. " box-magazine"
ENT.Author = VNTCB.author
ENT.Category = VNT_CATEGORY_AHL2

ENT.Spawnable = true
ENT.AdminOnly = false
