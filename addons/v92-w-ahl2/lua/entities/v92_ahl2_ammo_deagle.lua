
AddCSLuaFile( )

if not VNTCB then return false end

ENT.Type = "anim"
ENT.Base = VNT_BASE_AMMO_BOX

ENT.WeaponName = "v92_ahl2_ammo_deagle"
ENT.AMMOTOGIVE = VNTCB.Magazine.mDEagle[3]
ENT.AMMONAME = ".50 AE Desert Eagle"
ENT.AMMOTYPE = "50ae"
ENT.AMMOMDL = Model( "models/jessev92/ahl2/ammo/deagle.mdl" )

ENT.PrintName = ( "DEagle Magazine" )
ENT.Information = "Gives " .. ENT.AMMOTOGIVE .. " rounds in a " .. ENT.AMMONAME .. " box-magazine"
ENT.Author = VNTCB.author
ENT.Category = VNT_CATEGORY_AHL2

ENT.Spawnable = true
ENT.AdminOnly = false
