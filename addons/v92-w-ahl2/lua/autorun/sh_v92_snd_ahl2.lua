
AddCSLuaFile()

-------------------------------------------------------
--	Action Half-Life 2 Audio
--	Encoded by V92
--	Profile Link: http://steamcommunity.com/id/JesseVanover/
--	Workshop Link: http://steamcommunity.com/sharedfiles/filedetails/?id=
--	ModDB: http://www.moddb.com/mods/action-halflife-2/
-------------------------------------------------------

---------------------------------------------------------------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------------------------------------------------------
--	WEAPONS
---------------------------------------------------------------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------------------------------------------------------

--------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------
--	MELEE
--------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------

-------------------------------------------------------
--	ACTION HALF-LIFE 2
--	KUNG-FU
-------------------------------------------------------

sound.Add( {	["name"] = "AHL2.KungFu.Knuckles" ,
	["channel"] = CHAN_STATIC ,
	["volume"] = 0.3 ,
	["level"] = 75 ,
	["pitch"] = { 98 , 102 } ,
	["sound"] = { 
		"jessev92/ahl2/weapons/kungfu/knuckles.wav" ,
	} ,
} )

sound.Add( {	["name"] = "AHL2.KungFu.Kick" ,
	["channel"] = CHAN_STATIC ,
	["volume"] = 0.4 ,
	["level"] = 75 ,
	["pitch"] = { 98 , 102 } ,
	["sound"] = { 
		"jessev92/ahl2/weapons/kungfu/kick1.wav" ,
		"jessev92/ahl2/weapons/kungfu/kick2.wav" ,
	} ,
} )

sound.Add( {	["name"] = "AHL2.KungFu.KickHit" ,
	["channel"] = CHAN_STATIC ,
	["volume"] = 0.4 ,
	["level"] = 75 ,
	["pitch"] = { 98 , 102 } ,
	["sound"] = { 
		"jessev92/ahl2/weapons/kungfu/kickhit1.wav" ,
		"jessev92/ahl2/weapons/kungfu/kickhit2.wav" ,
	} ,
} )

sound.Add( {	["name"] = "AHL2.KungFu.Punch" ,
	["channel"] = CHAN_STATIC ,
	["volume"] = 0.4 ,
	["level"] = 75 ,
	["pitch"] = { 98 , 102 } ,
	["sound"] = { 
		"jessev92/ahl2/weapons/kungfu/punch1.wav" ,
		"jessev92/ahl2/weapons/kungfu/punch2.wav" ,
	} ,
} )

sound.Add( {	["name"] = "AHL2.KungFu.PunchWall" ,
	["channel"] = CHAN_STATIC ,
	["volume"] = 0.4 ,
	["level"] = 75 ,
	["pitch"] = { 98 , 102 } ,
	["sound"] = { 
		"jessev92/ahl2/weapons/kungfu/punchwall1.wav" ,
		"jessev92/ahl2/weapons/kungfu/punchwall2.wav" ,
	} ,
} )

sound.Add( {	["name"] = "AHL2.KungFu.PunchHit" ,
	["channel"] = CHAN_STATIC ,
	["volume"] = 0.4 ,
	["level"] = 75 ,
	["pitch"] = { 98 , 102 } ,
	["sound"] = { 
		"jessev92/ahl2/weapons/kungfu/punchhit1.wav" ,
		"jessev92/ahl2/weapons/kungfu/punchhit2.wav" ,
	} ,
} )

-------------------------------------------------------
--	ACTION HALF-LIFE 2
--	MELEE
-------------------------------------------------------

sound.Add( {	["name"] = "AHL2.Melee.Swing" ,
	["channel"] = CHAN_STATIC ,
	["volume"] = 0.3 ,
	["level"] = 75 ,
	["pitch"] = { 98 , 102 } ,
	["sound"] = { 
		"jessev92/ahl2/weapons/melee/miss1.wav" ,
		"jessev92/ahl2/weapons/melee/miss2.wav" ,
	} ,
} )

sound.Add( {	["name"] = "AHL2.Melee.HitBody" ,
	["channel"] = CHAN_STATIC ,
	["volume"] = 0.3 ,
	["level"] = 75 ,
	["pitch"] = { 98 , 102 } ,
	["sound"] = { 
		"jessev92/ahl2/weapons/melee/hit1.wav" ,
		"jessev92/ahl2/weapons/melee/hit2.wav" ,
	} ,
} )

sound.Add( {	["name"] = "AHL2.Melee.HitWorld" ,
	["channel"] = CHAN_STATIC ,
	["volume"] = 0.3 ,
	["level"] = 75 ,
	["pitch"] = { 98 , 102 } ,
	["sound"] = { 
		"jessev92/ahl2/weapons/melee/hitworld1.wav" ,
		"jessev92/ahl2/weapons/melee/hitworld2.wav" ,
	} ,
} )

--------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------
--	UNIQUES
--------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------

-------------------------------------------------------
--	ACTION HALF-LIFE 2
--	ASSAULT RIFLE
--	5.56x45mm NATO
-------------------------------------------------------

sound.Add( {	["name"] = "AHL2.AssaultRifle.Fire" ,
	["channel"] = CHAN_STATIC ,
	["volume"] = 1.0 ,
	["level"] = 120 ,
	["pitch"] = { 90 , 110 } ,
	["sound"] = { 
		"jessev92/ahl2/weapons/assaultrifle/assault_rifle_fire_1.wav" ,
		"jessev92/ahl2/weapons/assaultrifle/assault_rifle_fire_2.wav" ,
	} ,
} )

sound.Add( {	["name"] = "AHL2.AssaultRifle.FireEmpty" ,
	["channel"] = CHAN_STATIC ,
	["volume"] = 1.0 ,
	["level"] = 75 ,
	["pitch"] = { 90 , 110 } ,
	["sound"] = { 
		"jessev92/ahl2/weapons/assaultrifle/fireempty.wav" ,
	} ,
} )

sound.Add( {	["name"] = "AHL2.AssaultRifle.ClipIn" ,
	["channel"] = CHAN_STATIC ,
	["volume"] = 1.0 ,
	["level"] = 75 ,
	["pitch"] = { 98 , 102 } ,
	["sound"] = { 
		"jessev92/ahl2/weapons/assaultrifle/clipin.wav" ,
	} ,
} )

sound.Add( {	["name"] = "AHL2.AssaultRifle.ClipOut" ,
	["channel"] = CHAN_STATIC ,
	["volume"] = 1.0 ,
	["level"] = 75 ,
	["pitch"] = { 98 , 102 } ,
	["sound"] = { 
		"jessev92/ahl2/weapons/assaultrifle/clipout.wav" ,
	} ,
} )

sound.Add( {	["name"] = "AHL2.AssaultRifle.Cock" ,
	["channel"] = CHAN_STATIC ,
	["volume"] = 1.0 ,
	["level"] = 75 ,
	["pitch"] = { 98 , 102 } ,
	["sound"] = { 
		"jessev92/ahl2/weapons/assaultrifle/cock.wav" ,
	} ,
} )

sound.Add( {	["name"] = "AHL2.AssaultRifle.Switch" ,
	["channel"] = CHAN_STATIC ,
	["volume"] = 1.0 ,
	["level"] = 75 ,
	["pitch"] = { 98 , 102 } ,
	["sound"] = { 
		"jessev92/ahl2/weapons/pistol_50ae/switch.wav" ,
	} ,
} )

-------------------------------------------------------
--	ACTION HALF-LIFE 2
--	MP5k "SUBMACHINE GUN"
--	9x19mm NATO
-------------------------------------------------------

sound.Add( {	["name"] = "AHL2.MP5k.Fire" ,
	["channel"] = CHAN_STATIC ,
	["volume"] = 1.0 ,
	["level"] = 80 ,
	["pitch"] = { 98 , 102 } ,
	["sound"] = { 
		"jessev92/ahl2/weapons/mp5k/submac_fire_1.wav" ,
		"jessev92/ahl2/weapons/mp5k/submac_fire_2.wav" ,
	} ,
} )

sound.Add( {	["name"] = "AHL2.MP5k.FireSuppressed" ,
	["channel"] = CHAN_STATIC ,
	["volume"] = 0.3 ,
	["level"] = 80 ,
	["pitch"] = { 98 , 102 } ,
	["sound"] = { 
		"jessev92/ahl2/weapons/mp5k/firesl.wav" ,
	} ,
} )

sound.Add( {	["name"] = "AHL2.MP5k.FireEmpty" ,
	["channel"] = CHAN_STATIC ,
	["volume"] = 0.3 ,
	["level"] = 65 ,
	["pitch"] = { 98 , 102 } ,
	["sound"] = { 
		"jessev92/ahl2/weapons/mp5k/fireempty.wav" ,
	} ,
} )

sound.Add( {	["name"] = "AHL2.MP5k.ClipIn" ,
	["channel"] = CHAN_STATIC ,
	["volume"] = 0.3 ,
	["level"] = 65 ,
	["pitch"] = { 98 , 102 } ,
	["sound"] = { 
		"jessev92/ahl2/weapons/mp5k/clipin.wav" ,
	} ,
} )

sound.Add( {	["name"] = "AHL2.MP5k.ClipOut" ,
	["channel"] = CHAN_STATIC ,
	["volume"] = 0.3 ,
	["level"] = 65 ,
	["pitch"] = { 98 , 102 } ,
	["sound"] = { 
		"jessev92/ahl2/weapons/mp5k/clipout.wav" ,
	} ,
} )

sound.Add( {	["name"] = "AHL2.MP5k.Cock" ,
	["channel"] = CHAN_STATIC ,
	["volume"] = 0.3 ,
	["level"] = 65 ,
	["pitch"] = { 98 , 102 } ,
	["sound"] = { 
		"jessev92/ahl2/weapons/mp5k/cock.wav" ,
	} ,
} )

sound.Add( {	["name"] = "AHL2.MP5k.Switch" ,
	["channel"] = CHAN_STATIC ,
	["volume"] = 0.3 ,
	["level"] = 65 ,
	["pitch"] = { 98 , 102 } ,
	["sound"] = { 
		"jessev92/ahl2/weapons/mp5k/switch.wav" ,
	} ,
} )

-------------------------------------------------------
--	ACTION HALF-LIFE 2
--	SHOTGUN
--	12-GAUGE BUCKSHOT
-------------------------------------------------------

sound.Add( {	["name"] = "AHL2.Shotgun.Fire" ,
	["channel"] = CHAN_STATIC ,
	["volume"] = 0.6 ,
	["level"] = 80 ,
	["pitch"] = { 98 , 102 } ,
	["sound"] = { 
		"jessev92/ahl2/weapons/shotgun/fire.wav" ,
	} ,
} )

sound.Add( {	["name"] = "AHL2.Shotgun.FireEmpty" ,
	["channel"] = CHAN_STATIC ,
	["volume"] = 0.3 ,
	["level"] = 65 ,
	["pitch"] = { 98 , 102 } ,
	["sound"] = { 
		"jessev92/ahl2/weapons/assaultrifle/fireempty.wav" ,
	} ,
} )

sound.Add( {	["name"] = "AHL2.Shotgun.Reload" ,
	["channel"] = CHAN_STATIC ,
	["volume"] = 0.3 ,
	["level"] = 65 ,
	["pitch"] = { 98 , 102 } ,
	["sound"] = { 
		"jessev92/ahl2/weapons/assaultrifle/reload.wav" ,
	} ,
} )

sound.Add( {	["name"] = "AHL2.Shotgun.Pump" ,
	["channel"] = CHAN_STATIC ,
	["volume"] = 0.3 ,
	["level"] = 65 ,
	["pitch"] = { 98 , 102 } ,
	["sound"] = { 
		"jessev92/ahl2/weapons/assaultrifle/pump.wav" ,
	} ,
} )

-------------------------------------------------------
--	ACTION HALF-LIFE 2
--	SAWN-OFF "HAND-CANNON"
--	12-GAUGE BUCKSHOT
-------------------------------------------------------

sound.Add( {	["name"] = "AHL2.SawnOff.Fire" ,
	["channel"] = CHAN_STATIC ,
	["volume"] = 1.0 ,
	["level"] = 80 ,
	["pitch"] = { 98 , 102 } ,
	["sound"] = { 
		"jessev92/ahl2/weapons/sawnoff/sawnoff_fire_1.wav" ,
		"jessev92/ahl2/weapons/sawnoff/sawnoff_fire_2.wav" ,
	} ,
} )

sound.Add( {	["name"] = "AHL2.SawnOff.FireEmpty" ,
	["channel"] = CHAN_STATIC ,
	["volume"] = 0.3 ,
	["level"] = 65 ,
	["pitch"] = { 98 , 102 } ,
	["sound"] = { 
		"jessev92/ahl2/weapons/sawnoff/empty.wav" ,
	} ,
} )

sound.Add( {	["name"] = "AHL2.SawnOff.RoundIn" ,
	["channel"] = CHAN_STATIC ,
	["volume"] = 0.3 ,
	["level"] = 65 ,
	["pitch"] = { 98 , 102 } ,
	["sound"] = { 
		"jessev92/ahl2/weapons/sawnoff/roundin.wav" ,
	} ,
} )

sound.Add( {	["name"] = "AHL2.SawnOff.RoundOut" ,
	["channel"] = CHAN_STATIC ,
	["volume"] = 0.3 ,
	["level"] = 65 ,
	["pitch"] = { 98 , 102 } ,
	["sound"] = { 
		"jessev92/ahl2/weapons/sawnoff/roundout.wav" ,
	} ,
} )

sound.Add( {	["name"] = "AHL2.SawnOff.Cock" ,
	["channel"] = CHAN_STATIC ,
	["volume"] = 0.3 ,
	["level"] = 65 ,
	["pitch"] = { 98 , 102 } ,
	["sound"] = { 
		"jessev92/ahl2/weapons/sawnoff/cock.wav" ,
	} ,
} )

sound.Add( {	["name"] = "AHL2.SawnOff.Open" ,
	["channel"] = CHAN_STATIC ,
	["volume"] = 0.3 ,
	["level"] = 65 ,
	["pitch"] = { 98 , 102 } ,
	["sound"] = { 
		"jessev92/ahl2/weapons/sawnoff/open.wav" ,
	} ,
} )

sound.Add( {	["name"] = "AHL2.SawnOff.Close" ,
	["channel"] = CHAN_STATIC ,
	["volume"] = 0.3 ,
	["level"] = 65 ,
	["pitch"] = { 98 , 102 } ,
	["sound"] = { 
		"jessev92/ahl2/weapons/sawnoff/close.wav" ,
	} ,
} )

-------------------------------------------------------
--	ACTION HALF-LIFE 2
--	SNIPER RIFLE
--	8.6x70mm LAPUA MAGNUM
-------------------------------------------------------

sound.Add( {	["name"] = "AHL2.Sniper.Fire" ,
	["channel"] = CHAN_STATIC ,
	["volume"] = 1.0 ,
	["level"] = 120 ,
	["pitch"] = { 98 , 102 } ,
	["sound"] = { 
		"jessev92/ahl2/weapons/sniper/SNIPER_FIRE_1.wav" ,
		"jessev92/ahl2/weapons/sniper/SNIPER_FIRE_2.wav" ,
	} ,
} )

sound.Add( {	["name"] = "AHL2.Sniper.FireSuppressed" ,
	["channel"] = CHAN_STATIC ,
	["volume"] = 1.0 ,
	["level"] = 75 ,
	["pitch"] = { 98 , 102 } ,
	["sound"] = { 
		"jessev92/ahl2/weapons/Sniper/firesl.wav" ,
	} ,
} )

sound.Add( {	["name"] = "AHL2.Sniper.Zoom" ,
	["channel"] = CHAN_STATIC ,
	["volume"] = 0.3	,
	["level"] = 60 ,
	["pitch"] = { 100 } ,
	["sound"] = { 
		"jessev92/ahl2/weapons/Sniper/zoom.wav" ,
	} ,
} )

sound.Add( {	["name"] = "AHL2.Sniper.ZoomIn" ,
	["channel"] = CHAN_STATIC ,
	["volume"] = 0.3	,
	["level"] = 60 ,
	["pitch"] = { 105 } ,
	["sound"] = { 
		"jessev92/ahl2/weapons/Sniper/zoom.wav" ,
	} ,
} )

sound.Add( {	["name"] = "AHL2.Sniper.ZoomOut" ,
	["channel"] = CHAN_STATIC ,
	["volume"] = 0.3	,
	["level"] = 60 ,
	["pitch"] = { 95 } ,
	["sound"] = { 
		"jessev92/ahl2/weapons/Sniper/zoom.wav" ,
	} ,
} )

sound.Add( {	["name"] = "AHL2.Sniper.FireEmpty" ,
	["channel"] = CHAN_STATIC ,
	["volume"] = 1.0 ,
	["level"] = 75 ,
	["pitch"] = { 90 , 110 } ,
	["sound"] = { 
		"jessev92/ahl2/weapons/assaultrifle/fireempty.wav" ,
	} ,
} )

sound.Add( {	["name"] = "AHL2.Sniper.ClipIn" ,
	["channel"] = CHAN_STATIC ,
	["volume"] = 0.3 ,
	["level"] = 60 ,
	["pitch"] = { 100 } ,
	["sound"] = { 
		"jessev92/ahl2/weapons/Sniper/clipin.wav" ,
	} ,
} )

sound.Add( {	["name"] = "AHL2.Sniper.ClipOut" ,
	["channel"] = CHAN_STATIC ,
	["volume"] = 0.3 ,
	["level"] = 60 ,
	["pitch"] = { 100 } ,
	["sound"] = { 
		"jessev92/ahl2/weapons/Sniper/clipout.wav" ,
	} ,
} )

sound.Add( {	["name"] = "AHL2.Sniper.BoltIn" ,
	["channel"] = CHAN_STATIC ,
	["volume"] = 0.3 ,
	["level"] = 60 ,
	["pitch"] = { 100 } ,
	["sound"] = { 
		"jessev92/ahl2/weapons/Sniper/boltin.wav" ,
	} ,
} )

sound.Add( {	["name"] = "AHL2.Sniper.BoltOut" ,
	["channel"] = CHAN_STATIC ,
	["volume"] = 0.3 ,
	["level"] = 60 ,
	["pitch"] = { 100 } ,
	["sound"] = { 
		"jessev92/ahl2/weapons/Sniper/boltout.wav" ,
	} ,
} )

sound.Add( {	["name"] = "AHL2.Sniper.Switch" ,
	["channel"] = CHAN_STATIC ,
	["volume"] = 1.0 ,
	["level"] = 75 ,
	["pitch"] = { 98 , 102 } ,
	["sound"] = { 
		"jessev92/ahl2/weapons/pistol_50ae/switch.wav" ,
	} ,
} )

--------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------
--	SIDEARMS
--------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------

-------------------------------------------------------
--	ACTION HALF-LIFE 2
--	BERETTA 92FS
--	9x19mm NATO
-------------------------------------------------------

sound.Add( {	["name"] = "AHL2.Beretta.Fire" ,
	["channel"] = CHAN_STATIC ,
	["volume"] = 1.0 ,
	["level"] = 80 ,
	["pitch"] = { 95 , 105 } ,
	["sound"] = { 
		"jessev92/ahl2/weapons/beretta/beretta_fire_01.wav" ,
	} ,
} )

sound.Add( {	["name"] = "AHL2.Beretta.FireEmpty" ,
	["channel"] = CHAN_STATIC ,
	["volume"] = 0.3 ,
	["level"] = 65 ,
	["pitch"] = { 95 , 105 } ,
	["sound"] = { 
		"jessev92/ahl2/weapons/beretta/fireempty.wav" ,
	} ,
} )

sound.Add( {	["name"] = "AHL2.Beretta.FireSuppressed" ,
	["channel"] = CHAN_STATIC ,
	["volume"] = 0.3 ,
	["level"] = 80 ,
	["pitch"] = { 95 , 105 } ,
	["sound"] = { 
		"jessev92/ahl2/weapons/beretta/firesl.wav" ,
	} ,
} )

sound.Add( {	["name"] = "AHL2.Beretta.ClipIn" ,
	["channel"] = CHAN_STATIC ,
	["volume"] = 0.3 ,
	["level"] = 65 ,
	["pitch"] = { 95 , 105 } ,
	["sound"] = { 
		"jessev92/ahl2/weapons/beretta/clipin.wav" ,
	} ,
} )

sound.Add( {	["name"] = "AHL2.Beretta.ClipOut" ,
	["channel"] = CHAN_STATIC ,
	["volume"] = 0.3 ,
	["level"] = 65 ,
	["pitch"] = { 95 , 105 } ,
	["sound"] = { 
		"jessev92/ahl2/weapons/beretta/clipout.wav" ,
	} ,
} )

sound.Add( {	["name"] = "AHL2.Beretta.Cock" ,
	["channel"] = CHAN_STATIC ,
	["volume"] = 0.3 ,
	["level"] = 65 ,
	["pitch"] = { 95 , 105 } ,
	["sound"] = { 
		"jessev92/ahl2/weapons/beretta/cock.wav" ,
	} ,
} )

sound.Add( {	["name"] = "AHL2.Beretta.Slide" ,
	["channel"] = CHAN_STATIC ,
	["volume"] = 0.3 ,
	["level"] = 65 ,
	["pitch"] = { 95 , 105 } ,
	["sound"] = { 
		"jessev92/ahl2/weapons/beretta/slide.wav" ,
	} ,
} )

sound.Add( {	["name"] = "AHL2.Beretta.Switch" ,
	["channel"] = CHAN_STATIC ,
	["volume"] = 0.3 ,
	["level"] = 65 ,
	["pitch"] = { 95 , 105 } ,
	["sound"] = { 
		"jessev92/ahl2/weapons/beretta/switch.wav" ,
	} ,
} )

sound.Add( {	["name"] = "AHL2.Beretta.Swoosh1" ,
	["channel"] = CHAN_STATIC ,
	["volume"] = 0.3 ,
	["level"] = 65 ,
	["pitch"] = { 95 , 105 } ,
	["sound"] = { 
		"jessev92/ahl2/weapons/beretta/swoosh1.wav" ,
	} ,
} )

sound.Add( {	["name"] = "AHL2.Beretta.Swoosh2" ,
	["channel"] = CHAN_STATIC ,
	["volume"] = 0.3 ,
	["level"] = 65 ,
	["pitch"] = { 95 , 105 } ,
	["sound"] = { 
		"jessev92/ahl2/weapons/beretta/swoosh2.wav" ,
	} ,
} )

-------------------------------------------------------
--	ACTION HALF-LIFE 2
--	COLT 1911
--	.45 AUTOMATIC COLT PISTOL
-------------------------------------------------------

sound.Add( {	["name"] = "AHL2.1911.Fire" ,
	["channel"] = CHAN_STATIC ,
	["volume"] = 1.0 ,
	["level"] = 80 ,
	["pitch"] = { 95 , 105 } ,
	["sound"] = { 
		"jessev92/ahl2/weapons/1911/colt1911_fire_01.wav" ,
	} ,
} )

sound.Add( {	["name"] = "AHL2.1911.FireEmpty" ,
	["channel"] = CHAN_STATIC ,
	["volume"] = 0.3 ,
	["level"] = 60 ,
	["pitch"] = { 95 , 105 } ,
	["sound"] = { 
		"jessev92/ahl2/weapons/1911/fireempty.wav" ,
	} ,
} )

sound.Add( {	["name"] = "AHL2.1911.FireSuppressed" ,
	["channel"] = CHAN_STATIC ,
	["volume"] = 0.3 ,
	["level"] = 80 ,
	["pitch"] = { 95 , 105 } ,
	["sound"] = { 
		"jessev92/ahl2/weapons/1911/firesl.wav" ,
	} ,
} )

sound.Add( {	["name"] = "AHL2.1911.ClipIn" ,
	["channel"] = CHAN_STATIC ,
	["volume"] = 0.3 ,
	["level"] = 65 ,
	["pitch"] = { 95 , 105 } ,
	["sound"] = { 
		"jessev92/ahl2/weapons/1911/clipin.wav" ,
	} ,
} )

sound.Add( {	["name"] = "AHL2.1911.ClipOut" ,
	["channel"] = CHAN_STATIC ,
	["volume"] = 0.3 ,
	["level"] = 65 ,
	["pitch"] = { 95 , 105 } ,
	["sound"] = { 
		"jessev92/ahl2/weapons/1911/clipout.wav" ,
	} ,
} )

sound.Add( {	["name"] = "AHL2.1911.Slide" ,
	["channel"] = CHAN_STATIC ,
	["volume"] = 0.3 ,
	["level"] = 65 ,
	["pitch"] = { 95 , 105 } ,
	["sound"] = { 
		"jessev92/ahl2/weapons/1911/slide.wav" ,
	} ,
} )

sound.Add( {	["name"] = "AHL2.1911.Switch" ,
	["channel"] = CHAN_STATIC ,
	["volume"] = 0.3 ,
	["level"] = 65 ,
	["pitch"] = { 95 , 105 } ,
	["sound"] = { 
		"jessev92/ahl2/weapons/1911/switch.wav" ,
	} ,
} )

-------------------------------------------------------
--	ACTION HALF-LIFE 2
--	.357 MAGNUM REVOLVER
--	.357 MAGNUM
-------------------------------------------------------

sound.Add( {	["name"] = "AHL2.Revolver.Fire" ,
	["channel"] = CHAN_STATIC ,
	["volume"] = 1.0 ,
	["level"] = 80 ,
	["pitch"] = { 95 , 105 } ,
	["sound"] = { 
		"jessev92/ahl2/weapons/saa/saa_fire_1.wav" ,
		"jessev92/ahl2/weapons/saa/saa_fire_2.wav" ,
	} ,
} )

sound.Add( {	["name"] = "AHL2.Revolver.FireEmpty" ,
	["channel"] = CHAN_STATIC ,
	["volume"] = 0.3 ,
	["level"] = 65 ,
	["pitch"] = { 95 , 105 } ,
	["sound"] = { 
		"jessev92/ahl2/weapons/beretta/empty.wav" ,
	} ,
} )

sound.Add( {	["name"] = "AHL2.Revolver.RoundIn" ,
	["channel"] = CHAN_STATIC ,
	["volume"] = 0.3 ,
	["level"] = 65 ,
	["pitch"] = { 95 , 105 } ,
	["sound"] = { 
		"jessev92/ahl2/weapons/revolver/roundin.wav" ,
	} ,
} )

sound.Add( {	["name"] = "AHL2.Revolver.Open" ,
	["channel"] = CHAN_STATIC ,
	["volume"] = 0.3 ,
	["level"] = 65 ,
	["pitch"] = { 95 , 105 } ,
	["sound"] = { 
		"jessev92/ahl2/weapons/revolver/open.wav" ,
	} ,
} )

sound.Add( {	["name"] = "AHL2.Revolver.Close" ,
	["channel"] = CHAN_STATIC ,
	["volume"] = 0.3 ,
	["level"] = 65 ,
	["pitch"] = { 95 , 105 } ,
	["sound"] = { 
		"jessev92/ahl2/weapons/revolver/close.wav" ,
	} ,
} )

sound.Add( {	["name"] = "AHL2.Revolver.RoundOut" ,
	["channel"] = CHAN_STATIC ,
	["volume"] = 0.3 ,
	["level"] = 65 ,
	["pitch"] = { 95 , 105 } ,
	["sound"] = { 
		"jessev92/ahl2/weapons/revolver/roundout.wav" ,
	} ,
} )

-------------------------------------------------------
--	ACTION HALF-LIFE 2
--	COLT SINGLE-ACTION ARMY
--	.45 LONG COLT
-------------------------------------------------------

sound.Add( {	["name"] = "AHL2.SAA.Fire" ,
	["channel"] = CHAN_STATIC ,
	["volume"] = 1.0 ,
	["level"] = 80 ,
	["pitch"] = { 95 , 105 } ,
	["sound"] = { 
		"jessev92/ahl2/weapons/saa/saa_fire_1.wav" ,
		"jessev92/ahl2/weapons/saa/saa_fire_2.wav" ,
	} ,
} )

sound.Add( {	["name"] = "AHL2.SAA.FireEmpty" ,
	["channel"] = CHAN_STATIC ,
	["volume"] = 0.3 ,
	["level"] = 65 ,
	["pitch"] = { 95 , 105 } ,
	["sound"] = { 
		"jessev92/ahl2/weapons/saa/empty.wav" ,
	} ,
} )

sound.Add( {	["name"] = "AHL2.SAA.Cock" ,
	["channel"] = CHAN_STATIC ,
	["volume"] = 0.3 ,
	["level"] = 65 ,
	["pitch"] = { 95 , 105 } ,
	["sound"] = { 
		"jessev92/ahl2/weapons/saa/cock.wav" ,
	} ,
} )

sound.Add( {	["name"] = "AHL2.SAA.Reload" ,
	["channel"] = CHAN_STATIC ,
	["volume"] = 0.3 ,
	["level"] = 65 ,
	["pitch"] = { 95 , 105 } ,
	["sound"] = { 
		"jessev92/ahl2/weapons/saa/reload.wav" ,
	} ,
} )

sound.Add( {	["name"] = "AHL2.SAA.RoundIn" ,
	["channel"] = CHAN_STATIC ,
	["volume"] = 0.3 ,
	["level"] = 65 ,
	["pitch"] = { 95 , 105 } ,
	["sound"] = { 
		"jessev92/ahl2/weapons/saa/roundin.wav" ,
	} ,
} )

sound.Add( {	["name"] = "AHL2.SAA.Swoosh" ,
	["channel"] = CHAN_STATIC ,
	["volume"] = 0.3 ,
	["level"] = 65 ,
	["pitch"] = { 95 , 105 } ,
	["sound"] = { 
		"jessev92/ahl2/weapons/saa/swoosh.wav" ,
	} ,
} )

sound.Add( {	["name"] = "AHL2.SAA.TapSpan" ,
	["channel"] = CHAN_STATIC ,
	["volume"] = 0.3 ,
	["level"] = 65 ,
	["pitch"] = { 95 , 105 } ,
	["sound"] = { 
		"jessev92/ahl2/weapons/saa/tapspan.wav" ,
	} ,
} )

-------------------------------------------------------
--	ACTION HALF-LIFE 2
--	DESERT EAGLE
--	.50 ACTION EXPRESS
-------------------------------------------------------

sound.Add( {	["name"] = "AHL2.DEagle.Fire" ,
	["channel"] = CHAN_STATIC ,
	["volume"] = 1.0 ,
	["level"] = 80 ,
	["pitch"] = { 95 , 105 } ,
	["sound"] = { 
		"jessev92/ahl2/weapons/deagle/deagle_fire_01.wav" ,
	} ,
} )

sound.Add( {	["name"] = "AHL2.DEagle.FireEmpty" ,
	["channel"] = CHAN_STATIC ,
	["volume"] = 0.3 ,
	["level"] = 65 ,
	["pitch"] = { 95 , 105 } ,
	["sound"] = { 
		"jessev92/ahl2/weapons/deagle/fireempty.wav" ,
	} ,
} )

sound.Add( {	["name"] = "AHL2.DEagle.ClipIn" ,
	["channel"] = CHAN_STATIC ,
	["volume"] = 0.3 ,
	["level"] = 65 ,
	["pitch"] = { 95 , 105 } ,
	["sound"] = { 
		"jessev92/ahl2/weapons/deagle/clipin.wav" ,
	} ,
} )

sound.Add( {	["name"] = "AHL2.DEagle.ClipOut" ,
	["channel"] = CHAN_STATIC ,
	["volume"] = 0.3 ,
	["level"] = 65 ,
	["pitch"] = { 95 , 105 } ,
	["sound"] = { 
		"jessev92/ahl2/weapons/deagle/clipout.wav" ,
	} ,
} )

sound.Add( {	["name"] = "AHL2.DEagle.Slide" ,
	["channel"] = CHAN_STATIC ,
	["volume"] = 0.3 ,
	["level"] = 65 ,
	["pitch"] = { 95 , 105 } ,
	["sound"] = { 
		"jessev92/ahl2/weapons/deagle/slide.wav" ,
	} ,
} )

sound.Add( {	["name"] = "AHL2.DEagle.Switch" ,
	["channel"] = CHAN_STATIC ,
	["volume"] = 0.3 ,
	["level"] = 65 ,
	["pitch"] = { 95 , 105 } ,
	["sound"] = { 
		"jessev92/ahl2/weapons/deagle/switch.wav" ,
	} ,
} )

-------------------------------------------------------
--	ACTION HALF-LIFE 2
--	KUNAI KNIFE
--	KUNAI KNIVES
-------------------------------------------------------

sound.Add( {	["name"] = "AHL2.Kunai.Swing" ,
	["channel"] = CHAN_STATIC ,
	["volume"] = 0.3 ,
	["level"] = 65 ,
	["pitch"] = { 98 , 102 } ,
	["sound"] = { 
		"jessev92/ahl2/weapons/knife/swoosh1.wav" ,
		"jessev92/ahl2/weapons/knife/swoosh2.wav" ,
		"jessev92/ahl2/weapons/knife/swoosh3.wav" ,
	} ,
} )

sound.Add( {	["name"] = "AHL2.Kunai.HitWorld" ,
	["channel"] = CHAN_STATIC ,
	["volume"] = 0.3 ,
	["level"] = 65 ,
	["pitch"] = { 98 , 102 } ,
	["sound"] = { 
		"jessev92/ahl2/weapons/knife/hit_wall.wav" ,
	} ,
} )

sound.Add( {	["name"] = "AHL2.Kunai.HitBody" ,
	["channel"] = CHAN_STATIC ,
	["volume"] = 0.3 ,
	["level"] = 80 ,
	["pitch"] = { 98 , 102 } ,
	["sound"] = { 
		"jessev92/ahl2/weapons/knife/hit_flesh.wav" ,
		"jessev92/ahl2/weapons/knife/hit_vest.wav" ,
	} ,
} )

sound.Add( {	["name"] = "AHL2.Kunai.Throw" ,
	["channel"] = CHAN_STATIC ,
	["volume"] = 0.3 ,
	["level"] = 65 ,
	["pitch"] = { 98 , 102 } ,
	["sound"] = { 
		"jessev92/ahl2/weapons/knife/throw1.wav" ,
		"jessev92/ahl2/weapons/knife/throw2.wav" ,
	} ,
} )

--------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------
--	ITEMS
--------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------

-------------------------------------------------------
--	ACTION HALF-LIFE 2
--	FRAG GRENADE
--	M61 FRAGMENTATION GRENADES
-------------------------------------------------------

sound.Add( {	["name"] = "AHL2.Grenade.Bounce" ,
	["channel"] = CHAN_STATIC ,
	["volume"] = 0.3 ,
	["level"] = 80 ,
	["pitch"] = { 95 , 105 } ,
	["sound"] = { 
		"jessev92/ahl2/weapons/grenade/bounce1.wav" ,
		"jessev92/ahl2/weapons/grenade/bounce2.wav" ,
	} ,
} )

sound.Add( {	["name"] = "AHL2.Grenade.Explode" ,
	["channel"] = CHAN_STATIC ,
	["volume"] = 1.0 ,
	["level"] = 100 ,
	["pitch"] = { 80 , 120 } ,
	["sound"] = { 
		"jessev92/ahl2/weapons/grenade/explode1.wav" ,
		"jessev92/ahl2/weapons/grenade/explode2.wav" ,
	} ,
} )

sound.Add( {	["name"] = "AHL2.Grenade.PullPin" ,
	["channel"] = CHAN_STATIC ,
	["volume"] = 0.3 ,
	["level"] = 65 ,
	["pitch"] = { 95 , 105 } ,
	["sound"] = { 
		"jessev92/ahl2/weapons/grenade/pin.wav" ,
	} ,
} )
