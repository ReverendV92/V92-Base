
---------------------------------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------------------------
--	Half-Life 2: Expanded - Rifles, Carbines, Machine Guns
---------------------------------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------------------------

--	Catch-22 Diabolis Ex Machina Prevention

if SERVER then
	resource.AddWorkshop("505106454") 	--	V92 Code Bases
end

-- SOUND GUIDE
----------
-- Channel = Typically CHAN_WEAPON or CHAN_ITEM for gunshots and gun parts respectively; CHAN_BODY for foley
-- Volume = Leave at 1.0
-- Pitch = Typically between 95 and 105 for gunshots
-- Level = For gunshots, follow this estimation table as a LOOSE guideline:
----------
--	.22LR = 40dB
--	9x19mm = 60dB
--	.38 Special = 70dB
--	5.56x45mm = 70dB
--	7.62x51mm = 90dB
--	.45 ACP = 90dB
--	.50 AE = 95dB
--	.50 BMG = 100dB
--	12-Gauge Buckshot = 120dB
--	Explosion = 200dB
--	Suppressed = 2/3 of normal
--	Empty = 60dB 
--	Weapon sounds = ~50dB (use your head)
--	Foley = ~30dB (use your head)
----------
----------

if not VNTCB then
	Error( "V92 Content Bases not mounted: Removing Autorun File" )

	return false
end

if VNTCB.Plugins then
	table.insert( VNTCB.Plugins, 0, "VNT :: HL2X Rifle Pack" )
end

------------------------------------------------------------------------------------------
--	Half-Life 2: Expanded :: Rifles, Carbines, Machine Guns
------------------------------------------------------------------------------------------

---------------------------------------------
--	FN SCAR-L
---------------------------------------------

sound.Add( {
	name = "HL2_SCAR-L.Single" ,
	channel = CHAN_WEAPON ,
	volume = 1.0 ,
	level = 80 ,
	pitch = { 95 , 105 } ,
	sound = "jessev92/hl2/weapons/scarl/single.wav"
} )

util.PrecacheSound( "jessev92/hl2/weapons/scarl/single.wav" )

sound.Add( {
	name = "HL2_SCAR-L.MagOut" ,
	channel = CHAN_ITEM ,
	volume = 1.0 ,
	level = 50 ,
	pitch = { 95 , 105 } ,
	sound = "jessev92/hl2/weapons/scarl/magout.wav"
} )

util.PrecacheSound( "jessev92/hl2/weapons/scarl/magout.wav" )

sound.Add( {
	name = "HL2_SCAR-L.MagTap" ,
	channel = CHAN_ITEM ,
	volume = 1.0 ,
	level = 50 ,
	pitch = { 95 , 105 } ,
	sound = "jessev92/hl2/weapons/scarl/magtap.wav"
} )

util.PrecacheSound( "jessev92/hl2/weapons/scarl/magtap.wav" )

sound.Add( {
	name = "HL2_SCAR-L.MagIn" ,
	channel = CHAN_ITEM ,
	volume = 1.0 ,
	level = 50 ,
	pitch = { 95 , 105 } ,
	sound = "jessev92/hl2/weapons/scarl/MagIn.wav"
} )

util.PrecacheSound( "jessev92/hl2/weapons/scarl/MagIn.wav" )

sound.Add( {
	name = "HL2_SCAR-L.BoltPull" ,
	channel = CHAN_ITEM ,
	volume = 1.0 ,
	level = 50 ,
	pitch = { 95 , 105 } ,
	sound = "jessev92/hl2/weapons/scarl/BoltPull.wav"
} )

util.PrecacheSound( "jessev92/hl2/weapons/scarl/BoltPull.wav" )

sound.Add( {
	name = "HL2_SCAR-L.BoltRelease" ,
	channel = CHAN_ITEM ,
	volume = 1.0 ,
	level = 50 ,
	pitch = { 95 , 105 } ,
	sound = "jessev92/hl2/weapons/scarl/BoltRelease.wav"
} )

util.PrecacheSound( "jessev92/hl2/weapons/scarl/BoltRelease.wav" )

sound.Add( {
	name = "HL2_SCAR-L.Selector" ,
	channel = CHAN_ITEM ,
	volume = 1.0 ,
	level = 40 ,
	pitch = { 95 , 105 } ,
	sound = "jessev92/hl2/weapons/scarl/Selector.wav"
} )

util.PrecacheSound( "jessev92/hl2/weapons/scarl/Selector.wav" )

sound.Add( {
	name = "HL2_SCAR-L.Draw" ,
	channel = CHAN_BODY ,
	volume = 1.0 ,
	level = 45 ,
	pitch = { 95 , 105 } ,
	sound = "jessev92/hl2/weapons/scarl/draw.wav"
} )
util.PrecacheSound( "jessev92/hl2/weapons/scarl/draw.wav" )

sound.Add( {
	name = "HL2_SCAR-L.Holster" ,
	channel = CHAN_BODY ,
	volume = 1.0 ,
	level = 45 ,
	pitch = { 95 , 105 } ,
	sound = "jessev92/weapons/univ/holster1.wav"
} )
util.PrecacheSound( "jessev92/weapons/univ/holster1.wav" )

---------------------------------------------
--	RPK
---------------------------------------------

sound.Add( {
	name = "HL2_RPK.Single" ,
	channel = CHAN_WEAPON ,
	volume = 1.0 ,
	level = 85 ,
	pitch = { 95 , 105 } ,
	sound = "jessev92/hl2/weapons/rpk/fire.wav"
} )

util.PrecacheSound( "jessev92/hl2/weapons/rpk/fire.wav" )

sound.Add( {
	name = "HL2_RPK.MagOut" ,
	channel = CHAN_ITEM ,
	volume = 1.0 ,
	level = 50 ,
	pitch = { 95 , 105 } ,
	sound = "jessev92/hl2/weapons/sg552/magout.wav"
} )

util.PrecacheSound( "jessev92/hl2/weapons/sg552/magout.wav" )

sound.Add( {
	name = "HL2_RPK.MagTap" ,
	channel = CHAN_ITEM ,
	volume = 1.0 ,
	level = 50 ,
	pitch = { 95 , 105 } ,
	sound = "jessev92/hl2/weapons/sg552/magtap.wav"
} )

util.PrecacheSound( "jessev92/hl2/weapons/sg552/magtap.wav" )

sound.Add( {
	name = "HL2_RPK.MagIn" ,
	channel = CHAN_ITEM ,
	volume = 1.0 ,
	level = 50 ,
	pitch = { 95 , 105 } ,
	sound = "jessev92/hl2/weapons/sg552/MagIn.wav"
} )

util.PrecacheSound( "jessev92/hl2/weapons/sg552/MagIn.wav" )

sound.Add( {
	name = "HL2_RPK.BoltPull" ,
	channel = CHAN_ITEM ,
	volume = 1.0 ,
	level = 55 ,
	pitch = { 95 , 105 } ,
	sound = "jessev92/hl2/weapons/sg552/BoltPull.wav"
} )

util.PrecacheSound( "jessev92/hl2/weapons/sg552/BoltPull.wav" )

sound.Add( {
	name = "HL2_RPK.BoltRelease" ,
	channel = CHAN_ITEM ,
	volume = 1.0 ,
	level = 55 ,
	pitch = { 95 , 105 } ,
	sound = "jessev92/hl2/weapons/sg552/BoltRelease.wav"
} )

util.PrecacheSound( "jessev92/hl2/weapons/sg552/BoltRelease.wav" )

sound.Add( {
	name = "HL2_RPK.Selector" ,
	channel = CHAN_ITEM ,
	volume = 1.0 ,
	level = 40 ,
	pitch = { 95 , 105 } ,
	sound = "jessev92/hl2/weapons/sg552/Selector.wav"
} )

util.PrecacheSound( "jessev92/hl2/weapons/sg552/Selector.wav" )

sound.Add( {
	name = "HL2_RPK.Draw" ,
	channel = CHAN_BODY ,
	volume = 1.0 ,
	level = 55 ,
	pitch = { 95 , 105 } ,
	sound = "jessev92/hl2/weapons/rpk/draw.wav"
} )
util.PrecacheSound( "jessev92/hl2/weapons/rpk/draw.wav" )

sound.Add( {
	name = "HL2_RPK.Holster" ,
	channel = CHAN_BODY ,
	volume = 1.0 ,
	level = 55 ,
	pitch = { 95 , 105 } ,
	sound = "jessev92/weapons/univ/holster1.wav"
} )
util.PrecacheSound( "jessev92/weapons/univ/holster1.wav" )

---------------------------------------------
--	Colt M4 Carbine w/ M203
---------------------------------------------

sound.Add( {
	name = "HL2_OldSch_M4M203.Single" ,
	channel = CHAN_WEAPON ,
	volume = 1.0 ,
	level = 70 ,
	pitch = { 95 , 105 } ,
	sound = "^)jessev92/hl2/weapons/m4m203/fire1.wav"
} )
util.PrecacheSound( "jessev92/hl2/weapons/m4m203/fire1.wav" )

sound.Add( {
	name = "HL2_OldSch_M4M203.GL" ,
	channel = CHAN_WEAPON ,
	volume = 1.0 ,
	level = 60 ,
	pitch = { 95 , 105 } ,
	sound = "^)jessev92/ins1/weapons/m16a4/m203_fire.wav"
} )
util.PrecacheSound( "jessev92/ins1/weapons/m16a4/m203_fire.wav" )

sound.Add( {
	name = "HL2_OldSch_M4M203.Reload_Rotate" ,
	channel = CHAN_ITEM ,
	volume = 1.0 ,
	level = 50 ,
	pitch = { 95 , 105 } ,
	sound = "^)jessev92/hl2/weapons/m4m203/ar2_reload_rotate.wav"
} )
util.PrecacheSound( "jessev92/hl2/weapons/m4m203/ar2_reload_rotate.wav" )

sound.Add( {
	name = "HL2_OldSch_M4M203.Reload_Push" ,
	channel = CHAN_ITEM ,
	volume = 1.0 ,
	level = 50 ,
	pitch = { 95 , 105 } ,
	sound = "^)jessev92/hl2/weapons/m4m203/ar2_reload_push.wav"
} )
util.PrecacheSound( "jessev92/hl2/weapons/m4m203/ar2_reload_push.wav" )

sound.Add( {
	name = "HL2_OldSch_M4M203.Dry" ,
	channel = CHAN_ITEM ,
	volume = 1.0 ,
	level = 45 ,
	pitch = { 95 , 105 } ,
	sound = "^)jessev92/hl2/weapons/f2000/empty.wav"
} )
util.PrecacheSound( "jessev92/hl2/weapons/f2000/empty.wav" )

sound.Add( {
	name = "HL2_OldSch_M4M203.Draw" ,
	channel = CHAN_BODY ,
	volume = 1.0 ,
	level = 50 ,
	pitch = { 95 , 105 } ,
	sound = "^)jessev92/hl2/weapons/sg552/draw.wav"
} )
util.PrecacheSound( "jessev92/hl2/weapons/sg552/draw.wav" )

sound.Add( {
	name = "HL2_OldSch_M4M203.Holster" ,
	channel = CHAN_BODY ,
	volume = 1.0 ,
	level = 75 ,
	pitch = { 95 , 105 } ,
	sound = "^)jessev92/weapons/univ/holster1.wav"
} )
util.PrecacheSound( "jessev92/weapons/univ/holster1.wav" )

---------------------------------------------
--	ZM LR300
---------------------------------------------

sound.Add( {
	name = "HL2_LR300.Single" ,
	channel = CHAN_WEAPON ,
	volume = 1.0 ,
	level = 70 ,
	pitch = { 95 , 105 } ,
	sound = "^)jessev92/hl2/weapons/m4m203/fire1.wav"
} )
util.PrecacheSound( "jessev92/hl2/weapons/m4m203/fire1.wav" )

sound.Add( {
	name = "HL2_LR300.GL" ,
	channel = CHAN_WEAPON ,
	volume = 1.0 ,
	level = 65 ,
	pitch = { 95 , 105 } ,
	sound = "^)jessev92/ins1/weapons/m16a4/m203_fire.wav"
} )
util.PrecacheSound( "jessev92/weapons/univ/launcher1.wav" )

sound.Add( {
	name = "HL2_LR300.ReloadRotate" ,
	channel = CHAN_ITEM ,
	volume = 1.0 ,
	level = 50 ,
	pitch = { 95 , 105 } ,
	sound = "^)jessev92/hl2/weapons/lr300/reload_rotate.wav"
} )
util.PrecacheSound( "jessev92/hl2/weapons/lr300/reload_rotate.wav" )

sound.Add( {
	name = "HL2_LR300.ReloadPush" ,
	channel = CHAN_ITEM ,
	volume = 1.0 ,
	level = 50 ,
	pitch = { 95 , 105 } ,
	sound = "^)jessev92/hl2/weapons/lr300/reload_push.wav"
} )
util.PrecacheSound( "jessev92/hl2/weapons/lr300/reload_push.wav" )

sound.Add( {
	name = "HL2_LR300.Dry" ,
	channel = CHAN_ITEM ,
	volume = 1.0 ,
	level = 45 ,
	pitch = { 95 , 105 } ,
	sound = "^)jessev92/hl2/weapons/f2000/empty.wav"
} )
util.PrecacheSound( "jessev92/hl2/weapons/f2000/empty.wav" )

sound.Add( {
	name = "HL2_LR300.Draw" ,
	channel = CHAN_BODY ,
	volume = 1.0 ,
	level = 45 ,
	pitch = { 95 , 105 } ,
	sound = "^)jessev92/weapons/univ/draw2.wav"
} )
util.PrecacheSound( "jessev92/weapons/univ/draw2.wav" )

---------------------------------------------
--	H&K HK53
---------------------------------------------

sound.Add( {
	name = "HL2_HK53.Single" ,
	channel = CHAN_WEAPON ,
	volume = 1.0 ,
	level = 70 ,
	pitch = { 95 , 105 } ,
	sound = "^)jessev92/hl2/weapons/hk53/single.wav"
} )
util.PrecacheSound( "jessev92/hl2/weapons/sg552/single.wav" )

sound.Add( {
	name = "HL2_HK53.SingleSil" ,
	channel = CHAN_WEAPON ,
	volume = 1.0 ,
	level = 50 ,
	pitch = { 95 , 105 } ,
	sound = "^)jessev92/hl2/weapons/hk53/single_s.wav"
} )
util.PrecacheSound( "jessev92/hl2/weapons/sg552/single_s.wav" )

sound.Add( {
	name = "HL2_HK53.GL" ,
	channel = CHAN_WEAPON ,
	volume = 1.0 ,
	level = 65 ,
	pitch = { 95 , 105 } ,
	sound = "^)jessev92/ins1/weapons/m16a4/m203_fire.wav"
} )
util.PrecacheSound( "jessev92/weapons/univ/launcher1.wav" )

sound.Add( {
	name = "HL2_HK53.ReloadRotate" ,
	channel = CHAN_ITEM ,
	volume = 1.0 ,
	level = 50 ,
	pitch = { 95 , 105 } ,
	sound = "^)jessev92/hl2/weapons/hk53/reload_rotate.wav"
} )
util.PrecacheSound( "jessev92/hl2/weapons/hk53/reload_rotate.wav" )

sound.Add( {
	name = "HL2_HK53.ReloadPush" ,
	channel = CHAN_ITEM ,
	volume = 1.0 ,
	level = 50 ,
	pitch = { 95 , 105 } ,
	sound = "^)jessev92/hl2/weapons/hk53/reload_push.wav"
} )
util.PrecacheSound( "jessev92/hl2/weapons/hk53/reload_push.wav" )

sound.Add( {
	name = "HL2_HK53.Dry" ,
	channel = CHAN_WEAPON ,
	volume = 1.0 ,
	level = 40 ,
	pitch = { 95 , 105 } ,
	sound = "^)jessev92/hl2/weapons/f2000/empty.wav"
} )
util.PrecacheSound( "jessev92/hl2/weapons/f2000/empty.wav" )

sound.Add( {
	name = "HL2_HK53.Draw" ,
	channel = CHAN_BODY ,
	volume = 1.0 ,
	level = 45 ,
	pitch = { 95 , 105 } ,
	sound = "^)jessev92/hl2/weapons/sg552/draw.wav"
} )
util.PrecacheSound( "jessev92/hl2/weapons/sg552/draw.wav" )

sound.Add( {
	name = "HL2_HK53.Holster" ,
	channel = CHAN_BODY ,
	volume = 1.0 ,
	level = 45 ,
	pitch = { 95 , 105 } ,
	sound = "^)jessev92/weapons/univ/holster1.wav"
} )
util.PrecacheSound( "jessev92/weapons/univ/holster1.wav" )

---------------------------------------------
--	FN Herstal F2000
---------------------------------------------

sound.Add( {
	name = "HL2_FN2000.Single" ,
	channel = CHAN_WEAPON ,
	volume = 1.0 ,
	level = 70 ,
	pitch = { 95 , 105 } ,
	sound = "^)jessev92/hl2/weapons/f2000/single.wav"
} )
util.PrecacheSound( "jessev92/hl2/weapons/f2000/single.wav" )

sound.Add( {
	name = "HL2_FN2000.GL" ,
	channel = CHAN_WEAPON ,
	volume = 1.0 ,
	level = 65 ,
	pitch = { 95 , 105 } ,
	sound = "^)jessev92/ins1/weapons/m16a4/m203_fire.wav"
} )
util.PrecacheSound( "jessev92/ins1/weapons/m16a4/m203_fire.wav" )

sound.Add( {
	name = "HL2_FN2000.ReloadRotate" ,
	channel = CHAN_ITEM ,
	volume = 1.0 ,
	level = 50 ,
	pitch = { 95 , 105 } ,
	sound = "^)jessev92/hl2/weapons/f2000/reload_rotate.wav"
} )
util.PrecacheSound( "jessev92/hl2/weapons/f2000/reload_rotate.wav" )

sound.Add( {
	name = "HL2_FN2000.ReloadPush" ,
	channel = CHAN_ITEM ,
	volume = 1.0 ,
	level = 50 ,
	pitch = { 95 , 105 } ,
	sound = "^)jessev92/hl2/weapons/f2000/reload_push.wav"
} )
util.PrecacheSound( "jessev92/hl2/weapons/f2000/reload_push.wav" )

sound.Add( {
	name = "HL2_FN2000.Dry" ,
	channel = CHAN_ITEM ,
	volume = 1.0 ,
	level = 40 ,
	pitch = { 95 , 105 } ,
	sound = "^)jessev92/hl2/weapons/f2000/empty.wav"
} )
util.PrecacheSound( "jessev92/hl2/weapons/f2000/empty.wav" )

sound.Add( {
	name = "HL2_FN2000.Draw" ,
	channel = CHAN_BODY ,
	volume = 1.0 ,
	level = 50 ,
	pitch = { 95 , 105 } ,
	sound = "^)jessev92/weapons/univ/draw2.wav"
} )
util.PrecacheSound( "jessev92/weapons/univ/draw2.wav" )

---------------------------------------------
--	Sig Sauer SG-552
---------------------------------------------
sound.Add({	name	= "HL2_SG552.Single",
	channel			= CHAN_WEAPON,
	volume			= 1.0,
	level			= 70,
	pitch			= { 95, 105 },
	sound			= "^)jessev92/hl2/weapons/sg552/single.wav",
})
util.PrecacheSound("jessev92/hl2/weapons/sg552/single.wav")

sound.Add({	name	= "HL2_SG552.MagOut",
	channel			= CHAN_ITEM,
	volume			= 1.0,
	level			= 50,
	pitch			= { 95, 105 },
	sound			= "^)jessev92/hl2/weapons/sg552/magout.wav",
})
util.PrecacheSound("jessev92/hl2/weapons/sg552/magout.wav")

sound.Add({	name	= "HL2_SG552.MagTap",
	channel			= CHAN_ITEM,
	volume			= 1.0,
	level			= 50,
	pitch			= { 95, 105 },
	sound			= "jessev92/hl2/weapons/sg552/magtap.wav",
})
util.PrecacheSound("jessev92/hl2/weapons/sg552/magtap.wav")

sound.Add({	name	= "HL2_SG552.MagIn",
	channel			= CHAN_ITEM,
	volume			= 1.0,
	level			= 50,
	pitch			= { 95, 105 },
	sound			= "^)jessev92/hl2/weapons/sg552/MagIn.wav",
})
util.PrecacheSound("jessev92/hl2/weapons/sg552/MagIn.wav")

sound.Add({ name	= "HL2_SG552.BoltPull",
	channel			= CHAN_ITEM,
	volume			= 1.0,
	level			= 55,
	pitch			= { 95, 105 },
	sound			= "^)jessev92/hl2/weapons/sg552/BoltPull.wav",
})
util.PrecacheSound("jessev92/hl2/weapons/sg552/BoltPull.wav")

sound.Add({	name	= "HL2_SG552.BoltRelease",
	channel			= CHAN_ITEM,
	volume			= 1.0,
	level			= 55,
	pitch			= { 95, 105 },
	sound			= "^)jessev92/hl2/weapons/sg552/BoltRelease.wav",
})
util.PrecacheSound("jessev92/hl2/weapons/sg552/BoltRelease.wav")

sound.Add({ name	= "HL2_SG552.Selector",
	channel			= CHAN_ITEM,
	volume			= 1.0,
	level			= 40,
	pitch			= { 95, 105 },
	sound			= "^)jessev92/hl2/weapons/sg552/Selector.wav",
})
util.PrecacheSound("jessev92/hl2/weapons/sg552/Selector.wav")

sound.Add({	name	= "HL2_SG552.Draw",
	channel			= CHAN_BODY,
	volume			= 1.0,
	level			= 45,
	pitch			= { 95, 105 },
	sound			= "^)jessev92/hl2/weapons/sg552/draw.wav",
})
util.PrecacheSound("jessev92/hl2/weapons/sg552/draw.wav")

sound.Add({ name	= "HL2_SG552.Holster",
	channel			= CHAN_BODY,
	volume			= 1.0,
	level			= 50,
	pitch			= { 95, 105 },
	sound			= "^)jessev92/weapons/univ/holster1.wav",
})
util.PrecacheSound("jessev92/weapons/univ/holster1.wav")
