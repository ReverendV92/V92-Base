
------------------------------------------------------
--	Half-Life 2: Extras								--
--	SIG SG552										--
--	5.56x45mm Auto/Burst/Semi-Auto rifle			--
--	Underbarrel 40x46mm NATO grenade launcher		--
------------------------------------------------------

AddCSLuaFile()

SWEP.PrintName		= "SG-552"						-- (String) Printed name on menu

if not VNTCB then
	Error( "V92 Content Bases not mounted; Removing Weapon: " .. SWEP.PrintName .. "\n" )
	return false
end

SWEP.Base 			= VNTCB.Bases.Wep					-- (String) Weapon base parent this is a child of
SWEP.Spawnable 		= true							-- (Boolean) Can be spawned via the menu
SWEP.AdminOnly 		= false							-- (Boolean) Admin only spawnable
------------------------------------------------------
--	Client Information								--	Info used in the client block of the weapon
------------------------------------------------------
SWEP.WeaponName	= "v92_hl2_rif_sig552"			-- (String) Name of the weapon script
SWEP.WeaponEntityName		= SWEP.WeaponName .. "_ent"	-- (String) Name of the weapon entity in Lua/Entities/Entityname.lua
SWEP.Manufacturer		= VNTCB.Manufacturer.SIG					-- (String) Gun company that makes this weapon
SWEP.CountryOfOrigin	= VNTCB.Country.DEU				-- (String) Country of origin
SWEP.MagazineName		= VNTCB.Magazine.mSIG556			-- (String) The name of the magazine the weapon uses - used in my Weapon Magazine System
SWEP.Category		= VNTCB.Category.HL2X				-- (String) Category
SWEP.Instructions	= VNTCB.instructions			-- (String) Instruction
SWEP.Author			= VNTCB.author					-- (String) Author
SWEP.Contact		= VNTCB.contact					-- (String) Contact
SWEP.Slot		= VNTCB.Bucket.Carbine				-- (Integer) Bucket to place weapon in, 1 to 6
SWEP.SlotPos		= VNTCB.Slot.Carbine					-- (Integer) Bucket position
SWEP.ViewModelFOV			= 60							-- (Integer) First-person field of view
SWEP.WorkshopID = "709718975" -- (Integer) Workshop ID number of the upload that contains this file.
------------------------------------------------------
--	Model Information								--
------------------------------------------------------
SWEP.ViewModelFlip	= false							-- (Boolean) Only used for vanilla CS:S models
SWEP.ViewModel		= Model("models/jessev92/hl2/weapons/sg552_v.mdl") 	-- (String) View model - v_*
SWEP.WorldModel		= Model("models/jessev92/hl2/weapons/sg552_w.mdl") 	-- (String) World model - w_*
SWEP.UseHands		= false							-- (Boolean) Leave at false unless the model uses C_Arms
SWEP.HoldType = "ar2" -- (String) Hold type for our weapon, refer to wiki for animation sets
------------------------------------------------------
--	Gun Types										--	Set the type of weapon - ONLY PICK ONE!
------------------------------------------------------
SWEP.WeaponType		= VNTCB.WeaponType.Carbine				-- (Integer) 1=Melee, 2=Pistol, 3=Rifle, 4=Shotgun, 5=Sniper, 6=Grenade Launcher, 7=Rocket Launcher
------------------------------------------------------
--	Primary Fire Settings							--	Settings for the primary fire of the weapon
------------------------------------------------------
SWEP.Primary.ClipSize			= 30							-- (Integer) Size of a magazine
SWEP.Primary.DefaultClip	= 30							-- (Integer) Default number of ammo you spawn with
SWEP.Primary.Ammo = "556x45mmnato"				-- (String) Primary ammo used by the weapon, bullets probably
SWEP.Primary.Recoil = 1								-- (Float) 	Recoil factor, Good ranges are from 0.5 to 5; this MULTIPLIES with the CVar!
SWEP.Primary.PureDmg			= VNTCB.Ammo.a556NATO[1]			-- (Integer) Base damage, put one number here and the base will do the rest
SWEP.Primary.RPM			= 700							-- (Integer) Go to a wikipedia page and look at the RPM of the weapon, then put that here - the base will do the math
------------------------------------------------------
--	Gun Mechanics									--	Various things to tweak the effects and feedback
------------------------------------------------------
SWEP.FireMode		= { true, true, true, true }	-- (Table: Boolean, Boolean, Boolean, Boolean ) Enable different fire modes on the weapon; Has modes, Has Single, Has Burst, Has Auto - in that order. You can have more than one, but the first must be true
SWEP.CurrentMode		= 1								-- (Integer) Current fire mode of the weapon; used to set the default mode; corresponds to the FireMode table
SWEP.Weight 		= 4								-- (Integer) The weight in Kilogrammes of our weapon - used in my weapon weight mod!
SWEP.StrongPenetration	= VNTCB.Ammo.a556NATO[2]			-- (Integer) Max penetration
SWEP.WeakPenetration	= VNTCB.Ammo.a556NATO[3]		-- (Integer) Max wood penetration
SWEP.EffectiveRange = 400 -- (Integer) Effective range of the weapon in metres.
SWEP.StoppageRate = 1200 -- (Integer) Rate of stoppages in the weapon, look up the real world number estimations and just throw that in here.
SWEP.BurstCount = 3 -- (Integer) Amounts of shots to be fired by burst
SWEP.BurstDelay = 0.05 -- (Float) Time between bolt cycles in the burst
------------------------------------------------------
--	Special FX										--	Muzzle flashes, shell casings, etc
------------------------------------------------------
SWEP.MuzzleAttach = 1 -- (Integer) The number of the attachment point for muzzle flashes, typically "1"
SWEP.MuzzleFlashType = 3 -- (Integer) The number of the muzzle flash to use; see Lua/Effects/fx_muzzleflash.Lua
SWEP.ShellAttach = 2 -- (String) The name of the attachment point for shell ejections, typically "2" or "eject"
SWEP.ShellType = 15 -- (Integer) The shell to use, see Lua/Effects/FX_ShellEject for integers
SWEP.ShellDelay = 0 -- (Float) 	Time between shot firing and shell ejection; useful for bolt-actions and things like that that need a delay
------------------------------------------------------
--	Custom Sounds									--
--		Setup sounds here!							--
------------------------------------------------------
SWEP.Sounds = {
	["Primary"] = Sound("HL2_SG552.Single"), 	-- (String) Primary shoot sound
	["Primary_Dry"] = Sound("VNTCB.SWep.Empty5"), 	-- (String) Primary dry fire sound
	["Reload"] = Sound("HL2_SG552.Draw"), 	-- (String) Reload sound

	["Noise_Close"] = Sound( "BF3.BulletCraft.Noise.Rifle.Close" ),
	["Noise_Distant"] = Sound( "BF3.BulletCraft.Noise.Forest.Distant" ) ,
	["Noise_Far"] = Sound( "BF3.BulletCraft.Noise.Forest.Far" ) ,
	["CoreBass_Close"] = Sound( "BF3.BulletCraft.CoreBass.Close.LMG_7" ),
	["CoreBass_Distant"] = Sound( "BF3.BulletCraft.CoreBass.Distant.LMG_6" ),
	["HiFi"] = Sound( "BF3.BulletCraft.HiFi.G3" ),
	["Reflection_Close"] = Sound( "BF3.BulletCraft.Reflection.Forest.Close" ) , 
	["Reflection_Far"] = Sound( "BF3.BulletCraft.Reflection.Forest.Far" ) ,
}

SWEP.SelectorSwitchSNDType = 2 -- (Integer) 1=US , 2=RU
SWEP.UsesSuperSonicAmmo = true -- (Boolean) Is the weapon using supersonic or subsonic ammo?
------------------------------------------------------
--	Ironsight & Run Positions						--	Set our model transforms for running and ironsights
------------------------------------------------------
SWEP.IronSightsPos 		= Vector (-2.95, -4, -0.101) 	-- (Vector) Ironsight XYZ Transform
SWEP.IronSightsAng 		= Vector (3.299, 0, 0) 		-- (Vector) Ironsight XYZ Rotation
SWEP.RunArmOffset 		= Vector (3, -3, 0) 			-- (Vector) Sprinting XYZ Transform
SWEP.RunArmAngle 		= Vector (-13, 46, -26) 		-- (Vector) Sprinting XYZ Rotation
------------------------------------------------------
--	Setup Clientside Info							--	This block must be in every weapon!
------------------------------------------------------
if CLIENT then
	SWEP.WepSelectIcon 		= surface.GetTextureID("vgui/hud/" .. SWEP.WeaponName )
	SWEP.RenderGroup 		= RENDERGROUP_BOTH
	language.Add( SWEP.WeaponName, SWEP.PrintName )
	killicon.Add( SWEP.WeaponName, "vgui/entities/".. SWEP.WeaponName , Color( 255, 255, 255 ) )
end
------------------------------------------------------
--	SWEP:Initialize() 							--	Called when the weapon is first loaded
------------------------------------------------------
function SWEP:Initialize()
	self.HoldMeRight = VNTCB.HoldType.Rifle -- (String) Hold type table for our weapon, Lua/autorun/sh_v92_base_swep.Lua
end

SWEP.Sequences.Draw = { "draw" }
SWEP.Sequences.Holster = { "holster" }
SWEP.Sequences.Primary = { "fire1" , "fire2" , "fire3" , "fire4" }
SWEP.Sequences.Idle = { "idle" }
SWEP.Sequences.Reload = { "reload" }
