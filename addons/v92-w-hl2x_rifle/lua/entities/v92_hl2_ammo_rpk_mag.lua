AddCSLuaFile( )

if not VNTCB then
	Error( "V92 Content Bases not mounted: Removing Entity" )

	return false
end

ENT.Type = "anim"
ENT.Base = VNT_BASE_AMMO_BOX
ENT.WeaponName = "v92_hl2_ammo_rpk_mag"
ENT.AMMOTOGIVE = 40
ENT.AMMONAME = "RPK 40rd. Mag"
ENT.AMMOTYPE = "762x39mmwp"
ENT.AMMOMDL = Model( "models/jessev92/hl2/items/rpk_mag.mdl" )
ENT.PrintName = ( ENT.AMMONAME )
ENT.Information = "Gives " .. ENT.AMMOTOGIVE .. " rounds in an " .. ENT.AMMONAME .. " mag"
ENT.Author = VNTCB.author
ENT.Category = VNT_CATEGORY_HL2EXPANDED
ENT.Spawnable = true
ENT.AdminOnly = false