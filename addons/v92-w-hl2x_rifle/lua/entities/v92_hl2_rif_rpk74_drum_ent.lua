AddCSLuaFile( )

if not VNTCB then
	Error( "V92 Content Bases not mounted: Removing Entity" )

	return false
end

ENT.Base = VNT_BASE_WEAPON_ENTITY
ENT.Type = "anim"
ENT.PrintName = "RPK-74M Drum"
ENT.Author = VNTCB.author
ENT.Information = "Uses 7.62x39mm Warsaw Pact Ammo"
ENT.Category = VNT_CATEGORY_HL2EXPANDED
ENT.Spawnable = true
ENT.AdminOnly = false
ENT.WeaponName = "v92_hl2_rif_rpk74_drum_ent" -- (String) Name of this entity
ENT.SWepName = "v92_hl2_rif_rpk74_drum" -- (String) Name of the weapon entity in Lua/weapons/swep_name.lua
ENT.SEntModel = Model( "models/jessev92/hl2/weapons/rpk_syn_drum_w.mdl" ) -- (String) Model to use