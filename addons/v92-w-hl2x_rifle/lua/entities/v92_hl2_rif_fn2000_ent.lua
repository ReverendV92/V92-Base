AddCSLuaFile( )

if not VNTCB then
	Error( "V92 Content Bases not mounted: Removing Entity" )

	return false
end

ENT.Base = VNT_BASE_WEAPON_ENTITY
ENT.Type = "anim"
ENT.PrintName = "F2000 GL1"
ENT.Author = VNTCB.author
ENT.Information = "Uses 5.56x45mm NATO Ammo"
ENT.Category = VNT_CATEGORY_HL2EXPANDED
ENT.Spawnable = true
ENT.AdminOnly = false
ENT.WeaponName = "v92_hl2_rif_fn2000_ent" -- (String) Name of this entity
ENT.SWepName = "v92_hl2_rif_fn2000" -- (String) Name of the weapon entity in Lua/weapons/swep_name.lua
ENT.SEntModel = Model( "models/jessev92/hl2/weapons/f2000_w.mdl" ) -- (String) Model to use