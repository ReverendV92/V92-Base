
AddCSLuaFile( )

ENT.PrintName = "M1919A4 Browning Machine Gun"

if not VNTCB then
	Error( "V92 Content Bases not mounted; Removing Entity:\n" .. ENT.PrintName )
	return false
--elseif IsMounted( "dod" ) == false then
	--Error( "Day of Defeat: Source not mounted: Removing Entity\n" )
	--return false
end

ENT.Base = VNT_BASE_WEAPON_ENTITY
ENT.Type = "anim"
ENT.Author = VNTCB.author
ENT.Information = "Uses .30-06 (7.62x63mm) Ammo"
ENT.Category = VNT_CATEGORY_DODS
ENT.Spawnable = true
ENT.AdminOnly = false
ENT.SWepName = "v92_dods_m1919a4_30cal" -- (String) Name of the weapon entity in Lua/weapons/swep_name.lua
ENT.WeaponName = ENT.SWepName .. "_ent"	-- (String) Name of this entity
ENT.SEntModel = Model( "models/jessev92/dods/weapons/30cal_w.mdl" ) -- (String) Model to use