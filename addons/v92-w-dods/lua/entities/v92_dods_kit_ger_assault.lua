
AddCSLuaFile( )
if not VNTCB then return false end
ENT.Base = VNT_BASE_KITBAG
ENT.Type = "anim"
ENT.PrintName = "German Assault"
ENT.Author = VNTCB.author
ENT.Category = VNT_CATEGORY_DODS
ENT.Information = "German Assault Kit"
ENT.Spawnable = true
ENT.AdminOnly = false
ENT.RenderGroup = RENDERGROUP_BOTH
ENT.WeaponsToGive = { "v92_dods_mp40" , "v92_dods_p38" , "v92_dods_stielhandgranate" , "v92_dods_nebelhandgranate" } -- (String) Name of the weapon entity in Lua/weapons/swep_name.lua
ENT.AmmoToGive = { 
	["9x19mmnato"] = 248,
	["m24stielhandgranate"] = 1,
	["nb39nebelhandgranate"] = 1,
}
ENT.KitBagName = "v92_dods_kit_ger_assault" -- (String) Name of this entity
ENT.KitBagModel = Model( "models/JesseV92/dods/props/crates/supply_crate02_static.mdl" ) -- (String) Model to use
ENT.KitBagSkin = 1 -- (Integer) Skin to use
ENT.RemoveOnSpawn = false -- Remove the weapon on spawn, I.E. for the fists or unarmed SWeps
ENT.ContentCollisionSound = Sound( "BaseCombatWeapon.WeaponDrop" ) -- Physics collisions
ENT.BagCollisionSound = Sound( "Bounce.Wood" ) -- Physics collisions
ENT.PickupSound = Sound( "Breakable.Crate" ) -- Pickup sounds

ENT.GibOne = { Model("models/jessev92/dods/props/crates/supply_crate02_gib1_static.mdl") , Vector( 0 , 0 , 0 ) , Angle( 0 , 0 , 0 ) , 1 } -- ( Table: First Gib Model , Vector(Pos) , Angle(Ang) , Skin(Integer) )
ENT.GibTwo = { Model("models/jessev92/dods/props/crates/supply_crate02_gib2_static.mdl") , Vector( 0 , 0 , 5 ) , Angle( 0 , 10 , 0 ) , 1 } -- ( Table: First Gib Model , Vector(Pos) , Angle(Ang) , Skin(Integer) )
