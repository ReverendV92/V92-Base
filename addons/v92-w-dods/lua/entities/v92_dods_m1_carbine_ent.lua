
AddCSLuaFile( )

ENT.PrintName = "M1 Carbine"

if not VNTCB then
	Error( "V92 Content Bases not mounted; Removing Entity:\n" .. ENT.PrintName )
	return false
--elseif IsMounted( "dod" ) == false then
	--Error( "Day of Defeat: Source not mounted: Removing Entity\n" )
	--return false
end

ENT.Base = VNT_BASE_WEAPON_ENTITY
ENT.Type = "anim"
ENT.Author = VNTCB.author
ENT.Information = "Uses .30 Carbine (7.62x33mm) Ammo"
ENT.Category = VNT_CATEGORY_DODS
ENT.Spawnable = true
ENT.AdminOnly = false
ENT.SWepName = "v92_dods_m1_carbine" -- (String) Name of the weapon entity in Lua/weapons/swep_name.lua
ENT.WeaponName = ENT.SWepName .. "_ent"	-- (String) Name of this entity
ENT.SEntModel = Model( "models/jessev92/dods/weapons/m1carbine_w.mdl" ) -- (String) Model to use