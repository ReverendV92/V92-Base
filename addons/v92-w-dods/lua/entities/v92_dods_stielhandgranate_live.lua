
------------------------------------------------------
--	Day of Defeat: Source							--
--	Model 24 Stielhandgranate			 			--
------------------------------------------------------

AddCSLuaFile( )

ENT.PrintName = "Live Stielhandgranate"

if not VNTCB then
	Error( "V92 Content Bases not mounted; Removing Weapon: " .. SWEP.PrintName .. "\n" )
	return false
end

AddCSLuaFile()

ENT.Type = "anim"
ENT.Base = "v92_grenade_frag"

ENT.Type = "anim" -- Type of entity
ENT.Author = VNTCB.Info.author -- Author name
ENT.Contact = VNTCB.Info.contact -- Contact
ENT.Purpose = VNTCB.Info.purpose -- Purpose
ENT.Instructions = VNTCB.Info.instructions -- Instructions

-- Model to use
ENT.Model = Model( "models/jessev92/dods/weapons/stielhandgranate_w.mdl" )

-- Mass in KG
ENT.Mass = 5

-- Sound table
ENT.Sounds = {

	["BounceConcrete"] = Sound( "BF1942.Grenade.Collide_Concrete" ) ,
	["BounceMetal"] = Sound( "BF1942.Grenade.Collide_Metal" ) ,
	["BounceSand"] = Sound( "BF1942.Grenade.Collide_Sand" ) ,
	["BounceWood"] = Sound( "BF1942.Grenade.Collide_Wood" ) ,
	["Debris"] = Sound( "DODS.Explode.Debris" ) ,
	["Explosion"] = Sound( "DODS.Explode" ) ,
	["WaterExplosion"] = Sound( "WaterExplosionEffect.Sound" ) ,

}
