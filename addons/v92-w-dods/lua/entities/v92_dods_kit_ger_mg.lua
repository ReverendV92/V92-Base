
AddCSLuaFile( )
if not VNTCB then return false end
ENT.Base = VNT_BASE_KITBAG
ENT.Type = "anim"
ENT.PrintName = "German MG"
ENT.Author = VNTCB.author
ENT.Category = VNT_CATEGORY_DODS
ENT.Information = "German Machine Gunner Kit"
ENT.Spawnable = true
ENT.AdminOnly = false
ENT.RenderGroup = RENDERGROUP_BOTH
ENT.WeaponsToGive = { "v92_dods_mg42" , "v92_dods_p38" , "v92_dods_spade" } -- (String) Name of the weapon entity in Lua/weapons/swep_name.lua
ENT.AmmoToGive = { 
	["792x57mmmauser"] = 500,
	["9x19mmnato"] = 24,
}
ENT.KitBagName = "v92_dods_kit_ger_mg" -- (String) Name of this entity
ENT.KitBagModel = Model( "models/JesseV92/dods/props/crates/supply_crate02_static.mdl" ) -- (String) Model to use
ENT.KitBagSkin = 1 -- (Integer) Skin to use
ENT.RemoveOnSpawn = false -- Remove the weapon on spawn, I.E. for the fists or unarmed SWeps
ENT.ContentCollisionSound = Sound( "BaseCombatWeapon.WeaponDrop" ) -- Physics collisions
ENT.BagCollisionSound = Sound( "Bounce.Wood" ) -- Physics collisions
ENT.PickupSound = Sound( "Breakable.Crate" ) -- Pickup sounds

ENT.GibOne = { Model("models/jessev92/dods/props/crates/supply_crate02_gib1_static.mdl") , Vector( 0 , 0 , 0 ) , Angle( 0 , 0 , 0 ) , 1 } -- ( Table: First Gib Model , Vector(Pos) , Angle(Ang) , Skin(Integer) )
ENT.GibTwo = { Model("models/jessev92/dods/props/crates/supply_crate02_gib2_static.mdl") , Vector( 0 , 0 , 5 ) , Angle( 0 , 10 , 0 ) , 1 } -- ( Table: First Gib Model , Vector(Pos) , Angle(Ang) , Skin(Integer) )
