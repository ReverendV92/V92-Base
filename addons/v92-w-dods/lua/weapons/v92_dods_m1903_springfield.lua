
------------------------------------------------------
--	Day of Defeat: Source							--
--	M1903 Springfield Sniper Rifle					--
--	7.62x63mm (.30-06) Bolt-Action rifle			--
------------------------------------------------------

AddCSLuaFile( )

SWEP.PrintName = "M1903 Springfield" -- (String) Printed name on menu

if not VNTCB then
	Error( "V92 Content Bases not mounted; Removing Weapon: " .. SWEP.PrintName .. "\n" )
	return false
--elseif IsMounted( "dod" ) == false then
	--Error( "Day of Defeat: Source not mounted; Removing Weapon: " .. SWEP.PrintName .. "\n" )
	--return false
end

SWEP.Base = VNT_BASE_WEAPON_SHELL -- (String) Weapon base parent this is a child of
SWEP.Spawnable = true -- (Boolean) Can be spawned via the menu
SWEP.AdminOnly = false -- (Boolean) Admin only spawnable
------------------------------------------------------
--	Client Information								--	Info used in the client block of the weapon
------------------------------------------------------
SWEP.WeaponName = "v92_dods_m1903_springfield" -- (String) Name of the weapon script
SWEP.WeaponEntityName = SWEP.WeaponName .. "_ent" -- (String) Name of the weapon entity in Lua/Entities/Entityname.lua
SWEP.Manufacturer = VNT_WEAPON_MANUFACTURER_SPRINGFIELD -- (String) Gun company that makes this weapon
SWEP.CountryOfOrigin = VNT_WEAPON_COUNTRY_UNITEDSTATES -- (String) Country of origin
SWEP.MagazineName = VNTCB.Magazine.m3006Stripper -- (String) The name of the magazine the weapon uses - used in my Weapon Magazine System
SWEP.Category = VNT_CATEGORY_DODS -- (String) Category
SWEP.Instructions = VNTCB.instructions -- (String) Instruction
SWEP.Author = VNTCB.author -- (String) Author
SWEP.Contact = VNTCB.contact -- (String) Contact
SWEP.Slot = VNT_WEAPON_BUCKETPOS_RIFLE -- (Integer) Bucket to place weapon in, 1 to 6
SWEP.SlotPos = VNT_WEAPON_SLOTPOS_RIFLE -- (Integer) Bucket position
SWEP.WorkshopID = "891239198" -- (Integer) Workshop ID number of the upload that contains this file.

------------------------------------------------------
--	Model Information								--
------------------------------------------------------
SWEP.ViewModelFlip = false -- (Boolean) Only used for vanilla CS:S models
SWEP.ViewModel = Model( "models/jessev92/dods/weapons/springfield_c.mdl" ) -- (String) View model - v_*
SWEP.WorldModel = Model( "models/jessev92/dods/weapons/springfield_w.mdl" ) -- (String) World model - w_*
SWEP.UseHands = true -- (Boolean) Leave at false unless the model uses C_Arms
SWEP.HoldType = "ar2" -- (String) Hold type for our weapon, refer to wiki for animation sets
------------------------------------------------------
--	Gun Types										--	Set the type of weapon - ONLY PICK ONE!
------------------------------------------------------

------------------------------------------------------
--	Primary Fire Settings							--	Settings for the primary fire of the weapon
------------------------------------------------------
SWEP.Primary.ClipSize = VNTCB.Magazine.m3006Stripper[3] -- (Integer) Size of a magazine
SWEP.Primary.DefaultClip = VNTCB.Magazine.m3006Stripper[3] -- (Integer) Default number of ammo you spawn with
SWEP.Primary.Ammo = "762x63mm" -- (String) Primary ammo used by the weapon, bullets probably
SWEP.Primary.PureDmg = VNTCB.Ammo.a3006[ 1 ] -- (Integer) Base damage, put one number here and the base will do the rest
SWEP.Primary.RPM = 38 -- (Integer) Go to a wikipedia page and look at the RPM of the weapon, then put that here - the base will do the math
------------------------------------------------------
--	Gun Mechanics									--	Various things to tweak the effects and feedback
------------------------------------------------------
SWEP.CanChamber = false -- (Boolean) Can we load a round into the chamber?
SWEP.FireMode = { false , true , false , false } -- (Table: Boolean, Boolean, Boolean, Boolean ) Enable different fire modes on the weapon; Has modes, Has Single, Has Burst, Has Auto - in that order. You can have more than one, but the first must be true
SWEP.CurrentMode = 1 -- (Integer) Current fire mode of the weapon; used to set the default mode; corresponds to the FireMode table
SWEP.Weight = 4 -- (Integer) The weight in Kilogrammes of our weapon - used in my weapon weight mod!
SWEP.StrongPenetration = VNTCB.Ammo.a3006[ 2 ] -- (Integer) Max penetration
SWEP.WeakPenetration = VNTCB.Ammo.a3006[ 3 ] -- (Integer) Max wood penetration
SWEP.EffectiveRange = 914 -- (Integer) Effective range of the weapon in metres.
SWEP.StoppageRate = 0 -- (Integer) Rate of stoppages in the weapon, look up the real world number estimations and just throw that in here.
SWEP.DisableHoldIronToggle = true -- (Boolean) Force this weapon to use toggle irons instead of hold irons
------------------------------------------------------
--	Special FX										--	Muzzle flashes, shell casings, etc
------------------------------------------------------
SWEP.MuzzleAttach = 1 -- (Integer) The number of the attachment point for muzzle flashes, typically "1"
SWEP.MuzzleFlashType = 4 -- (Integer) The number of the muzzle flash to use; see Lua/Effects/fx_muzzleflash.Lua
SWEP.ShellAttach = 2 -- (Integer) The number of the attachment point for shell ejections, typically "2"
SWEP.ShellType = 18 -- (Integer) The shell to use, see Lua/Effects/FX_ShellEject for integers
SWEP.EjectLastShell = false -- (Boolean) Does the weapon eject the last shell casing? I.E. in DoD:S pack, the ACT_VM_FIRELAST doesn't pull the bolt to eject, so we don't want to eject a shell casing
SWEP.ShellDelay = 0.50 -- (Float) Time between shot firing and shell ejection; useful for bolt-actions and things like that that need a delay
------------------------------------------------------
--	Shotgun Settings								--	Settings for shotguns
------------------------------------------------------

SWEP.StartInsertsARound = true -- (Boolean) Does the start reloading anim insert a round in the sequence?
------------------------------------------------------
--	Sniper Settings									--	Settings for sniper rifles
------------------------------------------------------
SWEP.ScopeType = 1 -- (Integer) Type of scope, 0=none, 1=overlay, 2=Render Target
SWEP.ScopeMaterial = 4 -- (Integer) Type of overlay, 0=Red Dot, 1=EOTech, 2=ACOG, 3=SVD, 4=M14, 5=L42A1, 6=PSG1, 7=German tri-bar
SWEP.ScopeMultipliers = { 1 , 0.2 , 0.6 } -- (Table: Float, Float, Float) Zoom Muliplier, Default Zoom x, Close Zoom x
SWEP.BoltAction = true -- (Boolean) Is this bolt action? Removes the player from the scope after firing
SWEP.ReturnToScope = false -- (Boolean) Return to scope after cycling the bolt?
SWEP.HasVariableZoom = true -- (Boolean) Does the weapon have variable zoom?
------------------------------------------------------
--	Custom Sounds									--	Setup sounds here!
------------------------------------------------------

SWEP.Sounds = {
	["Primary"] = Sound( "DODS.Springfield.Shoot" ), -- (String) Primary shoot sound
	["PrimaryDry"] = Sound( "VNTCB.SWep.Empty5" ), -- (String) Primary dry fire sound
--	["Reload"] = Sound( "DODS.Springfield.WorldReload" ), -- (String) Reload sound

	["Noise_Close"] = Sound( "BF3.BulletCraft.Noise.Rifle.Close" ),
	["Noise_Distant"] = Sound( "BF3.BulletCraft.Noise.Forest.Distant" ),
	["Noise_Far"] = Sound( "BF3.BulletCraft.Noise.Forest.Far" ),
	["CoreBass_Close"] = Sound( "BF3.BulletCraft.CoreBass.Close.OneShot_5" ),
	["CoreBass_Distant"] = Sound( "BF3.BulletCraft.CoreBass.Distant.OneShot_1" ),
	["HiFi"] = Sound( "BF3.BulletCraft.HiFi.BoltAction" ),
	["Reflection_Close"] = Sound( "BF3.BulletCraft.Reflection.Forest.Close" ),
	["Reflection_Far"] = Sound( "BF3.BulletCraft.Reflection.Forest.Far" ),
}

//SWEP.ReloadSNDDelay = 0.08 -- (Float) Reload sound delay
SWEP.SelectorSwitchSNDType = 1 -- (Integer) 1=US , 2=RU
SWEP.UsesSuperSonicAmmo = true -- (Boolean) Is the weapon using supersonic or subsonic ammo?
------------------------------------------------------
--	Ironsight & Run Positions						--	Set our model transforms for running and ironsights
------------------------------------------------------
SWEP.IronSightsPos = Vector(-5.321, -12, 1.1)
SWEP.IronSightsAng = Vector(0, 0, 0)
SWEP.RunArmOffset = Vector(3, -14, -4)
SWEP.RunArmAngle = Vector(-17, 70, -38)

------------------------------------------------------
--	Setup Clientside Info							--	This block must be in every weapon!
------------------------------------------------------
if CLIENT then

	SWEP.WepSelectIcon = surface.GetTextureID( "vgui/hud/" .. SWEP.WeaponName )
	SWEP.RenderGroup = RENDERGROUP_BOTH
	language.Add( SWEP.WeaponName , SWEP.PrintName )
	killicon.Add( SWEP.WeaponName , "vgui/entities/" .. SWEP.WeaponName , Color( 255 , 255 , 255 ) )

elseif SERVER then

	resource.AddWorkshop( SWEP.WorkshopID )

end

------------------------------------------------------
--	SWEP:Initialize() 							--	Called when the weapon is first loaded
------------------------------------------------------
function SWEP:Initialize( )

	self.HoldMeRight = VNTCB.HoldType.Rifle -- (String) Hold type table for our weapon, Lua/autorun/sh_v92_base_swep.Lua

end

SWEP.SeqPrimary = { "shoot" }
SWEP.SeqPrimaryEmpty = { "shoot_empty" }
SWEP.SeqIdle = { "idle" }
SWEP.SeqReloadStart = { "reload_start" }
SWEP.SeqReloadLoop = { "reload_loop" }
SWEP.SeqReloadEnd = { "reload_end" }
SWEP.SeqDraw = { "draw" }
