
------------------------------------------------------
--	Day of Defeat: Source							--
--	M1918A2 Browning Automatic Rifle				--
--	.30-06 / 7.62x63mm Light Machine Gun			--
------------------------------------------------------

AddCSLuaFile( )

SWEP.PrintName = "M1918A2 BAR" -- (String) Printed name on menu

if not VNTCB then
	Error( "V92 Content Bases not mounted; Removing Weapon: " .. SWEP.PrintName .. "\n" )
	return false
--elseif IsMounted( "dod" ) == false then
	--Error( "Day of Defeat: Source not mounted; Removing Weapon: " .. SWEP.PrintName .. "\n" )
	--return false
end

SWEP.Base = VNT_BASE_WEAPON -- (String) Weapon base parent this is a child of
SWEP.Spawnable = true -- (Boolean) Can be spawned via the menu
SWEP.AdminOnly = false -- (Boolean) Admin only spawnable
------------------------------------------------------
--	Client Information								--	Info used in the client block of the weapon
------------------------------------------------------
SWEP.WeaponName = "v92_dods_m1918a2_bar" -- (String) Name of the weapon script
SWEP.WeaponEntityName = SWEP.WeaponName .. "_ent" -- (String) Name of the weapon entity in Lua/Entities/Entityname.lua
SWEP.Manufacturer = VNT_WEAPON_MANUFACTURER_WINCHESTER -- (String) Gun company that makes this weapon
SWEP.CountryOfOrigin = VNT_WEAPON_COUNTRY_UNITEDSTATES -- (String) Country of origin
SWEP.MagazineName = VNTCB.Magazine.mBAR -- (String) The name of the magazine the weapon uses - used in my Weapon Magazine System
SWEP.Category = VNT_CATEGORY_DODS -- (String) Category
SWEP.Instructions = VNTCB.instructions -- (String) Instruction
SWEP.Author = VNTCB.author -- (String) Author
SWEP.Contact = VNTCB.contact -- (String) Contact
SWEP.Slot = VNT_WEAPON_BUCKETPOS_LMG -- (Integer) Bucket to place weapon in, 1 to 6
SWEP.SlotPos = VNT_WEAPON_SLOTPOS_LMG -- (Integer) Bucket position
SWEP.ViewModelFOV = 80 -- (Integer) First-person field of view
SWEP.WorkshopID = "891239198" -- (Integer) Workshop ID number of the upload that contains this file.

------------------------------------------------------
--	Model Information								--
------------------------------------------------------
SWEP.ViewModelFlip = false -- (Boolean) Only used for vanilla CS:S models
SWEP.ViewModel = Model( "models/jessev92/dods/weapons/bar_c.mdl" ) -- (String) View model - v_*
SWEP.WorldModel = Model( "models/jessev92/dods/weapons/bar_w.mdl" ) -- (String) World model - w_*
SWEP.UseHands = true -- (Boolean) Leave at false unless the model uses C_Arms
SWEP.HoldType = "ar2" -- (String) Hold type for our weapon, refer to wiki for animation sets
------------------------------------------------------
--	Gun Types										--	Set the type of weapon - ONLY PICK ONE!
------------------------------------------------------
SWEP.WeaponType = VNTCB.WeaponType.LMG -- (Integer) 1=Melee, 2=Pistol, 3=Rifle, 4=Shotgun, 5=Sniper, 6=Grenade Launcher, 7=Rocket Launcher
------------------------------------------------------
--	Primary Fire Settings							--	Settings for the primary fire of the weapon
------------------------------------------------------
SWEP.Primary.ClipSize = VNTCB.Magazine.mBAR[3] -- (Integer) Size of a magazine
SWEP.Primary.DefaultClip = VNTCB.Magazine.mBAR[3] -- (Integer) Default number of ammo you spawn with
SWEP.Primary.Ammo = "762x63mm" -- (String) Primary ammo used by the weapon, bullets probably
SWEP.Primary.PureDmg = VNTCB.Ammo.a3006[ 1 ] -- (Integer) Base damage, put one number here and the base will do the rest
SWEP.Primary.RPM = 300 -- (Integer) Go to a wikipedia page and look at the RPM of the weapon, then put that here - the base will do the math
------------------------------------------------------
--	Gun Mechanics									--	Various things to tweak the effects and feedback
------------------------------------------------------
SWEP.CanReloadWhenNotEmpty = true -- (Boolean) Can we reload when not empty? true=M16, false=M1 Garand
SWEP.FireMode = { true , false , false , true } -- (Table: Boolean, Boolean, Boolean, Boolean ) Enable different fire modes on the weapon; Has modes, Has Single, Has Burst, Has Auto - in that order. You can have more than one, but the first must be true
SWEP.HasSpecialFireMode = { true , true , 1 , 650 } -- (Table: Boolean, Boolean, Integer, Integer) Has special fire mode, Is auto, burst count, special mode RPM; used on the Browning Automatic Rifle
SWEP.SpecialModeMessage = "FA - Fast Automatic" -- (String) Message printed when switching to the special mode
SWEP.CurrentMode = 3 -- (Integer) Current fire mode of the weapon; used tow set the default mode; corresponds to the FireMode table
SWEP.Weight = 9 -- (Integer) The weight in Kilogrammes of our weapon - used in my weapon weight mod!
SWEP.StrongPenetration = VNTCB.Ammo.a3006[ 2 ] -- (Integer) Max penetration
SWEP.WeakPenetration = VNTCB.Ammo.a3006[ 3 ] -- (Integer) Max wood penetration
SWEP.EffectiveRange = 150 -- (Integer) Effective range of the weapon in metres.
SWEP.StoppageRate = 1500 -- (Integer) Rate of stoppages in the weapon, look up the real world number estimations and just throw that in here.
------------------------------------------------------
--	Special FX										--	Muzzle flashes, shell casings, etc
------------------------------------------------------
SWEP.MuzzleAttach = 1 -- (Integer) The number of the attachment point for muzzle flashes, typically "1"
SWEP.MuzzleFlashType = 4 -- (Integer) The number of the muzzle flash to use; see Lua/Effects/fx_muzzleflash.Lua
SWEP.ShellAttach = 2 -- (Integer) The number of the attachment point for shell ejections, typically "2"
SWEP.ShellType = 18 -- (Integer) The shell to use, see Lua/Effects/FX_ShellEject for integers
------------------------------------------------------
--	Custom Sounds									--	Setup sounds here!
------------------------------------------------------

SWEP.Sounds = {
	["Primary"] = Sound( "DODS.BAR.Single" ), -- (String) Primary shoot sound
	["PrimaryDry"] = Sound( "VNTCB.SWep.Empty5" ), -- (String) Primary dry fire sound
--	["Reload"] = Sound( "DODS.BAR.WorldReload" ), -- (String) Reload sound

	["Noise_Close"] = Sound( "BF3.BulletCraft.Noise.Rifle.Close" ),
	["Noise_Distant"] = Sound( "BF3.BulletCraft.Noise.Forest.Distant" ),
	["Noise_Far"] = Sound( "BF3.BulletCraft.Noise.Forest.Far" ),
	["CoreBass_Close"] = Sound( "BF3.BulletCraft.CoreBass.Close.LMG_12" ),
	["CoreBass_Distant"] = Sound( "BF3.BulletCraft.CoreBass.Distant.LMG_11" ),
	["HiFi"] = Sound( "BF3.BulletCraft.HiFi.M249_2" ),
	["Reflection_Close"] = Sound( "BF3.BulletCraft.Reflection.Forest.Close" ),
	["Reflection_Far"] = Sound( "BF3.BulletCraft.Reflection.Forest.Far" ),
}

SWEP.SelectorSwitchSNDType = 1 -- (Integer) 1=US , 2=RU
SWEP.UsesSuperSonicAmmo = true -- (Boolean) Is the weapon using supersonic or subsonic ammo?
------------------------------------------------------
--	Ironsight & Run Positions						--	Set our model transforms for running and ironsights
------------------------------------------------------
SWEP.IronSightsPos = Vector(-6.25, -3, 5.53)
SWEP.IronSightsAng = Vector(0, 0, 0)
SWEP.RunArmOffset = Vector(0, -6, -5)
SWEP.RunArmAngle = Vector(15, 70, -20)

------------------------------------------------------
--	Setup Clientside Info							--	This block must be in every weapon!
------------------------------------------------------
if CLIENT then

	SWEP.WepSelectIcon = surface.GetTextureID( "vgui/hud/" .. SWEP.WeaponName )
	SWEP.RenderGroup = RENDERGROUP_BOTH
	language.Add( SWEP.WeaponName , SWEP.PrintName )
	killicon.Add( SWEP.WeaponName , "vgui/entities/" .. SWEP.WeaponName , Color( 255 , 255 , 255 ) )

elseif SERVER then

	resource.AddWorkshop( SWEP.WorkshopID )

end

------------------------------------------------------
--	SWEP:Initialize() 							--	Called when the weapon is first loaded
------------------------------------------------------
function SWEP:Initialize( )

	self.HoldMeRight = VNTCB.HoldType.LMG -- (String) Hold type table for our weapon, Lua/autorun/sh_v92_base_swep.Lua

end

SWEP.SeqPrimary = { "down_shoot1" , "down_shoot2" , "down_shoot3" }
SWEP.SeqIdle = { "down_idle" }
SWEP.SeqReload = { "down_reload" }
SWEP.SeqDraw = { "down_draw" }
SWEP.SeqMode = { "down_to_up" }
