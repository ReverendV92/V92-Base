
------------------------------------------------------
--	Day of Defeat: Source							--
--	M1919A4 Browning Machine Gun					--
--	.30-06 / 7.62x63mm Machine Gun					--
------------------------------------------------------

AddCSLuaFile( )

SWEP.PrintName = "M1919A4" -- (String) Printed name on menu

if not VNTCB then
	Error( "V92 Content Bases not mounted; Removing Weapon: " .. SWEP.PrintName .. "\n" )
	return false
--elseif IsMounted( "dod" ) == false then
	--Error( "Day of Defeat: Source not mounted; Removing Weapon: " .. SWEP.PrintName .. "\n" )
	--return false
end

SWEP.Base = VNT_BASE_WEAPON_BIPOD -- (String) Weapon base parent this is a child of
SWEP.Spawnable = false --true -- (Boolean) Can be spawned via the menu
SWEP.AdminOnly = false -- (Boolean) Admin only spawnable
------------------------------------------------------
--	Client Information								--	Info used in the client block of the weapon
------------------------------------------------------
SWEP.WeaponName = "v92_dods_m1919a4_30cal" -- (String) Name of the weapon script
SWEP.WeaponEntityName = SWEP.WeaponName .. "_ent" -- (String) Name of the weapon entity in Lua/Entities/Entityname.lua
SWEP.Manufacturer = VNT_WEAPON_MANUFACTURER_WINCHESTER -- (String) Gun company that makes this weapon
SWEP.CountryOfOrigin = VNT_WEAPON_COUNTRY_UNITEDSTATES -- (String) Country of origin
SWEP.MagazineName = VNTCB.Magazine.m3006Belt -- (String) The name of the magazine the weapon uses - used in my Weapon Magazine System
SWEP.Category = VNT_CATEGORY_DODS -- (String) Category
SWEP.Instructions = VNTCB.instructions -- (String) Instruction
SWEP.Author = VNTCB.author -- (String) Author
SWEP.Contact = VNTCB.contact -- (String) Contact
SWEP.Purpose = VNTCB.purpose -- (String) Purpose
SWEP.Slot = VNT_WEAPON_BUCKETPOS_LMG -- (Integer) Bucket to place weapon in, 1 to 6
SWEP.SlotPos = VNT_WEAPON_SLOTPOS_LMG -- (Integer) Bucket position
SWEP.ViewModelFOV = 50 -- (Integer) First-person field of view
SWEP.WorkshopID = "891239198" -- (Integer) Workshop ID number of the upload that contains this file.

------------------------------------------------------
--	Model Information								--
------------------------------------------------------
SWEP.ViewModelFlip = false -- (Boolean) Only used for vanilla CS:S models
SWEP.ViewModel = Model( "models/jessev92/dods/weapons/30cal_c.mdl" ) -- (String) View model - v_*
SWEP.WorldModel = Model( "models/jessev92/dods/weapons/30cal_w.mdl" ) -- (String) World model - w_*
SWEP.UseHands = true -- (Boolean) Leave at false unless the model uses C_Arms
SWEP.HoldType = "ar2" -- (String) Hold type for our weapon, refer to wiki for animation sets
------------------------------------------------------
--	Gun Types										--	Set the type of weapon - ONLY PICK ONE!
------------------------------------------------------
SWEP.WeaponType = VNTCB.WeaponType.LMG -- (Integer) 1=Melee, 2=Pistol, 3=Rifle, 4=Shotgun, 5=Sniper, 6=Grenade Launcher, 7=Rocket Launcher
--SWEP.DeployableType = 5 -- (Integer) 0=None, 1=Grenade Launcher, 2=Bipod, 3=Model Swap Bipod, 4=Model Swap Stock Deploy, 5=Deployable
--SWEP.DeployedModeUsesStockIrons = true -- (Boolean) The deployed mode uses the vectors for the folded/unfolded stock irons?
------------------------------------------------------
--	Primary Fire Settings							--	Settings for the primary fire of the weapon
------------------------------------------------------
SWEP.Primary.ClipSize = VNTCB.Magazine.m3006Belt[3] -- (Integer) Size of a magazine
SWEP.Primary.DefaultClip = VNTCB.Magazine.m3006Belt[3] -- (Integer) Default number of ammo you spawn with
SWEP.Primary.Ammo = "762x63mm" -- (String) Primary ammo used by the weapon, bullets probably
SWEP.Primary.PureDmg = VNTCB.Ammo.a3006[ 1 ] -- (Integer) Base damage, put one number here and the base will do the rest
SWEP.Primary.RPM = 600 -- (Integer) Go to a wikipedia page and look at the RPM of the weapon, then put that here - the base will do the math
------------------------------------------------------
--	Gun Mechanics									--	Various things to tweak the effects and feedback
------------------------------------------------------
SWEP.FireMode = { false , false , false , true } -- (Table: Boolean, Boolean, Boolean, Boolean ) Enable different fire modes on the weapon; Has modes, Has Single, Has Burst, Has Auto - in that order. You can have more than one, but the first must be true
SWEP.CurrentMode = 3 -- (Integer) Current fire mode of the weapon; used to set the default mode; corresponds to the FireMode table
SWEP.Weight = 14 -- (Integer) The weight in Kilogrammes of our weapon - used in my weapon weight mod!
SWEP.StrongPenetration = VNTCB.Ammo.a3006[ 2 ] -- (Integer) Max penetration
SWEP.WeakPenetration = VNTCB.Ammo.a3006[ 3 ] -- (Integer) Max wood penetration
SWEP.EffectiveRange = 1400 -- (Integer) Effective range of the weapon in metres.
SWEP.StoppageRate = 2500 -- (Integer) Rate of stoppages in the weapon, look up the real world number estimations and just throw that in here.
SWEP.BlackFriday = true -- (Boolean) Black Friday Doorbuster Sales
------------------------------------------------------
--	Special FX										--	Muzzle flashes, shell casings, etc
------------------------------------------------------
SWEP.MuzzleAttach = 1 -- (Integer) The number of the attachment point for muzzle flashes, typically "1"
SWEP.MuzzleFlashType = 4 -- (Integer) The number of the muzzle flash to use; see Lua/Effects/fx_muzzleflash.Lua
SWEP.ShellAttach = 2 -- (Integer) The number of the attachment point for shell ejections, typically "2"
SWEP.ShellType = 18 -- (Integer) The shell to use, see Lua/Effects/FX_ShellEject for integers
------------------------------------------------------
--	Machine Gun Settings							--	Things typically used on machine guns
------------------------------------------------------
SWEP.BeltFed = false -- (Boolean) Is it belt fed?
SWEP.BeltFedAnimatedDoD = true -- (Boolean) Is the belt empty/full set up via animations like DoD:S?
------------------------------------------------------
--	Custom Sounds									--	Setup sounds here!
------------------------------------------------------

SWEP.Sounds = {
	["Primary"] = Sound( "DODS.30cal.Shoot" ), -- (String) Primary shoot sound
	["PrimaryDry"] = Sound( "VNTCB.SWep.Empty5" ), -- (String) Primary dry fire sound
--	["Reload"] = Sound( "DODS.30cal.WorldReload" ), -- (String) Reload sound

	["Noise_Close"] = Sound( "BF3.BulletCraft.Noise.Rifle.Close" ),
	["Noise_Distant"] = Sound( "BF3.BulletCraft.Noise.Forest.Distant" ),
	["Noise_Far"] = Sound( "BF3.BulletCraft.Noise.Forest.Far" ),
	["CoreBass_Close"] = Sound( "BF3.BulletCraft.CoreBass.Close.LMG_12" ),
	["CoreBass_Distant"] = Sound( "BF3.BulletCraft.CoreBass.Distant.LMG_11" ),
	["HiFi"] = Sound( "BF3.BulletCraft.HiFi.M240_2" ),
	["Reflection_Close"] = Sound( "BF3.BulletCraft.Reflection.Forest.Close" ),
	["Reflection_Far"] = Sound( "BF3.BulletCraft.Reflection.Forest.Far" ),
}

SWEP.ReloadSNDDelay = 0.005 -- (Float) Reload sound delay
SWEP.SelectorSwitchSNDType = 1 -- (Integer) 1=US , 2=RU
SWEP.UsesSuperSonicAmmo = true -- (Boolean) Is the weapon using supersonic or subsonic ammo?
------------------------------------------------------
--	Ironsight & Run Positions						--	Set our model transforms for running and ironsights
------------------------------------------------------
SWEP.IronSightsPos = Vector(-4.45, -5, 4.099)
SWEP.IronSightsAng = Vector(0, -0.401, 9)
SWEP.StockIronSightsPos = Vector( 0 , 0 , 0 ) -- (Vector) Unfolded Stock Ironsight XYZ Transform
SWEP.StockIronSightsAng = Vector( 0 , 0 , 0 ) -- (Vector) Unfolded Stock Ironsight XYZ Rotation
SWEP.RunArmOffset = Vector(4, -14, 0)
SWEP.RunArmAngle = Vector(-10, 70, -19)

------------------------------------------------------
--	Setup Clientside Info							--	This block must be in every weapon!
------------------------------------------------------
if CLIENT then

	SWEP.WepSelectIcon = surface.GetTextureID( "vgui/hud/" .. SWEP.WeaponName )
	SWEP.RenderGroup = RENDERGROUP_BOTH
	language.Add( SWEP.WeaponName , SWEP.PrintName )
	killicon.Add( SWEP.WeaponName , "vgui/entities/" .. SWEP.WeaponName , Color( 255 , 255 , 255 ) )

elseif SERVER then

	resource.AddWorkshop( SWEP.WorkshopID )

end

------------------------------------------------------
--	SWEP:Initialize() 							--	Called when the weapon is first loaded
------------------------------------------------------
function SWEP:Initialize( )

	self.HoldMeRight = VNTCB.HoldType.M1919 -- (String) Hold type table for our weapon, Lua/autorun/sh_v92_base_swep.Lua

end

SWEP.SeqPrimary = { "upshoot8" }
SWEP.SeqPrimaryEmpty = { "upshoot" }
SWEP.SeqPrimary1 = { "upshoot1" }
SWEP.SeqPrimary2 = { "upshoot2" }
SWEP.SeqPrimary3 = { "upshoot3" }
SWEP.SeqPrimary4 = { "upshoot4" }
SWEP.SeqPrimary5 = { "upshoot5" }
SWEP.SeqPrimary6 = { "upshoot6" }
SWEP.SeqPrimary7 = { "upshoot7" }
SWEP.SeqPrimary8 = { "upshoot8" }

SWEP.SeqDeployedFireEmpty = { "downshoot" }
SWEP.SeqDeployedFire = { "downshoot_1" , "downshoot_2" , "downshoot_3" }
SWEP.SeqDeployedFire1 = { "downshoot1" }
SWEP.SeqDeployedFire2 = { "downshoot2" }
SWEP.SeqDeployedFire3 = { "downshoot3" }
SWEP.SeqDeployedFire4 = { "downshoot4" }
SWEP.SeqDeployedFire5 = { "downshoot5" }
SWEP.SeqDeployedFire6 = { "downshoot6" }
SWEP.SeqDeployedFire7 = { "downshoot7" }
SWEP.SeqDeployedFire8 = { "downshoot8" }

SWEP.SeqDeploy = { "uptodown8" }
SWEP.SeqDeployEmpty = { "uptodownempty" }
SWEP.SeqDeploy1 = { "uptodown1" }
SWEP.SeqDeploy2 = { "uptodown2" }
SWEP.SeqDeploy3 = { "uptodown3" }
SWEP.SeqDeploy4 = { "uptodown4" }
SWEP.SeqDeploy5 = { "uptodown5" }
SWEP.SeqDeploy6 = { "uptodown6" }
SWEP.SeqDeploy7 = { "uptodown7" }
SWEP.SeqDeploy8 = { "uptodown8" }

SWEP.SeqIdleDeployedEmpty = { "downidleempty" }
SWEP.SeqIdleDeployed = { "downidle8" }
SWEP.SeqIdleDeployed1 = { "downidle1" }
SWEP.SeqIdleDeployed2 = { "downidle2" }
SWEP.SeqIdleDeployed3 = { "downidle3" }
SWEP.SeqIdleDeployed4 = { "downidle4" }
SWEP.SeqIdleDeployed5 = { "downidle5" }
SWEP.SeqIdleDeployed6 = { "downidle6" }
SWEP.SeqIdleDeployed7 = { "downidle7" }
SWEP.SeqIdleDeployed8 = { "downidle8" }

SWEP.SeqUnDeploy = { "downtoup8" }
SWEP.SeqUnDeployEmpty = { "downtoupempty" }
SWEP.SeqUnDeploy1 = { "downtoup1" }
SWEP.SeqUnDeploy2 = { "downtoup2" }
SWEP.SeqUnDeploy3 = { "downtoup3" }
SWEP.SeqUnDeploy4 = { "downtoup4" }
SWEP.SeqUnDeploy5 = { "downtoup5" }
SWEP.SeqUnDeploy6 = { "downtoup6" }
SWEP.SeqUnDeploy7 = { "downtoup7" }
SWEP.SeqUnDeploy8 = { "downtoup8" }

SWEP.SeqIdleUnDeployed = { "upidle8" }
SWEP.SeqIdleUnDeployedEmpty = { "upidleempty" }
SWEP.SeqIdleUnDeployed1 = { "upidle1" }
SWEP.SeqIdleUnDeployed2 = { "upidle2" }
SWEP.SeqIdleUnDeployed3 = { "upidle3" }
SWEP.SeqIdleUnDeployed4 = { "upidle4" }
SWEP.SeqIdleUnDeployed5 = { "upidle5" }
SWEP.SeqIdleUnDeployed6 = { "upidle6" }
SWEP.SeqIdleUnDeployed7 = { "upidle7" }
SWEP.SeqIdleUnDeployed8 = { "upidle8" }

SWEP.SeqReload = { "reload" }
SWEP.SeqReloadDeployed = { "reload" }
SWEP.SeqDraw = { "draw" }
