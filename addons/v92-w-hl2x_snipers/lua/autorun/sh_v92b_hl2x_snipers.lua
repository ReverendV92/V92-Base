
---------------------------------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------------------------
--	Half-Life 2: Expanded - Sniper Rifles
---------------------------------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------------------------

--	Catch-22 Diabolis Ex Machina Prevention

if SERVER then
	resource.AddWorkshop("505106454") 	--	V92 Code Bases
end

-- SOUND GUIDE
----------
-- Channel = Typically CHAN_WEAPON or CHAN_ITEM for gunshots and gun parts respectively; CHAN_BODY for foley
-- Volume = Leave at 1.0
-- Pitch = Typically between 95 and 105 for gunshots
-- Level = For gunshots, follow this estimation table as a LOOSE guideline:
----------
--	.22LR = 40dB
--	9x19mm = 60dB
--	.38 Special = 70dB
--	5.56x45mm = 70dB
--	7.62x51mm = 90dB
--	.45 ACP = 90dB
--	.50 AE = 95dB
--	.50 BMG = 100dB
--	12-Gauge Buckshot = 120dB
--	Explosion = 200dB
--	Suppressed = 2/3 of normal
--	Empty = 60dB 
--	Weapon sounds = ~50dB (use your head)
--	Foley = ~30dB (use your head)
----------
----------

if not VNTCB then
	Error( "V92 Content Bases not mounted :: Removing Autorun File :: HL2X Snipers\n" )

	return false
end

if VNTCB.Plugins then
	table.insert( VNTCB.Plugins, 0, "VNT :: HL2X Sniper Pack" )
end

------------------------------------------------------------------------------------------
--	HL2X :: Sniper Weapons
------------------------------------------------------------------------------------------

---------------------------------------------
--	Remington Model 40 Sniper Rifle
---------------------------------------------

sound.Add( {
	name = "HL2_M40A3.Single" ,
	channel = CHAN_WEAPON ,
	volume = 1.0 ,
	level = 90 ,
	pitch = { 95 , 105 } ,
	sound = "^)jessev92/hl2/weapons/m40a3/fire.wav"
} )
util.PrecacheSound( "jessev92/hl2/weapons/m40a3/fire.wav" )

sound.Add( {
	name = "HL2_M40A3.Reload" ,
	channel = CHAN_ITEM ,
	volume = 1.0 ,
	level = 50 ,
	pitch = { 95 , 105 } ,
	sound = "^)jessev92/hl2/weapons/m40a3/reload.wav"
} )
util.PrecacheSound( "jessev92/hl2/weapons/m40a3/reload.wav" )

sound.Add( {
	name = "HL2_M40A3.Draw" ,
	channel = CHAN_BODY ,
	volume = 1.0 ,
	level = 50 ,
	pitch = { 95 , 105 } ,
	sound = "^)jessev92/weapons/univ/draw1.wav"
} )
util.PrecacheSound( "jessev92/weapons/univ/draw1.wav" )

sound.Add( {
	name = "HL2_M40A3.Foley" ,
	channel = CHAN_BODY ,
	volume = 1.0 ,
	level = 50 ,
	pitch = { 95 , 105 } ,
	sound = "^)jessev92/weapons/univ/draw2.wav"
} )
util.PrecacheSound( "jessev92/weapons/univ/draw2.wav" )

---------------------------------------------
--	Mosin Nagant Model 1891 Carbine
---------------------------------------------

sound.Add( {
	name = "HL2_Mosin.Single" ,
	channel = CHAN_WEAPON ,
	volume = 1.0 ,
	level = 85 ,
	pitch = { 95 , 105 } ,
	sound = "^)jessev92/hl2/weapons/mosin/single.wav"
} )
util.PrecacheSound( "jessev92/hl2/weapons/mosin/single.wav" )

sound.Add( {
	name = "HL2_Mosin.Reload" ,
	channel = CHAN_ITEM ,
	volume = 1.0 ,
	level = 50 ,
	pitch = { 95 , 105 } ,
	sound = "^)jessev92/hl2/weapons/mosin/reload_3.wav"
} )
util.PrecacheSound( "jessev92/hl2/weapons/mosin/reload_3.wav" )

sound.Add( {
	name = "HL2_Mosin.Draw" ,
	channel = CHAN_BODY ,
	volume = 1.0 ,
	level = 50 ,
	pitch = { 95 , 105 } ,
	sound = "^)jessev92/weapons/univ/draw1.wav"
} )
util.PrecacheSound( "jessev92/weapons/univ/draw1.wav" )

sound.Add( {
	name = "HL2_Mosin.Holster" ,
	channel = CHAN_BODY ,
	volume = 1.0 ,
	level = 50 ,
	pitch = { 95 , 105 } ,
	sound = "^)jessev92/weapons/univ/holster1.wav"
} )
util.PrecacheSound( "jessev92/weapons/univ/holster1.wav" )

sound.Add( {
	name = "HL2_Mosin.Foley" ,
	channel = CHAN_BODY ,
	volume = 1.0 ,
	level = 75 ,
	pitch = { 95 , 105 } ,
	sound = "^)jessev92/weapons/univ/draw2.wav"
} )
util.PrecacheSound( "jessev92/weapons/univ/draw2.wav" )