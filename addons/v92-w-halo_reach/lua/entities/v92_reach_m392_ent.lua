
AddCSLuaFile()
------------------------------------------------------
if not VNTCB then return false end					--	Prevent this file from loading if for some odd reason the base Lua isn't loaded
------------------------------------------------------
ENT.Base				= VNT_BASE_WEAPON_ENTITY			--	Weapon Entity Base
ENT.Type				= "anim"					--	Type
ENT.PrintName			= "M392"					--	Printed Name
ENT.Author				= VNTCB.author				--	Author
ENT.Information			= "Uses 7.62 NATO ammo, and M392 magazines"	--	Info like ammo and magazine
ENT.Category			= VNT_CATEGORY_HALOREACH			--	Category
ENT.Spawnable			= true						--	Spawnable?
ENT.AdminOnly			= false						--	Admin Only?
ENT.SWepName			= "v92_reach_m392" -- (String) Name of the weapon entity in Lua/weapons/swep_name.lua
ENT.WeaponName			= ENT.SWepName .. "_ent" -- (String) Name of this entity	
ENT.SEntModel			= Model( "models/jessev92/halo/reach/weapons/unsc/m392_w.mdl" ) -- (String) Model to use
