
AddCSLuaFile()
------------------------------------------------------
if not VNTCB then return false end					--	Prevent this file from loading if for some odd reason the base Lua isn't loaded
------------------------------------------------------
ENT.Base				= VNT_BASE_WEAPON_ENTITY			--	Weapon Entity Base
ENT.Type				= "anim"					--	Type
ENT.PrintName			= "Medkit"					--	Printed Name
ENT.Author				= VNTCB.author				--	Author
ENT.Information			= "Uses Medbags"	--	Info like ammo and magazine
ENT.Category			= VNT_CATEGORY_HALOREACH			--	Category
ENT.Spawnable			= true						--	Spawnable?
ENT.AdminOnly			= false						--	Admin Only?
-- (String) Name of the weapon entity in Lua/weapons/swep_name.lua
ENT.SWepName			= "v92_reach_medkit"			
-- (String) Name of this entity				
ENT.WeaponName			= ENT.SWepName .. "_ent"
-- (String) Model to use
ENT.SEntModel			= Model( "models/jessev92/halo/reach/weapons/unsc/medkit_w.mdl" )
