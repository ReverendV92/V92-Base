
------------------------------------------------------
--	Halo: Reach										--
--	MA37 Assault Rifle								--
------------------------------------------------------

AddCSLuaFile( )

SWEP.PrintName = "MA37 GL" -- (String) Printed name on menu

if not VNTCB then
	Error( "V92 Content Bases not mounted; Removing Weapon: " .. SWEP.PrintName .. "\n" )
	return false
end

SWEP.Base = VNTCB.Bases.Wep -- (String) Weapon base parent this is a child of
SWEP.Spawnable = true -- (Boolean) Can be spawned via the menu
SWEP.AdminOnly = false -- (Boolean) Admin only spawnable
------------------------------------------------------
--	Client Information								--	Info used in the client block of the weapon
------------------------------------------------------
SWEP.WeaponName = "v92_reach_ma37g" -- (String) Name of the weapon script
SWEP.WeaponEntityName = SWEP.WeaponName .. "_ent" -- (String) Name of the weapon entity in Lua/Entities/Entityname.lua
SWEP.Manufacturer = VNTCB.Manufacturer.MIS -- (String) Gun company that makes this weapon
SWEP.CountryOfOrigin = VNTCB.Country.MAR -- (String) Country of origin
SWEP.MagazineName = "MA5 Magazine" -- (String) The name of the magazine the weapon uses - used in my Weapon Magazine System
SWEP.Category = VNTCB.Category.Reach -- (String) Category
SWEP.Instructions = "Uses 7.62 NATO ammo, 40mm grenades, and MA5 magazines" -- (String) Instruction
SWEP.Purpose = VNTCB.purpose -- (String) Purpose of the weapon
SWEP.Author = VNTCB.author -- (String) Author
SWEP.Contact = VNTCB.contact -- (String) Contact
SWEP.Slot = VNTCB.Bucket.Rifle -- (Integer) Bucket to place weapon in, 1 to 6
SWEP.SlotPos = VNTCB.Slot.Rifle -- (Integer) Bucket position
SWEP.ViewModelFOV = 54 -- (Integer) First-person field of view
SWEP.WorkshopID = "503422951" -- (Integer) Workshop ID number of the upload that contains this file.
------------------------------------------------------
--	Model Information								--
------------------------------------------------------
SWEP.ViewModelFlip = false -- (Boolean) Only used for vanilla CS:S models
SWEP.ViewModel = Model( "models/jessev92/halo/reach/weapons/unsc/ma37g_v.mdl" ) -- (String) View model - v_*
SWEP.WorldModel = Model( "models/jessev92/halo/reach/weapons/unsc/ma37g_w.mdl" ) -- (String) World model - w_*
SWEP.UseHands = false -- (Boolean) Leave at false unless the model uses C_Arms
SWEP.HoldType = "ar2" -- (String) Hold type for our weapon, refer to wiki for animation sets
------------------------------------------------------
--	Gun Types										--	Set the type of weapon - ONLY PICK ONE!
------------------------------------------------------
SWEP.WeaponType = VNTCB.WeaponType.Rifle -- (Integer) 1=Melee, 2=Pistol, 3=Rifle, 4=Shotgun, 5=Sniper, 6=Grenade Launcher, 7=Rocket Launcher
SWEP.DeployableType		= 1								-- (Integer) 0=None, 1=Grenade Launcher, 2=Bipod
------------------------------------------------------
--	Primary Fire Settings							--	Settings for the primary fire of the weapon
------------------------------------------------------
SWEP.Primary.ClipSize = 32 -- (Integer) Size of a magazine
SWEP.Primary.DefaultClip = 32 -- (Integer) Default number of ammo you spawn with
SWEP.Primary.Ammo = "762x51mmnato" -- (String) Primary ammo used by the weapon, bullets probably
SWEP.Primary.Cone = 0.005 -- (Float) 	Accuracy of the weapon; 0.004 is the average for rifles
SWEP.Primary.RPM = 700 -- (Integer) Go to a wikipedia page and look at the RPM of the weapon, then put that here - the base will do the math
SWEP.Primary.PureDmg = VNTCB.Ammo.a762NATO[ 1 ] -- (Integer) Base damage, put one number here and the base will do the rest
------------------------------------------------------
--	Secondary Fire Settings							--	Settings for the alt-fire; if it has none, leave this be
------------------------------------------------------
SWEP.Secondary.ClipSize			= -1							-- (Integer) Size of a secondary magazine; if no alt-fire, set to -1
SWEP.Secondary.DefaultClip	= 3								-- (Integer) Default number of projectiles in the alt fire mag; if none, set to -1
SWEP.Secondary.Ammo		= "40x46mmgrenade"			-- (String) Ammo used by the grenade launcher, if it doesn't have one, leave at "none"
------------------------------------------------------
--	Gun Mechanics									--	Various things to tweak the effects and feedback
------------------------------------------------------
SWEP.FireMode = { true , true , false , true } -- (Table: Boolean, Boolean, Boolean, Boolean ) Enable different fire modes on the weapon; Has modes, Has Single, Has Burst, Has Auto - in that order. You can have more than one, but the first must be true
SWEP.CurrentMode = 1 -- (Integer) Current fire mode of the weapon; used to set the default mode; corresponds to the FireMode table
SWEP.Weight = 5 -- (Integer) The weight in Kilogrammes of our weapon - used in my weapon weight mod!
SWEP.StrongPenetration = VNTCB.Ammo.a762NATO[ 2 ] -- (Integer) Max penetration
SWEP.WeakPenetration = VNTCB.Ammo.a762NATO[ 3 ] -- (Integer) Max wood penetration
SWEP.EffectiveRange = 150 -- (Integer) Effective range of the weapon in metres.
SWEP.Settings.Jamming.MeanRateOfFailure = 1000 -- (Integer) Rate of stoppages in the weapon, look up the real world number estimations and just throw that in here.
------------------------------------------------------
--	Special FX										--	Muzzle flashes, shell casings, etc
------------------------------------------------------
SWEP.MuzzleAttach = 2 -- (Integer) The number of the attachment point for muzzle flashes, typically "1"
SWEP.ShellAttach = 1 -- (Integer) The number of the attachment point for shell ejections, typically "2"
SWEP.ShellType = 18 -- (Integer) The shell to use, see Lua/Effects/FX_ShellEject for integers
------------------------------------------------------
--	Custom Sounds									--		Setup sounds here!
------------------------------------------------------
SWEP.SND_PrimaryFire = Sound( "REACH.MA37.Single" ) -- (String) Primary shoot sound
SWEP.SND_Primary_DryFire = Sound( "REACH.MA37.Dry" ) -- (String) Primary dry fire sound
SWEP.SND_GrenadeLauncherFire = Sound("REACH.M319.Single") -- (String) Grenade launcher sound
SWEP.SND_Reload = Sound( "REACH.MA37.Deploy" ) -- (String) Reload sound
------------------------------------------------------
--	Ironsight & Run Positions						--	Set our model transforms for running and ironsights
------------------------------------------------------
SWEP.IronSightsPos = Vector( -3.29 , -5 , -0.3 ) -- (Vector) Ironsight XYZ Transform
SWEP.IronSightsAng = Vector( 0 , 0 , 0 ) -- (Vector) Ironsight XYZ Rotation
SWEP.RunArmOffset = Vector( 4.0928 , 0.4246 , 2.3712 ) -- (Vector) Sprinting XYZ Transform
SWEP.RunArmAngle = Vector( -18.4406 , 33.1846 , 0 ) -- (Vector) Sprinting XYZ Rotation

------------------------------------------------------
--	Setup Clientside Info							--	This block must be in every weapon!
------------------------------------------------------
if CLIENT then
	SWEP.DrawCrosshair = true
	SWEP.AccurateCrosshair = true
	SWEP.CSMuzzleFlashes = true
	SWEP.WepSelectIcon = surface.GetTextureID( "vgui/hud/" .. SWEP.WeaponName )
	SWEP.SwayScale = SWEP.SwayScale
	SWEP.BobScale = SWEP.BobScale
	SWEP.RenderGroup = RENDERGROUP_BOTH
	language.Add( SWEP.WeaponName , SWEP.PrintName )
	killicon.Add( SWEP.WeaponName , "vgui/entities/" .. SWEP.WeaponName , Color( 255 , 255 , 255 ) )
elseif SERVER then
	resource.AddWorkshop( SWEP.WorkshopID )
end
------------------------------------------------------
--	SWEP:Initialize() 							--	Called when the weapon is first loaded
------------------------------------------------------
function SWEP:Initialize( )
	self.HoldMeRight = VNTCB.HoldType.Rifle -- (String) Hold type table for our weapon, Lua/autorun/sh_v92_base_swep.Lua
end

function SWEP:CustomHolster( )
	local ViewModel = self.Owner:GetViewModel( )
	ViewModel:SetSubMaterial( 4, "" )
end

function SWEP:CustomThink( )

	local ViewModel = self.Owner:GetViewModel( )

	local compassAng = (self.Owner:GetAimVector():Angle().y)

	if CLIENT then
		if compassAng > 22 and compassAng < 68		then 
			ViewModel:SetSubMaterial( 4, "models/jessev92/halo/reach/weapons/common/compass_nw" )
		elseif compassAng > 67 and compassAng < 113 	then 
			ViewModel:SetSubMaterial( 4, "models/jessev92/halo/reach/weapons/common/compass_w" )
		elseif compassAng > 112 and compassAng < 157 	then 
			ViewModel:SetSubMaterial( 4, "models/jessev92/halo/reach/weapons/common/compass_sw" )
		elseif compassAng > 156 and compassAng < 202	then 
			ViewModel:SetSubMaterial( 4, "models/jessev92/halo/reach/weapons/common/compass_s" )
		elseif compassAng > 201 and compassAng < 246	then 
			ViewModel:SetSubMaterial( 4, "models/jessev92/halo/reach/weapons/common/compass_se" )
		elseif compassAng > 245 and compassAng < 291	then 
			ViewModel:SetSubMaterial( 4, "models/jessev92/halo/reach/weapons/common/compass_e" )
		elseif compassAng > 290 and compassAng < 336	then 
			ViewModel:SetSubMaterial( 4, "models/jessev92/halo/reach/weapons/common/compass_ne" )
		else
			ViewModel:SetSubMaterial( 4, "models/jessev92/halo/reach/weapons/common/compass_n" ) 
		end
	end	
	

	local ammoString = tostring( self:Clip1() )
	local ammoOnes = string.Right( ammoString, 1 )
	local ammoTens = string.Left( ammoString, 1 )
	
	if self:Clip1() < 10 then
		ammoTens = "0"
	end
	
	ViewModel:SetBodygroup( 7 , tonumber( ammoTens ) )
	ViewModel:SetBodygroup( 6 , tonumber( ammoOnes ) )

end