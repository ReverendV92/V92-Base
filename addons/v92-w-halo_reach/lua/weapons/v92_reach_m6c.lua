
------------------------------------------------------
--	Halo: Reach										--
--	M6C Magnum Pistol								--
------------------------------------------------------

AddCSLuaFile()

SWEP.PrintName = "M6C" -- (String) Printed name on menu

if not VNTCB then
	Error( "V92 Content Bases not mounted; Removing Weapon: " .. SWEP.PrintName .. "\n" )
	return false
end

SWEP.Base = VNTCB.Bases.Wep -- (String) Weapon base parent this is a child of
SWEP.Spawnable = true -- (Boolean) Can be spawned via the menu
SWEP.AdminOnly = false -- (Boolean) Admin only spawnable
------------------------------------------------------
--	Client Information								--	Info used in the client block of the weapon
------------------------------------------------------
SWEP.WeaponName = "v92_reach_m6c" -- (String) Name of the weapon script
SWEP.WeaponEntityName = SWEP.WeaponName .. "_ent" -- (String) Name of the weapon entity in Lua/Entities/Entityname.lua
SWEP.Manufacturer = VNTCB.Manufacturer.MIS -- (String) Gun company that makes this weapon
SWEP.CountryOfOrigin = VNTCB.Country.MAR -- (String) Country of origin
SWEP.MagazineName = VNTCB.Magazine.mHaloM6 -- (String) The name of the magazine the weapon uses - used in my Weapon Magazine System
SWEP.Category = VNTCB.Category.Reach -- (String) Category
SWEP.Instructions = "Uses .50AE ammo and M6 magnum magazines"		-- (String) Instruction
SWEP.Purpose = VNTCB.purpose -- (String) Purpose of the weapon
SWEP.Author = VNTCB.author -- (String) Author
SWEP.Contact = VNTCB.contact -- (String) Contact
SWEP.Slot = VNTCB.Bucket.Pistol -- (Integer) Bucket to place weapon in, 1 to 6
SWEP.SlotPos = VNTCB.Slot.Pistol -- (Integer) Bucket position
SWEP.ViewModelFOV = 70 -- (Integer) First-person field of view
SWEP.WorkshopID = "503422951" -- (Integer) Workshop ID number of the upload that contains this file.
------------------------------------------------------
--	Model Information								--
------------------------------------------------------
SWEP.ViewModelFlip = false -- (Boolean) Only used for vanilla CS:S models
SWEP.ViewModel = Model("models/jessev92/halo/reach/weapons/unsc/m6c_v.mdl") -- (String) View model - v_*
SWEP.WorldModel = Model("models/jessev92/halo/reach/weapons/unsc/m6c_w.mdl") -- (String) World model - w_*
SWEP.UseHands = false -- (Boolean) Leave at false unless the model uses C_Arms
SWEP.HoldType = "pistol" -- (String) Hold type for our weapon, refer to wiki for animation sets
------------------------------------------------------
--	Gun Types										--	Set the type of weapon - ONLY PICK ONE!
------------------------------------------------------
SWEP.WeaponType = VNTCB.WeaponType.Pistol -- (Integer) 1=Melee, 2=Pistol, 3=Rifle, 4=Shotgun, 5=Sniper, 6=Grenade Launcher, 7=Rocket Launcher
------------------------------------------------------
--	Primary Fire Settings							--	Settings for the primary fire of the weapon
------------------------------------------------------
SWEP.Primary.ClipSize = 8 -- (Integer) Size of a magazine
SWEP.Primary.DefaultClip = 8 -- (Integer) Default number of ammo you spawn with
SWEP.Primary.Ammo = "50ae" -- (String) Primary ammo used by the weapon, bullets probably
SWEP.Primary.RPM = 350 -- (Integer) Go to a wikipedia page and look at the RPM of the weapon, then put that here - the base will do the math
SWEP.Primary.PureDmg = VNTCB.Ammo.a50AE[1] -- (Integer) Base damage, put one number here and the base will do the rest
------------------------------------------------------
--	Gun Mechanics									--	Various things to tweak the effects and feedback
------------------------------------------------------
SWEP.FireMode = { false, true, false, false } -- (Table: Boolean, Boolean, Boolean, Boolean ) Enable different fire modes on the weapon; Has modes, Has Single, Has Burst, Has Auto - in that order. You can have more than one, but the first must be true
SWEP.CurrentMode = 1 -- (Integer) Current fire mode of the weapon; used to set the default mode; corresponds to the FireMode table
SWEP.Weight = 3 -- (Integer) The weight in Kilogrammes of our weapon - used in my weapon weight mod!
SWEP.StrongPenetration	= VNTCB.Ammo.a50AE[2] -- (Integer) Max penetration
SWEP.WeakPenetration	= VNTCB.Ammo.a50AE[3] -- (Integer) Max wood penetration
SWEP.EffectiveRange = 23 -- (Integer) Effective range of the weapon in metres.
SWEP.Settings.Jamming.MeanRateOfFailure = 1500 -- (Integer) Rate of stoppages in the weapon, look up the real world number estimations and just throw that in here.
------------------------------------------------------
--	Special FX										--	Muzzle flashes, shell casings, etc
------------------------------------------------------
SWEP.MuzzleAttach = 1 -- (Integer) The number of the attachment point for muzzle flashes, typically "1"
SWEP.ShellAttach = 2 -- (Integer) The number of the attachment point for shell ejections, typically "2"
SWEP.ShellType = 5 -- (Integer) The shell to use, see Lua/Effects/FX_ShellEject for integers
------------------------------------------------------
--	Custom Sounds									--		Setup sounds here!
------------------------------------------------------
SWEP.SND_PrimaryFire = Sound("REACH.MagnumG.Single") -- (String) Primary shoot sound
SWEP.SND_Primary_DryFire = Sound("REACH.Magnum.Empty") -- (String) Primary dry fire sound
SWEP.SND_Reload = Sound("REACH.Magnum.Draw") -- (String) Reload sound
SWEP.SND_ZoomIn = Sound( "REACH.Magnum.ZoomIn" ) -- (String) Sound used when zooming into a scope
SWEP.SND_ZoomOut = Sound( "REACH.Magnum.ZoomOut" ) -- (String) Sound used when zooming out of a scope
------------------------------------------------------
--	Ironsight & Run Positions						--	Set our model transforms for running and ironsights
------------------------------------------------------
SWEP.IronSightsPos = Vector(-2.42, -3, 1.45) 	-- (Vector) Ironsight XYZ Transform
SWEP.IronSightsAng = Vector(-0.2, 0.1, 0) 			-- (Vector) Ironsight XYZ Rotation
SWEP.RunArmOffset = Vector(0, -8, -7)
SWEP.RunArmAngle = Vector(70, 0, 0)
------------------------------------------------------
--	Setup Clientside Info							--	This block must be in every weapon!
------------------------------------------------------
if CLIENT then
	SWEP.DrawCrosshair		= false
	SWEP.AccurateCrosshair	= true
	SWEP.CSMuzzleFlashes	= true
	SWEP.WepSelectIcon 		= surface.GetTextureID("vgui/hud/" .. SWEP.WeaponName )
	SWEP.RenderGroup 		= RENDERGROUP_BOTH
	language.Add( SWEP.WeaponName, SWEP.PrintName )
	killicon.Add( SWEP.WeaponName, "vgui/entities/".. SWEP.WeaponName , Color( 255, 255, 255 ) )
elseif SERVER then
	resource.AddWorkshop( SWEP.WorkshopID )
end
------------------------------------------------------
--	SWEP:Initialize() 							--	Called when the weapon is first loaded
------------------------------------------------------
function SWEP:Initialize( )
	self.HoldMeRight = VNTCB.HoldType.Pistol -- (String) Hold type table for our weapon, Lua/autorun/sh_v92_base_swep.Lua
end
