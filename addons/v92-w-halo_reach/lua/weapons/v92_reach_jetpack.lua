
--[[

AddCSLuaFile()

if SERVER then 
	SWEP.Weight = 8
	SWEP.HoldType = "dual"
end

if CLIENT then

	SWEP.Category		= "Halo Reach"
	SWEP.PrintName		= "Jetpack"
	SWEP.Author			= "V92"
	SWEP.Contact		= "Steam @ V92"
	SWEP.Purpose		= "Fly Around"
	SWEP.Instructions	= "PRI: Fly Up, SEC: Fly Forward, RELOAD: Turn On/Off"	
	SWEP.Slot			= 4
	SWEP.SlotPos		= 52
	SWEP.SwayScale		= 3
	SWEP.BobScale		= 1
	
	SWEP.DrawAmmo		= false
	SWEP.DrawCrosshair	= true
	
end

SWEP.Spawnable				= true
SWEP.AdminOnly				= true

SWEP.ViewModel				= Model( "models/weapons/c_arms.mdl" )
SWEP.WorldModel				= Model("models/jessev92/halo/reach/weapons/jetpack_w.mdl")
SWEP.UseHands				= true

SWEP.Primary.ClipSize		= -1
SWEP.Primary.DefaultClip	= -1
SWEP.Primary.Automatic		= true
SWEP.Primary.Ammo			= "none"

SWEP.Secondary.ClipSize		= -1
SWEP.Secondary.DefaultClip	= -1
SWEP.Secondary.Automatic	= true
SWEP.Secondary.Ammo			= "none"

sound.Add({	name		= "VNT_HaloReachJetPack.Thrust",
	channel		= CHAN_WEAPON,
	level		= 75,
	volume		= 1.0,
	pitch		= { 95, 105 },
	sound		= {"jessev92/bf2/weapons/Rocket_engine_start_idle.wav"},
})

local JetPackOn = false
local ActionDelay = CurTime()
local ThrustSound = CreateSound( self, Sound("VNT_HaloReachJetPack.Thrust"), 2552 )

function SWEP:Initialize()
	self:SetHoldType( "duel" )
end

function SWEP:Reload()
	if ActionDelay <= CurTime() then
		if JetPackOn != true then 
			JetPackOn = true 
			self:SetBodygroup( 1, 1 )
			ActionDelay = CurTime() + 3
		else 
			JetPackOn = false
			self:SetBodygroup( 1, 0 )
			ActionDelay = CurTime() + 3
		end
	end
end
 
function SWEP:Think()
	if JetPackOn == true then
		self:SetBodygroup( 1, 1 )
		if self.Owner:KeyDown( IN_JUMP ) then
			self.Owner:SetVelocity( self.Owner:GetUp() * 15 )
			self:SetBodygroup( 1, 1 )
		end
		if self.Owner:KeyDown( IN_FORWARD ) then
			self.Owner:SetVelocity( self.Owner:GetForward() * 15 )
			self:SetBodygroup( 1, 2 )
		end
		if self.Owner:KeyDown( IN_BACK ) then
			self.Owner:SetVelocity( self.Owner:GetForward() * -15 )
			self:SetBodygroup( 1, 3 )
		end
		if self.Owner:KeyDown( IN_JUMP ) or self.Owner:KeyDown( IN_FORWARD ) or self.Owner:KeyDown( IN_BACK ) then
			ThrustSound:Play()
		else
			ThrustSound:Stop()
		end
	else
		self:SetBodygroup( 1, 0 )
		ThrustSound:Stop()
	end
end

function SWEP:PrimaryAttack()
	return false
end

function SWEP:SecondaryAttack()
	return false
end

--]]
