
------------------------------------------------------
--	Halo: Reach										--
--	M392 DMR										--
------------------------------------------------------

AddCSLuaFile()

SWEP.PrintName = "M392" -- (String) Printed name on menu

if not VNTCB then
	Error( "V92 Content Bases not mounted; Removing Weapon: " .. SWEP.PrintName .. "\n" )
	return false
end

SWEP.Base = VNTCB.Bases.Wep -- (String) Weapon base parent this is a child of
SWEP.Spawnable = true -- (Boolean) Can be spawned via the menu
SWEP.AdminOnly = false -- (Boolean) Admin only spawnable
------------------------------------------------------
--	Client Information								--	Info used in the client block of the weapon
------------------------------------------------------
SWEP.WeaponName = "v92_reach_m392" -- (String) Name of the weapon script
SWEP.WeaponEntityName = SWEP.WeaponName .. "_ent" -- (String) Name of the weapon entity in Lua/Entities/Entityname.lua
SWEP.Manufacturer = VNTCB.Manufacturer.MIS -- (String) Gun company that makes this weapon
SWEP.CountryOfOrigin = VNTCB.Country.VAR -- (String) Country of origin
SWEP.MagazineName = "M392 Magazine" -- (String) The name of the magazine the weapon uses - used in my Weapon Magazine System
SWEP.Category = VNTCB.Category.Reach -- (String) Category
SWEP.Instructions = "Uses 7.62 NATO ammo and M392 magazines"		-- (String) Instruction
SWEP.Purpose = VNTCB.purpose -- (String) Purpose of the weapon
SWEP.Author = VNTCB.author -- (String) Author
SWEP.Contact = VNTCB.contact -- (String) Contact
SWEP.Slot = VNTCB.Bucket.DMR -- (Integer) Bucket to place weapon in, 1 to 6
SWEP.SlotPos = VNTCB.Slot.DMR -- (Integer) Bucket position
SWEP.ViewModelFOV = 70 -- (Integer) First-person field of view
SWEP.WorkshopID = "503422951" -- (Integer) Workshop ID number of the upload that contains this file.
------------------------------------------------------
--	Model Information								--
------------------------------------------------------
SWEP.ViewModelFlip = false -- (Boolean) Only used for vanilla CS:S models
SWEP.ViewModel = Model("models/jessev92/halo/reach/weapons/unsc/m392_v.mdl") -- (String) View model - v_*
SWEP.WorldModel = Model("models/jessev92/halo/reach/weapons/unsc/m392_w.mdl") -- (String) World model - w_*
SWEP.UseHands = false -- (Boolean) Leave at false unless the model uses C_Arms
SWEP.HoldType = "ar2" -- (String) Hold type for our weapon, refer to wiki for animation sets
------------------------------------------------------
--	Gun Types										--	Set the type of weapon - ONLY PICK ONE!
------------------------------------------------------
SWEP.WeaponType = VNTCB.WeaponType.Sniper -- (Integer) 1=Melee, 2=Pistol, 3=Rifle, 4=Shotgun, 5=Sniper, 6=Grenade Launcher, 7=Rocket Launcher
------------------------------------------------------
--	Primary Fire Settings							--	Settings for the primary fire of the weapon
------------------------------------------------------
SWEP.Primary.ClipSize = 15 -- (Integer) Size of a magazine
SWEP.Primary.DefaultClip = 15 -- (Integer) Default number of ammo you spawn with
SWEP.Primary.Ammo = "762x51mmnato" -- (String) Primary ammo used by the weapon, bullets probably
SWEP.Primary.RPM = 350 -- (Integer) Go to a wikipedia page and look at the RPM of the weapon, then put that here - the base will do the math
SWEP.Primary.PureDmg = VNTCB.Ammo.a762NATO[1] -- (Integer) Base damage, put one number here and the base will do the rest
------------------------------------------------------
--	Gun Mechanics									--	Various things to tweak the effects and feedback
------------------------------------------------------
SWEP.FireMode = { true, true, false, true } -- (Table: Boolean, Boolean, Boolean, Boolean ) Enable different fire modes on the weapon; Has modes, Has Single, Has Burst, Has Auto - in that order. You can have more than one, but the first must be true
SWEP.CurrentMode = 1 -- (Integer) Current fire mode of the weapon; used to set the default mode; corresponds to the FireMode table
SWEP.Weight = 3 -- (Integer) The weight in Kilogrammes of our weapon - used in my weapon weight mod!
SWEP.StrongPenetration	= VNTCB.Ammo.a762NATO[2] -- (Integer) Max penetration
SWEP.WeakPenetration	= VNTCB.Ammo.a762NATO[3] -- (Integer) Max wood penetration
SWEP.EffectiveRange = 950 -- (Integer) Effective range of the weapon in metres.
SWEP.Settings.Jamming.MeanRateOfFailure = 2500 -- (Integer) Rate of stoppages in the weapon, look up the real world number estimations and just throw that in here.
------------------------------------------------------
--	Special FX										--	Muzzle flashes, shell casings, etc
------------------------------------------------------
SWEP.MuzzleAttach = 1 -- (Integer) The number of the attachment point for muzzle flashes, typically "1"
SWEP.ShellAttach = 2 -- (Integer) The number of the attachment point for shell ejections, typically "2"
SWEP.ShellType = 18 -- (Integer) The shell to use, see Lua/Effects/FX_ShellEject for integers
------------------------------------------------------
--	Sniper Settings									--	Settings for sniper rifles
------------------------------------------------------
SWEP.ScopeType = 2 -- (Integer) Type of scope, 0=none, 1=overlay, 2=Render Target
SWEP.ScopeMaterial = 0 -- (Integer) Type of overlay, 0=Red Dot, 1=EOTech, 2=ACOG, 3=SVD, 4=M14, 5=L42A1, 6=PSG1, 7=German tri-bar
SWEP.ScopeMaterialCustom = nil -- (String: Material) Path to the scope overlay you want to use if you use an overlay scope
SWEP.ScopeMultipliers = { 8 , 4 } -- (Table: Float, Float) X magnification of the scope, ACOG would 4, etc.
SWEP.ScopeOverlayScale = 0.5 -- (Float) Scale of the scope overlay
SWEP.DefaultZoom = 1 -- (Float) Current zoom, default to 1; DON'T CHANGE
SWEP.HasVariableZoom = true -- (Boolean) Does the weapon have variable zoom?
------------------------------------------------------
--	Custom Sounds									--		Setup sounds here!
------------------------------------------------------
SWEP.SND_PrimaryFire = Sound("REACH.DMR.Single") -- (String) Primary shoot sound
SWEP.SND_Primary_DryFire = Sound("REACH.MA37.Empty") -- (String) Primary dry fire sound
SWEP.SND_Reload = Sound("REACH.MA37.Draw") -- (String) Reload sound
SWEP.SND_ZoomIn = Sound( "REACH.BR.ZoomIn" ) -- (String) Sound used when zooming into a scope
SWEP.SND_ZoomOut = Sound( "REACH.BR.ZoomOut" ) -- (String) Sound used when zooming out of a scope
------------------------------------------------------
--	Ironsight & Run Positions						--	Set our model transforms for running and ironsights
------------------------------------------------------
SWEP.IronSightsPos = Vector(-2.42, -3, 1.45) 	-- (Vector) Ironsight XYZ Transform
SWEP.IronSightsAng = Vector(-0.2, 0.1, 0) 			-- (Vector) Ironsight XYZ Rotation
SWEP.RunArmOffset = Vector(0, -8, -7)
SWEP.RunArmAngle = Vector(70, 0, 0)
------------------------------------------------------
--	Setup Clientside Info							--	This block must be in every weapon!
------------------------------------------------------
if CLIENT then
	SWEP.WepSelectIcon 		= surface.GetTextureID("vgui/hud/" .. SWEP.WeaponName )
	SWEP.RenderGroup 		= RENDERGROUP_BOTH
	language.Add( SWEP.WeaponName, SWEP.PrintName )
	killicon.Add( SWEP.WeaponName, "vgui/entities/".. SWEP.WeaponName , Color( 255, 255, 255 ) )
elseif SERVER then
	resource.AddWorkshop( SWEP.WorkshopID )
end
------------------------------------------------------
--	SWEP:Initialize() 							--	Called when the weapon is first loaded
------------------------------------------------------
function SWEP:Initialize( )
	self.HoldMeRight = VNTCB.HoldType.Rifle -- (String) Hold type table for our weapon, Lua/autorun/sh_v92_base_swep.Lua
end

function SWEP:CustomThink( )

	local ViewModel = self.Owner:GetViewModel( )

	local ammoString = tostring( self:Clip1() )
	local ammoOnes = string.Right( ammoString, 1 )
	local ammoTens = string.Left( ammoString, 1 )
	
	if self:Clip1() < 10 then
		ammoTens = "0"
	end
	
	ViewModel:SetBodygroup( 6 , tonumber( ammoTens ) )
	ViewModel:SetBodygroup( 5 , tonumber( ammoOnes ) )

end