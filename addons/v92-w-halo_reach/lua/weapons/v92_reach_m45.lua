
------------------------------------------------------
--	Halo: Reach										--
--	M90C Shotgun									--
------------------------------------------------------

AddCSLuaFile()

SWEP.PrintName = "M45E" -- (String) Printed name on menu

if not VNTCB then
	Error( "V92 Content Bases not mounted; Removing Weapon: " .. SWEP.PrintName .. "\n" )
	return false
end

SWEP.Base = VNTCB.Bases.Wep -- (String) Weapon base parent this is a child of
SWEP.Spawnable = true -- (Boolean) Can be spawned via the menu
SWEP.AdminOnly = false -- (Boolean) Admin only spawnable
------------------------------------------------------
--	Client Information								--	Info used in the client block of the weapon
------------------------------------------------------
SWEP.WeaponName = "v92_reach_m45" -- (String) Name of the weapon script
SWEP.WeaponEntityName = SWEP.WeaponName .. "_ent" -- (String) Name of the weapon entity in Lua/Entities/Entityname.lua
SWEP.Manufacturer = VNTCB.Manufacturer.WST -- (String) Gun company that makes this weapon
SWEP.CountryOfOrigin = VNTCB.Country.VAR -- (String) Country of origin
SWEP.MagazineName = VNTCB.Magazine.mSK8GMagnum -- (String) The name of the magazine the weapon uses - used in my Weapon Magazine System
SWEP.Category = VNTCB.Category.Reach -- (String) Category
SWEP.Instructions = "8-Gauge Magnum Shells"		-- (String) Instruction
SWEP.Purpose = VNTCB.purpose -- (String) Purpose of the weapon
SWEP.Author = VNTCB.author -- (String) Author
SWEP.Contact = VNTCB.contact -- (String) Contact
SWEP.Slot = VNTCB.Bucket.Shotgun -- (Integer) Bucket to place weapon in, 1 to 6
SWEP.SlotPos = VNTCB.Slot.Shotgun -- (Integer) Bucket position
SWEP.ViewModelFOV = 70 -- (Integer) First-person field of view
SWEP.WorkshopID = "503422951" -- (Integer) Workshop ID number of the upload that contains this file.
------------------------------------------------------
--	Model Information								--
------------------------------------------------------
SWEP.ViewModelFlip = false -- (Boolean) Only used for vanilla CS:S models
SWEP.ViewModel = Model("models/jessev92/halo/reach/weapons/unsc/m90c_v.mdl") -- (String) View model - v_*
SWEP.WorldModel = Model("models/jessev92/halo/reach/weapons/unsc/m90c_w.mdl") -- (String) World model - w_*
SWEP.UseHands = false -- (Boolean) Leave at false unless the model uses C_Arms
SWEP.HoldType = "shotgun" -- (String) Hold type for our weapon, refer to wiki for animation sets
------------------------------------------------------
--	Gun Types										--	Set the type of weapon - ONLY PICK ONE!
------------------------------------------------------
SWEP.WeaponType = VNTCB.WeaponType.Shotgun -- (Integer) 1=Melee, 2=Pistol, 3=Rifle, 4=Shotgun, 5=Sniper, 6=Grenade Launcher, 7=Rocket Launcher
------------------------------------------------------
--	Primary Fire Settings							--	Settings for the primary fire of the weapon
------------------------------------------------------
SWEP.Primary.ClipSize = 6 -- (Integer) Size of a magazine
SWEP.Primary.DefaultClip = 6 -- (Integer) Default number of ammo you spawn with
SWEP.Primary.Ammo = "sk8gaugemagnum" -- (String) Primary ammo used by the weapon, bullets probably
SWEP.Primary.RPM = 60 -- (Integer) Go to a wikipedia page and look at the RPM of the weapon, then put that here - the base will do the math
SWEP.Primary.PureDmg = VNTCB.Ammo.aSK8GMagnum[1] -- (Integer) Base damage, put one number here and the base will do the rest
SWEP.Primary.NumShots = 15 -- (Integer) How many bullets per shot; think of this as a shotgun shell, higher integer, more pellets
------------------------------------------------------
--	Gun Mechanics									--	Various things to tweak the effects and feedback
------------------------------------------------------
SWEP.FireMode = { false, true, false, false } -- (Table: Boolean, Boolean, Boolean, Boolean ) Enable different fire modes on the weapon; Has modes, Has Single, Has Burst, Has Auto - in that order. You can have more than one, but the first must be true
SWEP.CurrentMode = 1 -- (Integer) Current fire mode of the weapon; used to set the default mode; corresponds to the FireMode table
SWEP.Weight = 3 -- (Integer) The weight in Kilogrammes of our weapon - used in my weapon weight mod!
SWEP.StrongPenetration	= VNTCB.Ammo.aSK8GMagnum[2] -- (Integer) Max penetration
SWEP.WeakPenetration	= VNTCB.Ammo.aSK8GMagnum[3] -- (Integer) Max wood penetration
SWEP.EffectiveRange = 40 -- (Integer) Effective range of the weapon in metres.
SWEP.BlackFriday = true -- (Boolean) Black Friday Doorbuster Sales
SWEP.Settings.Jamming.MeanRateOfFailure = 0 -- (Integer) Rate of stoppages in the weapon, look up the real world number estimations and just throw that in here.
------------------------------------------------------
--	Special FX										--	Muzzle flashes, shell casings, etc
------------------------------------------------------
SWEP.MuzzleAttach = 1 -- (Integer) The number of the attachment point for muzzle flashes, typically "1"
SWEP.ShellAttach = 2 -- (Integer) The number of the attachment point for shell ejections, typically "2"
SWEP.ShellType = 2 -- (Integer) The shell to use, see Lua/Effects/FX_ShellEject for integers
------------------------------------------------------
--	Shotgun Settings								--	Settings for shotguns
------------------------------------------------------

------------------------------------------------------
--	Custom Sounds									--		Setup sounds here!
------------------------------------------------------
SWEP.SND_PrimaryFire = Sound("REACH.M45.Single") -- (String) Primary shoot sound
SWEP.SND_Primary_DryFire = Sound("REACH.M45.Empty") -- (String) Primary dry fire sound
SWEP.SND_Reload = Sound("REACH.M45.Draw") -- (String) Reload sound
------------------------------------------------------
--	Ironsight & Run Positions						--	Set our model transforms for running and ironsights
------------------------------------------------------
SWEP.IronSightsPos = Vector(-3.201, -3, 0.899)
SWEP.IronSightsAng = Vector(0, 0, 0)
SWEP.RunArmOffset = Vector(0, -1, 0)
SWEP.RunArmAngle = Vector(-16, 47, -21)
------------------------------------------------------
--	Setup Clientside Info							--	This block must be in every weapon!
------------------------------------------------------
if CLIENT then
	SWEP.WepSelectIcon 		= surface.GetTextureID("vgui/hud/" .. SWEP.WeaponName )
	SWEP.RenderGroup 		= RENDERGROUP_BOTH
	language.Add( SWEP.WeaponName, SWEP.PrintName )
	killicon.Add( SWEP.WeaponName, "vgui/entities/".. SWEP.WeaponName , Color( 255, 255, 255 ) )
elseif SERVER then
	resource.AddWorkshop( SWEP.WorkshopID )
end
------------------------------------------------------
--	SWEP:Initialize() 							--	Called when the weapon is first loaded
------------------------------------------------------
function SWEP:Initialize( )
	self.HoldMeRight = VNTCB.HoldType.Shotgun -- (String) Hold type table for our weapon, Lua/autorun/sh_v92_base_swep.Lua
end

function SWEP:CustomThink( )

	local ViewModel = self.Owner:GetViewModel( )

	local ammoString = tostring( self:Clip1() )
	local ammoOnes = string.Right( ammoString, 1 )
	local ammoTens = string.Left( ammoString, 1 )
	
	if self:Clip1() < 10 then
		ammoTens = "0"
	end
	
	ViewModel:SetBodygroup( 7 , tonumber( ammoTens ) )
	ViewModel:SetBodygroup( 8 , tonumber( ammoOnes ) )

end