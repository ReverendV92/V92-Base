
------------------------------------------------------
--	Halo: Reach										--
--	BR55 Battle Rifle								--
------------------------------------------------------

AddCSLuaFile()

SWEP.PrintName = "BR55" -- (String) Printed name on menu

if not VNTCB then
	Error( "V92 Content Bases not mounted; Removing Weapon: " .. SWEP.PrintName .. "\n" )
	return false
end

SWEP.Base = VNTCB.Bases.Wep -- (String) Weapon base parent this is a child of
SWEP.Spawnable = true -- (Boolean) Can be spawned via the menu
SWEP.AdminOnly = false -- (Boolean) Admin only spawnable
------------------------------------------------------
--	Client Information								--	Info used in the client block of the weapon
------------------------------------------------------
SWEP.WeaponName = "v92_reach_br55" -- (String) Name of the weapon script
SWEP.WeaponEntityName = SWEP.WeaponName .. "_ent" -- (String) Name of the weapon entity in Lua/Entities/Entityname.lua
SWEP.Manufacturer = VNTCB.Manufacturer.MIS -- (String) Gun company that makes this weapon
SWEP.CountryOfOrigin = VNTCB.Country.VAR -- (String) Country of origin
SWEP.MagazineName = VNTCB.Magazine.mHaloBR55 -- (String) The name of the magazine the weapon uses - used in my Weapon Magazine System
SWEP.Category = VNTCB.Category.Reach -- (String) Category
SWEP.Instructions = "Uses 9.5x40mm ammo and BR55 magazines"		-- (String) Instruction
SWEP.Purpose = VNTCB.purpose -- (String) Purpose of the weapon
SWEP.Author = VNTCB.author -- (String) Author
SWEP.Contact = VNTCB.contact -- (String) Contact
SWEP.Slot = VNTCB.Bucket.DMR -- (Integer) Bucket to place weapon in, 1 to 6
SWEP.SlotPos = VNTCB.Slot.DMR -- (Integer) Bucket position
SWEP.ViewModelFOV = 70 -- (Integer) First-person field of view
SWEP.WorkshopID = "503422951" -- (Integer) Workshop ID number of the upload that contains this file.
------------------------------------------------------
--	Model Information								--
------------------------------------------------------
SWEP.ViewModelFlip = false -- (Boolean) Only used for vanilla CS:S models
SWEP.ViewModel = Model("models/jessev92/halo/reach/weapons/unsc/br55_v.mdl") -- (String) View model - v_*
SWEP.AttachmentModel = Model("models/jessev92/halo/reach/weapons/unsc/dmr_compass_v.mdl") -- (String) View model - v_*
SWEP.WorldModel = Model("models/jessev92/halo/reach/weapons/unsc/br55_w.mdl") -- (String) World model - w_*
SWEP.UseHands = false -- (Boolean) Leave at false unless the model uses C_Arms
SWEP.HoldType = "ar2" -- (String) Hold type for our weapon, refer to wiki for animation sets
------------------------------------------------------
--	Gun Types										--	Set the type of weapon - ONLY PICK ONE!
------------------------------------------------------
SWEP.WeaponType = VNTCB.WeaponType.Sniper -- (Integer) 1=Melee, 2=Pistol, 3=Rifle, 4=Shotgun, 5=Sniper, 6=Grenade Launcher, 7=Rocket Launcher
------------------------------------------------------
--	Primary Fire Settings							--	Settings for the primary fire of the weapon
------------------------------------------------------
SWEP.Primary.ClipSize = 36 -- (Integer) Size of a magazine
SWEP.Primary.DefaultClip = 36 -- (Integer) Default number of ammo you spawn with
SWEP.Primary.Ammo = "95x40mm" -- (String) Primary ammo used by the weapon, bullets probably
SWEP.Primary.RPM = 350 -- (Integer) Go to a wikipedia page and look at the RPM of the weapon, then put that here - the base will do the math
SWEP.Primary.PureDmg = VNTCB.Ammo.a95x40mm[1] -- (Integer) Base damage, put one number here and the base will do the rest
------------------------------------------------------
--	Gun Mechanics									--	Various things to tweak the effects and feedback
------------------------------------------------------
SWEP.FireMode = { true, true, true, false } -- (Table: Boolean, Boolean, Boolean, Boolean ) Enable different fire modes on the weapon; Has modes, Has Single, Has Burst, Has Auto - in that order. You can have more than one, but the first must be true
SWEP.CurrentMode = 1 -- (Integer) Current fire mode of the weapon; used to set the default mode; corresponds to the FireMode table
SWEP.Weight = 3 -- (Integer) The weight in Kilogrammes of our weapon - used in my weapon weight mod!
SWEP.StrongPenetration	= VNTCB.Ammo.a95x40mm[2] -- (Integer) Max penetration
SWEP.WeakPenetration	= VNTCB.Ammo.a95x40mm[3] -- (Integer) Max wood penetration
SWEP.EffectiveRange = 900 -- (Integer) Effective range of the weapon in metres.
SWEP.Settings.Jamming.MeanRateOfFailure = 1000 -- (Integer) Rate of stoppages in the weapon, look up the real world number estimations and just throw that in here.
------------------------------------------------------
--	Special FX										--	Muzzle flashes, shell casings, etc
------------------------------------------------------
SWEP.MuzzleAttach = 1 -- (Integer) The number of the attachment point for muzzle flashes, typically "1"
SWEP.ShellAttach = 2 -- (Integer) The number of the attachment point for shell ejections, typically "2"
SWEP.ShellType = 18 -- (Integer) The shell to use, see Lua/Effects/FX_ShellEject for integers
------------------------------------------------------
--	Sniper Settings									--	Settings for sniper rifles
------------------------------------------------------
SWEP.ScopeType = 2 -- (Integer) Type of scope, 0=none, 1=overlay, 2=Render Target
SWEP.HasRTScope = true -- (Boolean) Does the weapon have variable zoom?
------------------------------------------------------
--	Custom Sounds									--		Setup sounds here!
------------------------------------------------------
SWEP.SND_PrimaryFire = Sound("REACH.DMR.Single") -- (String) Primary shoot sound
SWEP.SND_Primary_DryFire = Sound("REACH.MA37.Empty") -- (String) Primary dry fire sound
SWEP.SND_Reload = Sound("REACH.MA37.Draw") -- (String) Reload sound
SWEP.SND_ZoomIn = Sound( "REACH.BR.ZoomIn" ) -- (String) Sound used when zooming into a scope
SWEP.SND_ZoomOut = Sound( "REACH.BR.ZoomOut" ) -- (String) Sound used when zooming out of a scope
------------------------------------------------------
--	Ironsight & Run Positions						--	Set our model transforms for running and ironsights
------------------------------------------------------
SWEP.IronSightsPos = Vector(-2.83, -5, -0.32)
SWEP.IronSightsAng = Vector(0, 0, 0)
SWEP.RunArmOffset = Vector(0, -8, -7)
SWEP.RunArmAngle = Vector(70, 0, 0)
------------------------------------------------------
--	Setup Clientside Info							--	This block must be in every weapon!
------------------------------------------------------
if CLIENT then

	local texFSB = render.GetSuperFPTex( ) -- Full screen render target
	local oldRT = nil
	local ScopeMaterial = Material( "models/jessev92/halo/reach/weapons/dmr/scope_rt" )
	local camdata = { }
	local scopeattach = nil
	local scopeaim = nil
	local ViewModel = nil

	hook.Add( "RenderScene" , "VNTHaloBR55" , function( )
		if LocalPlayer( ):GetActiveWeapon( ).HasRTScope and LocalPlayer( ):IsValid( ) and LocalPlayer( ):Alive( ) then
			ViewModel = LocalPlayer( ):GetViewModel( )
			scopeattach = ViewModel:GetAttachment( ViewModel:LookupAttachment( "scope_pov" ) )
			scopeaim = ViewModel:GetAttachment( ViewModel:LookupAttachment( "scope_align" ) )

			camdata = {
				aspectratio = 1 ,
				x = 0 ,
				y = 0 ,
				w = ScrW( ) ,
				h = ScrH( ) ,
				angles = ( scopeaim.Pos - scopeattach.Pos ):Angle( ) ,
				origin = scopeattach.Pos ,
				fov = 16,
				drawhud = false ,
				drawViewModel = false ,
				ortho = false ,
				dopostprocess = false ,
				bloomtone = false
			}

			oldRT = render.GetRenderTarget( )
			render.SetFogZ( MATERIAL_FOG_NONE )
			render.FogColor( 255 , 255 , 255 )
			render.SetRenderTarget( texFSB )
			render.Clear( 0 , 0 , 0 , 255 )
			render.SetViewPort( 0 , 0 , ScrW( ) , ScrH( ) )
			cam.Start2D( )
			ViewModel:SetNoDraw( true )
			render.RenderView( camdata )
			render.SetMaterial( Material( "models/jessev92/halo/reach/weapons/dmr/dmr_lens" ) )
			render.DrawScreenQuadEx( camdata.x , camdata.y , camdata.w , camdata.h )
			ViewModel:SetNoDraw( false )
			cam.End2D( )
			render.SetRenderTarget( oldRT )
			render.SetViewPort( 0 , 0 , ScrW( ) , ScrH( ) )
		end
	end )

	function SWEP:CustomDrawHUD( )
		ScopeMaterial:SetTexture( "$basetexture" , texFSB )
	end

	SWEP.WepSelectIcon 		= surface.GetTextureID("vgui/hud/" .. SWEP.WeaponName )
	SWEP.RenderGroup 		= RENDERGROUP_BOTH
	language.Add( SWEP.WeaponName, SWEP.PrintName )
	killicon.Add( SWEP.WeaponName, "vgui/entities/".. SWEP.WeaponName , Color( 255, 255, 255 ) )
elseif SERVER then
	resource.AddWorkshop( SWEP.WorkshopID )
end
------------------------------------------------------
--	SWEP:Initialize() 							--	Called when the weapon is first loaded
------------------------------------------------------
function SWEP:Initialize( )
	self.HoldMeRight = VNTCB.HoldType.Rifle -- (String) Hold type table for our weapon, Lua/autorun/sh_v92_base_swep.Lua
end

function SWEP:CustomThink( )

	local ViewModel = self.Owner:GetViewModel( )
	if CLIENT then
	
		local ViewModel1 = self.Owner:GetViewModel( 1 )
		if ( IsValid( ViewModel1 ) ) then
			ViewModel1:SetWeaponModel( self.AttachmentModel , self )
			local compassAng = (self.Owner:GetAimVector():Angle().y)
			if ( compassAng > 22 and compassAng < 68 ) and ViewModel1:GetBodygroup( 0 ) != 8 then 
				ViewModel1:SetBodygroup( 0, 8 )
				return
			elseif ( compassAng > 67 and compassAng < 113 ) and ViewModel1:GetBodygroup( 0 ) != 7 then 
				ViewModel1:SetBodygroup( 0, 7 )
				return
			elseif ( compassAng > 112 and compassAng < 157 ) and ViewModel1:GetBodygroup( 0 ) != 6 then 
				ViewModel1:SetBodygroup( 0, 6 )
				return
			elseif ( compassAng > 156 and compassAng < 202 ) and ViewModel1:GetBodygroup( 0 ) != 5 then 
				ViewModel1:SetBodygroup( 0, 5 )
				return
			elseif ( compassAng > 201 and compassAng < 246 ) and ViewModel1:GetBodygroup( 0 ) != 4 then 
				ViewModel1:SetBodygroup( 0, 4 )
				return
			elseif ( compassAng > 245 and compassAng < 291 ) and ViewModel1:GetBodygroup( 0 ) != 3 then 
				ViewModel1:SetBodygroup( 0, 3 )
				return
			elseif ( compassAng > 290 and compassAng < 336 ) and ViewModel1:GetBodygroup( 0 ) != 2 then 
				ViewModel1:SetBodygroup( 0, 2 )
				return
			elseif ( compassAng > 336 and compassAng < 22 ) and ViewModel1:GetBodygroup( 0 ) != 1 then 
				ViewModel1:SetBodygroup( 0, 1 )
				return
			else
				ViewModel1:SetBodygroup( 0, 0 )
				return
			end
		end
	end	
	
	local ammoString = tostring( self:Clip1() )
	local ammoOnes = string.Right( ammoString, 1 )
	local ammoTens = string.Left( ammoString, 1 )
	
	if self:Clip1() < 10 then
		ammoTens = "0"
	end
	
	ViewModel:SetBodygroup( 9 , tonumber( ammoTens ) )
	ViewModel:SetBodygroup( 8 , tonumber( ammoOnes ) )

end