
-- Damage Inc., USA, Fixed-Wing, Douglas SBD-3 "Dauntless"
-- Model/Textures @ Mad Catz
-- Code/Shaders/Model Edit @ Reverend V92
-- Update: 2017/04/02

ENT.PrintName = "SBD-3 Dauntless" -- Printed name

if not ( vnt or VNTCB ) then
	Error( "V92 Content Bases not mounted; Removing Entity: " .. ENT.PrintName .. "\n" )
	return false
end

AddCSLuaFile()

ENT.Type = "anim" -- Type of entity - DON'T CHANGE
ENT.Base = VNT_BASE_VEHICLE_PLANE -- Entity base

ENT.Spawnable = true -- Spawnable?
ENT.AdminOnly = false -- Admin only?

if CLIENT then

	language.Add( "v92_plane_dmginc_sbd3", "SBD-3 Dauntless" ) -- Add a language entry

	ENT.Category = VNT_CATEGORY_DAMAGEINC -- Category
	ENT.Author = VNTCB.author -- Author
	ENT.Purpose = VNTCB.purpose	-- Purpose
	ENT.Instructions = VNTCB.instructions -- Instructions for use

elseif SERVER then

	resource.AddWorkshop( "894893543" ) -- Damage Inc. Vehicles

end

--	Models
ENT.Model = Model( "models/jessev92/dmginc/vehicles/sbd3_dauntless/hull.mdl" )

--	Functions
ENT.HasGear = true

--	Landing Gear
ENT.FlipLandingGearBG = false -- (Boolean) false = tyres visible by default on the model
ENT.GearBodyGroup = 12 -- (Integer) Tyre Bodygroup Number
ENT.GearAnimations = { "gearup" , "geardown" } -- (Table: Gear Up Sequence, Gear Down Sequence )
ENT.WheelHideTable = { 2 , 3 } -- (Table: Start Wheel Hide, End Wheel Hide)

--	Hatches/Canopies
ENT.HasHatch = true
ENT.HatchesStartClosed = true
ENT.HatchOnePoserName = "canopy_pilot"
ENT.HatchTwoPoserName = "canopy_gunner"

ENT.HasDoors = false

--	Flaps
ENT.HasFlaps = true

--	Rotors
ENT.RotorModel = Model( "models/jessev92/dmginc/vehicles/sbd3_dauntless/prop.mdl" )
ENT.RotorPhysicalModel = Model( "models/props_junk/sawblade001a.mdl" )
ENT.RotorPosition = Vector(160,0,30)
ENT.TopRotorDir	= 1

--	Gibs
ENT.GibModelOne = Model( "models/jessev92/dmginc/vehicles/sbd3_dauntless/gib/hull_1.mdl" )
ENT.GibModelTwo = Model( "models/jessev92/dmginc/vehicles/sbd3_dauntless/gib/hull_2.mdl" )
ENT.GibModelThree = Model( "models/jessev92/dmginc/vehicles/sbd3_dauntless/gib/pitch_left.mdl" )
ENT.GibModelFour = Model( "models/jessev92/dmginc/vehicles/sbd3_dauntless/gib/pitch_right.mdl" )
ENT.GibModelFive = Model( "models/jessev92/dmginc/vehicles/sbd3_dauntless/gib/tail.mdl" )
ENT.GibModelSix = Model( "models/jessev92/dmginc/vehicles/sbd3_dauntless/gib/wing_left_1.mdl" )
ENT.GibModelSeven = Model( "models/jessev92/dmginc/vehicles/sbd3_dauntless/gib/wing_left_2.mdl" )
ENT.GibModelEight = Model( "models/jessev92/dmginc/vehicles/sbd3_dauntless/gib/wing_left_3.mdl" )
ENT.GibModelNine = Model( "models/jessev92/dmginc/vehicles/sbd3_dauntless/gib/wing_right_1.mdl" )
ENT.GibModelTen = Model( "models/jessev92/dmginc/vehicles/sbd3_dauntless/gib/wing_right_2.mdl" )
ENT.GibModelEleven = Model( "models/jessev92/dmginc/vehicles/sbd3_dauntless/gib/wing_right_3.mdl" )

--	Specifications
ENT.SmokePos = Vector(150,0,30)
ENT.FirePos = Vector(150,0,30)
ENT.EngineForce = 335
ENT.Weight = 3347
ENT.SeatSwitcherPos = Vector(0,0,0)
ENT.AngBrakeMul = 0.01

--	Third-Person
ENT.thirdPerson = {
	["distance"] = 550
}

ENT.Agility = {
	["Thrust"] = 10
}

ENT.Wheels={
	{
		["mdl"] = Model( "models/jessev92/dmginc/vehicles/sbd3_dauntless/tyre_rear.mdl" ) ,
		["pos"] = Vector( -250 , 0 , -2 ) ,
		["friction"] = 150 ,
		["mass"] = 1500 ,
	},
	{
		["mdl"] = Model( "models/jessev92/dmginc/vehicles/sbd3_dauntless/tyre_front_left.mdl" ) ,
		["pos"] = Vector( 58 , 75 , -43 ) ,
		["friction"] = 150 ,
		["mass"] = 500 ,
	},
	{
		["mdl"] = Model( "models/jessev92/dmginc/vehicles/sbd3_dauntless/tyre_front_right.mdl" ) ,
		["pos"] = Vector( 58 , -75 , -43 ) ,
		["friction"] = 150 ,
		["mass"] = 500 ,
	},
}

ENT.Seats = {
	{
		["pos"] = Vector( 20 , 0 , 42 ) ,
		["exit"] = Vector( 10 , 60, 85 ) ,
		["weapons"] = { "Browning M2", "Bomb" } ,
    } ,
	{
		["pos"] = Vector( -35 , 0 , 35 ) ,
		["ang"] = Angle( 0 , 180 , 0 ) ,
		["exit"] = Vector( 10 , 60, 85 ) ,
    } ,
}							

ENT.Weapons = {
	["Browning M2"] = {
		["class"] = VNT_BASE_VEHICLE_POD_GATLING ,
		["info"] = {
			["Pods"] = {
				Vector( 115 , 15 , 62 ) ,
				Vector( 115 , -15 , 62 ) ,
			},
			["FireRate"] = 500 ,
			["Sequential"] = true ,
			["Sounds"] = {
				["shoot"] = Sound( "jessev92/vehicles/air/p40_warhawk/gun.wav" ) ,
				["stop"] = Sound( "jessev92/vehicles/air/p40_warhawk/gun_stop.wav" ) ,
			} ,
		} ,
	} ,
	["Bomb"] = {
		["class"] = VNT_BASE_VEHICLE_POD_BOMB ,
		["info"] = {
			["Pods"] = {
				Vector( 25 , -104 , -15 ) ,
				Vector( 25 , 104 , -15 ) ,
			},
			["model"] = Model( "models/jessev92/dmginc/vehicles/sbd3_dauntless/bomb.mdl" ) ,
			["reload"] = 10 ,
			["mode"] = false ,
		} ,
	} ,
}

--
ENT.Sounds = {

	--	Engine/Rotors
	["Start"] = Sound( "jessev92/vehicles/air/p40_warhawk/start.wav" ) ,
	["Blades"] = Sound( "jessev92/bf1942/vehicles/common/prop.wav" ) ,
	["BladesFar"] = Sound( "jessev92/bf1942/vehicles/common/prop.wav" ) ,

	["Engine"] = Sound( "jessev92/vehicles/air/p40_warhawk/external.wav" ) ,
	["EngineFar"] = Sound( "jessev92/vehicles/air/p40_warhawk/external.wav" ) ,
	["EngineDistant"] = Sound( "jessev92/vehicles/air/p40_warhawk/external.wav" ) ,

	-- Alarms
	["MissileTracking"] = Sound( "jessev92/bf3/vehicles/warnings/warning_missile_tracking.wav" ) ,
	["MissileLocked"] = Sound( "jessev92/bf3/vehicles/warnings/warning_missile_locked.wav" ) ,
	["MissileAlert"] = Sound( "jessev92/bf3/vehicles/warnings/warning_missile_inbound.wav" ) ,
	["MinorAlarm"] = Sound( "jessev92/bf3/vehicles/warnings/warning_air.wav" ) ,
	["LowHealth"] = Sound( "jessev92/bf3/vehicles/warnings/warning_damaged.wav" ) ,
	["CrashAlarm"] = Sound( "jessev92/bf3/vehicles/warnings/warning_disabled.wav" ) ,
	
	-- Hatch
	["HatchOpen"] = Sound( "physics/plastic/plastic_box_scrape_smooth_loop1.wav" ) ,
	["HatchClose"] = Sound( "physics/cardboard/cardboard_box_scrape_smooth_loop1.wav" ) ,
	["HatchWind"] = Sound( "ambient/wind/windgust_strong.wav" ) ,

	["DiveBrakes"] = Sound( "jessev92/bf1942/vehicles/common/airplanedive.wav" ) ,

	["GearDownStart"] = Sound( "jessev92/bf3/vehicles/reloads/largeweaponreload_02_start.wav" ) ,
	["GearDownLoop"] = Sound( "jessev92/bf3/vehicles/reloads/largeweaponreload_02_loop.wav" ) ,
	["GearDownStop"] = Sound( "jessev92/bf3/vehicles/reloads/largeweaponreload_02_stop.wav" ) ,
	["GearUpStart"] = Sound( "jessev92/bf3/vehicles/reloads/largeweaponreload_02_start.wav" ) ,
	["GearUpLoop"] = Sound( "jessev92/bf3/vehicles/reloads/largeweaponreload_02_loop.wav" ) ,
	["GearUpStop"] = Sound( "jessev92/bf3/vehicles/reloads/largeweaponreload_02_stop.wav" ) ,

	-- Interiors
	["Interior"] = Sound( "jessev92/bf3/vehicles/ah6_littlebird/cockpit.wav" ) ,
	
	-- Damage System
	["Fire"] = Sound( "jessev92/bf3/gamesounds/fires/fire_burn_1.wav" ) ,

	["Explosion_Nose"] = Sound( "jessev92/bf3/explosions/modularmodel/explosion_nose.wav" ) ,
	["Explosion_BassClose"] = Sound( "jessev92/bf3/explosions/modularmodel/explosion_bass_close_1.wav" ) ,
	["Explosion_BassDistant"] = Sound( "jessev92/bf3/explosions/modularmodel/explosion_bass_distant_1.wav" ) ,
	["Explosion_BassFar"] = Sound( "common/null.wav" ) ,
	["Explosion_Close"] = Sound( "jessev92/bf3/explosions/modularmodel/explosion_close_1.wav" ) ,
	["Explosion_Distant"] = Sound( "jessev92/bf3/explosions/modularmodel/explosion_distant_1.wav" ) ,
	["Explosion_Far"] = Sound( "jessev92/bf3/explosions/modularmodel/explosion_far_1.wav" ) ,
}
--]]

--function ENT:DrawPilotHud() end
--function ENT:DrawWeaponSelection() end

ENT.AutomaticFrameAdvance = true // needed for gear anims

function ENT:SpawnFunction(ply, tr)
	if (!tr.Hit) then return end
	local ent=ents.Create(ClassName)
	ent:SetPos(tr.HitPos+tr.HitNormal*70)
	ent:Spawn()
	ent:Activate()
	--ent:SetSkin(math.random(0,3))
	ent.Owner=ply
	return ent
end

ENT.Aerodynamics = {

	["Movement"] = {
	
		-- ["Pitch"] = Vector( forwards , right , up ) when moving FORWARDS
		-- Default: Vector( 0 , 0 ,-4 )
		["Pitch"] = Vector( 0 , 0 , 2 ) ,
		
		-- ["Roll"] = Vector( (backwards) (right) (down) ) when moving RIGHT
		-- Default: Vector( 0 , 0 , 0 )
		["Roll"] = Vector( 0 , 0 , 0 ) , 
		
		-- ["Yaw"] = Vector( (forwards) , (right) , (up) ) when moving UP
		-- Default: Vector( 1 , 0 , -2 )
		["Yaw"] = Vector( 1 , 0 , -2 ) , 
		
	} ,
	
	["Rotation"] = {
	
		["Pitch"] = Vector( 0 , -1.5 , 0 ) ,
		["Roll"] = Vector( 0 , 0 , 15 ) , -- Rotate towards flying direction
		["Yaw"] = Vector( 0 , -20 , 0 ) ,
		
	} ,
	
	["Rail"] = Vector( 1 , 5 , 20 ) ,
	["RailRotor"] = 3 , -- like Z rail but only active when moving and the rotor is turning
	
	["Drag"] = {
	
		["Directional"] = Vector( 0.01 , 0.01 , 0.01 ) ,
		["Angular"] = Vector( 0.01 , 0.01 , 0.01 ) ,

	} ,
	
	["AngleDrag"] = Vector( 0.01 , 0.01 , 0.01 ) ,

}

function ENT:PhysicsUpdate(ph)
	self:base(VNT_BASE_VEHICLE_PLANE).PhysicsUpdate(self,ph)

	--	Rotor Blur Code, Copied from WAC P-51D
	self:SetPropBlur( )

end

function ENT:addRotors()
	self:base(VNT_BASE_VEHICLE_PLANE).addRotors(self)
	
	self.RotorModel.TouchFunc=nil
end
