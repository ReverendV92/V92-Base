AddCSLuaFile( )

if not VNTCB then
	Error( "V92 Content Bases not mounted: Removing Entity" )

	return false
end

ENT.Base = VNT_BASE_WEAPON_ENTITY
ENT.Type = "anim"
ENT.PrintName = "BAR Mk.II Safari" -- (String) Printed name on menu
ENT.Author = VNTCB.author
ENT.Information = "Uses 7.62x51mm NATO Ammo and Elephant Rifle Magazines"
ENT.Category = VNT_CATEGORY_VNT
ENT.Spawnable = true
ENT.AdminOnly = false
ENT.SWepName = "v92_hit_safari" -- (String) Name of the weapon entity in Lua/weapons/swep_name.lua
ENT.WeaponName = ENT.SWepName .. "_ent" -- (String) Name of this entity
ENT.SEntModel = Model( "models/jessev92/hitman/weapons/safari_w.mdl" ) -- (String) Model to use