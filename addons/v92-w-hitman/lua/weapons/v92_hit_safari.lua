
------------------------------------------------------
--	Hitman: Blood Money								--
--	Elephant Rifle									--
--	7.62x57mm Springfield Semi-Auto rifle			--
------------------------------------------------------
--	It's 7.62x57mm in H:BM but I'm not making a new	--
--	ammo when .30-06 is basically the same thing	--
--	and the rifle is based on the IRL BAR Mk.II		--
------------------------------------------------------

AddCSLuaFile( )

SWEP.PrintName = "BAR Mk.II Safari" -- (String) Printed name on menu

if not VNTCB then
	Error( "V92 Content Bases not mounted; Removing Weapon: " .. SWEP.PrintName .. "\n" )
	return false
end

SWEP.Base = VNT_BASE_WEAPON -- (String) Weapon base parent this is a child of
SWEP.Spawnable = true -- (Boolean) Can be spawned via the menu
SWEP.AdminOnly = false -- (Boolean) Admin only spawnable
------------------------------------------------------
--	Client Information								--	Info used in the client block of the weapon
------------------------------------------------------
SWEP.WeaponName = "v92_hit_safari" -- (String) Name of the weapon script
SWEP.WeaponEntityName = SWEP.WeaponName .. "_ent" -- (String) Name of the weapon entity in Lua/Entities/Entityname.lua
SWEP.Manufacturer = VNT_WEAPON_MANUFACTURER_BROWNING -- (String) Gun company that makes this weapon
SWEP.CountryOfOrigin = VNT_WEAPON_COUNTRY_UNITEDSTATES -- (String) Country of origin
SWEP.MagazineName = VNTCB.Magazine.mBARMk2 -- (String) The name of the magazine the weapon uses - used in my Weapon Magazine System
SWEP.Category = VNT_CATEGORY_VNT -- (String) Category
SWEP.Instructions = VNTCB.instructions -- (String) Instruction
SWEP.Purpose = VNTCB.purpose -- (String) Purpose
SWEP.Author = VNTCB.author -- (String) Author
SWEP.Contact = VNTCB.contact -- (String) Contact
SWEP.Slot = VNT_WEAPON_BUCKETPOS_SNIPER -- (Integer) Bucket to place weapon in, 1 to 6
SWEP.SlotPos = VNT_WEAPON_SLOTPOS_SNIPER -- (Integer) Bucket position
SWEP.ViewModelFOV = 85 -- (Integer) First-person field of view
SWEP.DrawWeaponInfoBox = true -- (Boolean) Draw a verbose info box in the HUD buckets
SWEP.BounceWeaponIcon = false -- (Boolean) This causes that annoying icon bounce effect used on the Tool Gun
SWEP.DrawAmmo = false -- (Boolean) Draw our ammo, you can change this is it doesn't use ammo or you're hardcore
SWEP.DrawCrosshair = true -- (Boolean) You can change this is you're a git who can't aim
SWEP.AutoSwitchTo = false -- (Boolean) Auto-switch to this weapon when picked up? Leave to false - PLEASE
SWEP.AutoSwitchFrom = false -- (Boolean) Auto-switch away from this weapon when you pickup a new gun? Leave at false - PLEASE
SWEP.WorkshopID = "762491157" -- (Integer) Workshop ID number of the upload that contains this file.
------------------------------------------------------
--	Model Information								--
------------------------------------------------------
SWEP.ViewModelFlip = false -- (Boolean) Only used for vanilla CS:S models
SWEP.ViewModel = Model( "models/jessev92/hitman/weapons/safari_c.mdl" ) -- (String) View model - v_*
SWEP.WorldModel = Model( "models/jessev92/hitman/weapons/safari_w.mdl" ) -- (String) World model - w_*
SWEP.UseHands = true -- (Boolean) Leave at false unless the model uses C_Arms
SWEP.HoldType = "ar2" -- (String) Hold type for our weapon, refer to wiki for animation sets
------------------------------------------------------
--	Gun Types										--	Set the type of weapon - ONLY PICK ONE!
------------------------------------------------------

------------------------------------------------------
--	Sniper Settings									--	Settings for sniper rifles
------------------------------------------------------
SWEP.ScopeType = 1 -- (Integer) Type of scope, 0=none, 1=overlay, 2=Render Target
SWEP.ScopeMaterial = 3 -- (Integer) Type of overlay, 0=Red Dot, 1=EOTech, 2=ACOG, 3=SVD, 4=M14, 5=L42A1
SWEP.ScopeMultipliers = { 1 , 0.4 , 0.8 } -- (Table: Float, Float, Float) Zoom Muliplier, Default Zoom x, Close Zoom x
SWEP.BoltAction = false -- (Boolean) Is this bolt action? Removes the player from the scope after firing
SWEP.ReturnToScope = false -- (Boolean) Return to scope after cycling the bolt?
SWEP.HasVariableZoom = true -- (Boolean) Does the weapon have variable zoom?
SWEP.HasRTScope = true -- (Boolean) Does this have an RT scope?
SWEP.ZoomAmount = nil -- (Float) Don't Touch
SWEP.RTScopeCrosshair = Material( "jessev92/ui/scope/crosshair_german" ) -- The crosshair texture to use - check jessev92/ui/scope/ for textures
SWEP.RTScopeDirt = Material( "jessev92/ui/scope/scope_dirt" ) -- The dirt texture to use as an overlay
SWEP.RTScopeShadowMask = Material( "jessev92/ui/scope/scope_shadowmask" ) -- The shadow texture to use around the edges of the view
------------------------------------------------------
--	Primary Fire Settings							--	Settings for the primary fire of the weapon
------------------------------------------------------
SWEP.Primary.ClipSize = 10 -- (Integer) Size of a magazine
SWEP.Primary.DefaultClip = 10 -- (Integer) Default number of ammo you spawn with
SWEP.Primary.Ammo = "762x63mm" -- (String) Primary ammo used by the weapon, bullets probably
SWEP.Primary.RPM = 500 -- (Integer) Go to a wikipedia page and look at the RPM of the weapon, then put that here - the base will do the math
SWEP.Primary.PureDmg = VNTCB.Ammo.a3006[ 1 ] -- (Integer) Base damage, put one number here and the base will do the rest
------------------------------------------------------
--	Gun Mechanics									--	Various things to tweak the effects and feedback
------------------------------------------------------
SWEP.FireMode = { false , true , false , false } -- (Table: Boolean, Boolean, Boolean, Boolean ) Enable different fire modes on the weapon; Has modes, Has Single, Has Burst, Has Auto - in that order. You can have more than one, but the first must be true
SWEP.CurrentMode = 1 -- (Integer) Current fire mode of the weapon; used to set the default mode; corresponds to the FireMode table
SWEP.Weight = 4 -- (Integer) The weight in Kilogrammes of our weapon - used in my weapon weight mod!
SWEP.StrongPenetration = VNTCB.Ammo.a3006[ 2 ] -- (Integer) Max penetrationx
SWEP.WeakPenetration = VNTCB.Ammo.a3006[ 3 ] -- (Integer) Max wood penetration
SWEP.EffectiveRange = 700 -- (Integer) Effective range of the weapon in metres.
SWEP.StoppageRate = 1000 -- (Integer) Rate of stoppages in the weapon, look up the real world number estimations and just throw that in here.
------------------------------------------------------
--	Special FX										--	Muzzle flashes, shell casings, etc
------------------------------------------------------
SWEP.MuzzleFlashType = 1 -- (Integer) The number of the muzzle flash to use; see Lua/Effects/fx_muzzleflash.Lua
SWEP.ShellType = 18 -- (Integer) The shell to use, see Lua/Effects/FX_ShellEject for integers
SWEP.UseTracer = true -- (Boolean) Do we use tracers?
SWEP.TracerRandomizer = 1 -- (Integer) Between 0 and this number, chance to make a TracerType if UseTracer is true
SWEP.CustomTracer = "fx_tracer_vapor" -- (String) Custom TracerType effect; lua/effects/*.lua
------------------------------------------------------
--	Custom Sounds									--	Setup sounds here!
------------------------------------------------------
SWEP.Sounds = {

	["Primary"] = Sound( "HITMANBM.Safari.Single" ),
	["PrimaryDry"] = Sound( "VNTCB.SWep.Empty5" ),
	["Reload"] = Sound( "HITMANBM.Safari.Draw" ),
}

------------------------------------------------------
--	Ironsight & Run Positions						--	Set our model transforms for running and ironsights
------------------------------------------------------
SWEP.IronSightsPos = Vector(-0.018, 0, 0.009)
SWEP.IronSightsAng = Vector(0, 0, 0)
SWEP.RunArmOffset = Vector( 1.809 , -2.211 , 0 ) -- (Vector) Sprinting XYZ Transform
SWEP.RunArmAngle = Vector( -12 , 53 , -1 ) -- (Vector) Sprinting XYZ Rotation
------------------------------------------------------
--	Setup Clientside Info							--	This block must be in every weapon!
------------------------------------------------------

if CLIENT then

	local texFSB = render.GetSuperFPTex( ) -- Full screen render target
	local oldRT = nil
	local scopeattach = nil
	local scopeaim = nil
	local ViewModel = nil
	local RTXHair = SWEP.RTScopeCrosshair
	local RTDirt = SWEP.RTScopeDirt
	local RTShadow = SWEP.RTScopeShadowMask
	local camdata = { }

	hook.Add( "RenderScene" , "VNT_RTScope_HitmanBM_Safari" , function( )
		if LocalPlayer( ):GetActiveWeapon( ).ScopeType == 2 and LocalPlayer( ):IsValid( ) and LocalPlayer( ):Alive( ) then
			ViewModel = LocalPlayer( ):GetViewModel( )
			scopeattach = ViewModel:GetAttachment( ViewModel:LookupAttachment( "scope_pov" ) )
			scopeaim = ViewModel:GetAttachment( ViewModel:LookupAttachment( "scope_align" ) )
	local AimVector = LocalPlayer():GetAimVector()
	local AimOffset = Vector( AimVector.x , AimVector.y , AimVector.z )

			camdata = {
				aspectratio = 1 ,
				x = 0 ,
				y = 0 ,
				w = ScrW( ) ,
				h = ScrH( ) ,
				angles = (AimVector - AimOffset):Angle( ) ,
				origin = AimVector ,
				fov = 3,
				drawhud = false ,
				drawViewModel = false ,
				ortho = false ,
				dopostprocess = false ,
				bloomtone = false
			}

			oldRT = render.GetRenderTarget( )
			render.SetFogZ( MATERIAL_FOG_NONE )
			render.FogColor( 255 , 255 , 255 )
			render.SetRenderTarget( texFSB )
			render.Clear( 0 , 0 , 0 , 255 )
			render.SetViewPort( 0 , 0 , ScrW( ) , ScrH( ) )
			cam.Start2D( )
				ViewModel:SetNoDraw( true )
				--  If we have a crosshair
				if RTXHair != nil then
			   
					render.RenderView( camdata )
					render.SetMaterial( RTXHair )
					render.DrawScreenQuadEx( camdata.x , camdata.y , camdata.w , camdata.h )

				end
			   
				--[[
				--  If we have a dirt overlay
				if RTDirt != nil then
			   
					render.RenderView( camdata )
					render.SetMaterial( RTDirt )
					render.DrawScreenQuadEx( camdata.x , camdata.y , camdata.w , camdata.h )
				   
				end
			   
				--  If we have a shadow mask
				if RTShadow != nil then
			   
					render.RenderView( camdata )
					render.SetMaterial( RTShadow )
					render.DrawScreenQuadEx( camdata.x , camdata.y , camdata.w , camdata.h )
				   
				end
				--]]
				ViewModel:SetNoDraw( false )

			cam.End2D( )
			render.SetRenderTarget( oldRT )
			render.SetViewPort( 0 , 0 , ScrW( ) , ScrH( ) )
		end
	end )

	function SWEP:CustomDrawHUD( )
		local ScopeMaterial = Material( "models/jessev92/hitman/weapons/safari/crosshair_l42a1" )
		ScopeMaterial:SetTexture( "$basetexture" , texFSB )
	end
	
	SWEP.WepSelectIcon = surface.GetTextureID( "vgui/hud/" .. SWEP.WeaponName )
	SWEP.RenderGroup = RENDERGROUP_BOTH
	language.Add( SWEP.WeaponName , SWEP.PrintName )
	killicon.Add( SWEP.WeaponName , "vgui/entities/" .. SWEP.WeaponName , Color( 255 , 255 , 255 ) )
elseif SERVER then
	resource.AddWorkshop( SWEP.WorkshopID )
end

------------------------------------------------------
--	SWEP:Initialize() 								--	Called when the weapon is first loaded
------------------------------------------------------
function SWEP:Initialize( )
	self.HoldMeRight = VNTCB.HoldType.Sniper -- (String) Hold type table for our weapon, Lua/autorun/sh_v92_base_swep.Lua
end

SWEP.SeqIdle = { "safari_idle" }
SWEP.SeqDraw = { "safari_draw" }
SWEP.SeqReload = { "safari_reload" }
SWEP.SeqReloadEmpty = { "safari_reload_empty" }
SWEP.SeqHolster = { "safari_holster" }
SWEP.SeqPrimary = { "safari_shoot1" , "safari_shoot2" , "safari_shoot3" }
SWEP.SeqIronIn = { "safari_ironin" }
SWEP.SeqIronOut = { "safari_ironout" }
SWEP.SeqIronIdle = { "safari_ironidle" }
SWEP.SeqIronPrimary = { "safari_ironshoot" }
