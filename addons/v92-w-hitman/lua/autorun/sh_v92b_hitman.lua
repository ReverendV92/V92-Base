
---------------------------------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------------------------
--	Hitman: Blood Money Weaponry
---------------------------------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------------------------

--	Catch-22 Diabolis Ex Machina Prevention

if SERVER then
	resource.AddWorkshop("505106454") 	--	V92 Code Bases
end

-- SOUND GUIDE
----------
-- Channel = Typically CHAN_WEAPON or CHAN_ITEM for gunshots and gun parts respectively; CHAN_BODY for foley
-- Volume = Leave at 1.0
-- Pitch = Typically between 95 and 105 for gunshots
-- Level = For gunshots, follow this estimation table as a LOOSE guideline:
----------
--	.22LR = 40dB
--	9x19mm = 60dB
--	.38 Special = 70dB
--	5.56x45mm = 70dB
--	7.62x51mm = 90dB
--	.45 ACP = 90dB
--	.50 AE = 95dB
--	.50 BMG = 100dB
--	12-Gauge Buckshot = 120dB
--	Explosion = 200dB
--	Suppressed = 2/3 of normal
--	Empty = 60dB 
--	Weapon sounds = ~50dB (use your head)
--	Foley = ~30dB (use your head)
----------
----------


if not VNTCB then
	Error( "V92 Content Bases not mounted :: Removing Autorun File :: Hitman\n" )

	return false
end

if VNTCB.Plugins then
	table.insert( VNTCB.Plugins, 0, "VNT :: Hitman Pack" )
end

------------------------------------------------------------------------------------------
--	Hitman: Blood Money Weapons
------------------------------------------------------------------------------------------

---------------------------------------------
--	Browning Mk.II Safari Rifle
---------------------------------------------

sound.Add( {
	name = "HITMANBM.Safari.Single" ,
	channel = CHAN_WEAPON ,
	level = 90 ,
	volume = 1.0 ,
	pitch = { 95 , 105 } ,
	sound = { "^)jessev92/hitman/weapons/safari/fire.wav" }
} )
util.PrecacheSound( "jessev92/hitman/weapons/safari/fire.wav" )

sound.Add( {
	name = "HITMANBM.Safari.MagOut" ,
	channel = CHAN_ITEM ,
	level = 50 ,
	volume = 1.0 ,
	--pitch		= { 95, 105 },
	sound = { "^)jessev92/hitman/weapons/safari/clipout.wav" }
} )
util.PrecacheSound( "jessev92/hitman/weapons/safari/clipout.wav" )

sound.Add( {
	name = "HITMANBM.Safari.MagIn1" ,
	channel = CHAN_ITEM ,
	level = 50 ,
	volume = 1.0 ,
	--pitch		= { 95, 105 },
	sound = { "^)jessev92/hitman/weapons/safari/clipin1.wav" }
} )
util.PrecacheSound( "jessev92/hitman/weapons/safari/clipin1.wav" )

sound.Add( {
	name = "HITMANBM.Safari.MagIn2" ,
	channel = CHAN_ITEM ,
	level = 50 ,
	volume = 1.0 ,
	--pitch		= { 95, 105 },
	sound = { "^)jessev92/hitman/weapons/safari/clipin2.wav" }
} )
util.PrecacheSound( "jessev92/hitman/weapons/safari/clipin2.wav" )

sound.Add( {
	name = "HITMANBM.Safari.BoltBack" ,
	channel = CHAN_ITEM ,
	level = 55 ,
	volume = 1.0 ,
	--pitch		= { 95, 105 },
	sound = { "^)jessev92/hitman/weapons/safari/boltback.wav" }
} )
util.PrecacheSound( "jessev92/hitman/weapons/safari/boltback.wav" )

sound.Add( {
	name = "HITMANBM.Safari.BoltForward" ,
	channel = CHAN_ITEM ,
	level = 55 ,
	volume = 1.0 ,
	--pitch		= { 95, 105 },
	sound = { "^)jessev92/hitman/weapons/safari/boltforward.wav" }
} )
util.PrecacheSound( "jessev92/hitman/weapons/safari/boltforward.wav" )

sound.Add( {
	name = "HITMANBM.Safari.Draw" ,
	channel = CHAN_BODY ,
	level = 45 ,
	volume = 1.0 ,
	pitch = { 95 , 105 } ,
	sound = { "^)jessev92/weapons/univ/draw2.wav" }
} )
util.PrecacheSound( "jessev92/weapons/univ/draw2.wav" )

sound.Add( {
	name = "HITMANBM.Safari.Switch" ,
	channel = CHAN_ITEM ,
	volume = 1.0 ,
	level = 35 ,
	pitch = { 95 , 100 } ,
	sound = "^)jessev92/dods/weapons/bar/switch.wav"
} )
util.PrecacheSound( "jessev92/dods/weapons/bar/switch.wav" )

sound.Add( {
	name = "HITMANBM.Safari.ReloadRustle" ,
	channel = CHAN_BODY ,
	level = 35 ,
	volume = 1.0 ,
	pitch = { 95 , 105 } ,
	sound = { "^)jessev92/hitman/weapons/safari/foley.wav" }
} )
util.PrecacheSound( "jessev92/hitman/weapons/safari/foley.wav" )
