
if not ( vnt or VNTCB ) then
	Error( "V92 Content Bases not mounted; Removing Entity: " .. ENT.PrintName .. "\n" )
	return false

end

AddCSLuaFile()

ENT.Base = VNT_BASE_VEHICLE_POD
ENT.Type = "anim"

ENT.PrintName = "Gatling Pod Base"
ENT.Category = VNT_CATEGORY_VNT
ENT.Author = VNTCB.Info.author
ENT.Contact = VNTCB.Info.contact
ENT.Purpose = VNTCB.Info.purpose
ENT.Instructions = VNTCB.Info.instructions

ENT.Spawnable = false
ENT.AdminOnly = true

ENT.Name = ENT.PrintName
ENT.Ammo = 800
ENT.FireRate = 4000
ENT.Sequential = false
ENT.MuzzleFlashType = 1
ENT.Damage = 20
ENT.Force = 10
ENT.Tracer = 1

ENT.Sounds = {
	["shoot"] = Sound( "jessev92/vehicles/air/p40_warhawk/gun.wav" ) ,
	["stop"] = Sound( "jessev92/vehicles/air/p40_warhawk/gun_stop.wav" ) ,
}

function ENT:fireBullet(pos)

	if IsValid( self ) then

		if SERVER then

			if not self:takeAmmo( 1 ) then 

				return false
				
			else

				local bullet = { }
				bullet.Num = 1
				bullet.Src = self.aircraft:LocalToWorld( pos )
				bullet.Dir = self:GetForward( )
				bullet.Spread = Vector( 0.015 , 0.015 , 0 )
				bullet.Tracer = self.Tracer
				bullet.Force = self.Force
				bullet.Damage = self.Damage
				bullet.Attacker = self:getAttacker( )

				local effectdata = EffectData( )
				effectdata:SetOrigin( bullet.Src )
				effectdata:SetAngles( self:GetAngles( ) )
				effectdata:SetScale( self.MuzzleFlashType )
				util.Effect( "fx_muzzleflash" , effectdata )

				self.aircraft:FireBullets( bullet )

			end

		end

	end

end

function ENT:fire( )

	if IsValid( self ) then

		if SERVER then

			if not self.shooting then

				self.shooting = true
				self.sounds.stop:Stop( )
				self.sounds.shoot:Play( )

			end

			if self.Sequential then

				self.currentPod = self.currentPod or 1
				self:fireBullet( self.Pods[ self.currentPod ] , self:GetAngles( ) )
				self.currentPod = ( self.currentPod == #self.Pods and 1 or self.currentPod + 1 )

			else

				for key , PodPosition in pairs( self.Pods ) do

					self:fireBullet( PodPosition , self:GetAngles( ) )

				end

			end

			self:SetNextShot( self:GetLastShot( ) + 60 / self.FireRate )

		end

	end

end

function ENT:stop( )

	if IsValid( self ) then

		if SERVER then

			if self.shooting then

				self.sounds.shoot:Stop( )
				self.sounds.stop:Play( )
				self.shooting = false

			end

		end

	end

end
