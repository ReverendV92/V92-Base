
AddCSLuaFile( )

ENT.Base = "base_anim"
ENT.Type = "anim"

ENT.PrintName = "Aimed Gun"
ENT.Author = VNTCB.Info.author
ENT.Category = VNT_CATEGORY_VNT
ENT.Contact = VNTCB.Info.contact
ENT.Purpose = VNTCB.Info.purpose
ENT.Instructions = VNTCB.Info.instructions

ENT.Spawnable = false
ENT.AdminOnly = true

ENT.Name = ENT.PrintName -- Don't touch

ENT.Ammo = 1000 -- Ammo in reserve
ENT.FireRate = 600 -- Fire rate in rounds per minute
ENT.Force = 10 -- Force of bullet on target

ENT.Belt = 1
ENT.IsAimed = false -- Enable/Disable aiming; false=fixed gun, true=aimable gun

ENT.CoolDown = 4 -- Amount of heat to shed every second not firing
ENT.HeatMult = 12 -- Amount of heat to add per bullet fired; max heat is 1000
ENT.HeatTime = 1 -- Time to cool down after overheating the weapon in seconds
ENT.Caliber = 7.62 -- Millimetre size of the weapon
ENT.Spread = 0.6144 -- Cone of spread for shots
ENT.SndPitch = 100 -- Pitch of the firing sound, best left at 100

ENT.AmmoBelt = {

	-- Armour piercing belt
	{

		"armourpiercing_tracer" ,
		"armourpiercing" ,
		"armourpiercing" ,

	} ,

	-- Incendiary belt
	{

		"incendiary_tracer" ,
		"incendiary" ,
		"incendiary" ,

	} ,

	-- Incendiary/AP mixed belt
	{

		"incendiary_tracer" ,
		"armourpiercing" ,
		"incendiary" ,
		"armourpiercing_tracer" ,
		"incendiary" ,
		"armourpiercing" ,

	} ,

}

ENT.AmmoData = {
	["armourpiercing"] = {
		["class"] = "v92_bullet_ap" ,
		["info"] = {
			["Large"] = false , -- Large size bullet
			["SelfDestr"] = false , -- Bullet self-destructs
			["Flak"] = false , -- Explodes like flak
			["Tracer"] = false , -- Enable/Disable tracer
			["Timer"] = 2 , -- Timer after firing to remove or explode
			["col"] = Color( 0 , 0 , 0 , 255 ) , -- Colour of the bullet
			["Speed"] = 850 , -- Speed of the rounds in metres per second
			["Radius"]= 32 , -- Radius of the bullet's affected area
			["Penetrate"] = 15 , -- Hammer Units to penetrate through
			["BallisticDrag"] = 16 , -- Drag on the bullet, used to provide delayed drag
			["Drift"] = 0.42 , -- Amount of ballstics drift
			["Mass"] = 102 , -- Mass of the bullet in grams
			["TissueDamage"] = math.Rand( 34 , 45 ) , -- Damage to fleshy bits
			["EffectSize"] = 10 , -- Size of the effects
			["Size"] = 7.62 , -- Millimetre size of the rounds
		} ,
	},
	["armourpiercing_tracer"] = {
		["class"] = "v92_bullet_ap" ,
		["info"] = {
			["Large"] = false , -- Large size bullet
			["SelfDestr"] = false , -- Bullet self-destructs
			["Flak"] = false , -- Explodes like flak
			["Tracer"] = true , -- Enable/Disable tracer
			["Timer"] = 2 , -- Timer after firing to remove or explode
			["col"] = Color( 0 , 0 , 0 , 255 ) , -- Colour of the bullet
			["Speed"] = 850 , -- Speed of the rounds in metres per second
			["Radius"]= 32 , -- Radius of the bullet's affected area
			["Penetrate"] = 15 , -- Hammer Units to penetrate through
			["BallisticDrag"] = 16 , -- Drag on the bullet, used to provide delayed drag
			["Drift"] = 0.42 , -- Amount of ballstics drift
			["Mass"] = 102 , -- Mass of the bullet in grams
			["TissueDamage"] = math.Rand( 34 , 45 ) , -- Damage to fleshy bits
			["EffectSize"] = 10 , -- Size of the effects
			["Size"] = 7.62 , -- Millimetre size of the rounds
		} ,
	},
	["incendiary"] = {
		["class"] = "v92_bullet_api" ,
		["info"] = {
			["Large"] = false , -- Large size bullet
			["SelfDestr"] = false , -- Bullet self-destructs
			["Flak"] = false , -- Explodes like flak
			["Tracer"] = false , -- Enable/Disable tracer
			["Timer"] = 2 , -- Timer after firing to remove or explode
			["col"] = Color( 0 , 0 , 0 , 255 ) , -- Colour of the bullet
			["Speed"] = 850 , -- Speed of the rounds in metres per second
			["Radius"]= 32 , -- Radius of the bullet's affected area
			["Penetrate"] = 15 , -- Hammer Units to penetrate through
			["BallisticDrag"] = 16 , -- Drag on the bullet, used to provide delayed drag
			["Drift"] = 0.42 , -- Amount of ballstics drift
			["Mass"] = 102 , -- Mass of the bullet in grams
			["TissueDamage"] = math.Rand( 34 , 45 ) , -- Damage to fleshy bits
			["EffectSize"] = 10 , -- Size of the effects
			["Size"] = 7.62 , -- Millimetre size of the rounds
		} ,
	},
	["incendiary_tracer"] = {
		["class"] = "v92_bullet_ap" ,
		["info"] = {
			["Large"] = false , -- Large size bullet
			["SelfDestr"] = false , -- Bullet self-destructs
			["Flak"] = false , -- Explodes like flak
			["Tracer"] = true , -- Enable/Disable tracer
			["Timer"] = 2 , -- Timer after firing to remove or explode
			["col"] = Color( 0 , 0 , 0 , 255 ) , -- Colour of the bullet
			["Speed"] = 850 , -- Speed of the rounds in metres per second
			["Radius"]= 32 , -- Radius of the bullet's affected area
			["Penetrate"] = 15 , -- Hammer Units to penetrate through
			["BallisticDrag"] = 16 , -- Drag on the bullet, used to provide delayed drag
			["Drift"] = 0.42 , -- Amount of ballstics drift
			["Mass"] = 102 , -- Mass of the bullet in grams
			["TissueDamage"] = math.Rand( 34 , 45 ) , -- Damage to fleshy bits
			["EffectSize"] = 10 , -- Size of the effects
			["Size"] = 7.62 , -- Millimetre size of the rounds
		} ,
	},
}

ENT.Sounds = {
	["shoot"] = Sound( "BF2.FiringPort.M16_Loop" ) , -- Firing sound
	["stop"] = Sound( "" ) , -- Stop firing sound
	["blank"] = Sound( "VNT.M16A1.MagSpank" ) , -- Firing while jammed/overheated sound
	["clickshoot"] = Sound( "VNT.M16A1.MagSpank" ) , -- Sound that plays while near overheated/jammed 
	["clickstop"] = Sound( "" ) , -- Sound that plays while near overheated/jammed and fire is released
	["Jam"] = Sound( "VNT.FlareGun.Fire" ) , -- Sound that plays when the gun initially jams
	["GunReady"] = Sound( "VNT.M16A1.MagSpank" ) , -- Sound that plays when the gun is ready to fire again
}